<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DocStatus</label>
    <protected>false</protected>
    <values>
        <field>Language_Specific__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Page Var</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">IF(Ttile.StartsWith(Archieve,&quot;EOL ARCHIEVE&quot;) else non EOL IF(Article.WIP =true, WIp)else,NON WIP) IF(Article.ValdiationStatus = Verifeid, unverifed Public</value>
    </values>
</CustomMetadata>
