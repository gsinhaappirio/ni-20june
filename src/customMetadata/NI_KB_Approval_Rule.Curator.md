<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Curator</label>
    <protected>false</protected>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Submit For Approval</value>
    </values>
    <values>
        <field>Values__c</field>
        <value xsi:type="xsd:string">WIP,Unverified,Verified Internal,Verified Public</value>
    </values>
</CustomMetadata>
