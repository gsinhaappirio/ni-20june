<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Complete</label>
    <protected>false</protected>
    <values>
        <field>MaxVal__c</field>
        <value xsi:type="xsd:double">25.0</value>
    </values>
    <values>
        <field>MinVal__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
</CustomMetadata>
