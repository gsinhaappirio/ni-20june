<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Authenticated</label>
    <protected>false</protected>
    <values>
        <field>Values__c</field>
        <value xsi:type="xsd:string">Flag for Technical Edit;Flag for Style Edit;Flag for Delete;Flag to Merge</value>
    </values>
</CustomMetadata>
