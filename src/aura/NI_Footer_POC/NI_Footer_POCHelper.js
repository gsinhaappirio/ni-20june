({
	helperMethod : function(component) {
		var action = component.get("c.getHiearchySettings");
        action.setCallback(this, function(response){
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                //saving custom setting to attribute
                component.set("v.settings", response.getReturnValue());
                this.doInitScript(component);
            }
        });
        $A.enqueueAction(action);
	},
    doInitScript : function(component) {
        		var apiKey = component.get("v.settings").API_Key__c;
                var cookie = PNX.utils.Cookie; 
                var wrapperName = component.get("v.settings").Application_Name__c;
                var locale = cookie.get("locale");
        
                var myCallback = function(args) {}

                var injectWrapper = function(data) {
                    $visibleFooter = data[locale]['visibleFooter'];
                    //$invisibleFooter = data[locale]['invisibleFooter']; -- removed because it only contains DTM code and does't work as it loads too late

                    //used this method to add DTM code as otherwise it loads too late. Unfortunately Adobe Target isn't functional on SaaS pages because of this.

                    //DTM SCRIPT FOR PRODUCTION ENVIROMENTS//
                    //          $.getScript( "//assets.adobedtm.com/a3a673425858388bc1b1e0ed01eb252ad8ce12e0/satelliteLib-1401ce0fc9dae9e8602fcc34896d0a249c42abb2.js", function(){_satellite.pageBottom();}
                    //);

                    //DTM SCRIPT FOR STAGING ENVIROMENTS - THIS IS VERY IMPORTANT TO SWITCH TO PROD VERSION BEFORE GO-LIVE
                    $.getScript(
                        "//assets.adobedtm.com/a3a673425858388bc1b1e0ed01eb252ad8ce12e0/satelliteLib-1401ce0fc9dae9e8602fcc34896d0a249c42abb2-staging.js",
                        function() {
                            _satellite.pageBottom();
                        }
                    );

                    $.getScript(
                        "//www.ni.com/niassets/wrapper/js/cartCount.js",
                        function() {});
                    $.getScript(
                        "//www.ni.com/niassets/wrapper/js/cookielaw.js",
                        function() {});


                    $('#ni-vis-foot').html($visibleFooter).fadeIn(
                        600).promise().done(function() {
                        //$('#ni-invis-foot').html($invisibleFooter).promise().done(function(){
                        setTimeout(function() {
                            $(document).trigger(
                                "navBar");
                            $(document).trigger(
                                "globalGatewayPanel"
                            );
                            $(document).trigger(
                                "myAccount");
                            $(document).trigger(
                                "globalSearch");
                            $(document).trigger(
                                "upMobileData");
                        }, 800);

                    });
                }


                var fallback = function() {
                    var gg = $(".global-gateway");

                    // Add return path to the end of the GG link
                    gg
                        .attr("href", function(i, val) {
                            return val + "?rtrn=" +
                                encodeURIComponent(window.location
                                    .href);
                        });

                    var year = new Date();
                    $('.copywriteYear').html(year.getFullYear());

                    // Show the fall back wrapper
                    $('#ni-vis-head').find('.global-header').show();
                }

                var getWrapper = function() {
                    jQuery.ajax({
                        url: "//flux.ni.com/wrapper-markup/1/wrapper/" +
                            wrapperName + ".json?locale=" +
                            locale + "&ni-api-key=" +
                            apiKey,
                        cache: true,
                        timeout: 3000,
                        jsonp: 'callback',
                        jsonpCallback: 'injectWrapper',
                        dataType: "jsonp",
                        success: function(data) {
                            injectWrapper(data);
                        },
                        error: function() {
                            fallback();
                        }
                    });
                }


                //FOR GLOBAL GATEWAY ON SAAS
                if (!locale) {

                    locale = 'en-US';
                    getWrapper();

                } else {
                    getWrapper();
                }
	}
})