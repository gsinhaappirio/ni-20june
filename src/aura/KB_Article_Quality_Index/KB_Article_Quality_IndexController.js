/**
*
*Name        : KB_Article_Quality_IndexController 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 06th June, 2017
*Purpose     : Controller js for KB_Article_Quality_Index Component
* 
**/

({
    
    //Initialization function
	doInit : function(component, event, helper) {
        var articleId = component.get("v.recordId");
        helper.getVisibility(component,articleId);
    },
    
    //Function to calculate Score based on several article quality index parameters
    calculateScore : function(component, event, helper){
        if(helper.validateQualityValues(component)){
            var score = 0, uniqueValue=0, completeValue=0, contentClearValue=0, titleReflectsArticleValue=0, linksValidValue=0, metadataValue=0;
            if(component.get("v.newItem").Unique__c != null && component.get("v.newItem").Unique__c != '' && component.get("v.newItem").Unique__c != 'undefined'){
                uniqueValue = parseInt(component.get("v.newItem").Unique__c);
            }
            if(component.get("v.newItem").Complete__c != null && component.get("v.newItem").Complete__c != '' && component.get("v.newItem").Complete__c != 'undefined'){
                completeValue = parseInt(component.get("v.newItem").Complete__c);
            }
            if(component.get("v.newItem").Content_Clear__c != null && component.get("v.newItem").Content_Clear__c != '' && component.get("v.newItem").Content_Clear__c != 'undefined'){
                contentClearValue = parseInt(component.get("v.newItem").Content_Clear__c);
            }
            if(component.get("v.newItem").Title_Reflects_Article__c != null && component.get("v.newItem").Title_Reflects_Article__c != '' && component.get("v.newItem").Title_Reflects_Article__c != 'undefined'){
                titleReflectsArticleValue = parseInt(component.get("v.newItem").Title_Reflects_Article__c);
            }
            if(component.get("v.newItem").Links_Valid__c != null && component.get("v.newItem").Links_Valid__c != '' && component.get("v.newItem").Links_Valid__c != 'undefined'){
                linksValidValue = parseInt(component.get("v.newItem").Links_Valid__c);
            }
            if(component.get("v.newItem").Metadata_Correct__c != null && component.get("v.newItem").Metadata_Correct__c != '' && component.get("v.newItem").Metadata_Correct__c != 'undefined'){
                metadataValue = parseInt(component.get("v.newItem").Metadata_Correct__c);
            }
            
            score = uniqueValue+completeValue+contentClearValue+titleReflectsArticleValue+linksValidValue+metadataValue;
            var a = component.get("v.wrapperObj");
            a.totalScore = score;
            component.set("v.wrapperObj",a);
        }
    },
    
    //Function to freeze Quality Index
    submitRating : function(component, event, helper) { 
        if(helper.validateIndex(component)){
            var a = component.get("v.wrapperObj");
            a.isReadOnly = true;
            if(a.isReviewer){
                // Submit the article quality index
        		var newItem = component.get("v.newItem");
                helper.submitArticleQuality(component,newItem);
            }else{
                a.displayButtons = true;
                component.set("v.wrapperObj",a); 
            }
        }
    },
    
    //Function to approve the article after giving article quality index
    approveForm : function(component, event, helper){
        // Approve the article
        var newItem = component.get("v.newItem");
        helper.approveArticle(component, newItem);
    },
    
    //Function to reject the article after giving article quality index
    rejectForm : function(component, event, helper){
        if(component.get("v.wrapperObj").isReject === false){
            var a = component.get("v.wrapperObj");
            a.isReject = true;
            component.set("v.wrapperObj",a);  
        }
        else if(component.get("v.wrapperObj").isReject && helper.validateItemForm(component)){
            // Reject the article
            var newItem = component.get("v.newItem");
            helper.rejectArticle(component, newItem);
        }
    },
    
    //Function to submit the article for approval and saving the Quality Index
    submitForApproval : function(component, event, helper){
        if(!component.get('v.wrapperObj').isMajorEdit){
            var a = component.get("v.wrapperObj");
            a.isMajorEdit = true;
            component.set("v.wrapperObj",a);
        }else{
            if(component.get("v.majorEdit") != '-'){
                if(component.get('v.wrapperObj').isSubmitter){
                    helper.submitArticle(component);
                }else if(component.get('v.wrapperObj').isPublisher){
                    helper.publishArticles(component);
                }
            }
            else{
                component.set('v.failureMessage',$A.get("$Label.c.NI_Major_Edit_Validation_Msg"));
            }
        }
    }
})