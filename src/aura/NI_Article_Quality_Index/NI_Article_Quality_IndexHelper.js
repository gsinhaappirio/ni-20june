/**
*
*Name        : NI_Article_Quality_IndexHelper   
*Created By  : Meghana Agarwal (Appirio)
*Date        : 06th June, 2017
*Purpose     : Helper js for NI_Article_Quality_Index Component
*
**/

({
    
    //Function to get the initial details of component elements to display
	getVisibility : function(component, recordId) {
        var action = component.get("c.getComponentVisibility");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                var wrapObj = response.getReturnValue();
                component.set('v.wrapperObj',wrapObj);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Function to validate if the Reason of Rejection is blank
    validateItemForm : function(component){
    	// Error checking
        var validItem = true;
        // Reject Reason must not be blank
        var reasonField = component.find("rejectReason");
        var itemname = reasonField.get("v.value");
        if ($A.util.isEmpty(itemname)){
            validItem = false;
            reasonField.set("v.errors", [{message:'{!$Label.c.NI_Reason_Blank_Validation}'}]);
        } else {
            reasonField.set("v.errors", null);
        }
        return (validItem);
	},
    
    //Function to approve the article after giving article quality index
    approveArticle : function(component, newItem){
        var action = component.get("c.approveArticle");
        action.setParams({
            recordId : component.get("v.recordId"),
            unique : newItem.Unique__c,
            complete : newItem.Complete__c,
            contentClear : newItem.Content_Clear__c,
            titleReflectsArticle : newItem.Title_Reflects_Article__c,
            linksValid : newItem.Links_Valid__c,
            metaData : newItem.Metadata_Correct__c,
            comments : newItem.Comments__c,
            isEOL : component.get("v.wrapperObj").isEOLArchiveApprover
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                var a = component.get("v.wrapperObj");
                if(!a.isEOLArchiveApprover){
                    a.isEditable = false;
                    a.isVisible = true;
                    a.isAvailable = true;
                    a.isEOLArchive = true;
                    a.isEOLArchiveApprover = false;
                    a.comments = newItem.Comments__c;
                }else{
                    a.isEditable = false;
                    a.isVisible = false;
                    a.isAvailable = false;
                    a.isEOLArchive = true;
                    a.isEOLArchiveApprover = false;
                    //a.comments = newItem.Comments__c;
                }
                component.set("v.wrapperObj",a); 
                component.set('v.failureMessage','');
                $A.get('e.force:refreshView').fire();
            }else{
               	component.set('v.failureMessage',$A.get("$Label.c.NI_Approve_Error"));
            }
        });
        $A.enqueueAction(action);
    },
    
    //Function to reject the article after giving article quality index
    rejectArticle : function(component, newItem){
        var action = component.get("c.rejectArticle");
        action.setParams({
            recordId : component.get("v.recordId"),
            unique : newItem.Unique__c,
            complete : newItem.Complete__c,
            contentClear : newItem.Content_Clear__c,
            titleReflectsArticle : newItem.Title_Reflects_Article__c,
            linksValid : newItem.Links_Valid__c,
            metaData : newItem.Metadata_Correct__c,
            comments : newItem.Comments__c,
            rejectReason : newItem.Rejection_Reason__c,
            isEOL : component.get("v.wrapperObj").isEOLArchiveApprover
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                var a = component.get("v.wrapperObj");
                if(!a.isEOLArchiveApprover){
                    a.isEditable = false;
                    a.isVisible = true;
                    a.isAvailable = true;
                    a.isEOLArchive = false;
                    a.isEOLArchiveApprover = false;
                    a.comments = newItem.Comments__c;
                    a.rejectReason = newItem.Rejection_Reason__c;
                    a.isRejectedArticle = true;
                }
                else{
                    a.isEditable = false;
                    a.isVisible = false;
                    a.isAvailable = false;
                    a.isEOLArchive = false;
                    a.isEOLArchiveApprover = false;
                    //a.comments = newItem.Comments__c;
                    //a.rejectReason = newItem.Rejection_Reason__c;
                    a.isRejectedArticle = true;
                }
                component.set("v.wrapperObj",a); 
                component.set('v.failureMessage','');
                $A.get('e.force:refreshView').fire();
            }else{
                component.set('v.failureMessage',$A.get("$Label.c.NI_Reject_Error"));
            }
        });//
        $A.enqueueAction(action); 
    },
    
    //Function to validate the Quality Index field values for blank and not allowed values
    validateIndex : function(component){
    	// Error checking
        var validItem = true;
        var uniqueField = component.find("unique");
        var uniqueValue = uniqueField.get("v.value");
        var completeField = component.find("complete");
        var completeValue = completeField.get("v.value");
        var contentClearField = component.find("contentClear");
        var contentClearValue = contentClearField.get("v.value");
        var titleReflectsArticleField = component.find("titleReflectsArticle");
        var titleReflectsArticleValue = titleReflectsArticleField.get("v.value");
        var linksValidField = component.find("linksValid");
        var linksValidValue = linksValidField.get("v.value");
        var metaField = component.find("MetadataCorrect");
        var metaValue = metaField.get("v.value");
        if ($A.util.isEmpty(uniqueValue)){
            validItem = false;
            uniqueField.set("v.errors", [{message:'{!$Label.c.NI_Unique_Blank_Msg}'}]);
            completeField.set("v.errors", null);
           	contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", null);
        }
        else if ($A.util.isEmpty(completeValue)){
            validItem = false;
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", [{message:'{!$Label.c.NI_Complete_Blank_Msg}'}]);
            contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", null);
        }
        else if ($A.util.isEmpty(contentClearValue)){
            validItem = false;
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", null);
            contentClearField.set("v.errors", [{message:'{!$Label.c.NI_ContentClear_Blank_Msg}'}]);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", null);
        }
        else if ($A.util.isEmpty(titleReflectsArticleValue)){
            validItem = false;
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", null);
            contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", [{message:'{!$Label.c.NI_TitleReflectsArticle_Blank_Msg}'}]);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", null);
        }
        else if ($A.util.isEmpty(linksValidValue)){
            validItem = false;
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", null);
            contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", [{message:'{!$Label.c.NI_LinksValid_Blank_Msg}'}]);
            metaField.set("v.errors", null);
        }
        else if ($A.util.isEmpty(metaValue)){
            validItem = false;
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", null);
            contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", [{message:'{!$Label.c.NI_MetaCorrect_Blank_Msg}'}]);
        }
        else if(!this.validateQualityValues(component)){
             validItem = false;   
        }
        else {
            uniqueField.set("v.errors", null);
            completeField.set("v.errors", null);
            contentClearField.set("v.errors", null);
            titleReflectsArticleField.set("v.errors", null);
            linksValidField.set("v.errors", null);
            metaField.set("v.errors", null);
        }
        return (validItem);
	},
    
    //Function to validate if the Quality Index field values for not allowed values
    validateQualityValues : function(component){
    	// Error checking
        var validItem = true;
        var objRange = component.get("v.fieldRange");
        var uniqueField = component.find("unique");
        var uniqueValue = uniqueField.get("v.value");
        var completeField = component.find("complete");
        var completeValue = completeField.get("v.value");
        var contentClearField = component.find("contentClear");
        var contentClearValue = contentClearField.get("v.value");
        var titleReflectsArticleField = component.find("titleReflectsArticle");
        var titleReflectsArticleValue = titleReflectsArticleField.get("v.value");
        var linksValidField = component.find("linksValid");
        var linksValidValue = linksValidField.get("v.value");
        var metaField = component.find("MetadataCorrect");
        var metaValue = metaField.get("v.value");
        for(var i=0; i<objRange.length; i++){
            if(objRange[i].name==="Unique" && uniqueValue != null && uniqueValue != 'undefined' && uniqueValue.trim() != '' && (uniqueValue.match(/^\d+$/) === null || parseInt(uniqueValue)>parseInt(objRange[i].maxValue) || parseInt(uniqueValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", [{message:'{!$Label.c.NI_Unique_validation_Msg}'}]);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", null);
                break;
            }else if(objRange[i].name==="Complete" && completeValue != null && completeValue != 'undefined' && completeValue.trim() != '' && (completeValue.match(/^\d+$/) === null || parseInt(completeValue)>parseInt(objRange[i].maxValue) || parseInt(completeValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", [{message:'{!$Label.c.NI_Complete_Validation_Msg}'}]);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", null);
                break;
            }
            else if(objRange[i].name==="Content Clear" && contentClearValue != null && contentClearValue != 'undefined' && contentClearValue.trim() != '' && (contentClearValue.match(/^\d+$/) === null || parseInt(contentClearValue)>parseInt(objRange[i].maxValue) || parseInt(contentClearValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", [{message:'{!$Label.c.NI_ContentClear_Validation_Msg}'}]);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", null);
                break;
            }
            else if(objRange[i].name==="Title Reflects Article" && titleReflectsArticleValue != null && titleReflectsArticleValue != 'undefined' && titleReflectsArticleValue.trim() != '' && (titleReflectsArticleValue.match(/^\d+$/) === null || parseInt(titleReflectsArticleValue)>parseInt(objRange[i].maxValue) || parseInt(titleReflectsArticleValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", [{message:'{!$Label.c.NI_TitleReflectsArticle_Validation_Msg}'}]);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", null);
                break;
            }
            else if(objRange[i].name==="Links Valid" && linksValidValue != null && linksValidValue != 'undefined' && linksValidValue.trim() != '' && (linksValidValue.match(/^\d+$/) === null || parseInt(linksValidValue)>parseInt(objRange[i].maxValue) || parseInt(linksValidValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", [{message:'{!$Label.c.NI_LinksValid_Validation_Msg}'}]);
                metaField.set("v.errors", null);
                break;
            }
            else if(objRange[i].name==="Metadata Correct" && metaValue != null && metaValue != 'undefined' && metaValue.trim() != '' && (metaValue.match(/^\d+$/) === null || parseInt(metaValue)>parseInt(objRange[i].maxValue) || parseInt(metaValue)<parseInt(objRange[i].minValue))){
                validItem = false;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", [{message:'{!$Label.c.NI_MetaCorrect_Validation_Msg}'}]);
                break;
            }
            else{
                validItem = true;
                uniqueField.set("v.errors", null);
                completeField.set("v.errors", null);
                contentClearField.set("v.errors", null);
                titleReflectsArticleField.set("v.errors", null);
                linksValidField.set("v.errors", null);
                metaField.set("v.errors", null);
            }
        }
        return validItem;
	},
    
    //Function to submit the article for approval and saving the Quality Index
    submitArticle : function(component){
        var action = component.get("c.submit");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                if("SUCCESS" === response.getReturnValue()){
                    component.set("v.submitFailureMessage",'');
                    var a = component.get("v.wrapperObj");
                    a.isSubmitter = false;
                    a.isAvailable = false;
                    component.set('v.wrapperObj',a);
                    $A.get('e.force:refreshView').fire();
                }
                else if("FAILURE" === response.getReturnValue()){
                    component.set('v.submitFailureMessage',$A.get("$Label.c.NI_Article_Approval_Error_Msg")); 
                }
            }
            else{
                component.set('v.submitFailureMessage',$A.get("$Label.c.NI_Submit_Approval_Error_Msg"));   
            }
        });
        $A.enqueueAction(action); 
    },
    
    //Function to publish the article
    publishArticles : function(component){
        var action = component.get("c.publish");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                component.set("v.submitFailureMessage",'');
                var a = component.get("v.wrapperObj");
                a.isSubmitter = false;
                a.isPublisher = false;
               	a.isAvailable = false;
                if(a.isPublishPermission === true){
                   a.isEOLArchive = true; 
                }
                component.set('v.wrapperObj',a);
                $A.get('e.force:refreshView').fire();
            }
            else{
                component.set('v.submitFailureMessage',$A.get("$Label.c.NI_Publish_Error_Msg"));   
            }
        });
        $A.enqueueAction(action); 
    },
    
    //Function to Submit the article for EOL Archive
    EOLArchiveArticle : function(component){
        var action = component.get("c.submitForArchivalApproval");
        action.setParams({
            recordId : component.get("v.recordId"),
            approverId : component.get("v.wrapperObj").EOLApprover
        });
        action.setCallback(this, function(response){
            if("SUCCESS" === response.getState()){
                component.set('v.EOLSubmitFailureMessage','');
                var a = component.get("v.wrapperObj");
                a.isEOLArchive = false; 
                a.isSetEOLApprover = false;
                component.set('v.wrapperObj',a);
                $A.get('e.force:refreshView').fire();
            }else{
               	component.set('v.EOLSubmitFailureMessage',$A.get("$Label.c.NI_EOL_Submission_Error_Msg"));
            }
        });
        $A.enqueueAction(action);
    }
})