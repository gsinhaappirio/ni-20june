/**
*
*Name        : NI_Article_Quality_IndexController 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 06th June, 2017
*Purpose     : Controller js for NI_Article_Quality_Index Component
* 
**/

({
    
    //Initialization function
	doInit : function(component, event, helper) {
        var articleId = component.get("v.recordId");
        helper.getVisibility(component,articleId);
    },
    
    //Function to calculate Score based on several article quality index parameters
    calculateScore : function(component, event, helper){
        if(helper.validateQualityValues(component)){
            var score = 0, uniqueValue=0, completeValue=0, contentClearValue=0, titleReflectsArticleValue=0, linksValidValue=0, metadataValue=0;
            if(component.get("v.newItem").Unique__c != null && component.get("v.newItem").Unique__c != '' && component.get("v.newItem").Unique__c != 'undefined'){
                uniqueValue = parseInt(component.get("v.newItem").Unique__c);
            }
            if(component.get("v.newItem").Complete__c != null && component.get("v.newItem").Complete__c != '' && component.get("v.newItem").Complete__c != 'undefined'){
                completeValue = parseInt(component.get("v.newItem").Complete__c);
            }
            if(component.get("v.newItem").Content_Clear__c != null && component.get("v.newItem").Content_Clear__c != '' && component.get("v.newItem").Content_Clear__c != 'undefined'){
                contentClearValue = parseInt(component.get("v.newItem").Content_Clear__c);
            }
            if(component.get("v.newItem").Title_Reflects_Article__c != null && component.get("v.newItem").Title_Reflects_Article__c != '' && component.get("v.newItem").Title_Reflects_Article__c != 'undefined'){
                titleReflectsArticleValue = parseInt(component.get("v.newItem").Title_Reflects_Article__c);
            }
            if(component.get("v.newItem").Links_Valid__c != null && component.get("v.newItem").Links_Valid__c != '' && component.get("v.newItem").Links_Valid__c != 'undefined'){
                linksValidValue = parseInt(component.get("v.newItem").Links_Valid__c);
            }
            if(component.get("v.newItem").Metadata_Correct__c != null && component.get("v.newItem").Metadata_Correct__c != '' && component.get("v.newItem").Metadata_Correct__c != 'undefined'){
                metadataValue = parseInt(component.get("v.newItem").Metadata_Correct__c);
            }
            
            score = uniqueValue+completeValue+contentClearValue+titleReflectsArticleValue+linksValidValue+metadataValue;
            var a = component.get("v.wrapperObj");
            a.totalScore = score;
            component.set("v.wrapperObj",a);
        }
    },
    
    //Function to freeze Quality Index
    submitRating : function(component, event, helper) { 
        if(helper.validateIndex(component)){
            var a = component.get("v.wrapperObj");
            a.isReadOnly = true;
            a.displayButtons = true;
            component.set("v.wrapperObj",a);  
        }
    },
    
    //Function to approve the article after giving article quality index
    approveForm : function(component, event, helper){
        // Approve the article
        var newItem = component.get("v.newItem");
        helper.approveArticle(component, newItem);
    },
    
    //Function to reject the article after giving article quality index
    rejectForm : function(component, event, helper){
        if(component.get("v.wrapperObj").isReject == false){
            var a = component.get("v.wrapperObj");
            a.isReject = true;
            component.set("v.wrapperObj",a);  
        }
        else if(component.get("v.wrapperObj").isReject && helper.validateItemForm(component)){
            // Reject the article
            var newItem = component.get("v.newItem");
            helper.rejectArticle(component, newItem);
        }
    },
    
    //Function to submit the article for approval and saving the Quality Index
    submitForApproval : function(component, event, helper){
            helper.submitArticle(component);
    },
    
    //Function to publish the article
    publishArticle : function(component, event, helper){
        helper.publishArticles(component);
    },
    
    //Function to Submit the article for EOL Archive
    archiveArticle : function(component, event, helper){
        if(component.get("v.wrapperObj").isPublishPermission === true){
            helper.EOLArchiveArticle(component);
        }
        else if(component.get("v.wrapperObj").isPublishPermission === false && component.get("v.wrapperObj").isSetEOLApprover === false){
            var a = component.get("v.wrapperObj");
             a.isSetEOLApprover = true;
             component.set("v.wrapperObj",a);
        }
        else if(component.get("v.wrapperObj").isPublishPermission === false && component.get("v.wrapperObj").isSetEOLApprover === true){
            if(component.find('search_approver') != null && component.find('search_approver').get('v.selectedRecord') != null && component.find('search_approver').get('v.selectedRecord').value != ''){
                var a = component.get("v.wrapperObj");
                a.EOLApprover = component.find('search_approver').get('v.selectedRecord').value;
                component.set("v.wrapperObj",a); 
                helper.EOLArchiveArticle(component);
            }else{
                component.set('v.EOLSubmitFailureMessage',$A.get("$Label.c.NI_Select_EOL_Approver_Msg"));
            }
        }
    }
})