({
	doInit : function(component, event, helper) {
		var initRecord = component.get('v.initRecord');
        if(initRecord !=null ){
            var temp = {};
            temp.label=initRecord.Name;
            temp.value=initRecord.Id;
            component.set('v.selectedRecord',temp);
        }
        var selectedRecord = component.get('v.selectedRecord');
		if(selectedRecord != null){
            
			var selectedOption = component.find('SelectedOption');
	        $A.util.removeClass(selectedOption, 'slds-hide');
	        
	        var allOptions = component.find('availableOptions');
	        $A.util.addClass(allOptions, 'slds-hide');
	        
	        var search = component.find('InputSearch');
	        $A.util.addClass(search, 'slds-hide');
        }
	},
	
    onChangeSearchText : function(component, event, helper){
        var a = component.find("InputSearch");
        var searchInput = a.getElement().lastChild.value;
        //var searchInput = document.getElementById("search-input").value;
        if(searchInput.length == 0){
            component.set('v.searchResult',[]);
            var allOptions = component.find('availableOptions');
            $A.util.addClass(allOptions, 'slds-hide');
        }
        else if(searchInput.length >= 3){
	        var searchResult = [];
	        var retrieveOptions = component.get("c.getOptionsData");
	        retrieveOptions.setParams({searchText : searchInput,
                                       searchObject : component.get("v.searchObject"),
                                       searchField : component.get("v.searchField"),
                                       returnFields : component.get("v.returnFields"),
                                       orderBy :component.get("v.orderBy"),
                                       limitRec: component.get("v.limitRec"),
                                       condition :component.get("v.condition")
                                      });
	        retrieveOptions.setCallback(this,function(response){                          
	            var state = response.getState();
	            if (component.isValid() && state === "SUCCESS") {
	            	searchResult = response.getReturnValue();
	            	component.set('v.searchResult',searchResult);
	            	var allOptions = component.find('availableOptions');
            		$A.util.removeClass(allOptions, 'slds-hide');
	            }
	        });
	        $A.enqueueAction(retrieveOptions);
        }
	},
	
	onCloseSelectedRecord : function(component, event, helper){
		document.getElementById("search-input").value = '';
		
		var selectedOption = component.find('SelectedOption');
        $A.util.addClass(selectedOption, 'slds-hide');
        
        var search = component.find('InputSearch');
        $A.util.removeClass(search, 'slds-hide');
        
        var allOptions = component.find('availableOptions');
        $A.util.addClass(allOptions, 'slds-hide');
        
        component.set('v.selectedRecord', null);
	},
	
	onSelectRecord : function(component, event, helper){
		var selectedOption = component.find('SelectedOption');
        $A.util.removeClass(selectedOption, 'slds-hide');
        
        var allOptions = component.find('availableOptions');
        $A.util.addClass(allOptions, 'slds-hide');
        
        var search = component.find('InputSearch');
        $A.util.addClass(search, 'slds-hide');
        
        var selectedIndx = event.currentTarget.dataset.index;
        var listAll = component.get('v.searchResult');
        var selectedRecord = listAll[selectedIndx];
        component.set('v.selectedRecord', selectedRecord);
        //alert('Selected Record: ' + 'Id - ' + selectedRecord.value + '; Name - ' + selectedRecord.label);
    }
})