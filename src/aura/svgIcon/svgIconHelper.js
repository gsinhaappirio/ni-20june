({
    renderIcon: function(component) {
        var svgns = "http://www.w3.org/2000/svg";
        var xlinkns = "http://www.w3.org/1999/xlink";
        var svgroot = document.createElementNS(svgns, "svg");
        // Add an "href" attribute (using the "xlink" namespace)
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "href", component.get("v.url"));
        svgroot.setAttribute("class", component.get("v.class"));
        svgroot.setAttribute("aria-hidden", "true");      
        svgroot.appendChild(shape);
        var container = component.find("container").getElement();
        container.insertBefore(svgroot, container.firstChild);
    }
})