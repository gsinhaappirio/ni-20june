({
	doInit : function(component, event, helper) {
        var cookie = PNX.utils.Cookie,
            wrapperName = "saas-test",
            apiKey = 0,
            locale = cookie.get("locale"); 
        
        //FOR GLOBAL GATEWAY ON SAAS
        if (!locale) {
            locale = 'en-US';
            getWrapper(wrapperName,apiKey,locale);
        } else {
            getWrapper(wrapperName,apiKey,locale);
        }
    },
    	
    myCallback : function(args){
        
    },
     
    injectWrapper : function(data){
        $invisibleHeader = data[locale][
            'invisibleHeader'
        ],
            $visibleHeader = data[locale][
            'visibleHeader'
        ],
            $visibleFooter = data[locale][
            'visibleFooter'
        ];
        //$invisibleFooter = data[locale]['invisibleFooter']; -- removed because it only contains DTM code and does't work as it loads too late
        
        $('head').prepend($invisibleHeader);
        //used this method to add DTM code as otherwise it loads too late. Unfortunately Adobe Target isn't functional on SaaS pages because of this.
        
        //DTM SCRIPT FOR PRODUCTION ENVIROMENTS//
        
        $.getScript( "//assets.adobedtm.com/a3a673425858388bc1b1e0ed01eb252ad8ce12e0/satelliteLib-1401ce0fc9dae9e8602fcc34896d0a249c42abb2.js", function(){_satellite.pageBottom();}
                   );
        
        //DTM SCRIPT FOR STAGING ENVIROMENTS - THIS IS VERY IMPORTANT TO SWITCH TO PROD VERSION BEFORE GO-LIVE
        $.getScript(
            "//assets.adobedtm.com/a3a673425858388bc1b1e0ed01eb252ad8ce12e0/satelliteLib-1401ce0fc9dae9e8602fcc34896d0a249c42abb2-staging.js",
            function() {
                _satellite.pageBottom();
            }
        );
        
        $.getScript(
            "//www.ni.com/niassets/wrapper/js/cartCount.js",
            function() {});
        $.getScript(
            "//www.ni.com/niassets/wrapper/js/cookielaw.js",
            function() {});
        
        $('#ni-vis-head').html($visibleHeader).fadeIn(
            600);
        $('#ni-vis-foot').html($visibleFooter).fadeIn(
            600).promise().done(function() {
            //$('#ni-invis-foot').html($invisibleFooter).promise().done(function(){
            setTimeout(function() {
                $(document).trigger(
                    "navBar");
                $(document).trigger(
                    "globalGatewayPanel"
                );
                $(document).trigger(
                    "myAccount");
                $(document).trigger(
                    "globalSearch");
                $(document).trigger(
                    "upMobileData");
            }, 800);
            
        });
    },
    
    fallback : function(){
        var gg = $(".global-gateway");
        
        // Add return path to the end of the GG link
        gg
        .attr("href", function(i, val) {
            return val + "?rtrn=" +
                encodeURIComponent(window.location
                                   .href);
        });
        
        var year = new Date();
        $('.copywriteYear').html(year.getFullYear());
        
        // Show the fall back wrapper
        $('#ni-vis-head').find('.global-header').show();
    },

    getWrapper : function(wrapperName,apiKey,locale) {
        jQuery.ajax({
            url: "//flux.ni.com/wrapper-markup/1/wrapper/" +
            wrapperName + ".json?locale=" +
            locale + "&ni-api-key=" +
            apiKey,
            cache: true,
            timeout: 3000,
            jsonp: 'callback',
            jsonpCallback: 'injectWrapper',
            dataType: "jsonp",
            success: function(data) {
                debugger;
                injectWrapper(data);
            },
            error: function() {
                debugger;
                fallback();
            }
        });
    }
 
})