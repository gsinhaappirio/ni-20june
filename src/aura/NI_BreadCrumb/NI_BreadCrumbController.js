({
	doInit : function(component, event, helper) {
        var alpha = window.location.href.split("?")[0].split("/");

        var wrappers=new Array({'name' : $A.get("$Label.c.NI_Home"),'link' : $A.get("$Label.c.NI_Home_Link")},
                               {'name' : $A.get("$Label.c.NI_Support"),'link' : $A.get("$Label.c.NI_Support_Link")},
                               {'name' :alpha[alpha.length-1].split("-").join(" "),'link' : ''});
        
        component.set('v.wrappers', wrappers);
    }
})