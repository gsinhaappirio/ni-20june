/**
*
*Name        : KB_Authenticated_FeedbackHelper   
*Created By  : Meghana Agarwal (Appirio)
*Date        : 16th June, 2017
*Purpose     : Helper js for KB_Authenticated_Feedback Component
*
**/

({
    //Function to submit the article feedback
    createItem: function(component, newItem) {
        var articleLinks = null;
        if(component.find("links") != undefined){
			articleLinks = component.find("links").get("v.value");
        }
        var action = component.get("c.insertArticleFeedback");
        action.setParams({
            recordId : component.get("v.recordId"),
            summary : component.find("summary").get("v.value"),
            areaOfExpertise : component.find("areaOfExpertise").get("v.value"),
            comments : component.find("comments").get("v.value"),
            links: articleLinks
        });
        action.setCallback(this, function(response){
            if('SUCCESS' == response.getState()){
                var wrapObj = response.getReturnValue();
                if(!wrapObj.isError){
                    component.set('v.sucessMessage',wrapObj.message);
                    component.set('v.failureMessage','');
                    component.find("comments").set('v.value','');
                    component.set('v.newItem.Summary__c','v.summaryPickListValues[0]');
                    component.set('v.newItem.Article_Links__c','');
                    component.set('v.isMerge',false);
                    component.set('v.newItem.Area_of_Expertise__c','v.areaPickListValues[0]');
                }else{
                    component.set('v.sucessMessage','');
                    component.set('v.failureMessage',wrapObj.message);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Function to validate the values of article feedback
    validateItemForm: function(component) {
        // Error checking
        var validItem = true;
        // Name must not be blank
        var summaryField = component.find("summary");
        var summaryValue = summaryField.get("v.value");
        var areaField = component.find("areaOfExpertise");
        var areaValue = areaField.get("v.value");
        var commentsField = component.find("comments");
        var comments = commentsField.get("v.value");
        var linksField = component.find("links");
        var links = null;
        if(linksField != undefined){
        	links = linksField.get("v.value");
        }
        if ('' === summaryValue){
            validItem = false;
            summaryField.set("v.errors", [{message:'{!$Label.c.NI_Summary_Blank_Msg}'}]);
            areaField.set("v.errors",null);
            commentsField.set("v.errors",null);
            if(linksField != undefined){
				linksField.set("v.errors",null);
            }
        } 
        else if (linksField != undefined && (links === undefined || links.trim() === '')){
            validItem = false;
            summaryField.set("v.errors",null);
            linksField.set("v.errors", [{message:'{!$Label.c.NI_Links_Blank_Msg}'}]);
            areaField.set("v.errors",null);
            commentsField.set("v.errors",null);
        } 
        else if ('Flag for Technical Edit' === summaryValue && '' === areaValue){
            validItem = false;
            summaryField.set("v.errors",null);
            areaField.set("v.errors", [{message:'{!$Label.c.NI_Area_Of_Expertise_Validation_Msg}'}]);
        	commentsField.set("v.errors",null);
            if(linksField != undefined){
				linksField.set("v.errors",null);
            }
        } 
        else if (comments == null || comments === undefined || comments.trim() === ''){
            validItem = false;
            summaryField.set("v.errors",null);
            areaField.set("v.errors", null);
        	commentsField.set("v.errors",[{message:'{!$Label.c.NI_Feedback_Comments_Blank_Msg}'}]);
            if(linksField != undefined){
				linksField.set("v.errors",null);
            }
        }
        else {
            summaryField.set("v.errors", null);
            areaField.set("v.errors", null);
            commentsField.set("v.errors",null);
            if(linksField != undefined){
				linksField.set("v.errors",null);
            }
        }
        return (validItem);
    },
    
    //Function to get the picklist values of the article feedback drop down fields
    getPicklist : function(component){
		var action = component.get("c.getPicklist");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            if('SUCCESS' === response.getState()){
                var wrapObj = response.getReturnValue();
                if(!wrapObj.isError){
                    var map = wrapObj.pickListMap;
                    var feedback = wrapObj.feedbackList;
                    if(feedback != undefined && feedback.length > 0){
                        component.set('v.isFeedbackPresent', true);
                    }else{
                        component.set('v.isFeedbackPresent', false);
                    }
                    component.set('v.feedbackValues', feedback);
                    component.set('v.summaryPickListValues', map.summary);
                    component.set('v.areaPickListValues', map.areaOfExpertise);
                    component.set('v.successMessage','');
                    component.set('v.failureMessage','');
                }
                else{
                    component.set('v.sucessMessage','');
                    component.set('v.failureMessage',wrapObj.message);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    //Function to get the article feedbacks
    getFeedbacks : function(component){
        component.set('v.sucessMessage','');
        component.set('v.failureMessage','');
		var action = component.get("c.getFeedback");
        action.setParams({
            recordId : component.get("v.recordId"),
            flag : component.get("v.feedbackFlag")
        });
        action.setCallback(this, function(response){
            if('SUCCESS' === response.getState()){
                var wrapObj = response.getReturnValue();
                if(!wrapObj.isError){
                    var feedback = wrapObj.feedbackList;
                    if(feedback != undefined && feedback.length > 0){
                        component.set('v.isFeedbackPresent', true);
                    }else{
                        component.set('v.isFeedbackPresent', false);
                    }
                    component.set('v.feedbackValues', feedback);
                    component.set('v.successMessage','');
                    component.set('v.failureMessage','');
                }
                else{
                    component.set('v.sucessMessage','');
                    component.set('v.failureMessage',wrapObj.message);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    //Function to save the article feedbacks as complete
    save : function(component){
        var checkedFeedback = [];
        var checkObj = component.find("checkbox"); 
        var y = 0;
        if(checkObj != null && checkObj != undefined && checkObj.length === undefined){
            checkedFeedback[y] = checkObj.get("v.text");
        }
        else if(checkObj != null && checkObj != undefined && checkObj.length != undefined){
            for(var i=0; i<checkObj.length; i++){
                if(checkObj[i].isRendered() && checkObj[i].get("v.value") === true && checkObj[i].get("v.text") != null && checkObj[i].get("v.text") != ''){
                    checkedFeedback[y] = checkObj[i].get("v.text");
                    y++; 
                }
            }
        }
        if(checkedFeedback.length>0){
            var action = component.get("c.saveCompleteFeedback");
            action.setParams({
                recordId : component.get("v.recordId"),
                feedbackIds : checkedFeedback
            });
            action.setCallback(this, function(response){
                if('SUCCESS' === response.getState()){
                    var wrapObj = response.getReturnValue();
                    if(!wrapObj.isError){
                        var feedback = wrapObj.feedbackList;
                        component.set('v.sucessMessage',wrapObj.message);
                        component.set('v.failureMessage','');
                        component.set('v.feedbackFlag','v.summaryPickListValues[0]');
                        component.set("v.feedbackValues",feedback);
                        if(feedback != null && feedback != undefined && feedback.length > 0){
                            component.set('v.isFeedbackPresent',true);
                        }else{
                            component.set('v.isFeedbackPresent',false);
                        }
                    }
                    else{
                        component.set('v.sucessMessage','');
                        component.set('v.failureMessage',wrapObj.message);
                    }
                }
            });
            $A.enqueueAction(action);
        }
	}
})