/**
*
*Name        : KB_Authenticated_FeedbackController 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 16th June, 2017
*Purpose     : Controller js for KB_Authenticated_Feedback Component
* 
**/

({
    //Initialization function
    doInit : function(component, event, helper) {
        helper.getPicklist(component);
    },
    
    //Function to clear out the success and failure messages
    clearResult : function(component, event, helper) {
        component.set("v.sucessMessage","");
        component.set("v.failureMessage","");
        if(component.get("v.newItem.Summary__c") === $A.get("$Label.c.NI_Flag_To_Merge")){
            component.set("v.isMerge",true);
        }else{
            component.set("v.isMerge",false);
        }
    },
    
    //Function to submit the article feedback
    submitForm: function(component, event, helper) { 
        if(helper.validateItemForm(component)){
            // Create the new item
            var newItem = component.get("v.newItem");
            helper.createItem(component, newItem);
        }
    },
    
    //Function to fetch the article feedbacks
    fetchFeedbacks : function(component, event, helper){
        helper.getFeedbacks(component);
    },
    
    //Function to save the article feedback as complete
    saveCompletedFeedbacks : function(component, event, helper){
		helper.save(component);
    },
    
    //Function to toggle the visibility of Complete Feedback section
    toggleSection : function(component, event, helper) {  
        var cmpTarget = component.find('toggleSec');
        if($A.util.hasClass(cmpTarget,'slds-is-open')){ 
            $A.util.removeClass(cmpTarget, 'slds-is-open'); 
            component.set("v.expandLink","[+]"); 
            component.set("v.openComplete",false);
        } else{ 
            $A.util.addClass(cmpTarget, 'slds-is-open'); 
            component.set("v.expandLink","[-]");
            component.set("v.openComplete",true);
        } 
    },
    
    //Function to toggle the visibility of Submit Feedback section
    toggleSection1 : function(component, event, helper) {  
        var cmpTarget = component.find('toggleSec1');
        if($A.util.hasClass(cmpTarget,'slds-is-open')){ 
            $A.util.removeClass(cmpTarget, 'slds-is-open'); 
            component.set("v.expandLink1","[+]"); 
            component.set("v.openSubmit",false);
        } else{ 
            $A.util.addClass(cmpTarget, 'slds-is-open'); 
            component.set("v.expandLink1","[-]");
            component.set("v.openSubmit",true);
        } 
    }
})