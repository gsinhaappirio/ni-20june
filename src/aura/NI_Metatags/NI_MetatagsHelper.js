({
    getMetaData: function(component) {
        debugger;
        var pageUrl = window.location.href;
        var lang = 'en_US'
        if(pageUrl.indexOf("?")>0){
            var langParameter = pageUrl.split("?")[1];
            if(langParameter != null && langParameter.indexOf("=")>0){
            	lang = langParameter.split("=")[1];
            }
        }
	    var action = component.get("c.getData");
        action.setParams({
            recordId : component.get("v.recordId"),
            language : lang
        });
        action.setCallback(this, function(response){
            if('SUCCESS' == response.getState()){
                component.set('v.failureMessage','');
                component.set('v.metaWrappers',response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})