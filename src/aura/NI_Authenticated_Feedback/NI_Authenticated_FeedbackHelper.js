({
    createItem: function(component, newItem) {
        var action = component.get("c.insertArticleFeedback");
        action.setParams({
            recordId : component.get("v.recordId"),
            summary : component.find("summary").get("v.value"),
            areaOfExpertise : component.find("areaOfExpertise").get("v.value"),
            comments : component.find("comments").get("v.value")
        });
        action.setCallback(this, function(response){
            if('SUCCESS' == response.getState()){
                component.set('v.sucessMessage',$A.get("$Label.c.NI_Feedback_Success_Msg"));
                component.set('v.failureMessage','');
                component.find("comments").set('v.value','');
                component.set('v.newItem.Summary__c','v.summaryPickListValues[0]');
                component.set('v.newItem.Area_of_Expertise__c','v.areaPickListValues[0]');
            }else{
                component.set('v.sucessMessage','');
                component.set('v.failureMessage',$A.get("$Label.c.NI_Feedback_Failure_Msg"));
            }
        });
        $A.enqueueAction(action);
    },
    validateItemForm: function(component) {
        // Error checking
        var validItem = true;
        // Name must not be blank
        var summaryField = component.find("summary");
        var summaryValue = summaryField.get("v.value");
        var areaField = component.find("areaOfExpertise");
        var areaValue = areaField.get("v.value");
        var commentsField = component.find("comments");
        var comments = commentsField.get("v.value");
        if ('' === summaryValue){
            validItem = false;
            summaryField.set("v.errors", [{message:'{!$Label.c.NI_Summary_Blank_Msg}'}]);
            areaField.set("v.errors",null);
            commentsField.set("v.errors",null);
        } 
        else if ('Flag for Technical Edit' === summaryValue && '' === areaValue){
            validItem = false;
            summaryField.set("v.errors",null);
            areaField.set("v.errors", [{message:'{!$Label.c.NI_Area_Of_Expertise_Validation_Msg}'}]);
        	commentsField.set("v.errors",null);
        } 
        else if (comments === undefined || comments === '' || comments.trim() === ''){
            validItem = false;
            summaryField.set("v.errors",null);
            areaField.set("v.errors", null);
        	commentsField.set("v.errors",[{message:'{!$Label.c.NI_Feedback_Comments_Blank_Msg}'}]);
        }
        else {
            summaryField.set("v.errors", null);
            areaField.set("v.errors", null);
            commentsField.set("v.errors",null);
        }
        return (validItem);
    },
    getPicklist : function(component){
		var action = component.get("c.getPicklist");
        action.setCallback(this, function(response){
            if('SUCCESS' === response.getState()){
                var map = response.getReturnValue();
        		component.set('v.summaryPickListValues', map.summary);
                component.set('v.areaPickListValues', map.areaOfExpertise);
                component.set('v.successMessage','');
                component.set('v.failureMessage','');
            }
        });
        $A.enqueueAction(action);
	}
})