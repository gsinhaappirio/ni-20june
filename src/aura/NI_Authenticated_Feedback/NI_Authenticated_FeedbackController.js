({
    doInit : function(component, event, helper) {
        helper.getPicklist(component);
    },
    clearResult : function(component, event, helper) {
        component.set("v.sucessMessage","");
        component.set("v.failureMessage","");
    },
    submitForm: function(component, event, helper) { 
        if(helper.validateItemForm(component)){
            // Create the new item
            var newItem = component.get("v.newItem");
            helper.createItem(component, newItem);
        }
    }
})