({
    doInit : function(component, event, helper) {
        component.set("v.currentRating", 0);
        helper.getPicklist(component);
    },
    changeRating : function(component, event, helper) {
       	var ratingsRadio =  component.find("rdi");
        var rating = 0;                                               
        for(var i=0; i<ratingsRadio.length; i++){
            if(ratingsRadio[i].get("v.value")==true){
				rating = i+1;
            }                                            
        }                                               
        component.set("v.currentRating", rating);
    },
    submitForm: function(component, event, helper) { 
        if(helper.validateItemForm(component)){
            // Create the new item
            var newItem = component.get("v.newItem");
            helper.createItem(component, newItem);
        }
    }
})