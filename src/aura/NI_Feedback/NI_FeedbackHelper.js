({
    createItem: function(component, newItem) {
        var action = component.get("c.insertArticleFeedback");
        action.setParams({
            recordId : component.get("v.recordId"),
            rating : component.get("v.currentRating"),
            summary : component.find("summary").get("v.value"),
            details : component.find("detail").get("v.value")
        });
        action.setCallback(this, function(response){
            if('SUCCESS' == response.getState()){
                component.set('v.sucessMessage','Great! Your feedback has been recorded.');
                component.set('v.failureMessage','');
                component.set('v.currentRating',0);
                component.find("detail").set('v.value','');
                var ratingsRadio =  component.find("rdi");                                              
                for(var i=0; i<ratingsRadio.length; i++){
                    ratingsRadio[i].set('v.value',false);        
                } 
                component.set('v.newItem.Summary__c','v.pickListValues[0]');
            }else{
                component.set('v.sucessMessage','');
                component.set('v.failureMessage','Sorry! Some problem occured while recording your feedback.');
            }
        });
        $A.enqueueAction(action);
    },
    validateItemForm: function(component) {
        // Error checking
        var validItem = true;
        // Name must not be blank
        var nameField = component.find("summary");
        var itemname = nameField.get("v.value");
        if ($A.util.isEmpty(itemname)){
            validItem = false;
            nameField.set("v.errors", [{message:"Summary field can't be blank."}]);
        } else {
            nameField.set("v.errors", null);
        }
        // Comments must not be blank
        var commentsField = component.find("detail");
        var comments = commentsField.get("v.value");
        if ($A.util.isEmpty(comments)){
            validItem = false;
            commentsField.set("v.errors", [{message:"Details can't be blank."}]);
        } else {
            commentsField.set("v.errors", null);
        }
        return (validItem);
    },
    getPicklist : function(component) {
		var action = component.get("c.getPicklist");
        action.setCallback(this, function(response){
            if('SUCCESS' == response.getState()){
        		component.set('v.pickListValues', response.getReturnValue());
                component.set('v.successMessage','');
                component.set('v.failureMessage','');
            }
        });
        $A.enqueueAction(action);
	}
})