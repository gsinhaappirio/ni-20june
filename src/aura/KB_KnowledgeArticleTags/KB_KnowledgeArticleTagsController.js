({
    createNew : function(component, event, helper) {
        component.set("v.Edit",true);
        component.set("v.tagListforSelection",null);
    },
    save : function(component, event, helper) {
        helper.saveTagsInArticle(component);
    },
    cancel : function(component, event, helper) {
        component.set("v.Edit",false);
        component.set("v.tagListforSelection",component.get("v.tagListSelected"));
    },
    doInit: function(component, event, helper) {
       helper.retrieveTags(component);
    },
    onChangeSearchText: function(component, event, helper) {
        var searchInput = component.get("v.searchtext");
        if(searchInput.length >= 3){
       helper.retrieveTagsSearch(component);
        }
    },
    del : function(component, event, helper) {
        var deleteid = event.getSource().get("v.tabindex")
       helper.delTags(component,deleteid);
    },
    
})