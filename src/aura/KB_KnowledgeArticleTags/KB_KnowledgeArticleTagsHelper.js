({
    retrieveTags : function(cmp) {
        var action = cmp.get("c.getProductTagsRecords");
        console.log(action);
        action.setParams({
            recordId : cmp.get("v.recordId")
        });
        
        action.setCallback(this,function(response){
            debugger;
            var toastEvent = $A.get("e.force:showToast");
            console.log(toastEvent);
            
            var state = response.getState();
            if(state === "SUCCESS"){
                var value = response.getReturnValue();
                if(value.isSuccess){
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"success",
                        "message": value.message
                    });
                    cmp.set("v.tagListSelected",value.productTags);
                    cmp.set("v.tagListforSelection",value.productTags);
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": value.message
                    });
                }
            }else{
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": response.getError()
                });
            }
            // commented  to uncomment toastEvent.fire();
            
        });
         $A.enqueueAction(action);
    },
    
    retrieveTagsSearch : function(cmp) {
        debugger;
        var action = cmp.get("c.getProductTags");
        action.setParams({
            searchText : cmp.get("v.searchtext"),
            recordSelected :  cmp.get("v.tagListSelected")
        });
        
        action.setCallback(this,function(response){
            var toastEvent = $A.get("e.force:showToast");
            
            
            var state = response.getState();
            if(state === "SUCCESS"){
                var value = response.getReturnValue();
                if(value.isSuccess){
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"success",
                        "message": value.message
                    });
                    cmp.set("v.tagListforSelection",value.productTags);
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": value.message
                    });
                }
            }else{
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": response.getError()
                });
            }
            toastEvent.fire();
            
        });
         $A.enqueueAction(action);
    },
    saveTagsInArticle: function(cmp) {

        // create var for store record id's for selected checkboxes  
        var selId = [];
        // get all checkboxes 
        var getAllId = cmp.find("Inputcheck");
        var selctedCount = 0;
        // play a for loop and check every checkbox values 
        // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
        for (var i = 0; i < getAllId.length; i++) {
            if (getAllId[i].get("v.value") == true) {
                selId.push(getAllId[i].get("v.text"));
                selctedCount++;
            }
        }
        //get the count of selected records and diplay alert
        
        if(selctedCount == 0){
            alert('You should select one tags');
        return false;
        }
        
        var action = cmp.get("c.saveProductTags");
        action.setParams({
            recordId : cmp.get("v.recordId"),
            recordSelected :  selId
        });
        
        action.setCallback(this,function(response){
            debugger;
            var toastEvent = $A.get("e.force:showToast");
            
            
            var state = response.getState();
            if(state === "SUCCESS"){
                var value = response.getReturnValue();
                if(value.isSuccess){
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"success",
                        "message": value.message
                    });
                    cmp.set("v.tagListSelected",value.productTags);
                    cmp.set("v.tagListforSelection",value.productTags);
                    cmp.set("v.Edit",false);
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": value.message
                    });
                }
            }else{
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": response.getError()
                });
            }
            toastEvent.fire();
            
        });
         $A.enqueueAction(action);
    },
    delTags : function(cmp,deleteid) {


        var action = cmp.get("c.deleteProductTags");
        action.setParams({
            recordId : deleteid,
            articleId : cmp.get("v.recordId")

        });
        
        action.setCallback(this,function(response){
            debugger;
            var toastEvent = $A.get("e.force:showToast");
            
            
            var state = response.getState();
            if(state === "SUCCESS"){
                var value = response.getReturnValue();
                if(value.isSuccess){
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"success",
                        "message": value.message
                    });
                    cmp.set("v.tagListSelected",value.productTags);
                    cmp.set("v.tagListforSelection",value.productTags);
                    cmp.set("v.Edit",false);
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": value.message
                    });
                }
            }else{
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": response.getError()
                });
            }
            toastEvent.fire();
            
        });
         $A.enqueueAction(action);
    }
})