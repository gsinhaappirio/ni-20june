<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contact_Quote_Unique_ID</fullName>
        <description>Contact Quote Unique ID</description>
        <field>unique_key__c</field>
        <formula>Contact__r.Id  +  Quote__r.Id</formula>
        <name>Contact Quote Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
