<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_User_Welcome_Email_Alert</fullName>
        <ccEmails>sfdc.admins@ni.com</ccEmails>
        <description>New User Welcome Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>User_Templates/New_User_Welcome</template>
    </alerts>
    <alerts>
        <fullName>Notify_HR_about_user_activation</fullName>
        <ccEmails>hr.shared.service.requests@ni.com</ccEmails>
        <description>Notify HR about user activation</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Admin_Email_Templates/SFDC_User_Activated</template>
    </alerts>
    <alerts>
        <fullName>Notify_HR_about_user_deactivation</fullName>
        <ccEmails>hr.shared.service.requests@ni.com</ccEmails>
        <description>Notify HR about user deactivation</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Admin_Email_Templates/SFDC_User_Deactivated</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_User_source</fullName>
        <description>Populate Source field based on Location.</description>
        <field>Source__c</field>
        <formula>IF( 
	OR(
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;BRAZIL&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;CANADA&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;CHILE&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;COLOMBIA&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;COSTA RICA&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;MEXICO&quot;),
		BEGINS( UPPER(TRIM(Location_Name__c)), &quot;US&quot;)
	),
	&quot;Americas&quot;,
	IF(
		OR(
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;AUSTRIA&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;BELGIUM&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;CZECH REPUBLIC&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;DENMARK&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;FINLAND&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;FRANCE&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;GERMANY&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;HUNGARY&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;IRELAND&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;ITALY&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;NETHERLANDS&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;NORWAY&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;POLAND&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;PORTUGAL&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;ROMANIA&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;SLOVENIA&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;SPAIN&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;SWEDEN&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;SWITZERLAND&quot;),
			BEGINS( UPPER(TRIM(Location_Name__c)), &quot;UK&quot;)
		),
		&quot;Europe&quot;,
		IF(
			OR(
				BEGINS( UPPER(TRIM(Location_Name__c)), &quot;HONG KONG&quot;),
				BEGINS( UPPER(TRIM(Location_Name__c)), &quot;JAPAN&quot;),
				BEGINS( UPPER(TRIM(Location_Name__c)), &quot;KOREA&quot;),
				BEGINS( UPPER(TRIM(Location_Name__c)), &quot;CHINA&quot;),
				BEGINS( UPPER(TRIM(Location_Name__c)), &quot;TAIWAN&quot;)
			),
			&quot;APAC&quot;,
			&quot;&quot;
		)
	)
)</formula>
        <name>Update User source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New User Welcome Email</fullName>
        <actions>
            <name>New_User_Welcome_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notContain</operation>
            <value>user</value>
        </criteriaItems>
        <description>Work flow to send welcome email to new users.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>User source populate</fullName>
        <actions>
            <name>Update_User_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>WF Rule to update the User source field on new records or whenever Location Name is changed.</description>
        <formula>ISNEW(  )  ||  ISCHANGED( Location_Name__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
