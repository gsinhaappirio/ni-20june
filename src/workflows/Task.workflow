<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Task_is_completed</fullName>
        <description>Task is completed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Task_is_completed</template>
    </alerts>
    <rules>
        <fullName>Task is closed</fullName>
        <actions>
            <name>Task_is_completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( CreatedBy.Profile.Name &lt;&gt; &apos;NI Developer User&apos;, ISPICKVAL( Type__c ,&apos;Task&apos;), ISPICKVAL( Status ,&apos;Completed&apos;), LastModifiedById &lt;&gt; CreatedById )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
