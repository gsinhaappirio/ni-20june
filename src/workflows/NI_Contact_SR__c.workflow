<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_SR_Unique_ID</fullName>
        <description>Contact SR Unique ID</description>
        <field>Unique_Key__c</field>
        <formula>Contact__r.Id + Service_Request__r.Id</formula>
        <name>Contact SR Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
