<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NI_KB_Email_to_the_submitter_of_article_with_the_score_on_the_AQI_creation</fullName>
        <description>NI KB Email to the submitter of article with the score on the AQI creation.</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Submitted_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NI_Knowledge_Templates/NI_KB_Article_Creation_Email</template>
    </alerts>
    <rules>
        <fullName>NI KB Email notification and AQI creation</fullName>
        <actions>
            <name>NI_KB_Email_to_the_submitter_of_article_with_the_score_on_the_AQI_creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send and email to the submitter of article with the score on the AQI creation.</description>
        <formula>AND((Total_score_out_of_100__c &lt; 80),(ISBLANK(TEXT(Status__c))))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
