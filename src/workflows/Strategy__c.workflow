<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved_Status_Field_Update</fullName>
        <description>PlanForce</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved Status Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Status_Field_Update</fullName>
        <description>PlanForce</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected Status Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
