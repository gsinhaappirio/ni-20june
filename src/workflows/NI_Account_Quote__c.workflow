<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>account_quote_unique_id</fullName>
        <description>Account Quote Unique ID</description>
        <field>unique_key__c</field>
        <formula>account__r.Id +  quote__r.Id</formula>
        <name>Account Quote Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
