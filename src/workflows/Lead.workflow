<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Date_Accepted_Rejected</fullName>
        <field>Date_Accepted_Rejected__c</field>
        <formula>Today()</formula>
        <name>Date Accepted/Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Disqualified_Converted</fullName>
        <field>Date_Disqualified_Converted__c</field>
        <formula>Today()</formula>
        <name>Date Disqualified/Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Created_Time</fullName>
        <description>Updates the lead created time with now()</description>
        <field>Created_Time__c</field>
        <formula>NOW()</formula>
        <name>Lead Created Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Description_mapping</fullName>
        <description>Copies Description to Description mapping to use at lead conversion</description>
        <field>Description_mapping__c</field>
        <formula>Description</formula>
        <name>Populate Description mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Lead_External_Number</fullName>
        <field>Lead_External_Number__c</field>
        <formula>Lead_Number__c</formula>
        <name>Populate Lead External Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ready_To_Assign_Field_Update</fullName>
        <field>Ready_to_be_Assigned__c</field>
        <literalValue>1</literalValue>
        <name>Ready To Assign Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Resend_CMDM_Resolution_field</fullName>
        <field>Resend_CMDM_Resolution__c</field>
        <literalValue>0</literalValue>
        <name>Reset &quot;Resend CMDM Resolution&quot; field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Restricted_checkboxupdate</fullName>
        <description>Restricted Account Formula on Lead object checked then it&apos;ll check the Restricted Accountcheck field on Lead</description>
        <field>Restricted_Account__c</field>
        <literalValue>1</literalValue>
        <name>Restricted checkboxupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Qualified</fullName>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Status to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Truncate_Extension_Delimeter</fullName>
        <field>Phone</field>
        <formula>MID(Phone, 0, LEN(Phone)-2)</formula>
        <name>Truncate Extension Delimeter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDateAccepted</fullName>
        <description>When Lead Status=Accepted, a WF Field Update executes to capture current DATE for Date Accepted.</description>
        <field>Date_Accepted__c</field>
        <formula>today()</formula>
        <name>UpdateDateAccepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDateQualified</fullName>
        <description>When Lead Status=Qualified - a WF Field Update executes to capture system date in Date Qualified.  (for reporting - not on page layout)</description>
        <field>Date_Qualified__c</field>
        <formula>today()</formula>
        <name>UpdateDateQualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Time</fullName>
        <field>Closed_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Closed Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Counter_Closed_Converted_to_Quote</fullName>
        <description>Updates a number field on leads that is used for mathematical reporting. If status = &quot;Closed-Converted to Quote&quot;, 1. Otherwise, 0</description>
        <field>Converted_to_Quote_Counter__c</field>
        <formula>IF(ISPICKVAL(Status, &quot;Closed - Converted to Quote&quot;), 1, 0)</formula>
        <name>Update Counter-Closed-Converted to Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Lead_CMDM</fullName>
        <description>it&apos;ll update the Cmdm_Resolution_Complete__c</description>
        <field>Cmdm_Resolution_Complete__c</field>
        <literalValue>1</literalValue>
        <name>update Lead CMDM check box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>LeadOutboundMsg</fullName>
        <apiVersion>31.0</apiVersion>
        <endpointUrl>https://parsec-broken.ni.com/sfdc-integration/outbound/lead-resolution/1/outbound?WSDL</endpointUrl>
        <fields>City</fields>
        <fields>Company</fields>
        <fields>Country</fields>
        <fields>Email</fields>
        <fields>Fax</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MobilePhone</fields>
        <fields>Phone</fields>
        <fields>PostalCode</fields>
        <fields>State</fields>
        <fields>Street</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>deploy.user@ni.com</integrationUser>
        <name>LeadOutboundMsg</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CREQ Task</fullName>
        <actions>
            <name>CReq_Task</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Creates a new task when a cReq lead is created from marketing</description>
        <formula>AND ((!ISNULL(cREQ_Date__c)) ,  OR(ISNEW()  ,(ISCHANGED(cREQ_Date__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Format Phone and Extension</fullName>
        <actions>
            <name>Truncate_Extension_Delimeter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Strips off unnecessary commas from the end of the phone number on marketing leads</description>
        <formula>AND(   CONTAINS(Phone, &quot;,,&quot;),   FIND(&quot;,,&quot;, Phone) = LEN(Phone)-1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Created Time</fullName>
        <actions>
            <name>Lead_Created_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the lead created time since the standard createdDate field doesn&apos;t show time in reports.</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Status Accepted</fullName>
        <actions>
            <name>UpdateDateAccepted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notContain</operation>
            <value>queue</value>
        </criteriaItems>
        <description>WF to capture date when a Lead Status is changed to &quot;Accepted&quot; value updated &quot;Date Accepted&quot; field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Status Qualified</fullName>
        <actions>
            <name>UpdateDateQualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notContain</operation>
            <value>queue</value>
        </criteriaItems>
        <description>WF to capture date when a Lead Status is changed to &quot;Qualified&quot; value updated &quot;Date Qualified&quot; field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead WR%232  Converted</fullName>
        <actions>
            <name>Date_Disqualified_Converted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed - Disqualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>contains</operation>
            <value>Closed - Converted to Opportunity,Closed - Converted to Quote</value>
        </criteriaItems>
        <description>Stamps the date that the lead was either converted to opportunity, converted to quote, or disqualified on the lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead WR%233 Status Qualified</fullName>
        <actions>
            <name>UpdateDateQualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notContain</operation>
            <value>queue</value>
        </criteriaItems>
        <description>WF to capture date when a Lead Status is changed to &quot;Qualified&quot; value updated &quot;Date Qualified&quot; field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead WR%234 AccountContactResolution</fullName>
        <actions>
            <name>Reset_Resend_CMDM_Resolution_field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>LeadOutboundMsg</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send the outbound message to the CMDM resolution service</description>
        <formula>OR(   AND(     ISCHANGED( Resend_CMDM_Resolution__c ),     Resend_CMDM_Resolution__c = TRUE   ),   AND(     ISNEW(),     RecordTypeId = &apos;012i0000000xsPy&apos; /* Marketing Generated Lead */   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead WR%235 increment Counter When Converted to Quote</fullName>
        <actions>
            <name>Update_Counter_Closed_Converted_to_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates number field to allow for mathematical reporting. (Cannot use formula field in mathematical reporting)</description>
        <formula>OR(   AND(      NOT(ISPICKVAL(PRIORVALUE(Status), &quot;Closed - Converted to Quote&quot;)),      ISPICKVAL(Status, &quot;Closed - Converted to Quote&quot;)   ),   AND(      ISPICKVAL(PRIORVALUE(Status), &quot;Closed - Converted to Quote&quot;),      NOT(ISPICKVAL(Status, &quot;Closed - Converted to Quote&quot;))   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead WR%237 CMDM Resolution</fullName>
        <active>true</active>
        <description>This WF will fire when profile name &quot;NI Data Integrity User&quot; and lead record type &quot;Marketing Generated Lead&quot;.</description>
        <formula>OR(   Resend_CMDM_Resolution__c = TRUE,   AND(CreatedBy.Profile.Name  = &quot;NI Data Integrity User&quot;,  RecordType.Name = &quot;Marketing Generated Lead&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>update_Lead_CMDM</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate Description mapping</fullName>
        <actions>
            <name>Populate_Description_mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies the value of Description field to Description mapping to use at lead conversion</description>
        <formula>OR( 	AND( 		ISNEW(), 		NOT(ISNULL(Description)) 	), 	ISCHANGED(Description) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Lead External Number</fullName>
        <actions>
            <name>Populate_Lead_External_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates Lead External Number to access oracle data</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Restricted Account Check</fullName>
        <actions>
            <name>Restricted_checkboxupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It is used to check the Restricted Check Box is checked.</description>
        <formula>Account__r.Restricted_Account__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Ready To Assign Field</fullName>
        <actions>
            <name>Ready_To_Assign_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Generated Lead</value>
        </criteriaItems>
        <description>Set Ready_To_Assign__c field to true every time a SGL is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Lead Acceptance%2FRejection</fullName>
        <actions>
            <name>Date_Accepted_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow stamps lead acceptance or rejection.</description>
        <formula>( ISPICKVAL(Status, &apos;Accepted&apos;) || ISPICKVAL(Status, &apos;Closed - Rejected&apos;) ) &amp;&amp; NOT( BEGINS( OwnerId ,&apos;00G&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Closed Time</fullName>
        <actions>
            <name>Update_Closed_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the closed time on the lead so that we know when it was closed. Last modified date could be updated after a lead is closed.</description>
        <formula>AND( ISCHANGED(Status), NOT(CONTAINS(UPPER(TEXT(PRIORVALUE(Status))),&quot;CLOSED&quot;)), CONTAINS(UPPER(TEXT(Status)),&quot;CLOSED&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>CReq_Task</fullName>
        <assignedToType>owner</assignedToType>
        <description>Hi 
A CReq task has been created.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.cREQ_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>cREQ Task</subject>
    </tasks>
</Workflow>
