<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Description_Summary</fullName>
        <field>Description_Summary__c</field>
        <formula>IF(LEN( Description__c ) &gt; 252, LEFT( Description__c , 252) &amp; &apos;...&apos;, Description__c)</formula>
        <name>Description Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Name</fullName>
        <description>Update Account Text Name on MP Oppty for Sharing Rule</description>
        <field>MP_Account_Name_Text__c</field>
        <formula>MP_Account_NameFormula__c</formula>
        <name>Update Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Description Summary</fullName>
        <actions>
            <name>Description_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISNEW(),   ISCHANGED( Description__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Name Text</fullName>
        <actions>
            <name>Update_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>for Sharing Rules on MP Oppty</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
