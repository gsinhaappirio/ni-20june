<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contact_Fax</fullName>
        <field>Fax</field>
        <formula>IF(NOT(ISBLANK(Fax_Number__c)), 
IF(NOT(ISBLANK(Fax_Country_Code__c)),&quot;+&quot; &amp; Fax_Country_Code__c &amp; &quot; &quot;,&quot;&quot;) 
&amp; 
IF(NOT(ISBLANK(Fax_Area_Code__c)), Fax_Area_Code__c &amp; &quot; &quot;, &quot;&quot;) 
&amp; 
Fax_Number__c 
, 
&quot;&quot; 
)</formula>
        <name>Contact Fax</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Home_Phone</fullName>
        <field>HomePhone</field>
        <formula>IF(NOT(ISBLANK(Home_Phone_Number__c)), 
IF(NOT(ISBLANK(Home_Phone_Country_Code__c)),&quot;+&quot; &amp; Home_Phone_Country_Code__c &amp; &quot; &quot;,&quot;&quot;) 
&amp; 
IF(NOT(ISBLANK(Home_Phone_Area_Code__c)), Home_Phone_Area_Code__c &amp; &quot; &quot;, &quot;&quot;)
&amp; 
Home_Phone_Number__c 
, 
&quot;&quot; 
)</formula>
        <name>Contact Home Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Mailing_Street</fullName>
        <field>MailingStreet</field>
        <formula>IF(NOT(ISBLANK(Mailing_Street_1__c)),Mailing_Street_1__c,&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Mailing_Street_2__c)),IF(NOT(ISBLANK(Mailing_Street_1__c)),&quot;, &quot; &amp; Mailing_Street_2__c,Mailing_Street_2__c),&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Mailing_Street_3__c)),IF(OR(NOT(ISBLANK(Mailing_Street_1__c)),NOT(ISBLANK(Mailing_Street_2__c))),&quot;, &quot; &amp; Mailing_Street_3__c,Mailing_Street_3__c),&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Mailing_Street_4__c)),IF(OR(NOT(ISBLANK(Mailing_Street_1__c)),NOT(ISBLANK(Mailing_Street_2__c)),NOT(ISBLANK(Mailing_Street_3__c))),&quot;, &quot; &amp; Mailing_Street_4__c,Mailing_Street_4__c),&quot;&quot;)</formula>
        <name>Contact Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Mobile_Phone</fullName>
        <field>MobilePhone</field>
        <formula>IF(NOT(ISBLANK(Mobile_Phone_Number__c)), 
    IF(NOT(ISBLANK(Mobile_Phone_Country_Code__c)),&quot;+&quot; &amp; Mobile_Phone_Country_Code__c &amp; &quot; &quot;,&quot;&quot;)
    &amp;
    IF(NOT(ISBLANK(Mobile_Phone_Area_Code__c)), Mobile_Phone_Area_Code__c &amp; &quot; &quot;, &quot;&quot;)
    &amp;
    Mobile_Phone_Number__c
    ,
    &quot;&quot;
)</formula>
        <name>Contact Mobile Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Other_Phone</fullName>
        <field>OtherPhone</field>
        <formula>IF(NOT(ISBLANK(Other_Phone_Number__c)), 
    IF(NOT(ISBLANK(Other_Phone_Country_Code__c)),&quot;+&quot; &amp; Other_Phone_Country_Code__c &amp; &quot; &quot;,&quot;&quot;)
    &amp;
    IF(NOT(ISBLANK(Other_Phone_Area_Code__c)), Other_Phone_Area_Code__c &amp; &quot; &quot;, &quot;&quot;)
    &amp;
    Other_Phone_Number__c
    &amp;
    IF(NOT(ISBLANK(Other_Phone_Extension__c)), &quot;,,&quot; &amp; Other_Phone_Extension__c, &quot;&quot;)
    ,
    &quot;&quot;
)</formula>
        <name>Contact Other Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Other_Street</fullName>
        <field>OtherStreet</field>
        <formula>IF(NOT(ISBLANK(Other_Street_1__c)),Other_Street_1__c,&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Other_Street_2__c)),IF(NOT(ISBLANK(Other_Street_1__c)),&quot;, &quot; &amp; Other_Street_2__c,Other_Street_2__c),&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Other_Street_3__c)),IF(OR(NOT(ISBLANK(Other_Street_1__c)),NOT(ISBLANK(Other_Street_2__c))),&quot;, &quot; &amp; Other_Street_3__c,Other_Street_3__c),&quot;&quot;)
&amp;
IF(NOT(ISBLANK(Other_Street_4__c)),IF(OR(NOT(ISBLANK(Other_Street_1__c)),NOT(ISBLANK(Other_Street_2__c)),NOT(ISBLANK(Other_Street_3__c))),&quot;, &quot; &amp; Other_Street_4__c,Other_Street_4__c),&quot;&quot;)</formula>
        <name>Contact Other Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Phone</fullName>
        <field>Phone</field>
        <formula>IF(NOT(ISBLANK(Phone_Number__c)), 
    IF(NOT(ISBLANK(Phone_Country_Code__c)),&quot;+&quot; &amp; Phone_Country_Code__c &amp; &quot; &quot;,&quot;&quot;)
    &amp;
    IF(NOT(ISBLANK(Phone_Area_Code__c)), Phone_Area_Code__c &amp; &quot; &quot;, &quot;&quot;)
    &amp;
    Phone_Number__c
    &amp;
    IF(NOT(ISBLANK(Phone_Extension__c)), &quot;,,&quot; &amp; Phone_Extension__c, &quot;&quot;)
    ,
    &quot;&quot;
)</formula>
        <name>Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Privacy_Policy</fullName>
        <description>Always update the contact&apos;s privacy policy to be true upon creation</description>
        <field>Accepted_Privacy_Policy__c</field>
        <literalValue>1</literalValue>
        <name>Contact Privacy Policy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description_Summary</fullName>
        <field>Description_Summary__c</field>
        <formula>IF(LEN(Description) &gt; 252, LEFT(Description, 252) &amp; &apos;...&apos;, Description)</formula>
        <name>Description Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Department</fullName>
        <description>Copy the value of Department mapping to Department at lead conversion</description>
        <field>Department</field>
        <formula>Department_mapping__c</formula>
        <name>Update Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Department_Mapping</fullName>
        <description>Copy the value of Department to Department_Mapping__C field</description>
        <field>Department_mapping__c</field>
        <formula>Department</formula>
        <name>Update_Department_Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OtherPhone_Mapping</fullName>
        <description>Update Other_Phone_Mapping__c field with the value of OtherPhone standard field value</description>
        <field>Other_Phone_mapping__c</field>
        <formula>OtherPhone</formula>
        <name>Update_OtherPhone_Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Other_Phone</fullName>
        <description>Copy the value of Other Phone mapping to Other Phone at lead conversion</description>
        <field>OtherPhone</field>
        <formula>Other_Phone_mapping__c</formula>
        <name>Update Other Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>ContactOutboundMessage</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>https://parsec-broken.ni.com/sfdc-integration/outbound/cmdm/cdi-sync-inbox/1/SfdcContact</endpointUrl>
        <fields>AccountAlias1__c</fields>
        <fields>AccountAlias2__c</fields>
        <fields>AccountAlias3__c</fields>
        <fields>AccountId</fields>
        <fields>AccountNamePhonetic__c</fields>
        <fields>AccountNameRoman__c</fields>
        <fields>AccountName__c</fields>
        <fields>AccountShippingCity__c</fields>
        <fields>AccountShippingCountry__c</fields>
        <fields>AccountShippingPostalCode__c</fields>
        <fields>AccountShippingState__c</fields>
        <fields>AccountShippingStreet1__c</fields>
        <fields>AccountShippingStreet2__c</fields>
        <fields>AccountShippingStreet3__c</fields>
        <fields>AccountShippingStreet4__c</fields>
        <fields>AccountShippingStreet__c</fields>
        <fields>Alias__c</fields>
        <fields>Department</fields>
        <fields>Email</fields>
        <fields>Fax</fields>
        <fields>Fax_Area_Code__c</fields>
        <fields>Fax_Country_Code__c</fields>
        <fields>Fax_Number__c</fields>
        <fields>FirstName</fields>
        <fields>HomePhone</fields>
        <fields>Home_Phone_Area_Code__c</fields>
        <fields>Home_Phone_Country_Code__c</fields>
        <fields>Home_Phone_Number__c</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MailingCity</fields>
        <fields>MailingCountry</fields>
        <fields>MailingPostalCode</fields>
        <fields>MailingState</fields>
        <fields>MailingStreet</fields>
        <fields>Mailing_Street_1__c</fields>
        <fields>Mailing_Street_2__c</fields>
        <fields>Mailing_Street_3__c</fields>
        <fields>Mailing_Street_4__c</fields>
        <fields>MobilePhone</fields>
        <fields>Mobile_Phone_Area_Code__c</fields>
        <fields>Mobile_Phone_Country_Code__c</fields>
        <fields>Mobile_Phone_Number__c</fields>
        <fields>OtherCity</fields>
        <fields>OtherCountry</fields>
        <fields>OtherPhone</fields>
        <fields>OtherPostalCode</fields>
        <fields>OtherState</fields>
        <fields>OtherStreet</fields>
        <fields>Other_Phone_Area_Code__c</fields>
        <fields>Other_Phone_Country_Code__c</fields>
        <fields>Other_Phone_Number__c</fields>
        <fields>Other_Street_1__c</fields>
        <fields>Other_Street_2__c</fields>
        <fields>Other_Street_3__c</fields>
        <fields>Other_Street_4__c</fields>
        <fields>Phone</fields>
        <fields>Phone_Area_Code__c</fields>
        <fields>Phone_Country_Code__c</fields>
        <fields>Phone_Extension__c</fields>
        <fields>Phone_Number__c</fields>
        <fields>Phonetic_First_Name__c</fields>
        <fields>Phonetic_Last_Name__c</fields>
        <fields>Roman_First_Name__c</fields>
        <fields>Roman_Last_Name__c</fields>
        <fields>Salutation</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>deploy.user@ni.com</integrationUser>
        <name>ContactOutboundMessage</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Contact Privacy Policy</fullName>
        <actions>
            <name>Contact_Privacy_Policy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Touchpoint</fullName>
        <actions>
            <name>ContactOutboundMessage</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Contact updates on specific fields to send to CDI. Only for CDI</description>
        <formula>AND(           OR( ISNEW(),              ISCHANGED( AccountId ),              ISCHANGED( Alias__c ),              ISCHANGED( Department ),              ISCHANGED( Email ),              ISCHANGED( Fax ),              ISCHANGED( Fax_Area_Code__c ),              ISCHANGED( Fax_Country_Code__c ),              ISCHANGED( Fax_Number__c ),              ISCHANGED( FirstName ),              ISCHANGED( HomePhone ),              ISCHANGED( Home_Phone_Area_Code__c ),              ISCHANGED( Home_Phone_Country_Code__c ),              ISCHANGED( Home_Phone_Number__c ),              ISCHANGED( LastName ),              ISCHANGED( MailingCity ),              ISCHANGED( MailingCountry ),              ISCHANGED( MailingPostalCode ),              ISCHANGED( MailingState ),              ISCHANGED( MailingStreet ),              ISCHANGED( Mailing_Street_1__c ),              ISCHANGED( Mailing_Street_2__c ),              ISCHANGED( Mailing_Street_3__c ),              ISCHANGED( Mailing_Street_4__c ),              ISCHANGED( MobilePhone ),              ISCHANGED( Mobile_Phone_Area_Code__c ),              ISCHANGED( Mobile_Phone_Country_Code__c ),              ISCHANGED( Mobile_Phone_Number__c ),              ISCHANGED( OtherCity ),              ISCHANGED( OtherCountry ),              ISCHANGED( OtherPhone ),              ISCHANGED( OtherPostalCode ),              ISCHANGED( OtherState ),              ISCHANGED( OtherStreet ),              ISCHANGED( Other_Phone_Area_Code__c ),              ISCHANGED( Other_Phone_Country_Code__c ),              ISCHANGED( Other_Phone_Number__c ),              ISCHANGED( Other_Street_1__c ),              ISCHANGED( Other_Street_2__c ),              ISCHANGED( Other_Street_3__c ),              ISCHANGED( Other_Street_4__c ),              ISCHANGED( Phone ),              ISCHANGED( Phone_Area_Code__c ),              ISCHANGED( Phone_Country_Code__c ),              ISCHANGED( Phone_Extension__c ),              ISCHANGED( Phone_Number__c ),              ISCHANGED( Phonetic_First_Name__c ),              ISCHANGED( Phonetic_Last_Name__c ),              ISCHANGED( Roman_First_Name__c ),              ISCHANGED( Roman_Last_Name__c ),              ISCHANGED( Salutation ),              ISCHANGED( LastUndeletedDate__c )           )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Description Summary</fullName>
        <actions>
            <name>Description_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISNEW(),   ISCHANGED(Description) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Department field after Lead conversion</fullName>
        <actions>
            <name>Update_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to populate the Department standard field from its custom counterpart after Lead conversion</description>
        <formula>OR( 	AND( 		ISNEW(), 		ISBLANK(Department), 		NOT(ISBLANK(Department_mapping__c)) 	), 	AND( 		ISBLANK(Department), 		ISCHANGED(Department_mapping__c), 		NOT(ISBLANK(Department_mapping__c)) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Other Phone field after Lead conversion</fullName>
        <actions>
            <name>Update_Other_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to populate the Other Phone standard field from its custom counterpart after lead conversion</description>
        <formula>OR( 	AND( 		ISNEW(), 		ISBLANK(OtherPhone), 		NOT(ISBLANK(Other_Phone_mapping__c)) 	), 	AND( 		ISBLANK(OtherPhone), 		ISCHANGED(Other_Phone_mapping__c), 		NOT(ISBLANK(Other_Phone_mapping__c)) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update_Department_Mapping</fullName>
        <actions>
            <name>Update_Department_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy value from master field when it changes</description>
        <formula>ISCHANGED( Department )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update_OtherPhone_Mapping</fullName>
        <actions>
            <name>Update_OtherPhone_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy the value of OtherPhone in case of value change into Other_Phone_mapping__c</description>
        <formula>ISCHANGED( OtherPhone )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
