<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Owner_Rejection_Notification</fullName>
        <description>Send Owner Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Email_Templates/User_Reject_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>UPdate_Status_to_Rejected</fullName>
        <description>Update status to Rejected</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <description>Approval has been obtained</description>
        <field>Status__c</field>
        <literalValue>Processing</literalValue>
        <name>Update Status to Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_OPEN</fullName>
        <description>Sending Record back to OPEN after a Recall</description>
        <field>Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Status to OPEN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Pending</fullName>
        <description>changes status from OPEN to PENDING</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Update Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
