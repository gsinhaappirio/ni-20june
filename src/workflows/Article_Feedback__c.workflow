<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NI_Flag_For_Delete_Feedback_Email_Alert</fullName>
        <ccEmails>Content.Curation@ni.com</ccEmails>
        <description>NI Flag For Delete Feedback Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>NI_Group_to_Delete</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NI_Knowledge_Templates/NI_Flag_For_Delete_Feedback_Template</template>
    </alerts>
    <rules>
        <fullName>NI_Flag_For_Delete_Article_Feedback_Rule</fullName>
        <actions>
            <name>NI_Flag_For_Delete_Feedback_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Article_Feedback__c.Summary__c</field>
            <operation>equals</operation>
            <value>Flag for Delete,Flag to Merge</value>
        </criteriaItems>
        <description>Workflow to send email to queue - NI Queue to Delete when a feedback with Flag for Delete is created/updated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
