<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Expected_Revenue</fullName>
        <description>PlanForce- Update Expected Revenue</description>
        <field>Expected_Revenue__c</field>
        <formula>Opportunity__r.ExpectedRevenue</formula>
        <name>Update Expected Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage</fullName>
        <description>PlanForce-Update Opportunity Stage</description>
        <field>Opportunity_Stage__c</field>
        <formula>TEXT(Opportunity__r.StageName )</formula>
        <name>Update Opportunity Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity_Update_Fields</fullName>
        <actions>
            <name>Update_Expected_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PlanForce-Update Fields</description>
        <formula>NOT(ISNULL( Opportunity__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
