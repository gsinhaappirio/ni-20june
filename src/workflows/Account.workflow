<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_To_Approver</fullName>
        <description>Email To Approver</description>
        <protected>false</protected>
        <recipients>
            <recipient>jamie.parker@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Provisional_Account</template>
    </alerts>
    <alerts>
        <fullName>New_APAC_Provisional_Account_DI_to_Validate</fullName>
        <ccEmails>apac.provisional@ni.com</ccEmails>
        <description>New APAC Provisional Account-DI to Validate</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Provisional_Account</template>
    </alerts>
    <alerts>
        <fullName>New_CEER_Academic_Account_Notification</fullName>
        <description>New CEER Academic Account Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>julie.snodgrass@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maciej.antonik@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nikolai.roesch@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Academic_Notifications/New_CEER_Academic_Account_Has_Been_Created</template>
    </alerts>
    <alerts>
        <fullName>New_MED_Academic_or_Research_Account_Notification</fullName>
        <description>New MED Academic or Research Account Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>julie.snodgrass@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>marc.djaoui@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Academic_Notifications/New_MED_Academic_or_Research_Account_Has_Been_Created</template>
    </alerts>
    <alerts>
        <fullName>New_NER_Academic_Account_Notification</fullName>
        <description>New NER Academic Account Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>hannah.wade@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Academic_Notifications/New_NER_Academic_Account_Has_Been_Created</template>
    </alerts>
    <alerts>
        <fullName>New_Provisional_Account_DI_to_Validate</fullName>
        <ccEmails>dataentryrequests@ni.com</ccEmails>
        <description>New Provisional Account-DI to Validate</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Provisional_Account</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Phone</fullName>
        <field>Phone</field>
        <formula>IF(NOT(ISBLANK(Phone_Number__c)), 
    IF(NOT(ISBLANK(Phone_Country_Code__c)),&quot;+&quot; &amp; Phone_Country_Code__c &amp; &quot; &quot;,&quot;&quot;)
    &amp;
    IF(NOT(ISBLANK(Phone_Area_Code__c)), Phone_Area_Code__c &amp; &quot; &quot;, &quot;&quot;)
    &amp;
    Phone_Number__c
    &amp;
    IF(NOT(ISBLANK(Phone_Extension__c)), &quot; ext &quot; &amp; Phone_Extension__c, &quot;&quot;)
    ,
    &quot;&quot;
)</formula>
        <name>Account Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountTypePartner</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update AccountType Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_Customer</fullName>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Account Update AccountType Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_Distributor</fullName>
        <field>Type</field>
        <literalValue>Distributor</literalValue>
        <name>Account Update AccountType Distributor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_Gold</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update AccountType Gold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_Platinum</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update AccountType Platinum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_Silver</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update AccountType Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_AccountType_VAR</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update AccountType VAR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_SubtypeSilver</fullName>
        <field>Subtype__c</field>
        <literalValue>Alliance Silver</literalValue>
        <name>Account Update SubtypeSilver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Alliance</fullName>
        <field>Subtype__c</field>
        <literalValue>Alliance</literalValue>
        <name>Account Update Subtype Alliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Customer</fullName>
        <field>Subtype__c</field>
        <name>Account Update Subtype Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Distributor</fullName>
        <field>Subtype__c</field>
        <name>Account Update Subtype Distributor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Gold</fullName>
        <field>Subtype__c</field>
        <literalValue>Alliance Gold</literalValue>
        <name>Account Update Subtype Gold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Platinum</fullName>
        <field>Subtype__c</field>
        <literalValue>Alliance Platinum</literalValue>
        <name>Account Update Subtype Platinum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_Silver</fullName>
        <field>Subtype__c</field>
        <literalValue>Alliance Silver</literalValue>
        <name>Account Update Subtype Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Subtype_VAR</fullName>
        <field>Subtype__c</field>
        <literalValue>VAR</literalValue>
        <name>Account Update Subtype VAR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Type_Partner_Alliance</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update Type Partner Alliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Update_Type_Silver</fullName>
        <field>Type</field>
        <literalValue>Partner</literalValue>
        <name>Account Update Type Silver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description_Summary</fullName>
        <field>Description_Summary__c</field>
        <formula>IF(LEN(Description) &gt; 252, LEFT(Description, 252) &amp; &apos;...&apos;, Description)</formula>
        <name>Description Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Account_Cascading_OBM</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>https://parsec-broken.ni.com/sfdc-integration/outbound/cmdm/cdi-sync-inbox/1/SfdcAccountWithCascade</endpointUrl>
        <fields>Account_Name_Phonetic__c</fields>
        <fields>Account_Name_Roman__c</fields>
        <fields>Alias1__c</fields>
        <fields>Alias2__c</fields>
        <fields>Alias3__c</fields>
        <fields>Fax</fields>
        <fields>Fax_Area_Code__c</fields>
        <fields>Fax_Country_Code__c</fields>
        <fields>Fax_Number__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Phone</fields>
        <fields>Phone_Area_Code__c</fields>
        <fields>Phone_Country_Code__c</fields>
        <fields>Phone_Extension__c</fields>
        <fields>Phone_Number__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountry</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingState</fields>
        <fields>ShippingStreet</fields>
        <fields>Shipping_Street_1__c</fields>
        <fields>Shipping_Street_2__c</fields>
        <fields>Shipping_Street_3__c</fields>
        <fields>Shipping_Street_4__c</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>deploy.user@ni.com</integrationUser>
        <name>Account Cascading OBM</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Account_Non_Cascading_OBM1</fullName>
        <apiVersion>32.0</apiVersion>
        <description>Account Details on creation/Updates for non cascading</description>
        <endpointUrl>https://parsec-broken.ni.com/sfdc-integration/outbound/cmdm/cdi-sync-inbox/1/SfdcAccountWithCascade</endpointUrl>
        <fields>Account_Name_Phonetic__c</fields>
        <fields>Account_Name_Roman__c</fields>
        <fields>Alias1__c</fields>
        <fields>Alias2__c</fields>
        <fields>Alias3__c</fields>
        <fields>Fax</fields>
        <fields>Fax_Area_Code__c</fields>
        <fields>Fax_Country_Code__c</fields>
        <fields>Fax_Number__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Phone</fields>
        <fields>Phone_Area_Code__c</fields>
        <fields>Phone_Country_Code__c</fields>
        <fields>Phone_Extension__c</fields>
        <fields>Phone_Number__c</fields>
        <fields>ShippingCity</fields>
        <fields>ShippingCountry</fields>
        <fields>ShippingPostalCode</fields>
        <fields>ShippingState</fields>
        <fields>ShippingStreet</fields>
        <fields>Shipping_Street_1__c</fields>
        <fields>Shipping_Street_2__c</fields>
        <fields>Shipping_Street_3__c</fields>
        <fields>Shipping_Street_4__c</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>deploy.user@ni.com</integrationUser>
        <name>Account  Non Cascading OBM</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>APAC Provisional Account Request</fullName>
        <actions>
            <name>New_APAC_Provisional_Account_DI_to_Validate</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Provisional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Continent__c</field>
            <operation>equals</operation>
            <value>APAC</value>
        </criteriaItems>
        <description>Send all APAC provisional account requests to the data steward queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account - NO cascading Touch point</fullName>
        <actions>
            <name>Account_Non_Cascading_OBM1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Trigger the message from Salesforce on account creation and specific field changes. These fields are not part of cascading updates to the contacts.  These messages are only for CDI.</description>
        <formula>AND( 	OR( 		ISNEW(), 		ISCHANGED( Fax ), 		ISCHANGED( Fax_Area_Code__c ), 		ISCHANGED( Fax_Country_Code__c ), 		ISCHANGED( Fax_Number__c ), 		ISCHANGED( Phone ), 		ISCHANGED( Phone_Area_Code__c ), 		ISCHANGED( Phone_Country_Code__c ), 		ISCHANGED( Phone_Extension__c ), 		ISCHANGED( Phone_Number__c ), 		ISCHANGED( LastUndeletedDate__c ) 	), 	NOT( OR( 		ISCHANGED( Account_Name_Phonetic__c ), 		ISCHANGED( Account_Name_Roman__c ), 		ISCHANGED( Alias1__c ), 		ISCHANGED( Alias2__c ), 		ISCHANGED( Alias3__c ), 		ISCHANGED( Name ), 		ISCHANGED( ShippingCity ), 		ISCHANGED( ShippingCountry ), 		ISCHANGED( ShippingPostalCode ), 		ISCHANGED( ShippingState ), 		ISCHANGED( ShippingStreet ), 		ISCHANGED( Shipping_Street_1__c ), 		ISCHANGED( Shipping_Street_2__c ), 		ISCHANGED( Shipping_Street_3__c ), 		ISCHANGED( Shipping_Street_4__c ) 	)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Update Cascading Touchpoint</fullName>
        <actions>
            <name>Account_Cascading_OBM</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send OBM On specfic field changes those need to get caseded to the contact level on CDI. These messages are only for CDI.</description>
        <formula>AND(           OR( ISCHANGED( Account_Name_Phonetic__c ),              ISCHANGED( Account_Name_Roman__c ),              ISCHANGED( Alias1__c ),              ISCHANGED( Alias2__c ),              ISCHANGED( Alias3__c ),              ISCHANGED( Name ),              ISCHANGED( ShippingCity ),              ISCHANGED( ShippingCountry ),              ISCHANGED( ShippingPostalCode ),              ISCHANGED( ShippingState ),              ISCHANGED( ShippingStreet ),              ISCHANGED( Shipping_Street_1__c ),              ISCHANGED( Shipping_Street_2__c ),              ISCHANGED( Shipping_Street_3__c ),              ISCHANGED( Shipping_Street_4__c )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account-New Academic or Research MED</fullName>
        <actions>
            <name>New_MED_Academic_or_Research_Account_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Industry</field>
            <operation>equals</operation>
            <value>Academic,Research</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>France,FR,FRA,Israel,IL,ISR,Italy,IT,ITA,Portugal,PT,PRT,Spain,ES,ESP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Andorra,AD,AND,Canary Islands,IC,CNR,French Southern Territories,TF,ATF,Gibraltar,GI,GIB,Greece,GR,GRC,Malta,MT,MLT,Mayotte,YT,MYT,Monaco,MC,MCO,RÃ©union,RE,REU,San Marino,SM,SMR,Vatican City State,VA,VAT</value>
        </criteriaItems>
        <description>Workflow to send email to Marc Diaoui when a new Academic or Research MED account is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account-New Academic-CEER</fullName>
        <actions>
            <name>New_CEER_Academic_Account_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Account.Industry</field>
            <operation>equals</operation>
            <value>Academic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Austria,AT,AUT,Czech Republic,CZ,CZE,Germany,DE,DEU,Hungary,HU,HUN,Poland,PL,POL,Romania,RO,ROU,Slovakia,SK,SVK,Slovenia,SI,SVN,Switzerland,CH,CHE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Yugoslavia,YU,YUG,Liechtenstein,LI,LIE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Croatia,HR,HRV,Albania,AL,ALB,Bosnia and Herzegovina,BA,BIH,Bulgaria,BG,BGR,Macedonia,MK,MKD,Serbia and Montenegro,CS,SCG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Montenegro,ME,MNE,Serbia,RS,SRB,Yugoslavia,YU,YUG</value>
        </criteriaItems>
        <description>Workflow to send email to Nikolai Roesch and Maciej Antonik when a new CEER Academic account is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account-New Academic-NER</fullName>
        <actions>
            <name>New_NER_Academic_Account_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Industry</field>
            <operation>equals</operation>
            <value>Academic</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Belgium,BE,BEL,Denmark,DK,DNK,Finland,FI,FIN,Ireland,IE,IRL,Netherlands,NL,NLD,Norway,NO,NOR,Sweden,SE,SWE,United Kingdom,GB,GBR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
            <value>Faroe Islands,FO,FRO,Greenland,GL,GRL,Iceland,IS,ISL,Luxembourg,LU,LUX,Svalbard and Jan Mayen Islands,SJ,SJM</value>
        </criteriaItems>
        <description>Workflow to send email to David Baker when a new NER Academic account is created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account-Provisional Validation by DI</fullName>
        <actions>
            <name>New_Provisional_Account_DI_to_Validate</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Provisional</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Continent__c</field>
            <operation>notEqual</operation>
            <value>APAC</value>
        </criteriaItems>
        <description>Send all non-APAC provisional account requests to the data steward queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeAlliance</fullName>
        <actions>
            <name>Account_Update_Subtype_Alliance</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Type_Partner_Alliance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Alliance&apos; when Customer Category is updated</description>
        <formula>AND( ISCHANGED(Customer_Category__c), UPPER(TEXT(Customer_Category__c)) = &apos;ALLIANCE&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeCustomer</fullName>
        <actions>
            <name>Account_Update_AccountType_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Subtype_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Customer&apos; when Customer Category is updated</description>
        <formula>AND( 		 ISCHANGED(Customer_Category__c),  		 NOT( 		  OR( 			UPPER(TEXT(Customer_Category__c))  = &apos;SELECT INTEGRATOR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE PLATINUM&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR+&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE GOLD&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE AND VAR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE SILVER&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR NON-ALLIANCE&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;DISTRIBUTOR&apos; 		  ) 		 ) 		)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeDistributor</fullName>
        <actions>
            <name>Account_Update_AccountType_Distributor</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Subtype_Distributor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Distributor&apos; when Customer Category is updated</description>
        <formula>AND( 		 ISCHANGED(Customer_Category__c),  		 UPPER(TEXT(Customer_Category__c))  = &apos;DISTRIBUTOR&apos; 		)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeGold</fullName>
        <actions>
            <name>Account_Update_AccountType_Gold</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Subtype_Gold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Gold&apos; when Customer Category is updated</description>
        <formula>AND( ISCHANGED(Customer_Category__c), OR(  UPPER(TEXT(Customer_Category__c)) = &apos;VAR+&apos;, UPPER(TEXT(Customer_Category__c)) = &apos;ALLIANCE GOLD&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypePartner</fullName>
        <actions>
            <name>Account_Update_AccountTypePartner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Partner&apos; when Customer Category is updated</description>
        <formula>AND( 		 ISCHANGED(Customer_Category__c), 		  OR( 			UPPER(TEXT(Customer_Category__c))  = &apos;SELECT INTEGRATOR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE PLATINUM&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR+&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE GOLD&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE AND VAR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE SILVER&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;ALLIANCE&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR&apos;, 			UPPER(TEXT(Customer_Category__c))  = &apos;VAR NON-ALLIANCE&apos; 		  ) 		)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypePlatinum</fullName>
        <actions>
            <name>Account_Update_AccountType_Platinum</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Subtype_Platinum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Platinum&apos; when Customer Category is updated</description>
        <formula>AND( ISCHANGED(Customer_Category__c), OR( UPPER(TEXT(Customer_Category__c)) = &apos;SELECT INTEGRATOR&apos;, UPPER(TEXT(Customer_Category__c)) = &apos;ALLIANCE PLATINUM&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeSilver</fullName>
        <actions>
            <name>Account_Update_SubtypeSilver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Type_Silver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;Silver&apos; when Customer Category is updated</description>
        <formula>AND( ISCHANGED(Customer_Category__c), OR( UPPER(TEXT(Customer_Category__c)) = &apos;ALLIANCE AND VAR&apos;, UPPER(TEXT(Customer_Category__c)) = &apos;ALLIANCE SILVER&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account_Update_AccountTypeVAR</fullName>
        <actions>
            <name>Account_Update_AccountType_VAR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Update_Subtype_VAR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set account type to &apos;VAR&apos; when Customer Category is updated</description>
        <formula>AND( ISCHANGED(Customer_Category__c), OR(  UPPER(TEXT(Customer_Category__c)) = &apos;VAR&apos;, UPPER(TEXT(Customer_Category__c)) = &apos;VAR NON-ALLIANCE&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Description Summary</fullName>
        <actions>
            <name>Description_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISNEW(),   ISCHANGED(Description) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
