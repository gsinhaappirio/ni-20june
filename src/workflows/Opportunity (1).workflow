<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Big_Deal_Alert</fullName>
        <description>Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>chandran.nair@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>francis.griffiths@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jason.green@ni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>robert.morton@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Big_Deal_Template</template>
    </alerts>
    <alerts>
        <fullName>Big_Deal_Alert_IndRAA</fullName>
        <description>Big Deal Alert - IndRAA</description>
        <protected>false</protected>
        <recipients>
            <recipient>jayaram.pillai@ni.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Big_Deal_Template</template>
    </alerts>
    <alerts>
        <fullName>OPP01_Inform_Delivery_Status_Mail</fullName>
        <description>OPP01_Inform_Delivery_Status_Mail</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opp_Delivery_Status_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Close_Date</fullName>
        <field>CloseDate</field>
        <formula>today()</formula>
        <name>Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description_Summary</fullName>
        <field>Description_Summary__c</field>
        <formula>IF(LEN(Description) &gt; 252, LEFT(Description, 252) &amp; &apos;...&apos;, Description)</formula>
        <name>Description Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Update_by_Owner</fullName>
        <field>Last_Update_by_Owner__c</field>
        <formula>NOW()</formula>
        <name>Last Update by Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Account_Name</fullName>
        <description>Update the opportunity&apos;s account name field.</description>
        <field>Account_Name__c</field>
        <formula>Account.Name</formula>
        <name>Opportunity Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Description</fullName>
        <description>Copy Description mapping to Description after lead conversion</description>
        <field>Description</field>
        <formula>Description_mapping__c</formula>
        <name>Populate Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Opp_External_Number</fullName>
        <field>Opp_External_Number__c</field>
        <formula>Opportunity_Number__c</formula>
        <name>Populate Opp External Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Big Deal Alert</fullName>
        <actions>
            <name>Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount_USD__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000000</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Big Deal Alert - IndRAA</fullName>
        <actions>
            <name>Big_Deal_Alert_IndRAA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount_USD__c</field>
            <operation>greaterOrEqual</operation>
            <value>100000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Account_Region__c</field>
            <operation>equals</operation>
            <value>IndRAA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Description Summary</fullName>
        <actions>
            <name>Description_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISNEW(),   ISCHANGED(Description) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP02_Delvery Status Notification</fullName>
        <actions>
            <name>OPP01_Inform_Delivery_Status_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Delivery_Status__c</field>
            <operation>contains</operation>
            <value>Not Applicable,In Progress,Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Account Name</fullName>
        <actions>
            <name>Opportunity_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto-populate the opportunity&apos;s account name for global searching</description>
        <formula>OR(PRIORVALUE(AccountId) &lt;&gt; AccountId, ISBLANK(Account_Name__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OpportunityCloseDate</fullName>
        <actions>
            <name>Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Close Date field with today&apos;s date  whenever opportunity get closed.</description>
        <formula>AND(   NOT(ISPICKVAL(PRIORVALUE(StageName), &quot;Won&quot;)),   NOT(ISPICKVAL(PRIORVALUE(StageName), &quot;Lost&quot;)),   OR(     ISPICKVAL(StageName, &quot;Won&quot;),     ISPICKVAL(StageName, &quot;Lost&quot;)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Description</fullName>
        <actions>
            <name>Populate_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy the value of Description mapping field to Description after lead conversion</description>
        <formula>NOT(ISBLANK(Description_mapping__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Opp External Number</fullName>
        <actions>
            <name>Populate_Opp_External_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates Opp External Number to access oracle data</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
