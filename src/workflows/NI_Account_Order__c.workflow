<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Key</fullName>
        <description>Unique Key</description>
        <field>Unique_External_Id__c</field>
        <formula>account__r.Id + order__r.Id</formula>
        <name>Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
