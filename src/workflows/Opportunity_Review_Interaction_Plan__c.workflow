<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Insert_Sales_Stage</fullName>
        <field>Sales_Stage__c</field>
        <formula>TEXT(Opportunity__r.StageName)</formula>
        <name>Insert Sales Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_Date</fullName>
        <field>Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity Review%2FInteraction Plan</fullName>
        <actions>
            <name>Insert_Sales_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Inserting Sales Stage value from Opportunity stage when Opportunity Review/Interaction Plan is related to any Opportunity.</description>
        <formula>NOT(ISBLANK( Opportunity__c ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Completed Date</fullName>
        <actions>
            <name>Update_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Review_Interaction_Plan__c.Oppr_Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
