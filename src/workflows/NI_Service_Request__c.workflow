<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_SR_Unique_External_Id</fullName>
        <field>Unique_External_Id__c</field>
        <formula>Site__c + TEXT(Oracle_Service_Request_Id__c)</formula>
        <name>Update SR Unique External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
