<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Order_Unique_External_Id</fullName>
        <description>Update Unique External Id with site + oracle header id concatenation</description>
        <field>Unique_External_Id__c</field>
        <formula>Site__c  +   TEXT(Oracle_Header_Id__c)</formula>
        <name>Update Order Unique External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
