<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CampaignActive</fullName>
        <description>It&apos;ll update the Active check box has active</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>CampaignActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CampaignDeActive</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>CampaignDeActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Source_System_Default_To_SFDC</fullName>
        <description>Update the Source System field default to SFDC</description>
        <field>Source_System__c</field>
        <literalValue>SFDC</literalValue>
        <name>Campaign Source System Default To SFDC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_UpdateSourceCode</fullName>
        <description>Updates Campaign.source_code if Marketing Asset Record type is inserted with a &quot;blank&quot; value</description>
        <field>Source_Code__c</field>
        <formula>UPPER(CASESAFEID(Id))</formula>
        <name>Campaign_UpdateSourceCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign Source System Default To SFDC</fullName>
        <actions>
            <name>Campaign_Source_System_Default_To_SFDC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>iuser</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>einte</value>
        </criteriaItems>
        <description>On Campaign object Source System should be defaulted to SFDC when the created by is not integration user (alias: iuser) and not Eloqua Integration (alias: einte)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CampaignActiveUpdate</fullName>
        <actions>
            <name>CampaignActive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>This rule is used to update the Active check box in campaign if the status is Active</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CampaignDeactiveUpdate</fullName>
        <actions>
            <name>CampaignDeActive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Cancelled,Renewed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Campaign Source Code if Inserted w Null</fullName>
        <actions>
            <name>Campaign_UpdateSourceCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Source_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Asset Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Asset_Type__c</field>
            <operation>equals</operation>
            <value>Advertising,Web,Events</value>
        </criteriaItems>
        <description>Update Campaign Source Code Field if record is inserted with a blank source code (should be combo of Org ID and Record ID)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
