public class UserHandler {

    @future
    public static void updateLicenseCost(Set<Id> userIds){
        List<License_Cost__c> costList = License_Cost__c.getall().values();
        Map<String, License_Cost__c> costs = new Map<String, License_Cost__c>();
        for(License_Cost__c cost : costList){
            costs.put(cost.Name, cost);
        }
        
        Map<Id, Profile> profiles = new Map<Id, Profile>([Select Id, Name from Profile]);
        
        List<User> users = [Select Id, ProfileId from User where Id in :userIds];
        
        for(User user : users){
            Profile profile = profiles.get(user.profileId);
            License_Cost__c cost = costs.get(profile.name);
            user.License_Cost__c = cost != null ? cost.cost__c : 0;
        }
        
        update users;
        
    }
    
    @future
    public static void updateManager(Set<Id> userIds, Map<Id, String> managersMap){
    
        List<User> users = [Select Id, ManagerId from User where Id in :userIds];
        List<String> managerPersonNumbers;
        Map<String, Id> managerIdsMap;
        try {
            managerPersonNumbers = managersMap.values();
        } catch (Exception e) { managerPersonNumbers = null; }

        if (managerPersonNumbers != null && !managerPersonNumbers.isEmpty()){    
            List<User> managers = [Select Id, EmployeeNumber from User where EmployeeNumber in :managerPersonNumbers And EmployeeNumber != null And EmployeeNumber != ''];
            managerIdsMap = new Map<String, Id>();
            for(User manager : managers){
                if(manager.EmployeeNumber != null){
                    managerIdsMap.put(String.valueOf(manager.EmployeeNumber), manager.Id);
                }
            }
        }

        for (User user : users){
            String empNum;
            try {
                empNum = managersMap.get(user.Id);
            } catch (Exception e) { empNum = null; }
            if (empNum != null && managerIdsMap != null && !managerIdsMap.isEmpty()) {
                user.ManagerId = managerIdsMap.get(empNum);
            }
            else {
                user.ManagerId = null;
            }
        }
        
        update users;
        
    }
    
}