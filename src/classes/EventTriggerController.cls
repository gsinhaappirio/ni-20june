/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            EventTriggerController
   Description:     This class invokes methods of EventHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class EventTriggerController{
    private Boolean m_isExecuting = false;
    private integer batchSize = 0;
    
    /*Constructor*/
    public EventTriggerController (Boolean isExecuting, integer size){
        string errorDetail = system.label.Space;
        try{
            m_isExecuting = isExecuting;
            batchSize = size;
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }
     /**********************************************************************************************************************
    * Author:
    * Date: 
    * Param: List<Event> (Trigger.New),Map<Id,Event> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after insert methods of EventHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert (List<Event> newEventList){
        string errorDetail = system.label.Space;
        try{
            EventHandler.OppLastActiveDate (newEventList);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }
    
      
}