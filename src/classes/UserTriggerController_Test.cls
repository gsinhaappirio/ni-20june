@isTest(SeeAllData=true)
public class UserTriggerController_Test {

    @isTest
    public static void updateLicenseCostTest(){
        Test.startTest();
        User manager = TestUtils.getUserForProfile('man1', 'NI Sales Manager User');
        insert manager;
        
        User chatterOnlyPlusUser = TestUtils.getUserForProfile('guy2', 'NI Chatter Only Plus User');
        insert chatterOnlyPlusUser;
        
        List<User> newUsers = new List<User>();
        newUsers.add(manager);
        
        UserTriggerController.onBeforeInsert(newUsers);
        
        Map<Id, User> oldUserMap = new Map<Id, User>();
        oldUserMap.put(manager.Id, chatterOnlyPlusUser);
        
        UserTriggerController.onAfterUpdate(newUsers, oldUserMap);
        
        Test.stopTest();
    }

    @isTest
    public static void onAfterUpdateTest(){
        Test.startTest();
        User manager = TestUtils.getUserForProfile('man1', 'NI Sales Manager User');
        manager.EmployeeNumber = '10101010101';
        insert manager;
        
        User manager2 = TestUtils.getUserForProfile('guy2', 'NI Sales Manager User');
        manager2.EmployeeNumber = '20202020202';
        manager2.Manager_Person_Number__c = 303030303;
        insert manager2;
        
        List<User> newUsers = TestUtils.getUsersForProfile('Fake', 'NI Sales User', 10);
        for(User user : newUsers){
            user.Manager_Person_Number__c = Decimal.valueOf(manager.EmployeeNumber);
        }
        
        insert newUsers;
        
        UserTriggerController.onBeforeInsert(newUsers);
        
        User bigManager= TestUtils.getUserForProfile('BigMan', 'NI Sales Manager User');
        bigManager.EmployeeNumber = '303030303';
        insert bigManager;
        
        List<User> newUsers2 = new List<User>();
        newUsers2.add(bigManager);
        UserTriggerController.onBeforeInsert(newUsers2);
        
        Map<Id, User> oldUserMap = new Map<Id, User>();
        for(User user : newUsers){
            oldUserMap.put(user.Id, user);
            user.Manager_Person_Number__c = Decimal.valueOf(manager2.EmployeeNumber);
        }
        User userWithoutManager = TestUtils.getUserForProfile('GuyWithoutManager', 'NI Sales User');
        userWithoutManager.Manager_Person_Number__c = null;
        oldUserMap.put(newUsers.get(0).Id, userWithoutManager);
        
        UserTriggerController.onAfterUpdate(newUsers, oldUserMap);
        
        Test.stopTest();
    }
    
}