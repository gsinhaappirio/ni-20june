/**
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for Event_Trigger class.
  */

@isTest(SeeAllData=true)
Private class NI_Event_Trigger_Test{
    // Start of Constants for Test Methods
    private static final String ALIAS = 'sch1';
    private static final String SYSTEM_ADMIN = 'NI System Administrator';
    
     /**
    * @description       Test method to verify Event trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */
    private static testMethod void testEvent_Trigger_Test(){
        User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
        System.Dmlexception ex;
        
        System.runAs(runUser) {
        
            Boolean m_isExecuting = false;
            Integer batchSize = 0;
            
            //---------Insert Account-----------
            list<account> acclist = new list<Account>();
            account acc= new account(name= 'test',OwnerId = UserInfo.getUserId(),ShippingCountry = 'United States',ShippingStreet = '1/AX39',ShippingCity = 'New York',ShippingState = 'New York',ShippingPostalCode = '12119');
            acclist.add(acc);
            insert acclist;
            
            //---------Insert Opportunity-----------
            list<opportunity> opplist =  new list<opportunity>();    
            Opportunity opp = new opportunity(name='test2',AccountId=acclist[0].id,Multi_Phase_Opportunity__c=null,StageName='Qualify',ForecastCategoryName='funnel',CloseDate=system.today(),Amount = 1000  );
            opplist.add(opp);
            insert opplist;
            
            //---------Insert Event-----------
            list<event> eventlist = new list<event>();
            event event = new event ();
            eventlist.add(event);
            event.DurationInMinutes=120;
            event.ActivityDateTime=system.today();
            event.whatid=opplist[0].id;
            insert eventlist;
            update eventlist;
            
         
            test.starttest();
            
            //---------Event Controller Class Methods-----------
            EventTriggerController etc=new  EventTriggerController(m_isExecuting,batchSize);
            
            test.stoptest();
        
        }
    }
}