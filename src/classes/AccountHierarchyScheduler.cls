global class AccountHierarchyScheduler implements Schedulable {

    public static String CRON_EXP = '0 0 0 3 9 ? 2022';

    global void execute(SchedulableContext ctx) {
        String query = 'SELECT Id, Name, DunsNumber, DandbCompany.ParentOrHqDunsNumber, DandbCompany.GlobalUltimateDunsNumber, Parent.Name, Parent.DunsNumber, GlobalParent__c, GlobalParent__r.Name, GlobalParent__r.DunsNumber from Account where DandbCompany.GlobalUltimateDunsNumber != null AND Parent_Mismatch__c = true';
        Database.executeBatch(new BatchUtil(query, new AccountHierarchyBatchCaller()), 1);
    }
}