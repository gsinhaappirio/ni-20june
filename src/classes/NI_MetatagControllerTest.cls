/* Class Name   : NI_MetatagControllerTest
 * Description  : NI_MetatagController test class to handle all the unit test cases for the controller 
 * Created By   : Meghana Agarwal
 * Created On   : 22-06-2017

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Meghana Agarwal        22-06-2017                1                   Initial version
 * 
*/

@isTest
private class NI_MetatagControllerTest {
    
    //Test Method
    private static testMethod void testMetaData(){
        try{
            List<Knowledge__kav> articleList = KB_TestUtils.createArticles(1, 'Test-Article', true);
            test.startTest();
            Knowledge__kav art = articleList[0];
            List<NI_Meta_Info__mdt> metaDataRecords = [SELECT MasterLabel, Type__c, Value__c, Language_Specific__c FROM NI_Meta_Info__mdt];
            List<KB_Global_Wrapper> lst = NI_MetatagController.getData(art.Id,'en_US');
            System.assertEquals(lst.size(), metaDataRecords.size());
            test.stopTest();
        }
        catch(Exception e){
            System.debug('Error='+e.getMessage());
        }
    } 
}