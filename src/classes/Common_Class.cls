/**
  * @author        OffshoreTeam_Accenture
  * @date          20/11/2013
  * @description   Common Class for creation of common Data used in Test Classes.
  */
  
public with sharing class Common_Class{
    
    /**
      * @description       Method for defining User object record to be created for use in Test Class. 
      * @param             aliasId the Alias Id of the User record is to be created
      * @param             profileName the profile for which the User record is to be created
      * @return            User
      * @throws            NA
      */
    public static User runningUser(String aliasId, String profileName){
        Id profId = [SELECT Id FROM Profile WHERE Name=: profileName].Id;
        User userData = new User(FirstName=aliasId, LastName=aliasId, Alias=aliasId, IsActive=True, Email=aliasId+'@test.com.ni.com',
                                 ProfileId=profId, Username=aliasId+'@test.com.ni.com', EmailEncodingKey='UTF-8', CommunityNickname='Test',
                                 LanguageLocaleKey='en_US', LocaleSidKey='en_US', TimeZoneSidKey='GMT');
        return userData;
    }
}