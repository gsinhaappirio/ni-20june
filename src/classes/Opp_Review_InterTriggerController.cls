/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Opportunity_Review_Interaction_Plan
   Description:     This class invokes methods of Opportunity_Review_InteractionHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class Opp_Review_InterTriggerController{
    private Boolean m_isExecuting = false;
    private integer batchSize = 0;
    
    /*Constructor*/
    public Opp_Review_InterTriggerController(Boolean isExecuting, integer size){
        string errorDetail = system.label.Space;
        try{ 
            m_isExecuting = isExecuting;
            batchSize = size;
         }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();   
            System.debug(errorDetail);
        }
    }

   /**********************************************************************************************************************
    * Author:
    * Date: 
    * Param: List<Opportunity_Review_Interaction_Plan__c> (Trigger.New),Map<Id,Opportunity_Review_Interaction_Plan__c> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after insert methods of Opportunity_Review_InteractionHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert (List<Opportunity_Review_Interaction_Plan__c> newOpportunity_Review_Interaction_PlanList){
        string errorDetail = system.label.Space;
        try{ 
            Opportunity_Review_InteractionHandler.OppLastActiveDate(newOpportunity_Review_Interaction_PlanList);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();   
            System.debug(errorDetail);
        }
    }
}