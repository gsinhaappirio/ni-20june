@isTest(SeeAllData=true)
public with sharing class NI_Lead_To_Quote_Extension_Test {
    
    
    @isTest
    public static void constructorTest() {
        
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        System.assertEquals(ext.error, false);
        
    }
    
    @isTest
    public static void cancelTest() {
        
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        PageReference pageReference = ext.cancel();
        System.assertNotEquals(pageReference, null);
        
    }
    
    @isTest
    public static void setNoteCreationErrorStateTest(){
                List<SelectOption> contactItems;
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
    
        PageReference myPage = new ApexPages.StandardController(lead).view();
        ext.setNoteCreationErrorState(myPage, 'Error', true);
        
        System.assertNotEquals(myPage.getParameters().get(NI_Lead_To_Quote_Extension.ERROR_MESSAGE_PARAM), null);
        System.assertEquals(myPage.getParameters().get(NI_Lead_To_Quote_Extension.NOTES_PARAM), ext.notes);
    }
    
    @isTest
    public static void getNoteCreationErrorStateTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        
        ext.notes = 'Test note';
    	ext.sendToRequesterOnly = false;
        
        PageReference myPage = new ApexPages.StandardController(lead).view();
        ext.setNoteCreationErrorState(myPage, 'Error', false);
        ext.setNoteCreationErrorState(myPage, 'Error', true);
        ext.getNoteCreationErrorState(myPage);
        
        System.assertEquals(ext.error, true);
        System.assertEquals(ext.notes, myPage.getParameters().get(NI_Lead_To_Quote_Extension.NOTES_PARAM));
            
    }
    
    @isTest
    public static void createQuoteRequestNoteTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
        Boolean success = false;
        success = ext.createQuoteRequestNote();
        
    }
    
    @isTest
    public static void createQuoteRequestAttachmentTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.attachmentName = 'Test attachment';
        ext.attachmentBlob = Blob.valueOf('Test Data');
        ext.contentType = 'text/plain';
        Boolean success = false;
        success = ext.createQuoteRequestAttachment();
        
    }    
    
    @isTest(seeAllData=true)
    public static void mergeQuoteRequestTemplateEmailTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
        Messaging.SingleEmailMessage mail = ext.mergeQuoteRequestTemplateEmail(lead);
    
    }

    @isTest
    public static void sendQuoteRequestTemplateCustomSettingExceptionTest(){
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
        Boolean success = false;
        success = ext.sendQuoteRequestTemplateEmail();
    }        
    
    @isTest(seeAllData=true)
    public static void saveErrorAttachmentSizeTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
        ext.attachmentName = 'Test attachment';
        ext.attachmentBlob = Blob.valueOf('Test Data');
        ext.contentType = 'text/plain';
        ext.fileSize = 10000000;
        PageReference page;
        page = ext.save();
    
    }    
    
    @isTest(seeAllData=true)
    public static void saveTest(){
    
        Lead lead = createLead();
        NI_Lead_To_Quote_Extension ext = new NI_Lead_To_Quote_Extension(new ApexPages.StandardController(lead));
        ext.notes = 'Test note';
        ext.sendToRequesterOnly = false;
        ext.attachmentName = 'Test attachment';
        ext.attachmentBlob = Blob.valueOf('Test Data');
        ext.contentType = 'text/plain';
        PageReference page;
        page = ext.save();
    
    }
    
    static Lead createLead(){
        Lead lead = new Lead(FirstName='Chuck', LastName='Norris', Company='Test Company', LeadSource='Sales - Sales', Status='Accepted', Country='US',
                             Phone='123456789', Description='Test Description', State='TX', Street='Street 1', City='City 1', postalCode = 'PC1');
        insert lead;    
        
        lead = [SELECT id, lead_number__c, name, lead.owner.name, budget__c, timeframe__c, company, address, country, street, city, state, postalCode, phone, email, description 
                FROM Lead 
                WHERE id = :lead.id ];
        
        return lead;
    }    
    
    

}