@isTest
public with sharing class NI_Sales_Lead_Quote_WS_Test {

    @isTest(seeAllData=true)
    static void validateOppQuoteLink(){
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Lead_Quote_WS_MockImpl());
        String sfdcLeadId = '843Ads989ASD90s';
        Decimal sfdcQuoteID = 123;
        Decimal sfdcOrgID = 1;
        NI_Sales_Lead_Quote_WS.LeadQuote_WSSOAP service = new NI_Sales_Lead_Quote_WS.LeadQuote_WSSOAP();
        NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element response = service.LeadQuoteLink(sfdcLeadId, sfdcQuoteID, sfdcOrgID);
        
        System.assertEquals('S', response.status);
        
    }   

}