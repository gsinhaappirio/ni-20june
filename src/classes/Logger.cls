/**
 * Used for our custom logging object. This will create a new record
 * for the Log__c object.
 * Uses future methods so that users' operations aren't held up due to logging.
 * 
 * @author: Tyler Hobbs
 */
global class Logger {
    
    /** Virtual representation of the Log__c object. Used for webservice purposes mainly */
    private virtual class Log {
        public String type;
        public String location;
        public String message;
        public String stackTrace;
        public DateTime createdTime;
    }
    
    /** Everything needs to flow through this method. It checks for governor limits of the current session */
    private static void insertLog(String type, String location, String message, String stackTrace, DateTime requestedTime){
        if(Limits.getDmlRows() < Limits.getLimitDmlRows() && Limits.getDmlStatements() < Limits.getLimitDmlStatements()){
            Log__c logToInsert = new Log__c();
            logToInsert.type__c = type;
            logToInsert.Location__c = location;
            logToInsert.Message__c = message;
            logToInsert.Stack_Trace__c = stackTrace;
            logToInsert.Requested_Time__c = requestedTime;
            try{
                Database.insert(logToInsert, false);  
            }catch(Exception e){
                System.debug('DMLException thrown: ' + e.getMessage() + '\nStack Trace: \n' + e.getStackTraceString());
            }
        }else{
            System.debug('Governor limits have been exhausted. Skipping log creation as to not make the remaining processing fail.');
        }
    }
    
    global static void error(Exception e){
        insertLog('Error', null, e.getMessage(), e.getStackTraceString(), System.now());
    }
    
    global static void error(String location, Exception e){
        insertLog('Error', location, e.getMessage(), e.getStackTraceString(), System.now());
    }
    
    global static void error(String location, String message){
        insertLog('Error', location, message, null, System.now());
    }
    
    global static void error(String message){
        insertLog('Error', null, message, null, System.now());
    }
    
    global static void debug(String location, String message){
        insertLog('Debug', location, message, null, System.now());
    }
    
    global static void debug(String message){
        insertLog('Debug', null, message, null, System.now());
    }
    
    global static void debug(Integer message){
        insertLog('Debug', null, String.valueOf(message), null, System.now());
    }
    
    global static void debug(Decimal message){
        insertLog('Debug', null, String.valueOf(message), null, System.now());
    }
    
    global static void debug(Object obj){
        String message = String.valueOf(obj);
        insertLog('Debug', null, message, null, System.now());
    }
    
    /** potential future enhancement to allow debugging from javascript */
    /*
    webservice static void debug(String serializedLog){
        try{
            /*
            Expects a JSON string with the following format:
            {
                "type"          : "---",
                "location"      : "---",
                "message"       : "---",
                "stackTrace"    : "---",
            } 
            */
            /*
            Logger.Log log = (Logger.Log) JSON.deserialize(serializedLog, Logger.Log.class);
        }catch(Exception e){
            System.debug('Exception thrown: ' + e.getMessage() + '\nStack Trace: \n' + e.getStackTraceString());
        }
    }
    //*/
    
}