public class NI_TestUtils {
	//Method to create knowledge article records
    public static List<Knowledge__kav> createArticles(Integer count, String name, Boolean isInsert){
        List<Knowledge__kav> articleList = new List<Knowledge__kav>();
        for(Integer i=0; i<count; i++){
            Knowledge__kav article = new Knowledge__kav(Title=name+i, UrlName=name+i);
        	articleList.add(article);  
        }
    	if(isInsert) {
        	insert articleList;
        }
        return articleList;
    }
}