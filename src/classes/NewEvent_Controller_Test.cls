@isTest(SeeAllData=true)
public class NewEvent_Controller_Test {

    @isTest
    public static void multiphaseOppTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        Multi_phase_opportunity__c mpo = new Multi_phase_opportunity__c();
        mpo.Account__c = account.Id;
        insert mpo;
        
        PageReference pageRef = new PageReference('/apex/NewEvent?fromId=' + mpo.Id);
        Test.setCurrentPage(pageRef);
        NewEvent_Controller extension = new NewEvent_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
    @isTest
    public static void oppTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        Contact contact = new Contact();
        contact.firstName = 'First';
        contact.LastName = 'Last';
        contact.AccountId = account.Id;
        insert contact;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Fake Opp';
        opp.AccountId = account.id;
        opp.StageName = 'Qualified';
        opp.CloseDate = System.today()+10;
        insert opp;
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.contactId = contact.Id;
        ocr.OpportunityId = opp.Id;
        ocr.IsPrimary = true;
        insert ocr;
        
        PageReference pageRef = new PageReference('/apex/NewEvent?fromId=' + opp.Id);
        Test.setCurrentPage(pageRef);
        NewEvent_Controller extension = new NewEvent_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
    @isTest
    public static void contactTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        Contact contact = new Contact();
        contact.firstName = 'First';
        contact.LastName = 'Last';
        contact.AccountId = account.Id;
        insert contact;
        
        PageReference pageRef = new PageReference('/apex/NewEvent?fromId=' + contact.Id);
        Test.setCurrentPage(pageRef);
        NewEvent_Controller extension = new NewEvent_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
    @isTest
    public static void accountTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        PageReference pageRef = new PageReference('/apex/NewEvent?fromId=' + account.Id);
        Test.setCurrentPage(pageRef);
        NewEvent_Controller extension = new NewEvent_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
    @isTest
    public static void leadTest(){
        Test.startTest();
        Lead lead = new Lead();
        lead.LastName = 'Fake lead';
        lead.Status = 'Qualified';
        lead.LeadSource = 'Sales - Sales';
        lead.Country = 'US';
        lead.Company = 'Fake Company';
        insert lead;
        
        PageReference pageRef = new PageReference('/apex/NewEvent?fromId=' + lead.Id);
        Test.setCurrentPage(pageRef);
        NewEvent_Controller extension = new NewEvent_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
}