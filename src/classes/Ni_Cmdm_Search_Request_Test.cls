@isTest
public class Ni_Cmdm_Search_Request_Test {

    @isTest
    public static void QbeContactSearchRequestTypeTest(){
        Ni_Cmdm_Search_Request.QbeContactSearchRequestType req = new Ni_Cmdm_Search_Request.QbeContactSearchRequestType();
        req.filters = new Ni_Cmdm_Search_Request.QbeFilterType();
        req.contact = new Ni_Cmdm_Search_Enterprise_Object.Contact();
    }
    
    @isTest
    public static void SearchFaultTypeTest(){
        Ni_Cmdm_Search_Request.SearchFaultType type = new Ni_Cmdm_Search_Request.SearchFaultType();
        type.code = 'code';
        type.faultMessage = 'faultMessage';
    }
    
    @isTest
    public static void QbeAccountSearchRequestTypeTest(){
        Ni_Cmdm_Search_Request.QbeAccountSearchRequestType req = new Ni_Cmdm_Search_Request.QbeAccountSearchRequestType();
        req.filters = new Ni_Cmdm_Search_Request.QbeFilterType();
        req.account = new Ni_Cmdm_Search_Enterprise_Object.Account();
    }
    
    @isTest
    public static void QbeFilterTypeTest(){
        Ni_Cmdm_Search_Request.QbeFilterType type = new Ni_Cmdm_Search_Request.QbeFilterType();
        type.system_x = 'system';
        type.maxRows = 10;
        type.sforceIDsOnly = false;
    }
    
}