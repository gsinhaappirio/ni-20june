@isTest(SeeAllData=true)
public class OpportunityTransferTool_Test {

    @isTest
    public static void constructorTest(){
        Test.startTest();
        
        List<User> owners = [SELECT Id from User where Id in (Select OwnerId from Opportunity where StageName in ('Qualify','Advance')) LIMIT 2];
        if(owners.size()>1){
            User fromOwner = owners.get(0);
            User toOwner = owners.get(1);
        
        
            Test.setCurrentPageReference(new PageReference('Page.OpportunityTransferTool')); 
            System.currentPageReference().getParameters().put('fromId', fromOwner.Id);
            System.currentPageReference().getParameters().put('toId', toOwner.Id);
        }
        Opportunity opp = new Opportunity(name='Test Opp', StageName='Qualify', CloseDate=System.today()+10);
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        OpportunityTransferTool controller = new OpportunityTransferTool(sc);
        
        for(OpportunityTransferTool.OpportunityWrapper ow : controller.opps){
            ow.transfer = true;
        }
        
        controller.transfer();
        controller.toggleAllForTransfer();
        controller.navigateToList();
        controller.switchOwners();
        
        Test.stopTest();
        
    }
    
}