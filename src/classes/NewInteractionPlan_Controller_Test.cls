@isTest(SeeAllData=true)
public class NewInteractionPlan_Controller_Test {
    
    @isTest(SeeAllData=true)
    public static void oppTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        Contact contact = new Contact();
        contact.firstName = 'First';
        contact.LastName = 'Last';
        contact.AccountId = account.Id;
        insert contact;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Fake Opp';
        opp.AccountId = account.id;
        opp.StageName = 'Qualified';
        opp.CloseDate = System.today()+10;
        insert opp;
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.contactId = contact.Id;
        ocr.OpportunityId = opp.Id;
        ocr.IsPrimary = true;
        insert ocr;
        
        PageReference pageRef = new PageReference('/apex/NewInteractionPlan?fromId=' + opp.Id);
        Test.setCurrentPage(pageRef);
        NewInteractionPlan_Controller extension = new NewInteractionPlan_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    public static void leadTest(){
        Test.startTest();
        Lead lead = new Lead();
        lead.LastName = 'Fake lead';
        lead.Status = 'Qualified';
        lead.LeadSource = 'Sales - Sales';
        lead.Country = 'US';
        lead.Company = 'Fake Company';
        insert lead;
        
        PageReference pageRef = new PageReference('/apex/NewInteractionPlan?fromId=' + lead.Id);
        Test.setCurrentPage(pageRef);
        NewInteractionPlan_Controller extension = new NewInteractionPlan_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
}