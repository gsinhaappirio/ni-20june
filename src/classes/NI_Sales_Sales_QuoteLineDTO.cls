public with sharing class NI_Sales_Sales_QuoteLineDTO {

	public Decimal quoteLineNumber {get;set;}
	public String bookingClass {get;set;}
	public String partNumber {get;set;}
	public String partDescription {get;set;}
	public Decimal quantity {get;set;}
	public Decimal lineListPrice {get;set;}
	public String currencyCode {get;set;}	
	public Decimal lineAdjustedAmount {get;set;}
	public Decimal lineAdjustedPercent {get;set;}
	public Decimal lineQuotePrice {get;set;}
	public Decimal lineTotalPrice {get;set;}
	
	
	public NI_Sales_Sales_QuoteLineDTO(){}
	
	public NI_Sales_Sales_QuoteLineDTO(Decimal quoteLineNumber, String bookingClass, String partNumber, String partDescription, 
							 Decimal quantity, Decimal lineListPrice, String currencyCode, Decimal lineAdjustedAmount, 
							 Decimal lineAdjustedPercent, Decimal lineQuotePrice, Decimal lineTotalPrice){
							 
		this.quoteLineNumber = quoteLineNumber;
		this.bookingClass = bookingClass;
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.quantity = quantity;
		this.lineListPrice = lineListPrice;
		this.currencyCode = currencyCode;
		this.lineAdjustedAmount = lineAdjustedAmount;
		this.lineAdjustedPercent = lineAdjustedPercent;
		this.lineQuotePrice = lineQuotePrice;
		this.lineTotalPrice = lineTotalPrice;
								 
	}

}