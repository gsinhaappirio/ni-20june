@isTest
public class Ni_Cmdm_Account_Search_Result_Test {

    @isTest
    public static void noParametersTest(){
        Account account = new Account();
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, score, sourceSystem);
        System.assertNotEquals(accountResult.account, null);
        
        accountResult = new Ni_Cmdm_Account_Search_Result();
    }
    
    @isTest
    public static void ascendingSortTest(){
        
        List<Ni_Cmdm_Account_Search_Result> accounts = new List<Ni_Cmdm_Account_Search_Result>();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            String sourceSystem = 'Salesforce';
            Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, i, sourceSystem);
            accounts.add(accountResult);
        }
        
        accounts.sort();
        
        for(Integer i = 0; i < accounts.size(); i++){
            if(i < accounts.size()-1){
                System.assert(accounts.get(i+1).score < accounts.get(i).score);
            }
        }

    }
    
    public static void descendingSortTest(){
        
        List<Ni_Cmdm_Account_Search_Result> accounts = new List<Ni_Cmdm_Account_Search_Result>();
        for(Integer i = 9; i >= 0; i--){
            Account account = new Account();
            String sourceSystem = 'Salesforce';
            Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, i, sourceSystem);
            accounts.add(accountResult);
        }
        
        accounts.sort();
        
        for(Integer i = 0; i < accounts.size(); i++){
            if(i < accounts.size()-1){
                System.assert(accounts.get(i+1).score < accounts.get(i).score);
            }
        }

    }
    
}