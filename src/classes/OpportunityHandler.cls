/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            OpportunityHandler
   Description:     This class contains support actions for triggers on Opportunity  
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class OpportunityHandler {
    
    static final string CLASSNAME = 'OpportunityHandler';
    static final string METHODNAMEINT = 'insertAmtCount';
    static final string METHODNAMEUD = 'updateAmtCount';
    static final string METHODNAMEDEL = 'deleteAmtCount';
    static final string APP_NAMELBL = System.label.App_Name;
    static final string QUALIFY  = System.label.Qualify;
    static final string ADVANCE  = System.label.Advance;
    static final string WON  = System.label.Won;
    static final string LOST  = System.label.Lost;
    static final string CLOSING  = System.label.Closing;
    static final string OPPTRGRNAME = System.label.Opp_TrgeName;
    final static string ERRMSG = system.label.Opportunity_Errormsg ;
    private static Set<Id> updatedIds = new Set<Id>();
    
    
    @future
    public static void updateAmountUSD(Set<Id> opportunityIds){
        List<Opportunity> opps = [Select Id, CurrencyISOCode, Amount from Opportunity where Id in :opportunityIds];
        List<CurrencyType> currencyTypeList = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        Map<String, CurrencyType> currencyTypes = new Map<String, CurrencyType>();
        
        for(CurrencyType currencyType : currencyTypeList){
            currencyTypes.put(currencyType.ISOCode.toUpperCase().trim(), currencyType);
        }
        
        List<Opportunity> toUpdate = new List<Opportunity>();
        CurrencyType currencyTypeUSD = currencyTypes.get('USD');
        for(Opportunity opp : opps){
            if(opp.Amount != null){
                if(opp.CurrencyISOCode.equalsIgnoreCase('USD')){
                    opp.amount_usd__c = opp.Amount;
                }else{
                    CurrencyType oppCurrencyType = currencyTypes.get(opp.CurrencyISOCode);
                    Decimal convertedAmount = (opp.Amount/oppCurrencyType.ConversionRate) * currencyTypeUSD.ConversionRate;
                    opp.amount_usd__c = convertedAmount;
                }
                toUpdate.add(opp);
            }
        }
        if(!toUpdate.isEmpty()){
            Database.update(toUpdate, false);
        }
    }
    
    // update the Last_Update_by_Owner__c field if the user is the owner of the opportunity
    public static void updateLastUpdateByOwner(List<Opportunity> newOpportunityList){
        for(Opportunity opportunity : newOpportunityList){
            if(UserInfo.getUserId() == opportunity.OwnerId){
                opportunity.Last_Update_by_Owner__c = System.now();
            }
        }
    }
    
    // every time an opp is pushed out by at least a month, increment PushCount__c
    public static void updateOppPushCount(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap){
        
        for (Opportunity opportunity : newOpportunityList) { //Bulk trigger handler so that you can mass update opportunities and this fires for all'
            // gets new values for updated rows
            Date newCloseDate = opportunity.CloseDate;
            Date oldCloseDate = oldOpportunityMap.get(opportunity.Id).CloseDate;
            
            // if the month or year is higher, it's been pushed out
            if (oldCloseDate < newCloseDate && (oldCloseDate.month() < newCloseDate.month() || oldCloseDate.year() < newCloseDate.year())) {
                opportunity.PushCount__c = opportunity.PushCount__c != null ? opportunity.PushCount__c+1 : 1;
            }
        }
    }
    
    public static void updateAmtCount(List<Opportunity> newOpportunityList, Map<Id,Opportunity> oldOpportunityMap){

        // this should fire after the opportunity update
        Set<Id> multiPhaseOppIds = new Set<Id>();
        
        // figure out the list of multi phase opps to recalculate
        for(Opportunity newOpp : newOpportunityList){
        
            if(oldOpportunityMap != null){
                Opportunity oldOpp = oldOpportunityMap.get(newOpp.Id);
                if(oldOpp.Multi_Phase_Opportunity__c != null && newOpp.Multi_Phase_Opportunity__c != null && oldOpp.Multi_Phase_Opportunity__c != newOpp.Multi_Phase_Opportunity__c){
                    // if the opp was moved from one multiphase opp to another
                    if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                        multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                    }
                    if(!multiPhaseOppIds.contains(oldOpp.Multi_Phase_Opportunity__c)){
                        multiPhaseOppIds.add(oldOpp.Multi_Phase_Opportunity__c);
                    }
                }else if(oldOpp.Multi_Phase_Opportunity__c == null && newOpp.Multi_Phase_Opportunity__c != null){
                    // if the opp was added to a multiphase opp from nothing
                    if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                        multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                    }
                }else if(oldOpp.Multi_Phase_Opportunity__c != null && newOpp.Multi_Phase_Opportunity__c == null){
                    // if the opp was removed from a multiphase opp to nothing
                    if(!multiPhaseOppIds.contains(oldOpp.Multi_Phase_Opportunity__c)){
                        multiPhaseOppIds.add(oldOpp.Multi_Phase_Opportunity__c);
                    }
                }else{
                    // the multiphase stayed the same. check the attributes that changed
                    if(oldOpp.Amount != newOpp.Amount){
                        if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                            multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                        }
                    }else if((!oldOpp.StageName.equals(WON) && !oldOpp.StageName.equals(LOST)) && (newOpp.StageName.equals(WON) || newOpp.StageName.equals(LOST))) {
                        if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                            multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                        }
                    }else if((oldOpp.StageName.equals(WON) || oldOpp.StageName.equals(LOST)) && (!newOpp.StageName.equals(WON) && !newOpp.StageName.equals(LOST))) {
                        if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                            multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                        }
                    }else if(oldOpp.CurrencyIsoCode != newOpp.CurrencyIsoCode) {
                        if(!multiPhaseOppIds.contains(newOpp.Multi_Phase_Opportunity__c)){
                            multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                        }
                    }
                }
            }else{
                if(newOpp.Multi_Phase_Opportunity__c != null){
                    multiPhaseOppIds.add(newOpp.Multi_Phase_Opportunity__c);
                }
            }
            
        }
        
        // these are all of the multiphase opps that need to be recalculated
        Map<Id, Multi_Phase_Opportunity__c> multiPhaseOpps = new Map<Id, Multi_Phase_Opportunity__c>([Select Id, Current_Amount_del__c, OppCount__c, CurrencyIsoCode, (Select Id, amount, StageName, Multi_Phase_Opportunity__c, CurrencyIsoCode from Opportunities__r where StageName not in (:WON, :LOST)) from Multi_Phase_Opportunity__c where Id in :multiPhaseOppIds]);
        List<CurrencyType> currencyTypeList = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        Map<String, CurrencyType> currencyTypes = new Map<String, CurrencyType>();
        for(CurrencyType currencyType : currencyTypeList){
            currencyTypes.put(currencyType.ISOCode, currencyType);
        }
        
        System.debug('***** number of multiphase opps to update: ' + multiPhaseOpps.size());
        
        Map<Id, Decimal> amountMap = new Map<Id, Decimal>();
        Map<Id, Opportunity> childOpps = new Map<Id, Opportunity>();
        
        for(Multi_Phase_Opportunity__c multiPhaseOpp : multiPhaseOpps.values()){
            childOpps.putAll(multiPhaseOpp.Opportunities__r);
            multiPhaseOpp.OppCount__c = multiPhaseOpp.Opportunities__r.size();
            
            //if there are no more opportunities, it's needed to set the amount to zero
            if (multiPhaseOpp.OppCount__c == 0){
               amountMap.put(multiPhaseOpp.id, 0);
            }
        }
        
        // have to loop through opps, adding amounts
        for(Opportunity childOpp : childOpps.values()){
            Decimal amount = 0;
            if(amountMap.containsKey(childOpp.Multi_Phase_Opportunity__c)){
                System.debug('***** already exists. getting existing amount');
                amount = amountMap.get(childOpp.Multi_Phase_Opportunity__c);
            }
            
            Multi_Phase_Opportunity__c multiPhaseOpp = multiPhaseOpps.get(childOpp.Multi_Phase_Opportunity__c);
            
            // convert the currency to the org's currency
            CurrencyType oppCurrencyType = currencyTypes.get(childOpp.CurrencyISOCode);
            CurrencyType mpCurrencyType = currencyTypes.get(multiPhaseOpp.currencyIsoCode);
            
            System.debug('***** opp currency type: ' + oppCurrencytype.isocode + ' : ' + oppCurrencyType.conversionrate);
            System.debug('***** mp currency type: ' + mpCurrencytype.isocode + ' : ' + mpCurrencyType.conversionrate);
            
            Decimal convertedAmount = (childOpp.amount/oppCurrencyType.ConversionRate) * mpCurrencyType.ConversionRate;
            
            amount += childOpp.amount != null ? convertedAmount : 0;
            amountMap.put(childOpp.Multi_Phase_Opportunity__c, amount);
        }
        
        for(Id multiPhaseOppId : amountMap.keySet()){
            multiPhaseOpps.get(multiPhaseOppId).Current_Amount_del__c = amountMap.get(multiPhaseOppId);
        }
        
        update multiPhaseOpps.values();
        
    }

   /**********************************************************************************************************************
    * Author: 
    * Date:
    * Param: List<Opportunity> (Trigger.New),Map<Id,Opportunity> (Trigger.OldMap)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Delete
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/
 
    public static void deleteAmtCount(List<Opportunity> oldOpportunityList){
        // this should fire after the opp update
        Set<Id> multiPhaseOppIds = new Set<Id>();
        
        // figure out the list of multi phase opps to recalculate
        for(Opportunity oldOpp : oldOpportunityList){
            if(oldOpp.Multi_Phase_Opportunity__c != null){
                // if the opp was a part of a multiphase opp when it was deleted
                if(!multiPhaseOppIds.contains(oldOpp.Multi_Phase_Opportunity__c)){
                    multiPhaseOppIds.add(oldOpp.Multi_Phase_Opportunity__c);
                }
            }
        }
        
        // these are all of the multiphase opps that need to be recalculated
        Map<Id, Multi_Phase_Opportunity__c> multiPhaseOpps = new Map<Id, Multi_Phase_Opportunity__c>([Select Id, Current_Amount_del__c, OppCount__c, (Select Id, amount, StageName, Multi_Phase_Opportunity__c from Opportunities__r where StageName not in (:WON, :LOST)) from Multi_Phase_Opportunity__c where Id in :multiPhaseOppIds]);
        
        System.debug('***** number of multiphase opps to update: ' + multiPhaseOpps.size());
        
        Map<Id, Opportunity> childOpps = new Map<Id, Opportunity>();
        for(Multi_Phase_Opportunity__c multiPhaseOpp : multiPhaseOpps.values()){
            childOpps.putAll(multiPhaseOpp.Opportunities__r);
            multiPhaseOpp.OppCount__c = multiPhaseOpp.Opportunities__r.size();
            if(multiPhaseOpp.OppCount__c == 0){
                multiPhaseOpp.Current_Amount_del__c = 0;
            }
        }
        
        Map<Id, Decimal> amountMap = new Map<Id, Decimal>();
        // have to loop through opps, adding amounts
        for(Opportunity childOpp : childOpps.values()){
            Decimal amount = 0;
            if(amountMap.containsKey(childOpp.Multi_Phase_Opportunity__c)){
                System.debug('***** already exists in map. getting existing amount');
                amount = amountMap.get(childOpp.Multi_Phase_Opportunity__c);
            }
            amount += childOpp.amount != null ? childOpp.amount : 0;
            amountMap.put(childOpp.Multi_Phase_Opportunity__c, amount);
        }
        
        for(Id multiPhaseOppId : amountMap.keySet()){
            multiPhaseOpps.get(multiPhaseOppId).Current_Amount_del__c = amountMap.get(multiPhaseOppId);
        }
        
        update multiPhaseOpps.values(); 
    }
    
    public static void updatePlanOpportunities(List<Opportunity> newOpportunityList, Map<Id,Opportunity> oldOpportunityMap){
        Map<ID, Decimal> updatedExpectedRev=new Map<ID, Decimal>();
        Map<ID, String> updatedStgeNme=new Map<ID, String>();
        Set<Opportunity> updatedOpp= new Set<Opportunity>(); 
        
        for (Opportunity newOpp : newOpportunityList) {
            Opportunity oldOpp = oldOpportunityMap.get(newOpp.Id);
            if(oldOpp.ExpectedRevenue != newOpp.ExpectedRevenue || oldOpp.StageName != newOpp.StageName) {
                updatedOpp.add(newOpp);
                updatedExpectedRev.put(newOpp.Id, newOpp.ExpectedRevenue);
                updatedStgeNme.put(newOpp.Id, newOpp.StageName);
            }
        }
        
        if(updatedOpp.size() > 0){
            List<Plan_Opportunity__c> planOpps = new List<Plan_Opportunity__c>([SELECT Id, Opportunity__c, Expected_Revenue__c, Opportunity_Stage__c FROM Plan_Opportunity__c WHERE Opportunity__c IN :updatedOpp]);
            list<Plan_Opportunity__c> updatePlanOpps = new list<Plan_Opportunity__c>();
            if(planOpps.size() > 0){
                for(Plan_Opportunity__c planOpp : planOpps){
                    planOpp.Expected_Revenue__c = updatedExpectedRev.get(planOpp.Opportunity__c);
                    planOpp.Opportunity_Stage__c = updatedStgeNme.get(planOpp.Opportunity__c);
                    updatePlanOpps.add(planOpp);
                }
            }
            update updatePlanOpps;
        }
    }
    
    public static void updatePriceBook(List<Opportunity> newOpportunities){
        try{
            Id standardPriceBookId = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
            
            for(Opportunity opp : newOpportunities){
                if(opp.Pricebook2Id == null && standardPriceBookId != null){
                    System.debug('***** updating the pricebook id');
                    opp.Pricebook2Id = standardPriceBookId;
                    System.debug('***** finished updating the pricebook id');
                }
                System.debug('***** done with opp id: ' + opp.name);
            }
        }catch(Exception e){
            System.debug(e);                                        
        }
    }
}