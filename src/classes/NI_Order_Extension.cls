public class NI_Order_Extension {
    
    private final NI_Order__c order;
    public List<NI_Sales_Sales_OrderLineDTO> orderLineList = new List<NI_Sales_Sales_OrderLineDTO>();
    
    public NI_Order_Extension(ApexPages.StandardController stdController){
        
        this.order = (NI_Order__c)stdController.getRecord();
        
    }
    
    public List<NI_Sales_Sales_OrderLineDTO> getOrderLineList(){
        
        NI_Sales_Sales_Order_DAO dao = new NI_Sales_Sales_Order_DAO();
        this.orderLineList = dao.getOrderLines(String.valueOf(this.order.oracle_Header_Id__c));
        
        return this.orderLineList;
    }   

}