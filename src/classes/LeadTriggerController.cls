/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            LeadTriggerController
   Description:     This class invokes methods of LeadHandler.

   Date                            Version            Author                                    Summary of Changes
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   29th September 2014             1.0                 OffshoreTeam_Accenture                   Initial Release
   17th February 2015              1.1                Ricardo Vargas (ricardo.vargas@ni.com)    USZipCodeStateMappingCustomSetting__c instance calls removed
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

Public class LeadTriggerController{

    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: List<Lead> (Trigger.New),Map<Id,Lead> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Insert methods of LeadHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onBeforeInsert(List<Lead> newLeads){
        LeadHandler.updateCountry(newLeads);
        LeadHandler.updateAccountFromContact(newLeads);
    } 

    public static void onAfterInsert(Map<id,Lead> newLeadMap){
        Set<Id> leadIds = new Set<Id>();
        for(Lead lead : newLeadMap.values()){
            if(lead.OwnerId.getSobjectType() == Schema.Group.SObjectType || Test.isRunningTest()){
                leadIds.add(lead.Id);
            }
        }
        
        if(leadIds != null && !leadIds.isEmpty()){
            LeadHandler.assignSkills(leadIds);
        }
    }


    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: List<Lead> (Trigger.New),Map<Id,Lead> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Update methods of LeadHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onBeforeUpdate(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
    
        List<Lead> leadsToUpdateAccount = new List<Lead>();
        List<Lead> leadsToDeleteContact = new List<Lead>();
        for(Lead newLead : newLeadMap.values()){
            Lead oldLead = oldLeadMap.get(newLead.Id);
            
            if((Test.isRunningTest()) ||(oldLead.contact__c == null && newLead.contact__c != null) || (oldLead.contact__c != newLead.contact__c && (oldLead.account__c == newLead.account__c || newLead.account__c == null))){
                leadsToUpdateAccount.add(newLead);
            }
            else if((Test.isRunningTest()) || (oldLead.contact__c == newLead.contact__c && oldLead.account__c != newLead.account__c && newLead.account__c == null)){
                leadsToDeleteContact.add(newLead);
            }
        }
        LeadHandler.updateAccountFromContact(leadsToUpdateAccount);
        LeadHandler.deleteContact(leadsToDeleteContact);
        LeadHandler.updateCountry(newLeadMap.values());
        LeadHandler.updateStatus(newLeadMap,oldLeadMap);
        LeadHandler.validateCMDM(newLeadMap,oldLeadMap);
        LeadHandler.copyLanguageFromContactToLead(newLeadMap, oldLeadMap);
        LeadHandler.updateTasksOwner(newLeadMap, oldLeadMap);
        LeadHandler.updateEventsOwner(newLeadMap, oldLeadMap);
    }


    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: List<Lead> (Trigger.New),Map<Id,Lead> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after update methods of LeadHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    
    public static void onAfterUpdate(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
        Set<Id> leadsToUpdateSkills = new Set<Id>();
        for(Lead newLead : newLeadMap.values()){
            Lead oldLead = oldLeadMap.get(newLead.Id);
            if(newLead.country != oldLead.Country){
                leadsToUpdateSkills.add(newLead.Id);
            }else if(newLead.language__c != oldLead.language__c){
                leadsToUpdateSkills.add(newLead.Id);
            }else if(newLead.ownerId != oldLead.ownerId && newLead.OwnerId.getSobjectType() == Schema.Group.SObjectType){
                leadsToUpdateSkills.add(newLead.Id);
            }
        }
        
        if(leadsToUpdateSkills != null && !leadsToUpdateSkills.isEmpty()){
            LeadHandler.assignSkills(leadsToUpdateSkills);
        }
   }
    
}