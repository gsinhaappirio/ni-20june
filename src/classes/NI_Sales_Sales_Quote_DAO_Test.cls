/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NI_Sales_Sales_Quote_DAO_Test {

    @isTest(seeAllData=true)
    static void validateLinkOppToQuote() {
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote DAO';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        Test.startTest();        
        String sfdcOppId = opp.id;
        Double sfdcQuoteID = 123;
        Double sfdcOrgID = 1;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = dao.linkOppToQuote(sfdcOppId, sfdcQuoteID, sfdcOrgID);
        Test.stopTest();
        System.assertEquals('S', response.status);
    }
    
    @isTest(seeAllData=true)
    static void validateLinkLeadToQuote() {
        Test.setMock(WebServiceMock.class, new NI_Sales_Lead_Quote_WS_MockImpl());
        Lead ld = new Lead();
        ld.FirstName = 'Lead';
        ld.LastName = 'Test';
        ld.company = 'NI';
        ld.LeadSource = 'Marketing';
        ld.Country = 'US';
        insert ld;
        Test.startTest();        
        String sfdcLeadId = ld.id;
        Double sfdcQuoteID = 123;
        Double sfdcOrgID = 1;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element response = dao.linkLeadToQuote(sfdcLeadId, sfdcQuoteID, sfdcOrgID);
        Test.stopTest();
        System.assertEquals('S', response.status);
    }    
    
    //removed seeAllData=true, so the call will return error because there's no access to custom settings in test methods
    //are not allowed to see custom settings data
    @isTest
    static void validateLinkOppToQuoteGenericException() {
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        String sfdcOppId = '843Ads989ASD90s';
        Double sfdcQuoteID = 123;
        Double sfdcOrgID = 1;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = dao.linkOppToQuote(sfdcOppId, sfdcQuoteID, sfdcOrgID);
        
        System.assertEquals('E', response.status);
    }    
      
    @isTest(seeAllData=true)
    static void validateGetQuoteLines() {
        Test.setMock(WebServiceMock.class, new NI_Sales_QuoteWS_QuoteRequest_MockImpl());
        String QuoteId = '123';
        List<NI_Sales_Sales_QuoteLineDTO> quoteLinesList;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        
        quoteLinesList = dao.getQuoteLines(quoteId);
        
        System.assertEquals('1', quoteLinesList[0].partNumber);
        System.assertEquals('2', quoteLinesList[1].partNumber);
    } 
    
    @isTest(seeAllData=true)
    static void validateGetQuoteLinesWithError() {
        Test.setMock(WebServiceMock.class, new NI_Sales_QuoteWS_QuoteRequest_MockImpl(true));
        String QuoteId = '123';
        List<NI_Sales_Sales_QuoteLineDTO> quoteLinesList;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        
        quoteLinesList = dao.getQuoteLines(quoteId);
        
        System.assertNotEquals(null, quoteLinesList);
    }     
    
    @isTest
    static void validateGetQuoteLinesGenericException() {
        Test.setMock(WebServiceMock.class, new NI_Sales_QuoteWS_QuoteRequest_MockImpl());
        String QuoteId = '123';
        List<NI_Sales_Sales_QuoteLineDTO> quoteLinesList;
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        
        quoteLinesList = dao.getQuoteLines(quoteId);
        
        System.assertNotEquals(null, quoteLinesList);
    }    
    
}