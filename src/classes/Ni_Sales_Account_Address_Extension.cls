public class Ni_Sales_Account_Address_Extension {

    public Account account{get;set;}
    public Boolean editing{get;set;}
    public Boolean userCanEdit{get;set;}
    public ApexPages.StandardController controller{get;set;}

    public Ni_Sales_Account_Address_Extension(ApexPages.StandardController controller){
        account = (Account) controller.getRecord();
        
        Account_Settings__c accountSettings = Account_Settings__c.getInstance(UserInfo.getUserID());
        userCanEdit = accountSettings.Modify_Address__c;
        
        editing = false;
        this.controller = controller;
    }
    
    public PageReference edit(){
        editing = true;
        account = (Account) controller.getRecord();
        return null;
    }
    
    public PageReference save(){
        editing = false;
        controller.save();
        account = (Account) controller.getRecord();
        return null;
    }
    
    public PageReference cancel(){
        editing = false;
        account = (Account) controller.getRecord();
        return null;
    }

}