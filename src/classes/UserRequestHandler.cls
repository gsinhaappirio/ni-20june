public class UserRequestHandler {
    
    public static void validateUsername(List<User_Request__c> requests){
        String searchTokens = '';
        for(User_Request__c request : requests){
            searchTokens += '(' + request.email__c + ') OR ';
        }
        searchTokens = searchTokens.substring(0, searchTokens.length()-4);
        List<User> matchedUsers = (List<User>)[FIND :searchTokens IN ALL FIELDS RETURNING User(Id, Name, Username, FederationIdentifier)][0];
        for(User user : matchedUsers){
            for(User_Request__c request : requests){
                if(user.username != null && user.username.toLowerCase().trim().contains(request.email__c.toLowerCase().trim())){
                    request.addError('This email address is already taken by <a href=\'/' + user.Id + '\'>' + user.Name + '</a>', false);
                }else if(user.FederationIdentifier != null && user.FederationIdentifier.toLowerCase().trim().contains(request.email__c.toLowerCase().trim())){
                    request.addError('This federation ID is already taken by <a href=\'/' + user.Id + '\'>' + user.Name + '</a>', false);
                }
            }
        }
    }
    
    public static void populateFieldsFromClonedUser(List<User_Request__c> requests, Map<Id, User_Request__c> oldRequests){
        List<Id> clonedUserIds = new List<Id>();
        for(User_Request__c request : requests){
            if(request.Clone_User__c != null){
                clonedUserIds.add(request.Clone_User__c);
            }
        }
        
        Map<Id, User> clonedUsers = new Map<Id, User>([Select Id, ProfileId, Profile.Name, UserRoleId, UserRole.Name, Region_HCM__c from User where Id in :clonedUserIds]);
        for(User_Request__c request : requests){
            if(request.Clone_User__c != null){
                if(oldRequests != null){
                    //update, not insert
                    User_Request__c oldRequest = oldRequests.get(request.Id);
                    if(request.Clone_User__c != oldRequest.Clone_User__c){
                        // cloned user changed
                        User clonedUser = clonedUsers.get(request.Clone_User__c);
                        request.Profile__c = clonedUser.Profile.Name;
                        request.Role__c = clonedUser.UserRole.Name;
                        //request.Region__c = clonedUser.Region_HCM__c;
                    }
                }else{
                    User clonedUser = clonedUsers.get(request.Clone_User__c);
                    request.Profile__c = request.Profile__c != null ? request.Profile__c : clonedUser.Profile.Name;
                    request.Role__c = request.Role__c != null ? request.Role__c : clonedUser.UserRole.Name;
                    //request.Region__c = request.Region__c != null ? request.Region__c : clonedUser.Region_HCM__c;
                }
            }
        }
        
    }
    
    public static void createUser(List<User_Request__c> newRequests, Map<Id, User_Request__c> oldRequests){
        
        List<Id> clonedUserIds = new List<Id>();
        for(User_Request__c request : newRequests){
            if(request.Clone_User__c != null){
                clonedUserIds.add(request.Clone_User__c);
            }
        }
        
        Map<String, HCM_Record__c> hrDataMap = new Map<String, HCM_Record__c>();
        List<HCM_Record__c> hrDataList = [Select Id, Cost_Center__c, Department__c, Email__c, Email_Encoding_Key__c, 
                                          Employee_Number__c, First_Name__c, Language_Locale_Key__c, 
                                          Last_Name__c, Locale_SID_Key__c, Local_First_Name__c, Local_Last_Name__c, 
                                          Location_Name__c, HCM_Manager_Name__c, HCM_Manager_Person_Number__c, Middle_Name__c, 
                                          HCM_Region__c, Time_Zone_SID_Key__c from HCM_Record__c];
        for(HCM_Record__c hrData : hrDataList){
            hrDataMap.put(hrData.Email__c, hrData);
        }
        
        Map<Id, User> clonedUsers = new Map<Id, User>([Select Id, ProfileId, UserRoleId, UserRole.Name, Region_HCM__c from User where Id in :clonedUserIds]);
        
        List<Group> groups = [Select Id, Name, Type from Group where type = 'Regular'];
        Map<String, Group> queues = new Map<String, Group>();
        for(Group g : groups){
            queues.put(g.Name, g);
        }
        
        List<UserRole> rolesList = [Select Id, Name from UserRole];
        Map<String, UserRole> roles = new Map<String, UserRole>();
        for(UserRole role : rolesList){
            roles.put(role.Name, role);
        }
        
        Map<Id, Skill__c> skills = new Map<Id, Skill__c>([Select Id, Name from Skill__c]);
        
        List<User_Request__c> requestList = [Select Id, Email__c, Status__c, Profile__c, Queues__c, 
                                             Role__c, Clone_User__c, Region__c, Enable_Marketing__c, Enable_Forecast__c, Attempt_User_Creation__c,
                                             (Select Exclusive__c, Skill__c from User_Request_Skills__r) 
                                             from User_Request__c where Id in :newRequests];
        Map<String, User_Request__c> requestsMap = new Map<String, User_Request__c>();
        for(User_Request__c request : requestList){
            requestsMap.put(request.email__c, request);
        }
        
        List<User> usersToInsert = new List<User>();
        Map<String, List<User_Request_Skill__c>> requestedSkills = new Map<String, List<User_Request_Skill__c>>();
        
        List<Profile> profiles = [Select Id, Name from Profile];
        Map<String, Id> profileMap = new Map<String, Id>();
        for(Profile profile : profiles){
            profileMap.put(profile.Name, profile.Id);
        }
        
        Map<String, List<String>> selectedQueues = new Map<String, List<String>>();
        Map<String, Id> roleMap = new Map<String, Id>();
        List<String> serializedUserRequests = new List<String>();
        for(User_Request__c request : requestsMap.values()){
            User_Request__c oldRequest = oldRequests.get(request.Id);
            if(request.Attempt_User_Creation__c || (request.Status__c.equals('Processing') && !oldRequest.Status__c.equals('Processing'))){
                //create user
                HCM_Record__c hrData = hrDataMap.get(request.Email__c);
                if(hrData != null){
                    String communityNickname = request.email__c.toLowerCase().trim().replace('@ni.com','');
                    String alias = hrData.first_name__c.trim().substring(0,1).toLowerCase() + hrData.last_name__c.trim().toLowerCase();
                    if(alias.length() > 8){
                        alias = alias.substring(0, 8);
                    }
                    
                    Id profileId;
                    Id roleId;
                    String region;
                    if(request.Clone_User__c != null){
                        User clonedUser = clonedUsers.get(request.Clone_User__c);
                        if(clonedUser != null){
                            profileId = clonedUser.ProfileId;
                            roleId = clonedUser.UserRoleId;
                            region = clonedUser.Region_HCM__c;
                        }
                    }
                    
                    profileId = request.Profile__c != null ? profileMap.get(request.Profile__c) : profileId;
                    region = request.Region__c != null ? request.Region__c : region;
                    if(request.Role__c != null){
                        UserRole role = roles.get(request.Role__c);
                        roleId = role.Id;
                    }
                    
                    roleMap.put(request.Email__c, roleId);
                    User user = new User(FirstName=hrData.First_Name__c, LastName=hrData.Last_Name__c, Alias=alias, 
                                         IsActive=True, Email=request.Email__c, ProfileId=profileId, Username=request.Email__c, Region_HCM__c=hrData.HCM_Region__c,
                                         EmailEncodingKey=hrData.Email_Encoding_Key__c, CommunityNickname=communityNickname, LanguageLocaleKey=hrData.Language_Locale_Key__c,
                                         LocaleSidKey=hrData.Locale_SID_Key__c, TimeZoneSidKey=hrData.Time_Zone_SID_Key__c, Middle_Name__c=hrData.Middle_Name__c,
                                         Manager_Person_Number__c=hrData.HCM_Manager_Person_Number__c, Manager_HCM__c=hrData.HCM_Manager_Name__c, Location_Name__c=hrData.Location_Name__c,
                                         Local_Last_Name__c=hrData.Local_Last_Name__c, Local_First_Name__c=hrData.Local_First_Name__c, FederationIdentifier=hrData.Email__c,
                                         EmployeeNumber=hrData.Employee_Number__c, Department=hrData.Department__c, Cost_Center__c=hrData.Cost_Center__c,
                                         UserPermissionsMarketingUser=request.Enable_Marketing__c, ForecastEnabled=request.Enable_Forecast__c);
                    usersToInsert.add(user);
                    request.Status__c = 'Completed';
                    request.Attempt_User_Creation__c = false;
                    serializedUserRequests.add(JSON.serialize(request));
                    
                    List<User_Request_Skill__c> skillsList = new List<User_Request_Skill__c>();
                    for(User_Request_Skill__c requestedSkill : request.User_Request_Skills__r){
                        skillsList.add(requestedSkill);
                    }
                    requestedSkills.put(request.email__c, skillsList);
                    if(request.Queues__c != null){
                        selectedQueues.put(request.Email__c, request.Queues__c.split(';'));
                    }
                    
                }
            }
        }
        
        if(!usersToInsert.isEmpty()){
            Database.insert(usersToInsert, false);
            
            // insert user skills
            List<User_Skill__c> userSkillsToInsert = new List<User_Skill__c>();
            List<String> serializedGroupMembers = new List<String>();
            List<String> serializedUsers = new List<String>();
            
            for(User user : usersToInsert){
                List<User_Request_Skill__c> requestedSkillsList = requestedSkills.get(user.Username);
                for(User_Request_Skill__c requestedSkill : requestedSkillsList){
                    User_Skill__c userSkill = new User_Skill__c();
                    userSkill.User__c = user.Id;
                    userSkill.Skill__c = requestedSkill.Skill__c;
                    userSkill.Exclusive__c = requestedSkill.Exclusive__c;
                    userSkillsToInsert.add(userSkill);
                }
                
                List<String> groupNames = selectedQueues.get(user.Username);
                if(groupNames != null){
                    for(String queueName : groupNames){
                        Group g = queues.get(queueName);
                        if(g != null){
                            GroupMember groupMember = new GroupMember();
                            groupMember.groupId = g.Id;
                            groupMember.UserOrGroupId = user.Id;
                            // we cant make a another system insert after doing one already for the user
                            // so we need to serialize the groupmembers and pass it to a future method for insert
                            serializedGroupMembers.add(JSON.serialize(groupMember));
                        }
                    }
                }
                
                Id roleId = roleMap.get(user.Username);
                user.UserRoleId = roleId;
                serializedUsers.add(JSON.serialize(user));
                
            }
            insert userSkillsToInsert;
            addQueueMemberships(serializedGroupMembers);
            addUserRole(serializedUsers);
            updateUserRequests(serializedUserRequests);
            
        }
        
    }
    
    @future
    public static void updateUserRequests(List<String> serializedUserRequests)
    {
        // serializedusers already contains the userRoleId update
        List<User_Request__c> userRequests = new List<User_Request__c>();
        for (String serializedUserRequest : serializedUserRequests){
            User_Request__c userRequest = (User_Request__c) JSON.deserialize(serializedUserRequest, SObject.class);
            userRequests.add(userRequest);
        }
        update userRequests;
    }
    
    @future
    public static void addUserRole(List<String> serializedUsers)
    {
        // serializedusers already contains the userRoleId update
        List<User> users = new List<User>();
        for (String serializedUser : serializedUsers){
            User user = (User) JSON.deserialize(serializedUser, SObject.class);
            users.add(user);
        }
        update users;
    }
    
    
    @future
    public static void addQueueMemberships(List<String> serializedGroupMembers)
    {
        List<GroupMember> groupMembers = new List<GroupMember>();
        for (String serializedGroupMember : serializedGroupMembers){
            GroupMember gm = (GroupMember) JSON.deserialize(serializedGroupMember, SObject.class);
            groupMembers.add(gm);
        }
        insert groupMembers;
    }
    
}