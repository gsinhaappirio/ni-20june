@isTest
public class CampaignMemberHandler_Test {

    @isTest(SeeAllData=true)
    private static void updateLeadsTest(){
        
        Test.startTest();
        
        List<Campaign> campaigns = new List<Campaign>();
        for(Integer i = 0; i < 10; i++){
            Campaign campaign = new Campaign();
            campaign.Name = 'Test Campaign';
            campaign.Application__c = 'Datalogging'; 
            campaigns.add(campaign);
        }
        insert campaigns;
        
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.FirstName = 'First';
            lead.LastName = 'Last';
            lead.LeadSource = 'Sales - Sales';
            lead.Country = 'US';
            lead.Company = 'NI';
            leads.add(lead);
        }
        insert leads;
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        for(Integer i = 0; i < 10; i++){
            CampaignMember cm = new CampaignMember();
            cm.campaignId = campaigns[i].Id;
            cm.LeadId = leads[i].Id;
            campaignMembers.add(cm);
        }
        insert campaignMembers;
        
        Map<Id, CampaignMember> campaignMembersMap = new Map<Id, CampaignMember>();
        for(CampaignMember cm : campaignMembers){
            campaignMembersMap.put(cm.Id, cm);
        }
        //CampaignMemberHandler.updateLeads(campaignMembersMap);
        
        Test.stopTest();
        
    }
    
}