public class Ni_Sales_Lead_Details_Extension {
    
    public Lead lead {get;set;}
    
    public Account account {get;set;}
    public String accountId {get;set;}
    
    public Contact contact {get;set;}
    public String contactId {get;set;}
    
    public Ni_Sales_Lead_Details_Extension(ApexPages.StandardController controller) {
        lead = (Lead) controller.getRecord();
    }
    
    public PageReference refresh(){
        if(accountId != null && !accountId.equals('')){
            account = [select Name, Id from Account where Id =: accountId];
        }
        
        if(contactId != null && !contactId.equals('')){
            contact = [select Name, FirstName, LastName, Id from Contact where Id =: contactId];
        }
        
        return null;
    }
}