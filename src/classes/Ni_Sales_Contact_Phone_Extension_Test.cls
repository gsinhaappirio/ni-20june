@isTest
public with sharing class Ni_Sales_Contact_Phone_Extension_Test {
    
    @isTest
    public static void editTest(){
        PageReference pageRef = new PageReference('/apex/NI_Contact_Phone_Inline');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        
        Contact c = new Contact();
        c.firstName = 'Test';
        c.lastName = 'Test';
        c.account = a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        Ni_Sales_Contact_Phone_Extension extension = new Ni_Sales_Contact_Phone_Extension(sc);
        extension.edit();
        Test.stopTest();
        
        System.assertEquals(extension.editing, true);
    }
    
    @isTest
    public static void saveTest(){
        PageReference pageRef = new PageReference('/apex/NI_Contact_Phone_Inline');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        
        Contact c = new Contact();
        c.firstName = 'Test';
        c.lastName = 'Test';
        c.account = a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        Ni_Sales_Contact_Phone_Extension extension = new Ni_Sales_Contact_Phone_Extension(sc);
        extension.save();
        Test.stopTest();
        
        System.assertEquals(extension.editing, false);
    }
    
    @isTest
    public static void cancelTest(){
        PageReference pageRef = new PageReference('/apex/NI_Contact_Phone_Inline');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        
        Contact c = new Contact();
        c.firstName = 'Test';
        c.lastName = 'Test';
        c.account = a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        Ni_Sales_Contact_Phone_Extension extension = new Ni_Sales_Contact_Phone_Extension(sc);
        extension.cancel();
        Test.stopTest();
        
        System.assertEquals(extension.editing, false);
    }
    
}