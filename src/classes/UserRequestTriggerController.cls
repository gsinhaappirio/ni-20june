public class UserRequestTriggerController {
    
    public static void onBeforeInsert(List<User_Request__c> newRequestList){
        UserRequestHandler.validateUsername(newRequestList);
        UserRequestHandler.populateFieldsFromClonedUser(newRequestList, null);
    }
    
    public static void onBeforeUpdate(List<User_Request__c> newRequestList, Map<Id, User_Request__c> oldRequestMap){
        UserRequestHandler.populateFieldsFromClonedUser(newRequestList, oldRequestMap);
    }
    
    public static void onAfterUpdate(List<User_Request__c> newRequestList, Map<Id, User_Request__c> oldRequestMap){
        UserRequestHandler.createUser(newRequestList, oldRequestMap);
    }

}