@isTest
public class CampaignTriggerControllerTest {

    @isTest(seealldata=true)
    public static void onInsertAddMemberStatuses(){
        
        Test.startTest();
        
        //campaign
        Campaign campaign = new Campaign();
        campaign.name = 'Campaign';
        campaign.Status = 'Active';
        campaign.Type = 'Marketing Campaign';
        campaign.RecordTypeId = '012i0000000xsPw';
        campaign.Initiative_Category__c = 'Campaign';
        //campaign.Program_Name__c = 'Academic';
        campaign.Stripe__c = 'Software';
        campaign.Campaign_Purpose__c = 'Reputation';
        campaign.StartDate = System.today();
        campaign.EndDate = System.today()+10;
        campaign.Application__c = 'Datalogging'; 
        insert campaign;
        
        List<Campaign> campaigns = new List<Campaign>();
        campaigns.add(campaign);
        
        CampaignTriggerController.onAfterInsert(campaigns);
        
        Test.stopTest();
        
        //asset
        /*
        Campaign campaign = new Campaign();
        campaign.name = 'Asset';
        campaign.Asset_Type__c = 'Social';
        campaign.Program_Family__c = 'Sales Enablement';
        campaign.Status = 'Active';
        campaign.Buy_Cycle_Stage__c = 'Purchase';
        campaign.Type = 'Marketing Asset';
        campaign.RecordTypeId = '012i0000000xsPv';
        */
    }
    
}