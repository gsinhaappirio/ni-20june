@isTest
public class Oracle_ServiceRequestNoteTest {

    @isTest(seeAllData=true)
    static void Oracle_ServiceRequestNoteTest() {
        Test.setMock(WebServiceMock.class, new Oracle_ServiceRequestNoteMock());
        Oracle_ServiceRequestNote.SRNotes360SOAP service = new Oracle_ServiceRequestNote.SRNotes360SOAP();
        
        Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element[] response = service.getSRNotes(1);
        
        System.assertEquals(response[0].noteCreatedBy, 'creatorName1');
        System.assertEquals(response[0].noteDate, Integer.valueOf(DateTime.newInstanceGMT(2014,01,01).getTime() / 1000));
        System.assertEquals(response[0].noteType, 'typeName1');
        System.assertEquals(response[0].noteComments, 'comments1');
        
        System.assertEquals(response[1].noteCreatedBy, 'creatorName2');
        System.assertEquals(response[1].noteDate, Integer.valueOf(DateTime.newInstanceGMT(2014,01,01).getTime() / 1000));
        System.assertEquals(response[1].noteType, 'typeName2');
        System.assertEquals(response[1].noteComments, 'comments2');
    }
}