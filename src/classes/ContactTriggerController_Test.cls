@isTest(SeeAllData=true)
public class ContactTriggerController_Test {

    @isTest(SeeAllData=true)
    private static void onAfterUndelete(){
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test Account';
        account.Type = 'Customer';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 10; i++){
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'Contact' + String.valueOf(i);
            contact.account = account;
            contacts.add(contact);
        }
        
        insert contacts;
        
        ContactTriggerController.onAfterUndelete(contacts);
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsert(){
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test Account';
        account.Type = 'Customer';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        account.ownerId = null;
        
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 10; i++){
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'Contact' + String.valueOf(i);
            contact.account = account;
            contacts.add(contact);
        }
        
        insert contacts;
        
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for(Contact contact : contacts){
            contactMap.put(contact.Id, contact);
        }
        ContactTriggerController.onAfterInsert(contactMap);
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterUpdate(){
        Test.startTest();
        
        Account account = new Account();
        account.Name = 'Test Account';
        account.Type = 'Customer';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        account.ownerId = null;
        
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 10; i++){
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'Contact' + String.valueOf(i);
            contact.account = account;
            contacts.add(contact);
        }
        
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for(Contact contact : contacts){
            contactMap.put(contact.Id, contact);
        }
        ContactTriggerController.onAfterInsert(contactMap);
        Test.stopTest();
    }
    
}