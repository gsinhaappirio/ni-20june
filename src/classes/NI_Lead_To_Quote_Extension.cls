public with sharing class NI_Lead_To_Quote_Extension {

    // Inner class for throwing as custom exception
    public class CustomSettingException extends Exception {}

    private final Lead lead;
    public String notes { get; set; }
    public Boolean error { get; set; } // flag for note creation errors
    public String errorMessage { get; set; } // flag for note creation errors
    public String undefinedRequestorMessage { get; set; } 
    public Boolean enableAttachment { get; set; }
    public Boolean isUrgent { get; set; }
    public String urgentMsg { get; set; } 
    public Boolean sendToRequesterOnly {get;set;}
    public Boolean submitted { get; set; }
    public String client { get; set; } // flag for note creation errors
    public Decimal maxAttachmentSize { get; set; }
    //used to be displayed in user messages with a more friendly looking
    public Decimal roundedMaxAttachmentSize { get; set; }
    private Ni_Quote_Settings__c quoteSettings{get;set;}
    
    //declared Transient so attachment is not transmitted as part of the view state
    public Transient String attachmentName { get; set; } // attachment name
    public Transient Blob attachmentBlob { get; set; } // attachment content
    public Transient String contentType {get; set;} // attachment content-type
    public Transient Integer fileSize {get; set;} // attachment size      
    
    @testVisible
    private static final String ERROR_MESSAGE_PARAM = 'errorMessage';
    @testVisible
    private static final String ERROR_CAUSE_PARAM = 'errorCause';
    @testVisible
    private static final String NOTES_PARAM = 'notes';
    
    /**
    * Constructor
    * @param stdController an Lead standard controller
    */
    public NI_Lead_To_Quote_Extension(ApexPages.StandardController stdController){
        
        this.lead = [SELECT id, lead_number__c, name, owner.name, budget__c, timeframe__c, company, address, country, street, city, state, postalCode, phone, email, description 
                     FROM Lead 
                     WHERE id = :stdController.getRecord().id] ;
                     
        error = false;
        submitted = false;
        client = 'desktop';
        isUrgent = false;
        
        quoteSettings = Ni_Quote_Settings__c.getInstance(UserInfo.getUserID());
        enableAttachment = quoteSettings.Enable_attachments_on_quote_request__c;
        maxAttachmentSize = quoteSettings.Max_attach_size_limit__c;
        maxAttachmentSize = maxAttachmentSize != null && maxAttachmentSize > 0 ? maxAttachmentSize : 3145728;
        roundedMaxAttachmentSize = (Decimal)(maxAttachmentSize/(1024*1024)).setscale(2);
        undefinedRequestorMessage = quoteSettings.undefined_requestor_msg__c != null ? 
                                    quoteSettings.undefined_requestor_msg__c : '(Undefined Country/Requestor)';
        urgentMsg = quoteSettings.urgent_msg__c != null ? 
                                    quoteSettings.urgent_msg__c : 'The quote request will be marked as urgent so that the quote creator can prioritise the request. ' + 
                                                                  'Use of this flag will be monitored should only be used genuine customer urgency.';                                   
        
        // Verify whether current user has edit access to this Lead record
        if( ![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :lead.id].HasEditAccess ){
            throw new NoAccessException();
        }        
        
        PageReference page = ApexPages.currentPage();
        if(page.getParameters().get(ERROR_MESSAGE_PARAM) != null){
            getNoteCreationErrorState(page);
        }else{
            
            Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Country__c.fields.getMap();
            List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
            String query = 'SELECT ';
            for(Schema.SObjectField s : fldObjMapValues){
               query += s.getDescribe().getName() + ',';
            }
            // Trim last comma
            query = query.subString(0, query.length() - 1);
            query += ' FROM Country__c';
            List<Country__c> countries = Database.query(query);
            
            Map<String, Country__c> countryMap = new Map<String, Country__c>();
            for(Country__c country : countries){
                // name and full name are required so no need for a null check here
                countryMap.put(country.Name.toUpperCase().trim(), country);
                countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
                if(country.Three_Digit_ISO_Code__c != null){
                    countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
                }
                if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                    List<String> otherNames = country.Other_Names__c.split('\n');
                    for(String otherName : otherNames){
                        countryMap.put(otherName.toUpperCase().trim(), country);
                    }
                }
            }
            
            if(countryMap.get(lead.country.trim().toUpperCase()) != null){
                Country__c country = countryMap.get(lead.country.trim().toUpperCase());
                if(country.region__c.equalsIgnoreCase('IndRAA')){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Quote requests are not currently activated for IndRAA region.') );
                    error = true;
                }
            }
        }
        
    }    
    
    public PageReference refresh(){
        return null;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }    
    
    /**
    * Sends and creates a Quote request email and note respectively, then redirects to a new page according to the result.
    * @return a reference to the next page
    */
    public PageReference save(){
        
        PageReference page;
        Boolean success = false;
        submitted = true;
        System.debug('FileSize: ' + fileSize + ' Notes: ' + notes);
        
        /*if(notes == null || notes == ''){
            error = true;
            submitted = false;
            return null;
        }*/
        
        //Check for file size 
        if (fileSize != null){
            if (fileSize > maxAttachmentSize){
                System.debug('FileSize: ' + fileSize);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This file exceeds the maximum size limit of ' + roundedMaxAttachmentSize +' MB') );
                errorMessage = 'This file exceeds the maximum size limit of ' + roundedMaxAttachmentSize +' MB';
            }
        }
        
        if(ApexPages.hasMessages(ApexPages.severity.ERROR)) {
            System.debug('Error: ' + ApexPages.getMessages());
            error = true;
            return null;
        }
        
        if( sendQuoteRequestTemplateEmail() ) {
        
            success = createQuoteRequestNote();
            if (success && attachmentBlob != null && enableAttachment){
                success = createQuoteRequestAttachment();
            }
            
            if ( !isSF1 ){
                page = success ? new ApexPages.StandardController(lead).view() : ApexPages.currentPage();
                page.setRedirect(true);
            }
            else{
                return null;
            }
        }        
        
        return page;
    }
    
    /**
    * Redirects (back) to the Lead's default page.
    * @return a reference to the next page
    */
    public PageReference cancel(){
        return new ApexPages.StandardController(lead).view().setRedirect(true);
    }
    
    /**
    * Sets note creation error information on a page.
    * @param page a reference to the page whose error information is being set
    * @param errorMessage the error message to be set
    */
    @testVisible
    private void setNoteCreationErrorState(PageReference page, String errorMessage, boolean isNote) {
        // Removes technical info from error message
        String parsedErrorMessage = errorMessage.substringAfter(',');
        parsedErrorMessage = parsedErrorMessage == '' ? errorMessage : parsedErrorMessage;
        if (isnote){
            page.getParameters().put(ERROR_MESSAGE_PARAM, 'Automatic note creation failed. Please add note manually. Quote request email was sent successfully.');
        }
        else{
            page.getParameters().put(ERROR_MESSAGE_PARAM, 'Automatic attachment creation failed. Please add attachment manually. Quote request email was sent successfully.');
        }
        page.getParameters().put(ERROR_CAUSE_PARAM, parsedErrorMessage );
        page.getParameters().put(NOTES_PARAM, notes);
    }
    
    /**
    * Retrieves note creation error information from a page.
    * @param page a reference to the page whose error information is being retrieved
    */
    @testVisible
    private void getNoteCreationErrorState(PageReference page) {
        error = true;
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, page.getParameters().get(ERROR_MESSAGE_PARAM)) );
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, page.getParameters().get(ERROR_CAUSE_PARAM)) );
        errorMessage = page.getParameters().get(ERROR_CAUSE_PARAM);
        notes = page.getParameters().get(NOTES_PARAM);
    }
    
          /**
    * Creates a Note that documents a request for a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean createQuoteRequestNote(){
        Boolean success = false;
        try {
            insert new Note(ParentId = lead.Id, Title = 'Quote Request',  Body = notes);
            success = true;
            
        // Any exception will be handled in the same way
        } catch (Exception e) {
            setNoteCreationErrorState(ApexPages.currentPage(), e.getMessage(), true);
            System.debug(e.getTypeName() + ': Unable to create Quote Request Note: ' + e.getMessage());
        }
        
        return success;
    }
    
    /**
    * Creates a Note that documents a request for a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean createQuoteRequestAttachment(){
        Boolean success = false;
        try {
            Attachment attachment = new Attachment();
            attachment.Body = attachmentBlob;
            attachment.Name = attachmentName;
            attachment.ParentId = lead.Id;
            insert attachment;
            success = true;
            
        // Any exception will be handled in the same way
        } catch (Exception e) {
            setNoteCreationErrorState(ApexPages.currentPage(), e.getMessage(), false);
            System.debug(e.getTypeName() + ': Unable to create attachment: ' + e.getMessage());
        }
        
        return success;
    }       
    
    /**
    * Sends a template-based email to request a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean sendQuoteRequestTemplateEmail(){
    
        Messaging.SingleEmailMessage mail;
        Boolean success = false;
        
        try {
            mail = mergeQuoteRequestTemplateEmail(lead);
            
            //if running test, don't try to send the email
            if (!Test.isRunningTest()){
                List<Messaging.SendEmailResult> result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
            
            success = true;
        } catch (System.QueryException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: Template is missing'));
            errorMessage = 'Unable to locate EmailTemplate';
            System.debug(e.getTypeName() + ': Unable to locate EmailTemplate Quote_QuoteRequest_For_Lead: ' + e.getMessage());
        } catch (System.NullPointerException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: One or more merge fields required by the template are missing'));
            errorMessage = 'One of the merge fields required by email template is null';
            System.debug(e.getTypeName() + ': One of the merge fields required by email template' + ' Quote_QuoteRequest is null: ' + e.getMessage());
        } catch (CustomSettingException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: Default email routing address is missing'));
            errorMessage = 'Default email routing address is missing';
            System.debug(e.getMessage());
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email'));
            errorMessage = 'Unable to send Quote Request email';
            System.debug('Generic exception: ' + e.getTypeName() + ' ' + e.getMessage());
        }
        
        error = !success; 
        return success;
    }
    
    /**
    * Populates the merge fields on the quote request email template.
    * @param lead Lead object to use in the merge
    * @return the merged email message
    */
    @testVisible
    private Messaging.SingleEmailMessage mergeQuoteRequestTemplateEmail(Lead lead) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        EmailTemplate emailTemplate = [select subject, htmlValue from EmailTemplate where developername = 'Quote_QuoteRequest_For_Lead'];
            
        String countryString = lead.Country;
        String state = lead.state;
        
        String notesTmp = '';
        
        //Change of end of line character for <br/>, so TMD Jira ticket see the notes entered by user properly 
        if (notes != null){
            notesTmp = notes.replaceAll('<', '&lt;');
            notesTmp = notesTmp.replaceAll('>', '&gt;');
            notesTmp = notesTmp.replaceAll('\n', '<br/>');
        }
        
        // Template fields must be merged manually because a custom merge field is being included
        // Merge subject fields
        String subject = (String) emailTemplate.Subject;
        subject = subject.replace('{!Lead.Lead_Number__c}', lead.Lead_Number__c);
        subject = subject.replace('{!country}', countryString);
        subject = state != null ? subject.replace('{!state}', state) : subject.replace('{!state}, ', '');
        
        if (isUrgent){
            subject = 'Urgent: ' + subject; 
        }
        
        // Merge body fields
        String htmlBody = (String) emailTemplate.HtmlValue;
        htmlBody = htmlBody.replace('{!Lead.Lead_Number__c}', lead.Lead_Number__c);
        htmlBody = htmlBody.replace('{!Lead.Name}', lead.Name);
        htmlBody = htmlBody.replace('{!leadUrl}', URL.getSalesforceBaseUrl().toExternalForm() + '/' + lead.Id);
        
        // Avoiding possible NullPointerExceptions for blank fields
        String budget = 'No';
        
        budget = lead.Budget__c == null ? 'No' : ( lead.Budget__c == true ? 'Yes' : 'No' );
        String timeframe = lead.Timeframe__c == null ? '' : lead.Timeframe__c;
        String description = lead.Description == null ? '' : lead.Description;
        String company = lead.Company == null ? '' : lead.Company;
        String phone = lead.Phone == null ? '' : lead.Phone;
        String email = lead.Email == null ? '' : lead.Email;
        String fullName = lead.Owner.Name == null ? '' : lead.Owner.Name;
        
        htmlBody = htmlBody.replace('{!Lead.Budget__c}', budget);
        htmlBody = htmlBody.replace('{!Lead.OwnerFullName}', fullName);
        htmlBody = htmlBody.replace('{!Lead.Timeframe__c}', timeframe);
        htmlBody = htmlBody.replace('{!Lead.Description}', description);
        htmlBody = htmlBody.replace('{!notes}', notesTmp);
        htmlBody = htmlBody.replace('{!Lead.Company}', company);
        htmlBody = htmlBody.replace('{!Lead.Address}', lead.street + ', ' + lead.city
            + ', ' + lead.state + ' ' + lead.postalCode + ', ' + lead.country);
        htmlBody = htmlBody.replace('{!Lead.Phone}', phone);
        htmlBody = htmlBody.replace('{!Lead.Email}', email);
        htmlBody = htmlBody.replace('{!SendToRequesterOnlyImage}', sendToRequesterOnly ? '<br/><img src="' + quoteSettings.Send_to_Requester_Only_Url__c + '" /><br/>' : '');
        
        mail.setHtmlBody(htmlBody);
                
        // Set sender/recipient addresses
        mail.setSenderDisplayName(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        
        List<Country__c> countries = [Select Id, Full_Name__c, Name, Three_Digit_ISO_Code__c, Other_Names__c, Override_Lead_Language__c, Primary_Language__c, Quote_Request_Email__c from Country__c];
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        Country__c country;
        NI_Quote_Request_Email_Routing__c emailRoutingSettings;
        String toEmail;
        User currentUser = [Select id, name, source__c from User where id = :UserInfo.getUserId()];
        
        try{
            country = countryMap.get(countryString.toUpperCase().trim());
        
            if ( country != null && country.Quote_Request_Email__c != null ){
                toEmail = country.Quote_Request_Email__c;
            }
            else{
                
                subject = subject + ' ' + undefinedRequestorMessage;
                
                if (currentUser.source__c != null){
                    emailRoutingSettings = NI_Quote_Request_Email_Routing__c.getValues(currentUser.source__c.toUpperCase().trim());
                }
                
                if ( emailRoutingSettings != null ){
                    toEmail = emailRoutingSettings.email__c;
                }
                else{
                    emailRoutingSettings = NI_Quote_Request_Email_Routing__c.getValues('DEFAULT');
                    toEmail =  emailRoutingSettings != null ? emailRoutingSettings.email__c : null;
                }
            }
            
        }catch(Exception e){
            toEmail = null;
        }       
        
        if(toEmail == null ){
            throw new CustomSettingException('Exception: Country:' + country != null && country.Name != null ? country.Name : 'null' + ' has no email value for country \'Default\'.');
        }
        
        mail.setSubject(subject);
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setCcAddresses(new String[]{toEmail});
        mail.saveAsActivity = false;
        
        //add attachment
        if(attachmentBlob != null ){
            
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setFileName(attachmentName);
            attachment.setBody(attachmentBlob);
            attachment.setContentType(contentType);
            attachments.add(attachment);
            
            mail.setFileAttachments(attachments);
            
        }        
        
        mail.setCharset('UTF-8');
        return mail;
    }
    
}