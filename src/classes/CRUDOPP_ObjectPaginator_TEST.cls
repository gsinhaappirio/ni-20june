/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose:  This is a test to provide test coverage for  CRUDOPP_ObjectPaginator Class 
 ===========================================================================================================
 History 
----------------------- 
VERSION     AUTHOR              DATE        DETAIL 
   1.0 -    Sarbpreet,Reetika   18/4/2014   Created Test class
 =========================================================================================================== */
 
@IsTest
private class CRUDOPP_ObjectPaginator_TEST {

    private static void assertDefaultPageSize(CRUDOPP_ObjectPaginator paginator){
        System.assertEquals(CRUDOPP_ObjectPaginator.DEFAULT_PAGE_SIZE, paginator.pageSize);
    }
    
    private static void assertDefaultPageSizeOptions(CRUDOPP_ObjectPaginator paginator){
        System.assertEquals(CRUDOPP_ObjectPaginator.DEFAULT_PAGE_SIZE_OPTIONS, paginator.pageSizeIntegerOptions);
        final List<SelectOption> SEL_OPTIONS = new List<SelectOption>(); 
        for(Integer i : CRUDOPP_ObjectPaginator.DEFAULT_PAGE_SIZE_OPTIONS){
            SEL_OPTIONS.add(new SelectOption(''+i,''+i));
        }
        //ArrayUtils.assertArraysAreEqual(SEL_OPTIONS, paginator.pageSizeSelectOptions);
        System.assertNotEquals(null, paginator.getPageSizeOptions());
    }
    
    private static void assertDefaultSkipSize(CRUDOPP_ObjectPaginator paginator){
        System.assertEquals(CRUDOPP_ObjectPaginator.DEFAULT_SKIP_SIZE, paginator.skipSize);
    }
    
    private static void assertDefaultListener(CRUDOPP_ObjectPaginator paginator){
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(0, paginator.listeners.size());
    }
   
    private static testmethod void testConstructor01(){
        //global CRUDOPP_ObjectPaginator(){
        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator();        
        assertDefaultPageSize(paginator);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
        
        Integer pageCount = paginator.pageCount;
        Integer recordCount = paginator.recordCount;
        Boolean hasNext = paginator.hasNext;
        Boolean hasPrevious = paginator.hasPrevious;
        Integer pageStartPosition = paginator.pageStartPosition;
        Integer pageEndPosition = paginator.pageEndPosition;
        List<Integer> previousSkipPageNumbers = paginator.previousSkipPageNumbers;
        List<Integer> nextSkipPageNumbers = paginator.nextSkipPageNumbers;
        Integer pageNumberDisplayFriendly = paginator.pageNumberDisplayFriendly;
        Integer pageStartPositionDisplayFriendly = paginator.pageStartPositionDisplayFriendly;
        Integer pageEndPositionDisplayFriendly = paginator.pageEndPositionDisplayFriendly;
        Integer pageSize = paginator.getPageSize();        
        
        //paginator.skipToPage(1);
        //paginator.next();
        //paginator.previous();
        paginator.first();
        paginator.last();
    }
    
    private static testmethod void testConstructor02(){
        //global CRUDOPP_ObjectPaginator(CRUDOPP_ObjectPaginatorListener listener ){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor03(){
        //global CRUDOPP_ObjectPaginator(List<Integer> pageSizeIntegerOptions ){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{-1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor04(){
        //global CRUDOPP_ObjectPaginator(List<Integer> pageSizeIntegerOptions,CRUDOPP_ObjectPaginatorListener listener ){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor05(){
        //global CRUDOPP_ObjectPaginator(List<Integer> pageSizeIntegerOptions,Integer skipSize ){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,10);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor06(){
        //global CRUDOPP_ObjectPaginator(List<Integer> pageSizeIntegerOptions,Integer skipSize,CRUDOPP_ObjectPaginatorListener listener ){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,10,EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor07(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize ){

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor08(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,CRUDOPP_ObjectPaginatorListener listener ){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor09(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,Integer skipSize ){
        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,10);
        
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor10(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,Integer skipSize,CRUDOPP_ObjectPaginatorListener listener ){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor11(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor12(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions,CRUDOPP_ObjectPaginatorListener listener){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    
    private static testmethod void testConstructor13(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions,Integer skipSize){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,10);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
    
    private static testmethod void testConstructor14(){
        //global CRUDOPP_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions, Integer skipSize, CRUDOPP_ObjectPaginatorListener listener){
        final CRUDOPP_ObjectPaginatorListener EXAMPLE_LISTENER = new CRUDOPP_ObjectPaginatorListenerTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final CRUDOPP_ObjectPaginator paginator = new CRUDOPP_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
  
    private static List<Object> createTestObject(Integer count){
        List<Object> records = new List<Object>();
        for(Integer i = 0; i < count; i++){
            records.add(i,i); 
        }
        return records;
    }
}