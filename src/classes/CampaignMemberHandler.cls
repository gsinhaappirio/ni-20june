public class CampaignMemberHandler {

    @future
    public static void updateLeads(Set<Id> campaignMemberIds){
    
        List<CampaignMember> campaignMembers = [Select Id, LeadId from CampaignMember where Id in :campaignMemberIds];
        
        List<Id> leadIds = new List<Id>();
        for(CampaignMember campaignMember : campaignMembers){
            leadIds.add(campaignMember.LeadId);
        }
        
        List<Lead> leads = [Select Id from Lead where id in :leadIds and isConverted = false and Ready_to_be_Assigned__c = false];
        for(Lead lead : leads){
            lead.Ready_to_be_Assigned__c = true;
        }
        
        Database.update(leads, false);
    }
    
}