//Generated by wsdl2apex

public class NI_Sales_QuoteWS_Quote {
    public class quoteLines {
        public NI_Sales_QuoteWS_Quote.QuoteLine[] QuoteLine;
        private String[] QuoteLine_type_info = new String[]{'QuoteLine','http://www.ni.com/schemas/step-integration/1/quote',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.ni.com/schemas/step-integration/1/quote','true','false'};
        private String[] field_order_type_info = new String[]{'QuoteLine'};
    }
    public class QuoteLine {
        public String bookingClass;
        public String partNumber;
        public String partDescription;
        public Decimal quantity;
        public Decimal lineListPrice;
        public String currencyCode;
        public Decimal lineAdjustedAmount;
        public Decimal lineAdjustedPercent;
        public Decimal lineQuotePrice;
        public Decimal lineTotalPrice;
        public Decimal lineNumber;
        private String[] bookingClass_type_info = new String[]{'bookingClass','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] partNumber_type_info = new String[]{'partNumber','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] partDescription_type_info = new String[]{'partDescription','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] quantity_type_info = new String[]{'quantity','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineListPrice_type_info = new String[]{'lineListPrice','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] currencyCode_type_info = new String[]{'currencyCode','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineAdjustedAmount_type_info = new String[]{'lineAdjustedAmount','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineAdjustedPercent_type_info = new String[]{'lineAdjustedPercent','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineQuotePrice_type_info = new String[]{'lineQuotePrice','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineTotalPrice_type_info = new String[]{'lineTotalPrice','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] lineNumber_type_info = new String[]{'lineNumber','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.ni.com/schemas/step-integration/1/quote','true','false'};
        private String[] field_order_type_info = new String[]{'bookingClass','partNumber','partDescription','quantity','lineListPrice','currencyCode','lineAdjustedAmount','lineAdjustedPercent','lineQuotePrice','lineTotalPrice','lineNumber'};
    }
    public class QuoteHeader {
        public Decimal headerId;
        public DateTime creationDate;
        public Decimal quoteNumber;
        public Decimal quoteVersion;
        public Decimal orderId;
        private String[] headerId_type_info = new String[]{'headerId','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] creationDate_type_info = new String[]{'creationDate','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] quoteNumber_type_info = new String[]{'quoteNumber','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] quoteVersion_type_info = new String[]{'quoteVersion','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] orderId_type_info = new String[]{'orderId','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.ni.com/schemas/step-integration/1/quote','true','false'};
        private String[] field_order_type_info = new String[]{'headerId','creationDate','quoteNumber','quoteVersion','orderId'};
    }
    public class Quote {
        public NI_Sales_QuoteWS_Quote.QuoteHeader QuoteHeader;
        public NI_Sales_QuoteWS_Quote.quoteLines quoteLines;
        private String[] QuoteHeader_type_info = new String[]{'QuoteHeader','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] quoteLines_type_info = new String[]{'quoteLines','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.ni.com/schemas/step-integration/1/quote','true','false'};
        private String[] field_order_type_info = new String[]{'QuoteHeader','quoteLines'};
    }
    public class GetQuoteResponse_element {
        public NI_Sales_QuoteWS_Quote.Quote QuoteInformation;
        public String responseStatus;
        public String responseMessage;
        private String[] QuoteInformation_type_info = new String[]{'QuoteInformation','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] responseStatus_type_info = new String[]{'responseStatus','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] responseMessage_type_info = new String[]{'responseMessage','http://www.ni.com/schemas/step-integration/1/quote',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.ni.com/schemas/step-integration/1/quote','true','false'};
        private String[] field_order_type_info = new String[]{'QuoteInformation','responseStatus','responseMessage'};
    }
}