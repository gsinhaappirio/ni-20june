/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            NI_NoteTriggerController
   Description:     This class invokes methods of NI_NoteHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   30th September           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class NI_NoteTriggerController{
    private Boolean m_isExecuting = false;
    private integer batchSize = 0;
    
    /*Constructor*/
    public NI_NoteTriggerController (Boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        batchSize = size;
    }

    /**********************************************************************************************************************
    * Author:
    * Date: 
    * Param: List<Note> (Trigger.New),Map<Id,Note> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after insert methods of NI_NoteHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert(List<Note> newNoteList){
        
          NI_NoteHandler.OppLastActiveDate(newNoteList);    
    }
   
   
}