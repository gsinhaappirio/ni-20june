/**************************************************************************************************
*                                                                                                 *
*                     RT LIBRARY – Technical Goods                      						  *
*  <<< BC.K.000L – Adding/Editing/Remove Multiple Products on Opportunity via single page>>> 	  *
*                                               					 							  *
*                                                                                                 *
*-------------------------------------------------------------------------------------------------*
*                                                                                                 *
*                            Version 1.00                                                 		  *
*                                                                                                 *
* Creation Date: 15.Feb.2014                    Reetika Budhiraja           					  *
* Last Modified: 18.Apr.2014                  Sarbpreet Kaur         							  *
*                                                                                                 *
*                             © 2014 - Accenture - All Rights Reserved  						  *
***************************************************************************************************/
global interface CRUDOPP_ObjectPaginatorListener {
	void handlePageChange(List<Object> newPage);
}
/***************************************************************************************************
*                     RT LIBRARY – Technical Goods                      						   *
*                                                                                                  *
*                             © 2014 - Accenture - All Rights Reserved  						   *
****************************************************************************************************/