public class UserDeactivator implements Schedulable{
    
    public void execute(SchedulableContext SC){

        List<HCM_Record__c> empList = [SELECT Employee_Number__c from HCM_Record__c];
        List<User> usersToDeactivate = new List<User>();
        Map<String, HCM_Record__c> hcmMap = new Map<String, HCM_Record__c>();

        if(emplist.size() != 0){
            // Create map of HCM records with the EmployeeNumber as key
            for(HCM_Record__c hcm : empList)
                hcmMap.put(hcm.Employee_Number__c, hcm);

            //Select users who are NI employees - excluding test users, generic users, SFDC employees
            List<User> users = [SELECT EmployeeNumber, IsActive, Notes__c FROM User WHERE IsActive = TRUE AND EmployeeNumber != null];

            for(User user : users){
                //if a user is not in the HCM record map, then it needs to be deactivated
                if(!hcmMap.containsKey(user.EmployeeNumber)){
                    user.IsActive = FALSE;
                    if(String.isBlank(user.Notes__c))
                        user.Notes__c = 'Automatically deactivated on: ' + Datetime.now() + ' GMT';
                    else
                        user.Notes__c = user.Notes__c.normalizeSpace() + ' | Automatically deactivated on: ' + Datetime.now() + ' GMT';
                    
                    usersToDeactivate.add(user);
                }
            }        
            Database.update(usersToDeactivate,false);
        }
    }
}