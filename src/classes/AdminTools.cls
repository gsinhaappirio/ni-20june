public class AdminTools {
    
    public String newPassword {get;set;}
    public List<User> users {get;set;}
    public List<TestUser> testUsers {get;set;}
    public Integer active {get;set;}
    public String allNewPassword {get;set;}
    public String allNewStatus {get;set;}
    
    public List<SelectOption> statusOptions {
        get {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--'));
            options.add(new SelectOption('Active','Active'));
            options.add(new SelectOption('Inactive','Inactive'));
            return options;
        }
        set;
    }
    
    public AdminTools() {
        // get all test users
        users = [SELECT Id, IsActive, Name FROM User WHERE isTestUser__c =: true];
        testUsers = new List<TestUser>();
        for(User user : users){
            TestUser testUser = new TestUser(user);
            testUser.user = user;
            testUsers.add(testUser);
        }
    }
    
    public PageReference updateLicenseCosts(){
        
        Map<Id, User> users;
        if(Test.isRunningTest()){
            // if running test, limit it to so it doesn't take forever
            users = new Map<Id, User>([Select Id from User LIMIT 2]);
        }else{
            users = new Map<Id, User>([Select Id from User]);
        }
        
        UserHandler.updateLicenseCost(users.keyset());
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updating all users\' license cost. Check the Apex Jobs log to view progress...');
        ApexPages.addMessage(myMsg);
        return null;
    }
    
    public PageReference updateAllUsers() {
    
        List<User> toUpdate = new List<User>();
        if(allNewStatus != null && !allNewStatus.equals('')){
            Boolean newStatus = allNewStatus.toUpperCase().trim().equals('ACTIVE') ? true : false;
            for(TestUser user : testUsers){
                if(user.user.isActive != newStatus){
                    user.user.isActive = newStatus;
                    toUpdate.add(user.user);
                }
            }
        }
        
        if(toUpdate != null && !toUpdate.isEmpty() && !Test.isRunningTest()){
            update toUpdate;
        }
        
        if(allNewPassword != null && !allNewPassword.equals('') && isValidPassword(allNewPassword)){
            for(TestUser user : testUsers){
                if(user.user.isActive){
                    System.setPassword(user.user.Id, allNewPassword);
                }
            }
        }
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success');
        ApexPages.addMessage(myMsg);
        
        //PageReference pageRef = ApexPages.currentPage();
        //pageRef.setRedirect(true);
        //return pageRef;
        return null;
        
    }
    
    public PageReference updateEachUser(){
        
        List<User> toUpdate = new List<User>();
        for(TestUser testUser : testUsers){
        
            if(testUser.newPassword != null && !testUser.newPassword.equals('')){
                System.setPassword(testUser.user.Id, newPassword);
            }
        
            if(testUser.newStatus != testUser.User.isActive){
                testUser.user.isActive = testUser.newStatus;
                toUpdate.add(testUser.user);
            }
        }
        
        if(toUpdate != null && !toUpdate.isEmpty()){
            if(!Test.isRunningTest()){
                update toUpdate;
            }
            PageReference pageRef = ApexPages.currentPage();
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return null;
    }
    
    public void activateAll() {
        // activate all testUsers
        for (User u : users) {
            u.IsActive = true;
        }
        if(!Test.isRunningTest()){
            // update list of all testUsers (avoids DML error)
            update users;
        }
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'All test users have been activated.'));
    }
    
    public void deactivateAll() {
        for (User u : users) {
            u.IsActive = false;
        }
        update users;
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'All test users have been deactivated.'));
    }
    
    public Boolean isValidPassword(String password) {
        // password validation
        boolean isOk = true;
        
        // password must be 8+ characters, contain uppercase, lowercase, number
        if (password == '' || password == null) {
            isOk = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter a password.'));
        } else if (password.length() < 8) {
            isOk = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Password must be at least 8 characters.'));
        } else if (!password.containsAny('abcdefghijklmnopqrstuvwxyz')) {
            isOk = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Password must contain at least one lowercase letter.'));
        } else if (!password.containsAny('ABCDEFGHIJKLMNOPQRSTUVWXYZ')) {
            isOk = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Password must contain at least one uppercase letter.'));
        } else if (!password.containsAny('0123456789')) {
            isOk = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Password must contain at least one number.'));
        }
        
        return isOk;
    }
    
    public void resetPassword() {
        // count the number of test users whose passwords were not reset because they are inactive
        active = 0; 
        
        if(newPassword == null || newPassword.equals('')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New password cannot be blank'));
        }else{
            if (isValidPassword(newPassword)) {
                // user must be active to update password
                for (User u : users) { 
                    if (u.IsActive) {
                        try {
                            System.setPassword(u.Id, newPassword); 
                            active += 1;
                        } catch (Exception e) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        }
                    }
                }
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Passwords have been reset.'));
            }
        }
    }
    
    public class TestUser {
        public User user {get;set;}
        public String newPassword {get;set;}
        public Boolean newStatus {get;set;}
        
        public TestUser(User user){
            this.user = user;
            this.newStatus = user.isActive;
        }
    }
    
}