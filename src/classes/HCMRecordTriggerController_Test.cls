@isTest(SeeAllData=true)
public class HCMRecordTriggerController_Test {
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest(){
        Test.startTest();
        
        RecordType rt = [Select Id from RecordType where DeveloperName = 'Create_New_User'];
        
        User_Request__c request = new User_Request__c();
        request.RecordTypeId = rt.Id;
        request.email__c = 'test1@hcm.ni.com';
        request.Justification__c = 'testing';
        request.Profile__c = 'NI Sales Manager User';
        request.Queues__c = 'US East Geo';
        request.Region__c = 'AMER';
        request.Role__c = 'AMER-ACCT-REP';
        request.Status__c = 'Processing';
        insert request;
        
        User manager = [Select Id, EmployeeNumber, Cost_Center__c, Department, Name from User where isActive = true AND EmployeeNumber != null LIMIT 1];
        User user = TestUtils.getUserForProfile('hcm','NI Sales Manager User');
        insert user;
        
        HCM_Record__c hcmRecord = new HCM_Record__c();
        hcmRecord.Cost_Center__c = manager.Cost_Center__c;
        hcmRecord.Department__c = manager.Department;
        hcmRecord.Email__c = user.Email;
        hcmRecord.Email_Encoding_Key__c = 'UTF-8';
        hcmRecord.Employee_Number__c = '12345';
        hcmRecord.First_Name__c = 'HCMFirst';
        hcmRecord.Last_Name__c = 'HCMLast';
        hcmRecord.HCM_Manager_Name__c = manager.Name;
        hcmRecord.HCM_Manager_Person_Number__c = Decimal.valueOf(manager.EmployeeNumber);
        hcmRecord.HCM_Region__c = 'HCMRegion';
        //insert hcmRecord;
        
        List<HCM_Record__c> records = new List<HCM_Record__c>();
        records.add(hcmRecord);
        
        HCMRecordTriggerController.onAfterInsert(records);
        
        HCMRecordTriggerController.onAfterUpdate(records);
        
        Test.stopTest();
    }
    
}