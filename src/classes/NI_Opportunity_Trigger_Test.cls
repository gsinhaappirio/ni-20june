/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for opportunity_Trigger class.
  */

@isTest(seeAllData=true)
private class NI_Opportunity_Trigger_Test{

    @isTest
    private static void insertTest(){
        
        Test.startTest();
        Account account = new Account();
        account.name = 'Test Account';
        account.ShippingCountry = 'US';
        insert account;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp 123';
        opp.StageName = 'Qualify';
        opp.CloseDate = System.today();
        opp.accountId  = account.Id;
        insert opp;
        
        delete opp;
        Test.stopTest();
        
    }
    
}