@isTest
public class NI_OD_QUOTE_HDR_Extension_Test{

    @isTest(seeAllData=true)
    static void validateSave(){
          
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        NI_OD_QUOTE_HDR_ALL_V__x quote = new NI_OD_QUOTE_HDR_ALL_V__x();
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('Id',quote.id);
    
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        
        NI_OD_QUOTE_HDR_Extension quoteExt = new NI_OD_QUOTE_HDR_Extension(new ApexPages.StandardController(quote));
        quoteExt.dummyQuote.Opportunity__c = opp.id;
        
        PageReference detailPage;
        
        detailPage = quoteExt.save2();
        Test.stopTest();
        
        //System.assertNotEquals(null, detailPage);
        
       
    }
    
    @isTest(seeAllData=true)
    static void validateSave2(){
          
        Lead ld = new Lead();
        ld.FirstName = 'Lead';
        ld.LastName = 'Test';
        ld.company = 'NI';
        ld.LeadSource = 'Marketing';
        ld.Country = 'US';
        insert ld;
        
        NI_OD_QUOTE_HDR_ALL_V__x quote = new NI_OD_QUOTE_HDR_ALL_V__x();
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('Id',quote.id);
    
        Test.setMock(WebServiceMock.class, new NI_Sales_Lead_Quote_WS_MockImpl());
        
        NI_OD_QUOTE_HDR_Extension quoteExt = new NI_OD_QUOTE_HDR_Extension(new ApexPages.StandardController(quote));
        quoteExt.dummyQuote.Lead__c = ld.id;
        
        PageReference detailPage;
        
        detailPage = quoteExt.save2();
        Test.stopTest();
        
       // System.assertNotEquals(null, detailPage);
        
       
    }    
   
    @isTest(seeAllData=true)
    static void validateHasOpportunity(){
          
         System.debug('Begin validateHasOpportunity');
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        System.debug('after inserting opportunity ' + opp.id);
        
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        quote.Opportunity__c = opp.id;
        update quote;
        
        System.debug('after inserting quote ' + quote.opportunity__r.id);
      
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id',quote.id);
    
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
       
        System.debug('End validateHasOpportunity');
        System.assertEquals(true, quoteController.alreadyHasOpportunityOrLead);
        
        
       
    }      
    
   @isTest
    static void validateSaveNullPageWithNullOpp(){
          
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Opportunity__c = null;
        
        PageReference detailPage;
        
        detailPage = quoteController.save(); 
        
        System.assertEquals(null, detailPage);
        
       
    }       
    
    @isTest
    static void validateSaveNullPage(){
          
        NI_Quote__c quote = new NI_Quote__c();
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        //insert opp;
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Opportunity__c = opp.id;
        
        PageReference detailPage;
        
        detailPage = quoteController.save(); 
        
        //System.assertEquals(null, detailPage);
        
       
    }     
    
    @isTest
    static void validateCancel(){
          
        NI_OD_QUOTE_HDR_ALL_V__x quote = new NI_OD_QUOTE_HDR_ALL_V__x();
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_OD_QUOTE_HDR_Extension quoteExt = new NI_OD_QUOTE_HDR_Extension(new ApexPages.StandardController(quote));
        
        PageReference detailPage;
        
        detailPage = quoteExt.cancel(); 
        
        //System.assertNotEquals(null, detailPage);
        
    }      

}