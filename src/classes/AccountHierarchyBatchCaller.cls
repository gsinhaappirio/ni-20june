global class AccountHierarchyBatchCaller implements BatchUtil.BatchUtilable {
    
    public boolean HandleRecord(sObject s, BatchUtil callee) {
        String log = '';
        
        if (s.getSObjectType().getDescribe().getName().toUpperCase() == 'ACCOUNT'){
            
            callee.Log('Adjusting hierarchy for ' + s.get('Name') + '-' + s.get('Id') + '\n');
            Account account = (Account) s;
            
            Boolean addToUpdate = false;
            
            String globalUltimateDuns = account.DandbCompany.GlobalUltimateDunsNumber;
            Map<String, Account> accountMap = new Map<String, Account>();
            
            // query everything that rolls up to the same global ultimate duns number and has a DUNS itself
            List<Account> accounts = [SELECT Id, Name, DunsNumber, DandbCompany.ParentOrHqDunsNumber, DandbCompany.GlobalUltimateDunsNumber from Account where DunsNumber != null AND DandbCompany.GlobalUltimateDunsNumber = :globalUltimateDuns];
            for(Account acc : accounts){
                accountMap.put(acc.DunsNumber, acc);
            }
            
            if(account.DandbCompany != null && accountMap.containsKey(account.DandbCompany.ParentOrHqDunsNumber)){
                Account parent = accountMap.get(account.DandbCompany.ParentOrHqDunsNumber);
                System.debug('***** ' + account.DandbCompany.ParentOrHqDunsNumber + ' == ' + parent.DunsNumber);
                if(parent != null){
                    s.put('ParentId', parent.Id);
                    addToUpdate = true;
                }
            }
            
            String globalParentDuns = account.DandbCompany.GlobalUltimateDunsNumber;
            Account globalUltimateParent = accountMap.get(globalParentDuns);
            if(globalUltimateParent != null){
                s.put('GlobalParent__c', globalUltimateParent.Id);
                addToUpdate = true;
            }
            
            return addToUpdate;
            
        }else{
            callee.Log('Not an account, do nothing \n');
            return false;
        }
    }
    
}