@isTest(SeeAllData=true)
public class NewOpportunityReview_Controller_Test {

    @isTest
    public static void oppTest(){
        Test.startTest();
        Account account = new Account();
        account.name = 'Fake Account';
        account.ShippingStreet = '123 Fake St';
        account.ShippingCity = 'Austin';
        account.ShippingCountry = 'US';
        insert account;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Fake Opp';
        opp.AccountId = account.id;
        opp.StageName = 'Qualified';
        opp.CloseDate = System.today()+10;
        insert opp;
        
        PageReference pageRef = new PageReference('/apex/NewOpportunityReview?fromId=' + opp.Id);
        Test.setCurrentPage(pageRef);
        NewOpportunityReview_Controller extension = new NewOpportunityReview_Controller();
        extension.forwardToTaskCreation();
        
        Test.stopTest();
    }
    
}