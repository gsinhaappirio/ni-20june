@isTest
public with sharing class NI_Sales_Sales_ServiceRequestDAO_Test {
  @isTest
  public static void mapNoteToDTOTest(){
    Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element note = new Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element();
    NI_Sales_Sales_ServiceRequestDAO dao = new NI_Sales_Sales_ServiceRequestDAO();
    NI_Sales_Sales_ServiceRequestNoteDTO noteDTO;
    
    String createdBy = 'creatorName';
    DateTime creationDate = System.today();
    String type = 'typeName';
    String comments = 'typeName';
    
    note.NoteCreatedBy = createdBy;
    note.NoteDate =  (Integer) (creationDate.getTime() / 1000);
    note.noteType = type;
    note.noteComments = comments;
    
    noteDTO = dao.mapNoteToDTO(note);
    
    system.assertEquals(noteDTO.createdBy, createdBy);
    system.assertEquals(noteDTO.creationDate, creationDate);
    system.assertEquals(noteDTO.type, type);
    system.assertEquals(noteDTO.comments, comments);
  }
  
  @isTest(seeAllData=true)
  public static void getServiceRequestNotesTest(){
    Test.setMock(WebServiceMock.class, new Oracle_ServiceRequestNoteMock());
    NI_Sales_Sales_ServiceRequestDAO dao = new NI_Sales_Sales_ServiceRequestDAO();
    
    List<NI_Sales_Sales_ServiceRequestNoteDTO> response = dao.getServiceRequestNotes(1);
    
    System.assertEquals(response.get(0).createdBy, 'creatorName1');
    System.assertEquals(response.get(0).creationDate, DateTime.newInstanceGMT(2014,01,01));
    System.assertEquals(response.get(0).type, 'typeName1');
    System.assertEquals(response.get(0).comments, 'comments1');
    
    System.assertEquals(response.get(1).createdBy, 'creatorName2');
    System.assertEquals(response.get(1).creationDate, DateTime.newInstanceGMT(2014,01,01));
    System.assertEquals(response.get(1).type, 'typeName2');
    System.assertEquals(response.get(1).comments, 'comments2');
  }
  
  @isTest(seeAllData=true)
  public static void getServiceRequestNotesGenericExceptionTest(){
    Test.setMock(WebServiceMock.class, new Oracle_ServiceRequestNoteMock(true));
    NI_Sales_Sales_ServiceRequestDAO dao = new NI_Sales_Sales_ServiceRequestDAO();
    
    List<NI_Sales_Sales_ServiceRequestNoteDTO> response = dao.getServiceRequestNotes( 1);
  }
}