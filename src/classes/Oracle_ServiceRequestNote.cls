//Generated by wsdl2apex

public class Oracle_ServiceRequestNote {
    public class SRNotes360SOAP {
        public String osbServerKey = 'ServiceRequest360WS';
        public String endpoint_x;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.ni.com/sfdc/ei/360SRNotesResponse', 'Oracle_ServiceRequestNoteResponse', 'http://www.ni.com/sfdc/ei/360ViewRequest', 'Oracle_ServiceRequestHeader', 'http://www.ni.com/sfdc/ei/SRNotes360/', 'Oracle_ServiceRequestNote'};
        public Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element[] GetSRNotes(Long HeaderId) {
            NI_Server_URL_List__c osbServer = NI_Server_URL_List__c.getValues(osbServerKey);
            endpoint_x = osbServer.Server_URL__c;
            
            Oracle_ServiceRequestHeader.Request360_element request_x = new Oracle_ServiceRequestHeader.Request360_element();
            request_x.HeaderId = HeaderId;
            Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType response_x;
            Map<String, Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType> response_map_x = new Map<String, Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.ni.com/sfdc/ei/SRNotes360/GetSRNotes',
              'http://www.ni.com/sfdc/ei/360ViewRequest',
              'Request360',
              'http://www.ni.com/sfdc/ei/360SRNotesResponse',
              'ServiceRequestNotes',
              'Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.ServiceRequestNote;
        }
    }
}