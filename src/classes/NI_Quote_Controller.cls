public class NI_Quote_Controller {
    
    public final NI_Quote__c quote { get; set; }
    public Boolean alreadyHasOpportunityOrLead {get;set;}
    private ApexPages.StandardController sc;
    
    public Boolean error { get; set; } // flag for note creation errors
    public String errorMessage { get; set; } // flag for note creation errors
    public Boolean submitted { get; set; }
    public Boolean allowSubmitIfError{ get; set; } //flag to allow the user to submit again when there's an error
    public Boolean cleanFields {get; set;} //flag to clean UI fields
    
    //variables for SF1
    public String oppId { get; set; }
    public String oppName { get; set; }
    public String leadId { get; set; }
    public String leadName { get; set; }
     
    public NI_Quote_Controller(ApexPages.StandardController stdController){
        
        error = false;
        submitted = false;
        allowSubmitIfError = false;
        cleanFields = false;
        
        quote  = [SELECT Id, Name, oracle_header_id__c, org_id__c, lead__c, opportunity__c, opportunity__r.id, lead__r.id FROM NI_Quote__c 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
                   
        System.debug('NI_QUOTE_CONTROLLER.  Id: ' + quote.id + ' opp id: ' + this.quote.opportunity__r.id);
         
        if (this.quote.opportunity__r.id != null || this.quote.lead__r.id != null){
            addError(true, false, 'There is a lead or opportunity already linked to this quote. Any changes must be made on the quote form in Oracle.');
            submitted = true;
            alreadyHasOpportunityOrLead = true; 
            System.debug('This quote already has an opportunity');
        }
        else{
            alreadyHasOpportunityOrLead = false;
        }                   
    }

    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('isSF1')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
               ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
               (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }   
    
    public PageReference save() {
        
        PageReference detailPage;
        NI_Sales_Sales_Quote_DAO quotesDAO = new NI_Sales_Sales_Quote_DAO();
        submitted = true;
        error=false;
        allowSubmitIfError=false;
        cleanFields=false;
        
        if (isSF1){
            leadId = Apexpages.currentPage().getParameters().get('leadId');
            leadName = Apexpages.currentPage().getParameters().get('leadName');
            oppId = Apexpages.currentPage().getParameters().get('oppId');
            oppName = Apexpages.currentPage().getParameters().get('oppName');   
        }
        
        //System.debug('Parameters: ' + Apexpages.currentPage().getParameters());
        
        try{
            
            //check user didn't select both options
            if ( (quote.lead__c != null && quote.opportunity__c != null) || 
                 (isSF1 && leadName != '' && oppName != '') ){
                
                detailPage = null;
                cleanFields=true;
                addError(true, true, 'You must select just one option: Lead or Opportunity');
                
            }
            //check opportunity.  
            else if (quote.opportunity__c != null || (isSF1 && oppName != '') ){
                //if running on SF1, we need to query the opportunity manually
                if(isSF1){
                    if(oppId != ''){
                        quote.opportunity__c = oppId;
                    }
                    //opp name was entered directly, so we need to query the opp id
                    else{
                        Opportunity[] opps = [Select id from Opportunity where name = :oppName];
                        
                        if(opps.size() > 0){
                            quote.opportunity__c = opps[0].id;
                        }
                        else{
                            addError(true, true, 'There is no opportunity with name: ' + oppName);
                            detailPage = null;
                        }
                        
                    }
                }
                
                if (!error){
                    NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = quotesDAO.linkOppToQuote(String.valueOf(this.quote.opportunity__c), double.valueOf(this.quote.oracle_Header_Id__c), double.valueOf(this.quote.org_Id__c));
                
                    if (response != null){
                        if (response.status == 'S'){
                            update quote;
                            detailPage = (new ApexPages.StandardController(quote)).view();
                        }
                        else{
                            addError(true, false, 'There was an error adding this opportunity. Error: ' + response.message);
                            detailPage = null;
                        }
                        
                    }
                    else{
                        addError(true, false, 'There was an error adding this opportunity.');
                        detailPage = null;
                    }
                }
            }
            //check lead
            else if(quote.lead__c != null || (isSF1 && leadName != '') ){
                //if running on SF1, we need to query the opportunity manually
                if(isSF1){
                    if(leadId != ''){
                        quote.lead__c = leadId;
                    }
                    //opp name was entered directly, so we need to query the opp id
                    else{
                        Lead[] leads = [Select id from Lead where name = :leadName];
                        
                        if(leads.size() > 0){
                            quote.lead__c = leads[0].id;
                        }
                        else{
                            addError(true, true, 'There is no lead with name: ' + leadName);
                            detailPage = null;
                        }
                        
                    }
                }               
                
                if (!error){
                    
                    NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element response = 
                           quotesDAO.linkLeadToQuote(String.valueOf(this.quote.lead__c), double.valueOf(this.quote.oracle_Header_Id__c), double.valueOf(this.quote.org_Id__c));
                    
                    if (response != null){
                        if (response.status == 'S'){
                            update quote;
                            detailPage = (new ApexPages.StandardController(quote)).view();
                        }
                        else{
                            addError(true, false, 'There was an error adding this lead. Error: ' + response.message);
                            detailPage = null;
                        }
                        
                    }
                    else{
                        addError(true, false, 'There was an error adding this lead.');
                        detailPage = null;
                    }       
                }       
            
            }
            else{
                addError(true, true, 'You must choose one lead or opportunity.');
                detailPage = null;
            }
        }
        catch (Exception e){
            detailPage = null;
            addError(true, false, 'There was an error adding this lead/opportunity. ' + e.getMessage());
        }
        
        if ( !isSF1 ){
            return detailPage;
        }
        else{
            return null;
        }
        
    }
    
    public PageReference cancel() {
        return (new ApexPages.StandardController(quote)).view();
    }
    
    public void addError(Boolean pError, Boolean pAllowSubmitIfError, String pErrorMessage){
        error = pError;
        allowSubmitIfError = pAllowSubmitIfError;
        errorMessage = pErrorMessage;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, pErrorMessage));
    }

}