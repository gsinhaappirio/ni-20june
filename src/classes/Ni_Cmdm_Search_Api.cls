global Class Ni_Cmdm_Search_Api {
    
    public enum Source {ALL, SALESFORCE, ORACLE, ELOQUA}
    
    public Ni_Cmdm_Search_Api(){}
    
    /**
     * @param contact - the contact you want to search for
     * @param accountName - the name of the account that the contact belongs to
     * @param systems - the system you want to search. currently, only "ALL" is supported.
     * @param maxRows - the number of entries you want returned
     * @param salesforceIdsOnly - true returns only Ids for the SFDC entries. false returns all data for SFDC entries, including the Id
     * @return
     */
    public List<Ni_Cmdm_Contact_Search_Result> searchForContact(Contact contact, String systems, Integer maxRows, Boolean salesforceIdsOnly, Boolean salesforceOnTop){
        //add the filter criteria
        Ni_Cmdm_Search_Request.QbeFilterType filter = new Ni_Cmdm_Search_Request.QbeFilterType();
        filter.system_x = systems == null || systems.equals('') ? 'ALL' : systems;
        filter.maxRows = maxRows == null || maxRows < 0 ? 15 : maxRows;
        filter.sforceIDsOnly = salesforceIdsOnly == null ? false : salesforceIdsOnly;
        
        //convert the account object to the cmdm object
        Ni_Cmdm_Search_Enterprise_Object.Contact cmdmContact = new Ni_Cmdm_Search_Enterprise_Object.Contact();
        cmdmContact.FirstName = convertToAscii(contact.FirstName);
        cmdmContact.LastName = convertToAscii(contact.LastName);
        cmdmContact.Email = convertToAscii(contact.Email);
        cmdmContact.Phone = convertToAscii(contact.Phone);
        cmdmContact.MailingCountry = convertToAscii(contact.MailingCountry);
        
        if(cmdmContact.Phone != null && !cmdmContact.Phone.equals('')){
            cmdmContact.Phone = cmdmContact.Phone.trim();
            cmdmContact.Phone = cmdmContact.Phone.replaceAll('(\\s+)', '');
            cmdmContact.Phone = cmdmContact.Phone.replaceAll('[^0-9]', '');
        }
        
        Ni_Cmdm_Search_Enterprise_Object.Account cmdmAccount = new Ni_Cmdm_Search_Enterprise_Object.Account();
        cmdmAccount.name = convertToAscii(contact.account.Name);
        cmdmContact.Account = cmdmAccount;
        
        Ni_Cmdm_Search.SearchSOAP11BindingQSPort contactSearchPort = new Ni_Cmdm_Search.SearchSOAP11BindingQSPort();
        Ni_Cmdm_Search_Result.ContactSearchResultType result = contactSearchPort.QbeContactSearch(filter, cmdmContact);
        
        List<Ni_Cmdm_Contact_Search_Result> nonSfdcResults = new List<Ni_Cmdm_Contact_Search_Result>();
        List<Ni_Cmdm_Contact_Search_Result> sfdcResults = new List<Ni_Cmdm_Contact_Search_Result>();
        
        if(result.entryList.entry != null && result.entryList.entry.size() > 0){
            for(Ni_Cmdm_Search_Result.entry_element element : result.entryList.entry){
                Ni_Cmdm_Contact_Search_Result cmdmResult = new Ni_Cmdm_Contact_Search_Result(mapCmdmContactToSfdcContact(element.data.Contact), element.score, mapSourceSystemToReadableName(element.sourceRef.sourceSystem));
                
                if(element.sourceRef.memIdnum != null && !element.sourceRef.memIdnum.equals('')){
                    String sourceId = element.sourceRef.memIdnum;
                    Integer index = sourceId.indexOf('-');
                    if(index >= 0){
                        sourceId = sourceId.substring(0,index);
                    }
                    cmdmResult.sourceId = sourceId;
                }
                
                if(salesforceOnTop){
                    if(cmdmResult.sourceSystem.toLowerCase().equals('salesforce')){
                        sfdcResults.add(cmdmResult);
                    }else{
                        nonSfdcResults.add(cmdmResult);
                    }
                }else{
                    sfdcResults.add(cmdmResult);
                }
            }
            
            sfdcResults.sort();
                
            if(salesforceOnTop){
                nonSfdcresults.sort();
                sfdcResults.addAll(nonSfdcResults);
            }
        }else{
            sfdcResults = null;
        }
        
        return sfdcResults;
        
    }
    
    private Contact mapCmdmContactToSfdcContact(Ni_Cmdm_Search_Enterprise_Object.Contact cmdmContact){
    
        String jsonString = JSON.serialize(cmdmContact);
        
        // deserialize json into a map so that we can access the variable names
        Map<String, Object> jsonMap = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        Map<String, Object> cmdmMap = new Map<String, Object>();
        
        // clean up the variable names so that they are accessible using the api names
        for (String fieldName : jsonMap.keySet()){
            if(fieldName != null && !fieldName.equals('') && jsonMap.get(fieldName) != null){
                String field = fieldName.replace('_xc','__c');
                field = field.toLowerCase().trim();
                cmdmMap.put(field, jsonMap.get(fieldName));
            }
        }
        
        Contact contact = new Contact();
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Contact.fields.getMap();
        Map<String, String> params = new Map<String, String>();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                contact.put(fieldName, cmdmMap.get(fieldName));
            } catch (Exception e)    { 
                System.debug(e.getStackTraceString());
            }
        }

        contact.Account = mapCmdmAccountToSfdcAccount(cmdmContact.Account);
        
        return contact;
    }
    
    public List<Ni_Cmdm_Account_Search_Result> searchForAccount(Account account, String systems, Integer maxRows, Boolean salesforceIdsOnly, Boolean salesforceOnTop){
        
        //add the filter criteria
        Ni_Cmdm_Search_Request.QbeFilterType filter = new Ni_Cmdm_Search_Request.QbeFilterType();
        filter.system_x = systems == null || systems.equals('') ? 'ALL' : systems;
        // filter.maxRows = maxRows == null || maxRows < 0 ? 50 : maxRows;
        filter.maxRows = 50;
        filter.sforceIDsOnly = salesforceIdsOnly == null ? false : salesforceIdsOnly;
        
        //*
        //convert the account object to the cmdm object
        Ni_Cmdm_Search_Enterprise_Object.Account cmdmAccount = new Ni_Cmdm_Search_Enterprise_Object.Account();
        
        cmdmAccount.Name = account.Name;
        //cmdmAccount.Shipping_Street_1_xc = account.Shipping_Street_1__c;
        
        Ni_Cmdm_Search_Enterprise.Address add = new Ni_Cmdm_Search_Enterprise.address();
        
        if(account.ShippingStreet != null && !account.ShippingStreet.equals('')){
            add.street = account.ShippingStreet;
            cmdmAccount.ShippingStreet = account.ShippingStreet;
        }
        
        if(account.ShippingCity != null && !account.ShippingCity.equals('')){
            add.city = account.ShippingCity;
            cmdmAccount.ShippingCity = account.ShippingCity;
        }
        
        if(account.ShippingPostalCode != null && !account.ShippingPostalCode.equals('')){
            add.postalCode = account.ShippingPostalCode;
            cmdmAccount.ShippingPostalCode = account.ShippingPostalCode;
        }

        if(account.ShippingCountry != null && !account.ShippingCountry.equals('')){
            add.country = account.ShippingCountry;
            cmdmAccount.ShippingCountry = account.ShippingCountry;
        }
        
        cmdmAccount.ShippingAddress = add;
        //*/
        
        //Ni_Cmdm_Search_Enterprise_Object.Account cmdmAccount = mapSfdcAccountToCmdmAccount(account);
        
        //make the soap call
        Ni_Cmdm_Search.SearchSOAP11BindingQSPort accountSearchPort = new Ni_Cmdm_Search.SearchSOAP11BindingQSPort();
        Ni_Cmdm_Search_Result.AccountSearchResultType result = accountSearchPort.QbeAccountSearch(filter, cmdmAccount);
        
        List<Ni_Cmdm_Account_Search_Result> nonSfdcResults = new List<Ni_Cmdm_Account_Search_Result>();
        List<Ni_Cmdm_Account_Search_Result> sfdcResults = new List<Ni_Cmdm_Account_Search_Result>();
        
        if(result.entryList.entry != null && result.entryList.entry.size() > 0){
            for(Ni_Cmdm_Search_Result.entry_element element : result.entryList.entry){
                Ni_Cmdm_Account_Search_Result cmdmResult = new Ni_Cmdm_Account_Search_Result(mapCmdmAccountToSfdcAccount(element.data.Account), element.score, mapSourceSystemToReadableName(element.sourceRef.sourceSystem));
                if(salesforceOnTop){
                    if(cmdmResult.sourceSystem.toLowerCase().equals('salesforce')){
                        sfdcResults.add(cmdmResult);
                    }else{
                        nonSfdcResults.add(cmdmResult);
                    }
                }else{
                    sfdcResults.add(cmdmResult);
                }
            }
            
            sfdcResults.sort();
            
            if(salesforceOnTop){
                nonSfdcresults.sort();
                sfdcResults.addAll(nonSfdcResults);
            }
            
        }else{
            sfdcResults = null;
        }
        
        return sfdcResults;
    }
    
    /*
    private Ni_Cmdm_Search_Enterprise_Object.Account mapSfdcAccountToCmdmAccount(Account account){
    
        SObject sobj = account;
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        Map<String, Object> jsonMap = new Map<String, String>();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                if(sobj.get(fieldName) != null){
                    jsonMap.put(fieldName, sobj.get(fieldName));
                }
            } catch (Exception e)    { 
                System.debug(e.getStackTraceString());
            }
        }
        
        Map<String, Object> cmdmMap = new Map<String, String>();
        for (String fieldName : jsonMap.keySet()){
            if(fieldName != null && !fieldName.equals('') && jsonMap.get(fieldName) != null){
                String field = fieldName.replace('__c','_xc');
                cmdmMap.put(field, jsonMap.get(fieldName));
                //System.debug('+++++ ' + field + ': ' + jsonMap.get(fieldName));
            }
        }
        
        String jsonString = JSON.serialize(cmdmMap);
        
        Ni_Cmdm_Search_Enterprise_Object.Account cmdmAccount = (Ni_Cmdm_Search_Enterprise_Object.Account) JSON.deserialize(jsonString, Ni_Cmdm_Search_Enterprise_Object.Account.class);
        
        return cmdmAccount;
    }
    */
    
    private Account mapCmdmAccountToSfdcAccount(Ni_Cmdm_Search_Enterprise_Object.Account cmdmAccount){
        
        // serialize account
        String jsonString = JSON.serialize(cmdmAccount);
        
        // deserialize json into a map so that we can access the variable names
        Map<String, Object> jsonMap= (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        Map<String, Object> cmdmMap = new Map<String, Object>();
        
        // clean up the variable names so that they are accessible using the api names
        for (String fieldName : jsonMap.keySet()){
            if(fieldName != null && !fieldName.equals('') && jsonMap.get(fieldName) != null){
                String field = fieldName.replace('_xc','__c');
                field = field.toLowerCase().trim();
                cmdmMap.put(field, jsonMap.get(fieldName));
                System.debug('+++++ ' + field + ': ' + jsonMap.get(fieldName));
            }
        }
        
        Account account = new Account();
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        Map<String, String> params = new Map<String, String>();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                account.put(fieldName, cmdmMap.get(fieldName));
            } catch (Exception e)    { 
                System.debug(e.getStackTraceString());
            }
        }
        
        return account;
    }
    
    private String mapSourceSystemToReadableName(String sourceSystem){
        String readableSourceSystem = sourceSystem;
        
        if(sourceSystem == null){
            return null;
        }else{
            if(sourceSystem.trim().toLowerCase().equals('dnb')){
                readableSourceSystem = Label.Dun_and_Bradstreet;
            }else if(sourceSystem.trim().toLowerCase().equals('ec') || sourceSystem.trim().toLowerCase().equals('eo')){
                readableSourceSystem = Label.Fionn;
            }else if(sourceSystem.trim().toLowerCase().equals('cc') || sourceSystem.trim().toLowerCase().equals('co')){
                readableSourceSystem = Label.Arapaho;
            }else if(sourceSystem.trim().toLowerCase().equals('ebzc') || sourceSystem.trim().toLowerCase().equals('ebzo')){
                readableSourceSystem = Label.EBIZ;
            }else if(sourceSystem.trim().toLowerCase().equals('sfc') || sourceSystem.trim().toLowerCase().equals('sfa')){
                readableSourceSystem = Label.Salesforce;
            }else if(sourceSystem.trim().toLowerCase().equals('elqc')){
                readableSourceSystem = Label.Eloqua;
            }else if(sourceSystem.trim().toLowerCase().equals('up')){
                readableSourceSystem = Label.UserProfile;
            }if(sourceSystem.trim().toLowerCase().equals('wcc')){
                readableSourceSystem = Label.WCC;
            }
        }
        return readableSourceSystem;
    }
    
    public String convertToAscii(String s){
    
        if(s == null || s.equals('')){
            return s;
        }
        
        List<Integer> codes = new List<Integer>();
        Integer numberOfCharactersChanged = 0;
        Integer i = 0;
        while(i < s.length()){
            if(s.codePointAt(i) == 32){
                codes.add(32);
            }else if (s.codePointAt(i) > 65248){
                codes.add(s.codePointAt(i) - 65248);
                numberOfCharactersChanged++;
            }
            i++;
        }
        
        if(numberOfCharactersChanged == 0){
            return s;
        }else{
            return String.fromCharArray(codes);
        }
    }
    
    /*
    private String convertToTitleCase(String str){
        String result = '';
        if(str == null){
            return null;
        }else{
            str = str.toLowerCase();
            List<String> parts = str.split(' ');
            if(parts != null && parts.size() > 0){
                for(String s : parts){
                    if(result.equals('')){
                        if(s != null && !s.equals('')){
                            result += s.substring(0, 1).toUpperCase() + s.substring(1);
                        }
                    }else {
                        if(s != null && !s.equals('')){
                            result += ' ' + s.substring(0, 1).toUpperCase() + s.substring(1);
                        }
                    }
                }
            }
        }
        
        return result;
        
    }
    */
}