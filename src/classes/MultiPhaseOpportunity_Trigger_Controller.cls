public with sharing class MultiPhaseOpportunity_Trigger_Controller {

    /**********************************************************************************************************************
    * Author: mmurillo
    * Date: 
    * Param: List<Multi_Phase_Opportunity__c > (Trigger.New),Map<Id,Multi_Phase_Opportunity__c > (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Update methods of Multi_Phase_Opportunity__c .
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onBeforeUpdate(Map<id,Multi_Phase_Opportunity__c > newMultiPhaseMap, Map<id,Multi_Phase_Opportunity__c > oldMultiPhaseMap){
        
        MultiPhaseOpportunity_Handler.updateAmount(newMultiPhaseMap, oldMultiPhaseMap);
        
    }


}