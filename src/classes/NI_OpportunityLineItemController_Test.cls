@isTest(seeAllData = true)
public class NI_OpportunityLineItemController_Test
{

    static testMethod void updateProductTest()
    {
    
        test.starttest();
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
        Map<Id,string> productMap = new Map<Id, string>();
        List<Product2> pbe = [Select Id,Name from Product2];
        for(Product2 p:pbe){
            productMap.put(p.Id,p.Name);
        }
        Id pricebookId = Test.getStandardPricebookId();
        List<Product2> prodList = new List<Product2>();
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        
        insert opp; 
        for(Integer i = 0; i<10 ; i++){
            Product2 tempProd = new Product2(Name='Temp Prod'+i,Family='IP Communications',IsActive=true,ProductCode='77-77'+i);
            prodList.add(tempProd);
        }
        insert prodList;
        for(Integer i = 0; i<10 ; i++){     
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prodList[i].Id,UnitPrice = 100, IsActive = true);  
            pbeList.add(standardPrice);
        }
        insert pbeList;
        for(Integer i = 0; i<10 ; i++){    
       
            OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.id,PricebookEntryId=pbeList[i].id,Quantity=1,TotalPrice=500);
            oliList.add(oli);
        }
        insert oliList;
        NI_OpportunityLineItemTrigger_controller.updateProduct(oliList);
        
        Map <Id, OpportunityLineItem> oppLineItemMap = new Map <Id, OpportunityLineItem>();
        for(Integer i=0 ; i<10 ; i++){
            OpportunityLineItem oli=oliList[i];
            oppLineItemMap.put(oli.id,oli);
        }
        NI_OpportunityLineItemTrigger_controller.deleteProduct(oppLineItemMap);
        
        test.stoptest();
    }
}