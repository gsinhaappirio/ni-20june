public with sharing class NI_Sales_Sales_OrderLineDTO {


    public String partNumber {get;set;}
    public String partDescription {get;set;}
    public Decimal orderedQuantity {get;set;}
    public Decimal unitListPrice {get;set;}
    public Decimal unitSellingPrice {get;set;}
    public Decimal lineAdjustedPercent {get;set;}
    public Decimal lineTotalPrice {get;set;}	
	
	public NI_Sales_Sales_OrderLineDTO(){}
	
	public NI_Sales_Sales_OrderLineDTO(String partNumber, String partDescription, Decimal orderedQuantity, Decimal unitListPrice, Decimal unitSellingPrice){
							 
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.orderedQuantity = orderedQuantity;
		this.unitListPrice = unitListPrice;
		this.unitSellingPrice = unitSellingPrice;
		this.lineTotalPrice = this.unitSellingPrice * this.orderedQuantity;
		
		if (this.unitListPrice > 0){
			this.lineAdjustedPercent = 	Math.round((this.unitListPrice - this.unitSellingPrice) / (this.unitListPrice / 100));
		}			 
		else{
			this.lineAdjustedPercent = 0;
		}
	}


}