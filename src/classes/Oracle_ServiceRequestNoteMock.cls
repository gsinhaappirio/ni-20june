@isTest
public with sharing class Oracle_ServiceRequestNoteMock implements WebServiceMock {

    public class WSMockException extends Exception {}
    
    private boolean error;  
    
    
    public Oracle_ServiceRequestNoteMock() {
      this.error = false;
    }
    
    public Oracle_ServiceRequestNoteMock(boolean error) {
      this.error = error;
    }
      
    public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
            
            if( error ){
              throw new WSMockException();
            }
            
            // Response Object
            Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType calloutResponse = new Oracle_ServiceRequestNoteResponse.ServiceRequestNotesType();
            
            // Mock SR Note #1
            Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element element1 = new Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element();
            element1.noteCreatedBy = 'creatorName1';
            element1.noteDate = Integer.valueOf(DateTime.newInstanceGMT(2014,01,01).getTime() / 1000); // seconds to milliseconds
            element1.noteType = 'typeName1';
            element1.noteComments = 'comments1';
            
            // Mock SR Note #2
            Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element element2 = new Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element();
            element2.noteCreatedBy = 'creatorName2';
            element2.noteDate = Integer.valueOf(DateTime.newInstanceGMT(2014,01,01).getTime() / 1000);  // seconds to milliseconds
            element2.noteType = 'typeName2';
            element2.noteComments = 'comments2';
            
            Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element[] responseElement = 
                new Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element[]{element1, element2};
            calloutResponse.ServiceRequestNote = responseElement;
            response.put('response_x', calloutResponse);
    }
}