@isTest
public class NI_Service_Request_Extension_Test {
    @isTest(seeAllData=true)
    public static void controllerExtensionTest() {
    	Test.setMock(WebServiceMock.class, new Oracle_ServiceRequestNoteMock());
    	List<NI_Sales_Sales_ServiceRequestNoteDTO> noteList;
        NI_Service_Request__c sr = new NI_Service_Request__c();
        NI_Service_Request_Extension sre = new NI_Service_Request_Extension(new ApexPages.StandardController(sr));
        
        noteList = sre.noteList;
        
        System.assertEquals(noteList.get(0).createdBy, 'creatorName1');
	System.assertEquals(noteList.get(0).creationDate, DateTime.newInstanceGMT(2014,01,01));
	System.assertEquals(noteList.get(0).type, 'typeName1');
	System.assertEquals(noteList.get(0).comments, 'comments1');
	
	System.assertEquals(noteList.get(1).createdBy, 'creatorName2');
	System.assertEquals(noteList.get(1).creationDate, DateTime.newInstanceGMT(2014,01,01));
	System.assertEquals(noteList.get(1).type, 'typeName2');
	System.assertEquals(noteList.get(1).comments, 'comments2');
    }
}