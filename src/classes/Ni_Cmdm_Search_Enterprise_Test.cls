@isTest
public class Ni_Cmdm_Search_Enterprise_Test {
	
    @isTest
    public static void queryResultTest(){
        Ni_Cmdm_Search_Enterprise.QueryResult qr = new Ni_Cmdm_Search_Enterprise.QueryResult();
        qr.done = true;
        qr.queryLocator = 'empty';
        qr.records = null;
        qr.size = 10;
    }
    
    @isTest
    public static void locationTest(){
        Ni_Cmdm_Search_Enterprise.Location location = new Ni_Cmdm_Search_Enterprise.Location();
        location.latitude = 0.0;
        location.longitude = 0.0;
    }
    
    @isTest
    public static void addressTest(){
        Ni_Cmdm_Search_Enterprise.Address address = new Ni_Cmdm_Search_Enterprise.Address();
        address.city = 'Austin';
        address.country = 'United States';
        address.countryCode = 'US';
        address.postalCode = '78759';
        address.state = 'Texas';
        address.stateCode = 'TX';
        address.street = '11500 N Mopac Expwy';
    }
    
}