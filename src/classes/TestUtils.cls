public class TestUtils {
    
    public static Account getAccount(String accountName){
        Account account = new Account(Name=accountName);
        account.ShippingCity = 'Austin';
        account.ShippingPostalCode = '78759';
        account.ShippingCountry = 'US';
        return account;
    }
    
    public static Contact getContact(String lastName, Id accountId){
        Contact contact = new Contact(LastName=lastName, AccountId=accountId);
        return contact;
    }

    /**
      * @description       Method for defining User object record to be created for use in Test Class. 
      * @param             aliasId the Alias Id of the User record is to be created
      * @param             profileName the profile for which the User record is to be created
      * @return            User
      * @throws            NA
      */
    public static User getUserForProfile(String LastName, String profileName){
        Id profileId = [SELECT Id FROM Profile WHERE Name= :profileName].Id;
        User user = new User(FirstName='User', LastName=LastName, Alias='u' + LastName.toLowerCase().trim(), 
                                 IsActive=True, Email='user.' + LastName.toLowerCase().trim() +'@test.ni.com',
                                 ProfileId=profileId, Username='user.' + LastName.toLowerCase().trim() + '@test.ni.com', 
                                 EmailEncodingKey='UTF-8', CommunityNickname=LastName.toLowerCase().trim(), LanguageLocaleKey='en_US',
                                 LocaleSidKey='en_US', TimeZoneSidKey='GMT');
        return user;
    }
    
    public static List<User> getUsersForProfile(String lastName, String profileName, Integer count){
        Id profileId = [SELECT Id FROM Profile WHERE Name=: profileName].Id;
        List<User> users = new List<User>();
        for(Integer i = 0; i < count; i++){
            User user = new User(FirstName='User', LastName=LastName + String.valueOf(i), Alias='u' + LastName.toLowerCase().trim() + String.valueOf(i), 
                                 IsActive=True, Email='user.' + LastName.toLowerCase().trim() + String.valueOf(i) +'@test.ni.com',
                                 ProfileId=profileId, Username='user.' + LastName.toLowerCase().trim() + String.valueOf(i) + '@test.ni.com', 
                                 EmailEncodingKey='UTF-8', CommunityNickname='u' + LastName.toLowerCase().trim() + String.valueOf(i), LanguageLocaleKey='en_US',
                                 LocaleSidKey='en_US', TimeZoneSidKey='GMT');
            users.add(user);
        }
        return users;
    }
    
    public static List<Lead> getLeads(Integer count){
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < count; i++){
            Lead lead = new Lead();
            lead.FirstName = 'First';
            lead.LastName = 'Last';
            lead.Country = 'US';
            lead.Company = 'NI';
            lead.Status = 'Qualified';
            lead.LeadSource = 'Sales - Sales';
            leads.add(lead);
        }
        return leads;
    }
    
}