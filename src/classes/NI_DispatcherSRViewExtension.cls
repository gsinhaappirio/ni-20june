public class NI_DispatcherSRViewExtension {

    private ApexPages.StandardController sc;
    public String srId {get; set;}
    public boolean srFound {get; set;}
    
    public NI_DispatcherSRViewExtension (ApexPages.StandardController controller) {
        this.sc= controller;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('isSF1')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
               ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
               (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }

    public PageReference redirectToExternalObject() {
        srFound = false;
        NI_Service_Request__c sr= [Select id, site__c, Oracle_Service_Request_Id__c From NI_Service_Request__c Where Id = :ApexPages.currentPage().getParameters().get('id')];        
        String siteHeaderId = String.valueOf(sr.oracle_Service_Request_Id__c);
        
        NI_OD_SR_HDR_ALL_V__x [] externalSrs;
        
        if(Test.isRunningTest()){
            NI_OD_SR_HDR_ALL_V__x externalTestSr = new NI_OD_SR_HDR_ALL_V__x();
            externalSrs = new List<NI_OD_SR_HDR_ALL_V__x>();
            externalSrs.add(externalTestSr);
        }
        else{
            externalSrs = [select id from NI_OD_SR_HDR_ALL_V__x where externalid = :siteHeaderId ];
        }
        
        if(externalSrs.size() > 0){
            PageReference srPage = new ApexPages.StandardController(externalSrs[0]).view();
            srId = externalSrs[0].id;
            srFound = true;
            if (!isSF1){
                return srPage.setRedirect(true);
            }
            else{
                return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This Service Request has not been found in our external system.'));
            return null;
        }
        
    }
}