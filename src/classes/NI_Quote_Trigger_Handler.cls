public with sharing class NI_Quote_Trigger_Handler {
/**
    public void OnAfterUpdate(NI_Quote__c[] newQuotes){  
        
        for(NI_Quote__c newQuote : newQuotes){
              
            if(newQuote.Opportunity__c != null){
                
                //linkOppToQuote(String.valueOf(newQuote.Opportunity__c), double.valueOf(newQuote.OracleQuoteHeaderId__c), double.valueOf(newQuote.Org_Id__c) );
                  
            }
              
        }    
        
    }
    
    //Link a SDFC Opp to a Oracle Quote
    @future (callout=true)
    static void linkOppToQuote(String sfdcId, double oracleQuoteId, double orgId){
        
        System.debug('linkOppToQuote. SfdcID: ' + sfdcId + ' oracleQuoteId: ' + oracleQuoteId + 'orgId:' +  orgId);
        
        NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP quotesService = new NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP();

        NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = new NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element(); 
                
        try{
            response = quotesService.OppQuoteLink(sfdcId, oracleQuoteId, orgId);
        }
        catch(System.CalloutException e){
            System.debug('Error calling webservice quoteId: ' + String.valueOf(oracleQuoteId));
        }
        
    }  
    */

}