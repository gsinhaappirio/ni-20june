public with sharing class NI_Opportunity_To_Quote_Extension {
    
    // Inner class for throwing as custom exception
    public class CustomSettingException extends Exception {}

    private final Opportunity opportunity;
    public String contactId { get; set; }
    public List<SelectOption> contactItems { get; set; }
    public Map<ID, Contact> contactMap { get; set; }
    public String notes { get; set; }
    public Boolean error { get; set; } // flag for note creation errors
    public String errorMessage { get; set; } // flag for note creation errors
    public String undefinedRequestorMessage { get; set; } 
    public Boolean submitted { get; set; }
    public Boolean enableAttachment { get; set; }
    public Boolean isUrgent { get; set; }
    public String urgentMsg { get; set; } 
    public Boolean sendToRequesterOnly {get;set;}
    public String sendToRequesterOnlyHelpText {get;set;}
    
    public Decimal maxAttachmentSize { get; set; }
    //used to be displayed in user messages with a more friendly looking
    public Decimal roundedMaxAttachmentSize { get; set; }
    
    //declared Transient so attachment is not transmitted as part of the view state
    public Transient String attachmentName { get; set; } // attachment name
    public Transient Blob attachmentBlob { get; set; } // attachment content
    public Transient String contentType {get; set;} // attachment content-type
    public Transient Integer fileSize {get; set;} // attachment size
    
    @testVisible
    private static final String ERROR_MESSAGE_PARAM = 'errorMessage';
    @testVisible
    private static final String ERROR_CAUSE_PARAM = 'errorCause';
    @testVisible
    private static final String NOTES_PARAM = 'notes';
    @testVisible
    private static final String CONTACT_ID_PARAM = 'contactId';
    @testVisible
    private static final String EMAIL_TEMPLATE = 'Quote_Quote_Request';
    private Ni_Quote_Settings__c quoteSettings{get;set;}
    
    /**
    * Constructor
    * @param stdController an Opportunity standard controller
    */
    public NI_Opportunity_To_Quote_Extension(ApexPages.StandardController stdController){
        this.opportunity = (Opportunity) stdController.getRecord();
        contactItems = new List<SelectOption>();
        contactMap = new Map<ID, Contact>();
        error = false;
        submitted = false;
        isUrgent = false;
        sendToRequesterOnly = false;
        
        quoteSettings = Ni_Quote_Settings__c.getInstance(UserInfo.getUserID());
        enableAttachment = quoteSettings.Enable_attachments_on_quote_request__c;
        maxAttachmentSize = quoteSettings.Max_attach_size_limit__c;
        maxAttachmentSize = maxAttachmentSize != null && maxAttachmentSize > 0 ? maxAttachmentSize : 3145728;
        roundedMaxAttachmentSize = (Decimal)(maxAttachmentSize/(1024*1024)).setscale(2);
        undefinedRequestorMessage = quoteSettings.undefined_requestor_msg__c != null ? 
                                    quoteSettings.undefined_requestor_msg__c : '(Undefined Country/Requestor)';
        urgentMsg = quoteSettings.urgent_msg__c != null ? 
                                    quoteSettings.urgent_msg__c : 'The quote request will be marked as urgent so that the quote creator can prioritise the request. ' + 
                                                                  'Use of this flag will be monitored should only be used genuine customer urgency.'; 
        
        // Verify whether current user has edit access to this Opportunity record
        if( ![SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :opportunity.id].HasEditAccess ){
            throw new NoAccessException();
        }
        
        PageReference page = ApexPages.currentPage();
        if(page.getParameters().get(ERROR_MESSAGE_PARAM) != null){
            getNoteCreationErrorState(page);
        }
        
        for(List<Contact> contacts : [SELECT Id, Name, Phone, Email, 
                             Account.Name, Account.ShippingStreet, Account.ShippingCity, 
                             Account.ShippingState, Account.ShippingPostalCode, Account.ShippingCountry
                          FROM Contact 
                          WHERE Id IN (SELECT ContactId 
                                       FROM OpportunityContactRole 
                                       WHERE OpportunityId = :opportunity.Id)]){
            for(Contact contact : contacts){
                contactItems.add(new SelectOption(contact.Id, getQuoteToContact(contact)));
                contactMap.put(contact.Id,contact);
            }
        }
        
        if(contactItems.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please add a Contact to the Opportunity before requesting a Quote'));
        }else{
            Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Country__c.fields.getMap();
            List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
            String query = 'SELECT ';
            for(Schema.SObjectField s : fldObjMapValues){
               query += s.getDescribe().getName() + ',';
            }
            // Trim last comma
            query = query.subString(0, query.length() - 1);
            query += ' FROM Country__c';
            List<Country__c> countries = Database.query(query);
            
            Map<String, Country__c> countryMap = new Map<String, Country__c>();
            for(Country__c country : countries){
                // name and full name are required so no need for a null check here
                countryMap.put(country.Name.toUpperCase().trim(), country);
                countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
                if(country.Three_Digit_ISO_Code__c != null){
                    countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
                }
                if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                    List<String> otherNames = country.Other_Names__c.split('\n');
                    for(String otherName : otherNames){
                        countryMap.put(otherName.toUpperCase().trim(), country);
                    }
                }
            }
            
            if(countryMap.get(Opportunity.Account.ShippingCountry.trim().toUpperCase()) != null){
                Country__c country = countryMap.get(Opportunity.Account.ShippingCountry.trim().toUpperCase());
                if(country.region__c.equalsIgnoreCase('IndRAA')){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Quote requests are not currently activated for IndRAA region.') );
                    error = true;
                }
            }
        }
    }
    
    public PageReference refresh(){
        return null;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }    
    
    /**
    * Sends and creates a Quote request email and note respectively, then redirects to a new page according to the result.
    * @return a reference to the next page
    */
    public PageReference save(){
        PageReference page;
        Boolean success = false;
        submitted = true;
        
        //Check for file size 
        if (fileSize != null){
            if (fileSize > maxAttachmentSize){
                System.debug('FileSize: ' + fileSize);
                ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'This file exceeds the maximum size limit of ' + 
                                      roundedMaxAttachmentSize +' MB') );
                errorMessage = 'This file exceeds the maximum size limit of ' + roundedMaxAttachmentSize +' MB';
            }
        }
        
        if(ApexPages.hasMessages(ApexPages.severity.ERROR)) {
            System.debug('Error: ' + ApexPages.getMessages());
            error = true;
            return null;
        }        
        
       if( sendQuoteRequestTemplateEmail() ) {
        
            success = createQuoteRequestNote();
            if (success && attachmentBlob != null && enableAttachment ){
                success = createQuoteRequestAttachment();
            }
                    
            if ( !isSF1 ){
                page = success ? new ApexPages.StandardController(opportunity).view() : ApexPages.currentPage();
                page.setRedirect(true);
            }
            else{
                return null;
            }
        }
        
        return page;
    }
    
    /**
    * Redirects (back) to the Opportunity's default page.
    * @return a reference to the next page
    */
    public PageReference cancel(){
        return new ApexPages.StandardController(opportunity).view().setRedirect(true);
    }
    
    /**
    * Sets note creation error information on a page.
    * @param page a reference to the page whose error information is being set
    * @param errorMessage the error message to be set
    */
    @testVisible
    private void setNoteCreationErrorState(PageReference page, String errorMessage, boolean isNote) {
        // Removes technical info from error message
        String parsedErrorMessage = errorMessage.substringAfter(',');
        parsedErrorMessage = parsedErrorMessage == '' ? errorMessage : parsedErrorMessage;
        
        if (isnote){
            page.getParameters().put(ERROR_MESSAGE_PARAM, 'Automatic note creation failed. Please add note manually. Quote request email was sent successfully.');
        }
        else{
            page.getParameters().put(ERROR_MESSAGE_PARAM, 'Automatic attachment creation failed. Please add attachment manually. Quote request email was sent successfully.');
        }
        
        page.getParameters().put(ERROR_CAUSE_PARAM, parsedErrorMessage );
        page.getParameters().put(CONTACT_ID_PARAM, contactId);
        page.getParameters().put(NOTES_PARAM, notes);
    }
    
    /**
    * Retrieves note creation error information from a page.
    * @param page a reference to the page whose error information is being retrieved
    */
    @testVisible
    private void getNoteCreationErrorState(PageReference page) {
        error = true;
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, page.getParameters().get(ERROR_MESSAGE_PARAM)) );
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, page.getParameters().get(ERROR_CAUSE_PARAM)) );
        contactId = page.getParameters().get(CONTACT_ID_PARAM);
        notes = page.getParameters().get(NOTES_PARAM);
    }
    
    /**
    * Retrieves a formated string with a Contact's name and its associated Account's name.
    * @param contact the contact to use
    * @return the formatted string
    */
    private String getQuoteToContact(Contact contact){
        String quoteToContact;
        if(contact != null && contact.Account != null){
            quoteToContact = contact.Name + ' (' + contact.Account.Name + ')';
        }
        
        return quoteToContact;
    }
    
    /**
    * Creates a Note that documents a request for a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean createQuoteRequestNote(){
        Contact contact = contactMap.get(contactId);
        Boolean success = false;
        try {
            insert new Note(ParentId = opportunity.Id, Title = 'Quote Request',  Body = 'Quote to Contact: ' 
                + getQuoteToContact(contact) + '\n' + notes);
            success = true;
        // Any exception will be handled in the same way
        } catch (Exception e) {
            setNoteCreationErrorState(ApexPages.currentPage(), e.getMessage(), true);
            System.debug(e.getTypeName() + ': Unable to create Quote Request Note: ' + e.getMessage());
        }
        
        return success;
    }
    
    /**
    * Creates a Note that documents a request for a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean createQuoteRequestAttachment(){
        Contact contact = contactMap.get(contactId);
        Boolean success = false;
        try {
            Attachment attachment = new Attachment();
            attachment.Body = attachmentBlob;
            attachment.Name = attachmentName;
            attachment.ParentId = opportunity.Id;
            insert attachment;
            success = true;
            
        // Any exception will be handled in the same way
        } catch (Exception e) {
            setNoteCreationErrorState(ApexPages.currentPage(), e.getMessage(), false);
            System.debug(e.getTypeName() + ': Unable to create attachment: ' + e.getMessage());
        }
        
        return success;
    }    
    
    /**
    * Sends a template-based email to request a Quote.
    * @return a result flag
    */
    @testVisible
    private boolean sendQuoteRequestTemplateEmail(){
        Contact contact = contactMap.get(contactId);    
        Messaging.SingleEmailMessage mail;
        Boolean success = false;
        
        try {
            mail = mergeQuoteRequestTemplateEmail(contact, opportunity);
            
            //we can't send emails with tests
            if (!Test.isRunningTest()){
                List<Messaging.SendEmailResult> result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
            
            success = true;
            
        } catch (System.QueryException e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: Template is missing'));
            errorMessage = 'Unable to locate EmailTemplate';
            System.debug(e.getTypeName() + ': Unable to locate EmailTemplate ' + EMAIL_TEMPLATE + ': ' + e.getMessage());
            
        } catch (System.NullPointerException e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: One or more merge fields required by the template are missing'));
            errorMessage = 'One of the merge fields required by email template is null';
            System.debug(e.getTypeName() + ': One of the merge fields required by email template ' + EMAIL_TEMPLATE +
            ' is null: ' + e.getMessage());
            
        } catch (CustomSettingException e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email: Default email routing address is missing'));
            errorMessage = 'Default email routing address is missing';
            System.debug(e.getMessage());
        
        } catch (Exception e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to send Quote Request email'));
            errorMessage = 'Unable to send Quote Request email';
        }
        
        error = !success;
        return success;
    }
    
    /**
    * Populates the merge fields on the quote request email template.
    * @param contact Contact object to use in the merge
    * @param opportunity Opportunity object to use in the merge
    * @return the merged email message
    */
    @testVisible
    private Messaging.SingleEmailMessage mergeQuoteRequestTemplateEmail(Contact contact, Opportunity opportunity) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        EmailTemplate emailTemplate = [select subject, htmlValue from EmailTemplate where developername = :EMAIL_TEMPLATE];
            
        String shippingCountry = opportunity.Account.ShippingCountry;
        String shippingState = opportunity.Account.ShippingState;
        String fullName = opportunity.Owner.Name;
        
        String notesTmp = '';
        
        //Change of end of line character for <br/>, so TMD Jira ticket see the notes entered by user properly 
        if (notes != null){
            notesTmp = notes.replaceAll('<', '&lt;');
            notesTmp = notesTmp.replaceAll('>', '&gt;');
            notesTmp = notesTmp.replaceAll('\n', '<br/>');
        }
        
        // Template fields must be merged manually because a custom merge field is being included
        // Merge subject fields
        String subject = (String) emailTemplate.Subject;
        subject = subject.replace('{!Opportunity.Opportunity_Number__c}', opportunity.Opportunity_Number__c);
        subject = subject.replace('{!country}', shippingCountry);
        subject = shippingState != null ? subject.replace('{!state}', shippingState) : subject.replace('{!state}, ', '');
        
        if (isUrgent){
            subject = 'Urgent: ' + subject; 
        }
        
        // Merge body fields
        String htmlBody = (String) emailTemplate.HtmlValue;
        htmlBody = htmlBody.replace('{!Opportunity.Opportunity_Number__c}', opportunity.Opportunity_Number__c);
        htmlBody = htmlBody.replace('{!Opportunity.Name}', opportunity.Name);
        htmlBody = htmlBody.replace('{!opportunityUrl}', URL.getSalesforceBaseUrl().toExternalForm() + '/' + opportunity.Id);
        htmlBody = htmlBody.replace('{!Opportunity.OwnerFullName}', fullName);
        htmlBody = htmlBody.replace('{!notes}', notesTmp);
        htmlBody = htmlBody.replace('{!currency}', getCurrency(opportunity));
        htmlBody = htmlBody.replace('{!Contact.Account}', contact.Account.Name);
        htmlBody = htmlBody.replace('{!Contact.Name}', contact.Name);
        htmlBody = htmlBody.replace('{!shippingAddress}', contact.Account.shippingStreet + ', ' + contact.Account.shippingCity
            + ', ' + contact.Account.shippingState + ' ' + contact.Account.shippingPostalCode + ', ' + contact.Account.shippingCountry);
        // Avoiding possible NullPointerExceptions for blank fields
        String contactPhone = contact.Phone == null ? '' : contact.Phone;
        String contactEmail = contact.Email == null ? '' : contact.Email;
        htmlBody = htmlBody.replace('{!Contact.Phone}', contactPhone);
        htmlBody = htmlBody.replace('{!Contact.Email}', contactEmail);
        
        htmlBody = htmlBody.replace('{!SendToRequesterOnlyImage}', sendToRequesterOnly ? '<br/><img src="' + quoteSettings.Send_to_Requester_Only_Url__c + '" /><br/>' : '');
        mail.setHtmlBody(htmlBody);
                
        // Set sender/recipient addresses
        mail.setSenderDisplayName(UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        
        List<Country__c> countries = [Select Id, Full_Name__c, Name, Three_Digit_ISO_Code__c, Other_Names__c, Override_Lead_Language__c, Primary_Language__c, Quote_Request_Email__c from Country__c];
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        Country__c country;
        NI_Quote_Request_Email_Routing__c emailRoutingSettings;
        String email;
        
        User currentUser = [Select id, name, source__c from User where id = :UserInfo.getUserId()];
        
        try{
            country = countryMap.get(shippingCountry.toUpperCase().trim());
        
            if ( country != null && country.Quote_Request_Email__c != null ){
                email = country.Quote_Request_Email__c;
            }else{
                subject = subject + ' ' + undefinedRequestorMessage;
                
                if (currentUser.source__c != null){
                    emailRoutingSettings = NI_Quote_Request_Email_Routing__c.getValues(currentUser.source__c.toUpperCase().trim());
                }
                
                if ( emailRoutingSettings != null ){
                    email = emailRoutingSettings.email__c;
                }
                else{
                    emailRoutingSettings = NI_Quote_Request_Email_Routing__c.getValues('DEFAULT');
                    email =  emailRoutingSettings != null ? emailRoutingSettings.email__c : null;
                }
            }
            
        }catch(Exception e){
            email = null;
        }
        
        if(email == null ){
            throw new CustomSettingException('Exception: Country:' + country != null && country.Name != null ? country.Name : 'null' + ' has no email value for country \'Default\'.');
        }
        
        mail.setSubject(subject);
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setCcAddresses(new String[]{email});
        mail.saveAsActivity = false;
        
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
        
        //add attachment
        if(attachmentBlob != null ){
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setFileName(attachmentName);
            attachment.setBody(attachmentBlob);
            attachment.setContentType(contentType);
            attachments.add(attachment);
        }
    
        if(!attachments.isEmpty()){
            mail.setFileAttachments(attachments);
        }
        
        mail.setCharset('UTF-8');
        return mail;
    }
    
    /**
    * Gets the currency type used by an object
    * @param someObject the object to be evaluated
    * @return the currency's ISO code
    */
    private static String getCurrency(SObject someObject){
        String currencyIso = UserInfo.isMultiCurrencyOrganization() ? (String) someObject.get('CurrencyIsoCode') : UserInfo.getDefaultCurrency();
        return currencyIso;
    }
    
}