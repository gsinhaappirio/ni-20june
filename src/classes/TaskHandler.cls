/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            TaskHandler
Description:     This class contains support actions for triggers on Task 

Date                            Version            Author                                    Summary of Changes 
---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class TaskHandler {
    
    final static string TRGRNAME = 'Task_Trigger';
    final static string RCTYPE = 'cREQ';
    final static string CLASSNAME = 'TaskHandler';
    final static string METHODNAME = 'OppLastActiveDate';
    final  static string WON = system.label.won;
    final static string LOST = system.label.lost;
    final static string ERRMSG = system.label.Opportunity_Errormsg ;
    /**********************************************************************************************************************
* Author: 
* Date: 
* Param: List<Task> (Trigger.New)
* Return: void
* Description: 
***********************************************************************************************************************                          
* Applicable RecordTypes : All
* Dataload Bypass Check : Not Applicable
* Trigger Events : Before Insert, Before Update
* Summary of Changes :  - Initial version
**********************************************************************************************************************/
    
    /*
    public static void populateParentTypes(List<Task> newTaskList, Map<Id, Task> oldTaskMap){
        for(Task task : newTaskList){
            Task oldTask = oldTaskMap != null ? oldTaskMap.get(task.Id) : null;
            if(task.whatId != null && (oldTask == null || task.whatId != oldTask.whatId)){
                System.debug('***** what id changed:');
                task.what_parent_type__c = getObjectType(String.valueOf(task.whatId));
            }
            
            if(task.whoId != null && (oldTask == null || task.whoId != oldTask.whoId)){
                System.debug('***** who id changed:');
                task.who_parent_type__c = getObjectType(String.valueOf(task.whoId));
            }
            
        }
    }
    */
    
    private static String getObjectType(String recordId){
        String objectName = '';
        System.debug('+++++ record id is ' + recordId);
        try{
            //Get prefix from record ID
            //This assumes that you have passed at least 3 characters
            String idPrefix = String.valueOf(recordId).substring(0,3);
             
            //Get schema information
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe(); 
             
            //Loop through all the sObject types returned by Schema
            for(Schema.SObjectType stype : gd.values()){
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();
                System.debug('+++++ Prefix is ' + prefix);
                 
                //Check if the prefix matches with requested prefix
                if(prefix!=null && prefix.equals(idPrefix)){
                    objectName = r.getName();
                    System.debug('+++++ Object Name! ' + objectName);
                    break;
                }
            }
        }catch(Exception e){
            System.debug(e);
        }
        return objectName;
    }
    
    public static void OppLastActiveDate(List<Task> newTaskList){
        List<Database.SaveResult> dbSaveList = new List<Database.SaveResult> ();
        List<Id> Oppidlist = new list<Id>();
        List<opportunity> opplist = new list<opportunity>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        try{
            for (task record:newTaskList) {
                
                if(record.whatid != null && record.whatid.getsobjecttype() == opportunity.sobjecttype) {
                    Oppidlist.add(record.whatid);
                }
                
            }
            opplist =[select Last_Activity_Date_Plain__c,Stagename,Close_Reason__c,Target_Application_Value__c from  Opportunity where id IN : Oppidlist];
            for (task record:newTaskList) {
                for(opportunity oppObj : opplist ){
                    /*------- Check if the opportunity record meets the desired validation rule -------- */
                    if(((oppObj.StageName==WON)||(oppObj.StageName==Lost))&&((oppObj.Close_Reason__c==null)||(oppObj.Target_Application_Value__c==null))){
                        
                        record.addError(ERRMSG );
                    }
                    else{
                        oppObj.Last_Activity_Date_Plain__c=Date.TODAY();
                        oppsToUpdate.add(oppObj);
                    }
                }
                
            }
            dbSaveList = database.update(oppsToUpdate,false);
            
        }
        catch(Exception e){
            
            system.LoggingLevel newLoglevel = LoggingLevel.ERROR;
            if(e.getMessage().contains('Update failed. First exception on row 0;') ){
                UTIL_LoggingService.logDmlResults(dbSaveList,null,oppsToUpdate,'Sales','TaskHandler','OppLastActiveDate',trgrName,newLoglevel );
            } 
            
        } 
    } 
    
    public static void updateParentNextDueDate(Map<Id, Task> newTaskMap, Map<Id,Task> oldTaskMap){
        List<SObject> allObjects = new List<SObject>();
        List<Id> leadIds = new List<Id>();
        List<Id> oppIds = new List<Id>();
        List<Task> tasks = oldTaskMap != null ? oldTaskMap.values() : newTaskMap.values();
        
        String leadKeyPrefix = Lead.sObjectType.getDescribe().getKeyPrefix();
        String oppKeyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
        
        for(Task task : tasks){
            String whoPrefix = task.whoId != null ? String.valueOf(task.whoId).substring(0,3) : null;
            String whatPrefix = task.whatId != null ? String.valueOf(task.whatId).substring(0,3) : null;
            if(task.whoId != null && whoPrefix != null && whoPrefix.equalsIgnoreCase(leadKeyPrefix)){
                leadIds.add(task.whoId);
            }else if(task.whatId != null && whatPrefix != null && whatPrefix.equalsIgnoreCase(oppKeyPrefix)){
                System.debug('***** opportunity');
                oppIds.add(task.whatId);
            }
        }
        
        
        List<SObject> leads = leadIds != null && !leadIds.isEmpty() ? [Select Id, (SELECT Id, status, ActivityDate, isDeleted FROM Tasks where status != 'Completed' and ActivityDate != null ORDER BY ActivityDate ASC) from Lead where Id in :leadIds] : null;
        List<SObject> opps = oppIds != null && !oppIds.isEmpty() ? [Select Id, (SELECT Id, status, ActivityDate, isDeleted FROM Tasks where status != 'Completed' and ActivityDate != null ORDER BY ActivityDate ASC) from Opportunity where Id in :oppIds] : null;
        
        if(leads != null && !leads.isEmpty()){
            System.debug('***** found leads');
            allObjects.addAll(leads);
        }
        
        if(opps != null && !opps.isEmpty()){
            System.debug('***** found opps');
            allObjects.addAll(opps);
        }
        
        System.debug('***** number of parents: ' + allObjects.size());
        
        List<SObject> sobjList = new List<SObject>();
        if(allObjects != null && !allObjects.isEmpty()){
            for(SObject sobj : allObjects){
                //Lead lead = (Lead) sobj;
                tasks = sobj.getSObjects('Tasks');
                if(tasks != null && !tasks.isEmpty()){
                    System.debug('***** number of tasks for lead: ' + tasks != null && !tasks.isEmpty() ? tasks.size() : 0);
                    Task task = tasks[0];
                    if(task != null){
                        sobj.put('Next_Task_Due_Date__c', task.ActivityDate != null ? task.ActivityDate : null);
                        sobjList.add(sobj);
                    }else{
                        sobj.put('Next_Task_Due_Date__c', null);
                        sobjList.add(sobj);
                    }
                }else{
                    sobj.put('Next_Task_Due_Date__c', null);
                    sobjList.add(sobj);
                }
            }
            
            update sobjList;
        }
    }
    
    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Task> (Trigger.New),Map<Id,Task> (Trigger.OldMap)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : Before Update
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void initiateCReq(List<Task> newTaskList){
        string cREQ_TaskLabel= System.label.cREQ_Task;
        string errorDetail = system.label.Space;
        List<Database.SaveResult> dbSaveList = new List<Database.SaveResult> ();
        try{
            List<Task> creqTasks = new List<Task> ();
            List<id> leadIds = new List<id> ();
            for(task record :newTaskList) {
                if(record.Subject.equalsIgnoreCase(cREQ_TaskLabel)) {
                    creqTasks.add(record);
                    leadIds.add(record.whoid);
                    
                }
            }
            
            Map<id,Lead> mapleads = new Map<id,Lead>([select Id, cREQ_Type__c from lead where id in :leadIds]);
            
            for(task record :creqTasks ) {
                record.type__c = RCTYPE;
                record.subType__c=mapleads.get(record.whoid).cREQ_Type__c;
                
            }
            
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);        
        } 
        
    }
    
}