/**
*
*Name        : NI_Header_Controller 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 26th May, 2017
*Purpose     : Controller for header and footer
*
**/ 

public with sharing class  NI_Header_Controller {
    //Variables
    @AuraEnabled
	public String apiKeySetting 			{get;set;}
    @AuraEnabled
	public String wrapperNameSetting 	{get;set;}
    
    //Constructor
    public NI_Header_Controller(){
        Authentication_Wrapper__c setting = Authentication_Wrapper__c.getInstance();
        apiKeySetting = setting.API_Key__c;
        wrapperNameSetting = setting.Application_Name__c; 
    }
    
    /**
    * @method : getData3
    * @purpose : get the wrapper data for header and footer.
    *
    */
    @auraEnabled
    public static String getData3(String wrapperName, String locale, String apiKey){
        String urlData = 'https://flux.ni.com/wrapper-markup/1/wrapper/' + wrapperName + '.json?locale=' + locale + '&ni-api-key=' + apiKey;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndPoint(urlData);
        req.setHeader('Datatype', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setTimeout(3000);
        if(!Test.isRunningTest()){
            HttpResponse res= h.send(req);
            System.debug('res='+res.getBody());
            return res.getBody();
        }
        else{
            NI_Mock_Http_Response responseMock = new NI_Mock_Http_Response();
            return responseMock.respond(req).getBody();
        }
    }
    
    /**
    * @method : getData1
    * @purpose : return the response body
    *
    */
    @auraEnabled
    public static String getData1(){
        return 'res.getBody()';
    }
    
    /**
    * @method : getHiearchySettings
    * @purpose : return the Authentication_Wrapper__c custom setting instance
    *
    */
    @AuraEnabled
    public static Authentication_Wrapper__c getHiearchySettings(){
        return Authentication_Wrapper__c.getInstance();
    }
}