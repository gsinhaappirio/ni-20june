@isTest(SeeAllData=true)
public class Reset_Test_User_Passwords_Test {

    @isTest
    public static void initTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        List<SelectOption> options = controller.statusOptions;
        controller.allNewPassword = 'password1';
        controller.allNewStatus = 'INACTIVE';
        controller.updateAllUsers();
        controller.updateEachUser();
        Test.stopTest();
    }

    @isTest
    public static void validatePasswordTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        // unacceptable passwords
        System.assertEquals(controller.isValidPassword('abcdefg'), false);
        System.assertEquals(controller.isValidPassword('ABCde'), false);
        System.assertEquals(controller.isValidPassword('ab12345'), false);
        System.assertEquals(controller.isValidPassword('Ab2'), false);

        // acceptable password
        System.assertEquals(controller.isValidPassword('Abcdefg123'), true);

        Test.stopTest();
    }

    @isTest
    public static void resetPasswordNotAcceptedTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        controller.newPassword = 'abc123';
        controller.resetPassword();

        System.assertEquals(controller.active, 0);

        Test.stopTest();
    }

    @isTest
    public static void resetPasswordAcceptedTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        controller.newPassword = 'Abcd123';
        controller.resetPassword();

        Test.stopTest();
    }

    @isTest
    public static void activateAllTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.IsActive = true;
        u.isTestUser__c = true;
        insert u;

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        controller.activateAll();

        System.assertEquals(u.IsActive, true);

        Test.stopTest();
    }

    @isTest
    public static void deactivateAllTest() {
        PageReference pageRef = new PageReference('/apex/Reset_Test_User_Passwords');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.IsActive = false;
        u.isTestUser__c = true;
        insert u;

        Reset_Test_User_Passwords controller = new Reset_Test_User_Passwords();
        controller.deactivateAll();

        System.assertEquals(u.IsActive, false);

        Test.stopTest();
    }

}