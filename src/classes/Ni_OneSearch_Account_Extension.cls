public class Ni_OneSearch_Account_Extension {

    public Lead lead{get;set;}

    public List<Ni_Cmdm_Account_Search_Result> allResults{get;set;}
    public List<Ni_Cmdm_Account_Search_Result> results{get;set;}
    public Account account{get;set;}

    public String selectedIndex{get;set;}
    public String selectedCountry{get;set;}
    public List<SelectOption> selectableCountries{get;set;}
    public Boolean countryError{get;set;}

    public Boolean editing{get;set;}
    public Boolean firedSearch{get;set;}
    public Boolean firedCreate{get;set;}

    public List<Schema.FieldSetMember> leftFields{get;set;}
    public List<Schema.FieldSetMember> rightFields{get;set;}

    public String client{get;set;}
    public String screenWidthText{get;set;}
    public Boolean showSearchForm{get;set;}
    public Boolean developmentMode{get;set;}

    public String errorTitle{get;set;}
    public String errorMessage{get;set;}
    public Boolean done{get;set;}
    public String temp{get;set;}
    public String returnUrl{get;set;}
    public String type{get;set;}
    public String navigateToUrl{get;set;}

    public Map<String, List<FilterCriteria>> filter {get;set;}
    public String updatedFilterCategory {get;set;}
    public String updatedFilterName {get;set;}
    public Boolean filtered{get;set;}
    public String filterString{get;set;}
    
    public Boolean showDuplicates{get;set;}
    
    public List<Account> duplicateAccounts{get;set;}

    private Integer maxLocalResults = 5;
    private Integer maxCmdmResults = 15;

    public Boolean readyToSearch{get;set;}
    private Map<String, Country__c> countryMap{get;set;}

    public Ni_OneSearch_Account_Extension(ApexPages.StandardController controller) {

        lead = (Lead) controller.getRecord();
        account = (Account) createObjectFromParameters(System.currentPageReference().getParameters());
        
        returnUrl = System.currentPageReference().getParameters().get('returnUrl');
        
        //workaround for Android related bug:
        //only attaches encoded return url parameters when first called from OneSearch_Contacts.page
        if ( returnUrl!=null && getEncodedUrlPart(returnUrl)==null ){
            String retUrlParams = getEncodedUrlPart( System.currentPageReference().getUrl() );
            //System.debug('String retUrlParams: '+retUrlParams);
            returnUrl = (retUrlParams==null) ?  returnUrl : returnUrl+retUrlParams;
        }
                                                            
        
        client                  = System.currentPageReference().getParameters().get('client') != null ? System.currentPageReference().getParameters().get('client') : '';
        String fName            = System.currentPageReference().getParameters().get('firstname'); 
        String importParameter  = System.currentPageReference().getParameters().get('import');
        showDuplicates = false;
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Country__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String query = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues){
           query += s.getDescribe().getName() + ',';
        }
        // Trim last comma
        query = query.subString(0, query.length() - 1);
        query += ' FROM Country__c WHERE Embargoed__c = FALSE ORDER BY Full_Name__c ASC';
        List<Country__c> countries = Database.query(query);
        countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }

        if ( importParameter!=null && (importParameter.toLowerCase().trim().equals('true') || importParameter.toLowerCase().trim().equals('yes')  || importParameter.toLowerCase().trim().equals('1')) ){
            editing = true;
            gatherFieldSetForImport();
        }else {
            readyToSearch = readyToSearch(account);
        }

    }
    
    /*******************************
        * returns the custom encoded part of the given url if exists
        * encoder in: OneSearch_Contacts.page decoder in OneSearch_Accounts.page 
        * Wrote by Gergely (gergely2.lakatos@ni.com)
    *******************************/
    private String getEncodedUrlPart( String url )
    {
        Pattern MyPattern = Pattern.compile('Be61n.*3Ndl0');
        Matcher MyMatcher = MyPattern.matcher(url);

        if ( MyMatcher.find() )
            return '?'+MyMatcher.group();
        
        return null;
    }

    public PageReference performSearch(){
        List<Ni_Cmdm_Account_Search_Result> cmdmResults = cmdmSearch(account);
        List<Ni_Cmdm_Account_Search_Result> localResults = localSearch(account);
        results = mergeResults(cmdmResults, localResults);
        if(Test.isRunningTest()){
            List<Account> accounts = new List<Account>();
            for(Integer i = 0; i < 10; i++){
                Account account = new Account();
                account.Name = 'NI';
                account.ShippingStreet = '123 Fake St';
                account.ShippingCity = 'Austin';
                account.ShippingCountry = 'US';
                accounts.add(account);
            }

            results = new List<Ni_Cmdm_Account_Search_Result>();
            for(Integer i = 0; i < 10; i++){
                Ni_Cmdm_Account_Search_Result result = new Ni_Cmdm_Account_Search_Result();
                result.sourceSystem = 'Salesforce';
                result.account = accounts.get(i);
                results.add(result);
            }
        }
        allResults = results;
        results = applyFilterToResults(allResults);
        filter = buildFilterOptions(results);
        readyToSearch = false;
        return null;
    }

    private void gatherFieldSetForImport(){
        List<Schema.FieldSetMember> fields;
        fields = SObjectType.Account.FieldSets.OneSearch.getFields();

        Double half = fields.size()/2.0;
        Integer leftSize;
        if(Math.floor(half) == half){
            leftSize = half.intValue();
        }else{
            leftSize = Math.ceil(Double.valueOf(half)).intValue();
        }

        Integer rightSize = fields.size() - leftSize;
        leftFields = new List<Schema.FieldSetMember>();
        rightFields = new List<Schema.FieldSetMember>();

        for(Integer i = 0; i < leftSize; i++){
            leftFields.add(fields.get(i));
        }

        for(Integer i = leftSize; i < fields.size(); i++){
            rightFields.add(fields.get(i));
        }
        
        Set<Country__c> alreadyAdded = new Set<Country__c>();
        selectableCountries = new List<SelectOption>();
        selectableCountries.add(new SelectOption('', 'Please Select...'));
        for(Country__c country : countryMap.values()){
            if(!alreadyAdded.contains(country)){
                selectableCountries.add(new SelectOption(country.Name, country.Full_Name__c));
                alreadyAdded.add(country);
            }
            if(account.shippingCountry != null && (account.shippingCountry.trim().toUpperCase().equals(country.Name.toUpperCase()) || account.shippingCountry.toUpperCase().equals(country.Full_Name__c.toUpperCase()))){
                selectedCountry = country.Name;
            }
        }
    }

    private Boolean readyToSearch(SObject sobj){

        if(sobj == null){
            return false;
        }

        //confirm that all the necessary fields are available to search
        Map<String, Schema.SObjectField> fieldMap;
        fieldMap = Schema.SObjectType.Account.fields.getMap();

        Map<String, String> params = new Map<String, String>();
        for (String fieldName : fieldMap.keySet()) {
            Schema.SObjectField field = fieldMap.get(fieldName);
            Schema.DescribeFieldResult describe = field.getDescribe();
            if(describe != null && describe.isCreateable() && !describe.isNillable() && !describe.isDefaultedOnCreate()){
                //field is required
                if(sobj.get(fieldName) == null){
                    return false;
                }
            }
        }
        return true;
    }

    private Map<String, String> createParameterMap(sObject sobj, Boolean importing){
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        Map<String, String> params = new Map<String, String>();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                if(sobj.get(fieldName) != null && !fieldName.toLowerCase().equals('id')){
                    params.put(fieldName, String.valueOf(sobj.get(fieldName)));
                }
            } catch (Exception e)    {
                System.debug(e.getStackTraceString());
            }
        }

        if(importing){
            params.put('import', 'true');
        }

        if(returnUrl != null && !returnUrl.equals('')){
            params.put('returnUrl', returnUrl);
        }

        if(client != null && client != ''){
            params.put('client', client);
        }

        return params;
    }

    private SObject createObjectFromParameters(Map<String, String> params){

        SObject sobj = new Account();
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Account.fields.getMap();

        for (String fieldName : fieldMap.keySet()) {
            try {
                SObjectField fieldToken = fieldMap.get(fieldName);
                DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                String fieldStringValue = params.get(fieldName);
                Object fieldValue = null;
                if(fieldDescribe.getSOAPType() == Schema.SoapType.Boolean)
                    fieldValue = Boolean.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Date)
                    fieldValue = Date.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.DateTime)
                    fieldValue = DateTime.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Double)
                    fieldValue = Double.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.ID)
                    fieldValue = ID.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Integer)
                    fieldValue = Integer.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.String)
                    fieldValue = fieldStringValue;
                if(fieldValue!=null)
                    sobj.put(fieldName, fieldValue);
            } catch (Exception e)    {
                // Intentional capture to prevent from crashing
                System.debug(e.getStackTraceString());
            }
        }

        return sobj;
    }

    public PageReference createObject(){
        if(selectedCountry == null || selectedCountry.equals('')){
            countryError = true;
        }else{
            account.id = null;
            account.shippingCountry = selectedCountry.toUpperCase().trim();
            Database.SaveResult result = Database.insert(account, false);
            firedCreate = true;
            if (!result.isSuccess()) {
            
                // If the duplicate rule is an alert rule, we can try to bypass it
                if(showDuplicates){
                    Database.DMLOptions dml = new Database.DMLOptions(); 
                    dml.DuplicateRuleHeader.AllowSave = true;
                    Database.SaveResult sr2 = Database.insert(account, dml);
                    if (sr2.isSuccess()) {
                        done = true;
                        System.debug('Duplicate account has been inserted in Salesforce!');
                    }
                }else{
                    List<Id> duplicateIds = new List<Id>();
                    Datacloud.DuplicateResult duplicateResult;
                    // Insertion failed due to duplicate detected
                    for(Database.Error duplicateError : result.getErrors()){
                        duplicateResult = ((Database.DuplicateError)duplicateError).getDuplicateResult();
                    }
                    
                    for(Datacloud.MatchResult duplicateMatchResult : duplicateResult.getMatchResults()) {
                        for(Datacloud.MatchRecord duplicateMatchRecord : duplicateMatchResult.getMatchRecords()) {
                            duplicateIds.add(duplicateMatchRecord.getRecord().Id);
                        }
                    }
                    
                    duplicateAccounts = [Select Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, Owner.Name, OwnerId from Account where Id in :duplicateIds];
                    if(duplicateAccounts != null && !duplicateAccounts.isEmpty()){
                        showDuplicates = true;
                    }
                }
                
            }else{
                done = true;
            }
            
        }
        return null;
    }
    
    public void hideDuplicates(){
        firedCreate = false;
        done = false;
        showDuplicates = false;
    }

    public PageReference generateSearchUrl(){
        Map<String,String> params = createParameterMap(account, false);
        PageReference pr = new PageReference('/apex/onesearch_accounts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        navigateToUrl = pr.getUrl();
        System.debug('generateSearchUrl() navigateToUrl: '+navigateToUrl);
        return null;
    }

    public PageReference generateImportUrl(){
        editing = true;
        if(selectedIndex != null && !selectedIndex.equals('')){
            account = results.get(Integer.valueOf(selectedIndex)).account;
        }
        //returnUrl = System.currentPageReference().getUrl();
        Map<String,String> params = createParameterMap(account, true);
        PageReference pr = new PageReference('/apex/onesearch_accounts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        navigateToUrl = pr.getUrl();
        System.debug('generateImportUrl() navigateToUrl: '+navigateToUrl);
        return null;
    }

    public PageReference importObject() {
        editing = true;
        if(selectedIndex != null && !selectedIndex.equals('')){
            account = results.get(Integer.valueOf(selectedIndex)).account;
        }

        Map<String,String> params = createParameterMap(account, true);

        PageReference pr = new PageReference('/apex/onesearch_accounts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        pr.setRedirect(true);
        return pr;
    }

    public PageReference updateFilter(){

        List<FilterCriteria> fcList = new List<FilterCriteria>();

        for(String key : filter.keySet()){
            fcList.addAll(filter.get(key));
        }

        fcList.sort();

        //String filterParams = filterString != null ? filterString + ',' : '';
        String filterParams = '';
        for(FilterCriteria fc : fcList){
            if(fc.selected){
                filterParams += fc.category + ':' + fc.name + '~';
            }
        }

        filterString = filterParams != '' ? filterParams.substring(0, filterParams.length()-1) : '';

        results = applyFilterToResults(allResults);

        return null;
    }

    public PageReference clearFilters(){
        filterString = '';
        results = applyFilterToResults(allResults);
        return null;
    }

    public Map<String, String> getCurrentFilters(){

        if(filterString == null || filterString.equals('')){
            filterString = System.currentPageReference().getParameters().get('filter');
        }

        if(filterString == null || filterString.equals('')){
            filtered = false;
            return null;
        }

        List<String> filterParams = filterString.split('~');
        Map<String, String> currentParams = new Map<String, String>();
        for(String token : filterParams){
            String key = token.split(':').get(0).trim().toUpperCase();
            String value = token.split(':').get(1).trim().toUpperCase();
            currentParams.put(value, key);
        }

        if(currentParams != null && !currentParams.isEmpty()){
            filtered = true;
        }

        return currentParams;
    }

    public Map<String, List<FilterCriteria>> buildFilterOptions(List<Ni_Cmdm_Account_Search_Result> searchResults){

        if(searchResults == null || searchResults.isEmpty()){
            return null;
        }

        Map<String, List<FilterCriteria>> filterMap = new Map<String, List<FilterCriteria>>();
        Map<String, String> currentFilters = getCurrentFilters();

        Map<String, boolean> alreadyAddedValues = new Map<String, Boolean>();

        // add filter for country
        List<FilterCriteria> countries = new List<FilterCriteria>();
        for(Ni_Cmdm_Account_Search_Result result : searchResults){
            if(result.account.shippingcountry != null &&
               countryMap.get(result.account.shippingcountry.trim().toUpperCase()) != null &&
               alreadyAddedValues.get(countryMap.get(result.account.shippingcountry.trim().toUpperCase()).Name) == null){

                FilterCriteria fc = new FilterCriteria('Country', countryMap.get(result.account.shippingcountry.trim().toUpperCase()).Name, currentFilters != null && currentFilters.get(countryMap.get(result.account.shippingcountry.trim().toUpperCase()).Name) != null ? true : false);
                alreadyAddedValues.put(countryMap.get(result.account.shippingcountry.trim().toUpperCase()).Name, true);
                //alreadyAddedValues.put(result.account.shippingcountry.trim().toUpperCase(), true);
                countries.add(fc);
            }else if(result.account.shippingcountry != null &&
                     countryMap.get(result.account.shippingcountry.trim().toUpperCase()) == null &&
                     alreadyAddedValues.get(result.account.shippingcountry.trim().toUpperCase()) == null){
                FilterCriteria fc = new FilterCriteria('Country', result.account.shippingcountry, currentFilters != null && currentFilters.get(result.account.shippingcountry.trim().toUpperCase()) != null ? true : false);
                alreadyAddedValues.put(result.account.shippingcountry.trim().toUpperCase(), true);
                countries.add(fc);
            }
            /*
            else if(result.account.shippingcountry != null &&
                     alreadyAddedValues.get(result.account.shippingcountry.trim().toUpperCase()) == null &&
                     countrySettingsMap.get(result.account.shippingcountry.trim().toUpperCase()) != null &&
                     alreadyAddedValues.get(countrySettingsMap.get(result.account.shippingcountry.trim().toUpperCase()).Name) == null){
                FilterCriteria fc = new FilterCriteria('Country', result.account.shippingcountry, currentFilters != null && currentFilters.get(result.account.shippingcountry.trim().toUpperCase()) != null ? true : false);
                alreadyAddedValues.put(result.account.shippingcountry.trim().toUpperCase(), true);
                countries.add(fc);
            }
            */
        }
        countries.sort();
        if(!countries.isEmpty()){
            filterMap.put('Country', countries);
        }

        //add filter for source system
        List<FilterCriteria> systems = new List<FilterCriteria>();
        for(Ni_Cmdm_Account_Search_Result result : searchResults){
            if(result.sourceSystem != null && alreadyAddedValues.get(result.sourceSystem.trim().toUpperCase()) == null){
                FilterCriteria fc = new FilterCriteria('Sources', result.sourceSystem, currentFilters != null && currentFilters.get(result.sourceSystem.trim().toUpperCase()) != null ? true : false);
                alreadyAddedValues.put(result.sourceSystem.trim().toUpperCase(), true);
                systems.add(fc);
            }
        }
        if(!countries.isEmpty()){
            filterMap.put('Sources', systems);
        }

        return filterMap;
    }

    public List<Ni_Cmdm_Account_Search_Result> applyFilterToResults(List<Ni_Cmdm_Account_Search_Result> results){
        //public Map<String, List<FilterCriteria>> filter {get;set;}
        List<Ni_Cmdm_Account_Search_Result> filteredResults = new List<Ni_Cmdm_Account_Search_Result>();
        Map<String,String> currentFilters = getCurrentFilters();
        if(currentFilters != null){

            for(Ni_Cmdm_Account_Search_Result result : results){
                if(result.account.shippingcountry != null &&
                   countryMap.get(result.account.shippingcountry.trim().toUpperCase()) != null &&
                   currentFilters.get(countryMap.get(result.account.shippingcountry.trim().toUpperCase()).Name) != null){
                       // country is inside filter
                       filteredResults.add(result);
                }else if(result.account.shippingcountry != null &&
                         countryMap.get(result.account.shippingcountry.trim().toUpperCase()) == null &&
                         currentFilters.get(result.account.shippingcountry.trim().toUpperCase()) != null){
                    filteredResults.add(result);
                }
            }

            results = !filteredResults.isEmpty() ? filteredResults : results;

            filteredResults = new List<Ni_Cmdm_Account_Search_Result>();
            for(Ni_Cmdm_Account_Search_Result result : results){
                if(result.sourceSystem != null && currentFilters.get(result.sourceSystem.trim().toUpperCase()) != null){
                    // country is inside filter
                    filteredResults.add(result);
                }
            }

            results = !filteredResults.isEmpty() ? filteredResults : results;

        }

        return results;
    }

    public PageReference search() {
        Map<String,String> params = createParameterMap(account, false);
        PageReference pr = new PageReference('/apex/onesearch_accounts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        pr.setRedirect(true);
        return pr;
    }

    private List<Ni_Cmdm_Account_Search_Result> cmdmSearch(Account account){

        firedSearch = true;

        try{
            Ni_Cmdm_Search_Api cmdm = new Ni_Cmdm_Search_Api();
            results = cmdm.searchForAccount(account, 'ALL', 10, false, true);
        }catch(Exception e){
            system.debug('xxxxxxxxxxxxxxxxxxxxxxx'+e);
            results = null;
        }

        /*
        if(orgContactId != null && !orgContactId.equals('')){
            System.debug('********** have an org contact id');
            for(Ni_Cmdm_Contact_Search_Result result : results){
                System.debug('********** org contact id: ' + result.orgContactId);
                if(result.orgContactId != null && result.orgContactId.equals(orgContactId)){
                    System.debug('********** found an org contact id match : ' + result.contact.Id);
                    //navigateToPage('/' + result.contact.Id);
                    result.contact.FirstName = 'tyler';
                }
            }
        }
        */

        return results;
    }

    private List<Ni_Cmdm_Account_Search_Result> localSearch(SObject sobj){



        String searchQuery = '';
        searchQuery += account.name != null ? account.name + ' ' : '';
        searchQuery += account.ShippingCity != null ? account.ShippingCity + ' ' : '';
        searchQuery += account.Shipping_Street_1__c != null ? account.Shipping_Street_1__c + ' ' : '';
        searchQuery += account.ShippingStreet != null ? account.ShippingStreet + ' ' : '';
        searchQuery += account.ShippingPostalCode != null ? account.ShippingPostalCode + ' ' : '';
        searchQuery = searchQuery.trim();

        // can't perform search if the string is one character or less
        if(searchQuery.replace('*','').length() <= 1){
            return null;
        }

        searchQuery = String.escapeSingleQuotes(searchQuery);

        String query = 'FIND :searchQuery IN ALL FIELDS RETURNING Account (Id, Name, Alias1__c, Alias2__c, Alias3__c, ShippingStreet, Shipping_Street_1__c, Shipping_Street_2__c, Shipping_Street_3__c, Shipping_Street_4__c, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode, Phone where Account_Status__c = \'Active\')';
        List<List<SObject>> results = search.query(query);
        List<Account> accounts = (List<Account>)results[0];

        List<Ni_Cmdm_Account_Search_Result> localResults = new List<Ni_Cmdm_Account_Search_Result>();

        for(Account account : accounts){
            Ni_Cmdm_Account_Search_Result r = new Ni_Cmdm_Account_Search_Result();
            r.account = account;
            r.sourceSystem = 'Salesforce';
            localResults.add(r);
        }

        return localResults;
    }

    @TestVisible
    private List<Ni_Cmdm_Account_Search_Result> mergeResults(List<Ni_Cmdm_Account_Search_Result> cmdmResults, List<Ni_Cmdm_Account_Search_Result> localResults){

        if((cmdmResults == null || cmdmResults.isEmpty()) && (localResults == null || localResults.isEmpty())){
            return null;
        }

        List<Ni_Cmdm_Account_Search_Result> mergedResults = new List<Ni_Cmdm_Account_Search_Result>();
        Map<String, Ni_Cmdm_Account_Search_Result> mergedResultsMap = new Map<String, Ni_Cmdm_Account_Search_Result>();

        if(cmdmResults != null && !cmdmResults.isEmpty()){
            for(Ni_Cmdm_Account_Search_Result cmdmResult : cmdmResults){
                if(cmdmResult.sourceSystem != null && cmdmResult.sourceSystem.toLowerCase().equals('salesforce')){
                    mergedResults.add(cmdmResult);
                    mergedResultsMap.put(String.valueOf(cmdmResult.account.id), cmdmResult);
                }
            }
        }

        if(localResults != null && !localResults.isEmpty()){
            for(Ni_Cmdm_Account_Search_Result localResult : localResults){
                if(localResult.sourceSystem != null && localResult.sourceSystem.toLowerCase().equals('salesforce') &&
                    mergedResultsMap.get(String.valueOf(localResult.account.id)) == null){
                        mergedResults.add(localResult);
                }
            }
        }

        if(cmdmResults != null && !cmdmResults.isEmpty()){
            for(Ni_Cmdm_Account_Search_Result cmdmResult : cmdmResults){
                if(cmdmResult.sourceSystem != null && !cmdmResult.sourceSystem.toLowerCase().equals('salesforce')){
                    mergedResults.add(cmdmResult);
                }
            }
        }

        return mergedResults == null || mergedResults.size() == 0 ? null : mergedResults;
    }

    public class FilterCriteria implements Comparable {
        public String Category{get;set;}
        public String Name{get;set;}
        public Boolean selected{get;set;}

        public FilterCriteria(String category, String name, Boolean selected){
            this.category = category;
            this.name = name;
            this.selected = selected;
        }

        public Integer compareTo(Object compareTo) {
            FilterCriteria other = (FilterCriteria)compareTo;
            return this.name.toLowerCase().trim().compareTo((String)other.name.toLowerCase().trim());
        }

    }

}