public class UserTriggerController {
    
    public static void onBeforeInsert(List<User> newUsers){
        Set<Id> userIds = new Set<Id>();
        Set<Id> userIds2 = new Set<Id>();
        Map<Id, String> managersMap = new Map<Id, String>();
        List<User> subordinates = new List<User>();
        for(User user : newUsers){
            userIds.add(user.Id);
            userIds2.add(user.Id);
            if (user.Manager_Person_Number__c != null) {
                managersMap.put(user.Id, String.valueOf(user.Manager_Person_Number__c));
            }
            if (user.EmployeeNumber != null) {
                double empNum = Double.valueOf(user.EmployeeNumber);
                subordinates = [select Id From User Where IsActive = true And Manager_Person_Number__c = :empNum];
                if (subordinates != null && !subordinates.isEmpty()) {
                    for (User su : subordinates) {
                        userIds2.add(su.Id);
                        managersMap.put(su.Id, String.valueOf(user.EmployeeNumber));
                    }
                }
            }
        }
        UserHandler.updateManager(userIds2, managersMap);
        UserHandler.updateLicenseCost(userIds);
    }
    
    public static void onAfterUpdate(List<User> newUsers, Map<Id, User> oldUserMap){
        
        //update license cost if the profile changed
        Set<Id> userIds = new Set<Id>();
        for(User user : newUsers){
            User oldUser = oldUserMap.get(user.Id);
            if(user.ProfileId != oldUser.ProfileId){
                userIds.add(user.Id);
            }
        }
        
        if(userIds != null && !userIds.isEmpty()){
            UserHandler.updateLicenseCost(userIds);
        }
        
        // update manager, if changed
        Set<Id> userIds2 = new Set<Id>();
        Map<Id, String> managersMap = new Map<Id, String>();
        for(User user : newUsers){
            User oldUser = oldUserMap.get(user.Id);
            if(user.Manager_Person_Number__c != oldUser.Manager_Person_Number__c){
                userIds2.add(user.Id);
                if (user.Manager_Person_Number__c != null) {
                    managersMap.put(user.Id, String.valueOf(user.Manager_Person_Number__c));
                }
            }
        }
        
        
        if(userIds2 != null && !userIds2.isEmpty()){
            UserHandler.updateManager(userIds2, managersMap);
        }
        
        //Started By Meghana : To add/remove the permission set to the user if the user is/is not a curator.
        updateUserForPermissions(newUsers);
    }
    
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────   
    * Method called by trigger after user is inserted.
    * ──────────────────────────────────────────────────────────────────────────────────────────────────    
    * @param newUsers List<User>
    * @return void
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    */
    public static void onAfterInsert(List<User> newUsers){
        //update the permission set to the user if the user is/is not a curator.
        updateUserForPermissions(newUsers);
    } 
    
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────   
    * Method to add/remove the permission set to the user if the user is/is not a curator.
    * ──────────────────────────────────────────────────────────────────────────────────────────────────    
    * @param newUsers List<User>
    * @return void
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    */
    public static void updateUserForPermissions(List<User> newUsers){
        //update the permission set to the user if the user is/is not a curator.
        try{
            Set<User> curatorUserSet = new Set<User>();   
            Set<User> otherUserSet = new  Set<User>();
            Set<User> pIIUserSet = new  Set<User>();
            Set<Id> userIds = new Set<Id>();
            Map<Id,String> userErrorMap = new Map<Id,String>();
            List<PermissionSetAssignment> permissionAssignList = new List<PermissionSetAssignment>();
            List<PermissionSetAssignment> permissionDeleteList = new List<PermissionSetAssignment>();
            for(User user : newUsers){
                userIds.add(user.Id);
                if(user.Submitter_Role__c == Constants.NI_REVIEWER_ROLE){
                    pIIUserSet.add(user);
                }
                if(user.Submitter_Role__c == Constants.NI_CURATOR_ROLE){
                    curatorUserSet.add(user);
                    pIIUserSet.add(user);
                }else{
                    otherUserSet.add(user); 
                }
            }
            PermissionSet customPermSet = [select Id, Name 
                                           from PermissionSet 
                                           where Name =: Constants.NI_ARCHIVE_PERMISSION];
            List<PermissionSetAssignment> psaList = [SELECT ID, AssigneeId, PermissionSetId
                                                     FROM PermissionSetAssignment
                                                     WHERE AssigneeId IN : userIds AND PermissionSetId =: customPermSet.Id];
            for(User u : curatorUserSet){
                Boolean isPresent = false;
                for(PermissionSetAssignment ps : psaList){
                    if(ps.AssigneeId == u.Id){
                        isPresent = true;
                    }
                }
                if(!isPresent){
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.AssigneeId = u.Id;
                    psa.PermissionSetId = customPermSet.Id;
                    permissionAssignList.add(psa);
                }
            }
            
            PermissionSet customPIIPermSet = [select Id, Name 
                                              from PermissionSet 
                                              where Name =: Constants.KB_CREATE_PII_PERMISSION];
            
            List<PermissionSetAssignment> psaPIIList = [SELECT ID, AssigneeId, PermissionSetId
                                                        FROM PermissionSetAssignment
                                                        WHERE AssigneeId IN : userIds AND PermissionSetId =: customPIIPermSet.Id];
            
            for(User u : pIIUserSet){
                Boolean isPresent = false;
                for(PermissionSetAssignment ps : psaPIIList){
                    if(ps.AssigneeId == u.Id){
                        isPresent = true;
                    }
                }
                if(!isPresent){
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.AssigneeId = u.Id;
                    psa.PermissionSetId = customPIIPermSet.Id;
                    permissionAssignList.add(psa);
                }
            }
            
            //Insert the permission set for Curator user role
            if(permissionAssignList != null && !permissionAssignList.isEmpty()){
                Database.SaveResult[] sveInsertResults = Database.insert(permissionAssignList,true);
                for(Database.SaveResult sv : sveInsertResults){
                    if(!sv.isSuccess()){
                        Database.Error[] err = sv.getErrors();
                        for(Database.Error e : err){
                            userErrorMap.put(sv.getId(), e.getMessage());
                        }
                    }
                }
            }
            
            for(User u : otherUserSet){
                for(PermissionSetAssignment ps : psaList){
                    if(ps.AssigneeId == u.Id){
                        permissionDeleteList.add(ps);
                    }
                }
            }
            //Delete the permission set for other user roles
            if(permissionDeleteList != null && !permissionDeleteList.isEmpty()){
                Database.DeleteResult[] deleteResults = Database.delete(permissionDeleteList,true);
                for(Database.DeleteResult dr : deleteResults){
                    if(!dr.isSuccess()){
                        Database.Error[] err = dr.getErrors();
                        for(Database.Error e : err){
                            userErrorMap.put(dr.getId(), e.getMessage());
                        }
                    }
                }
            }
            if(userErrorMap != null && !userErrorMap.isEmpty()){
                for(User usr : newUsers){
                    if(userErrorMap.containsKey(usr.Id)){
                        usr.addError(userErrorMap.get(usr.Id));
                    }
                }
            }
        }catch(Exception e){
            newUsers[0].addError(e.getMessage());
        }
    } 
    //Ended By Meghana : To add/remove the permission set to the user if the user is/is not a curator.
}