public with sharing class Ni_Sales_Lead_Edit_Extension{
    
    public Lead lead {get;set;}
    
    public Account account {get;set;}
    public String accountId {get;set;}
    
    public Contact contact {get;set;}
    public String contactId {get;set;}
    
    public Boolean runAssignmentRules {get;set;}
    public Boolean showErrors{get;set;}
    public Boolean updateSelections{get;set;}
    
    public List<SelectOption> selectableCountries{get;set;}
    public String selectedCountry{get;set;}
    
    public Ni_Sales_Lead_Edit_Extension(ApexPages.StandardController controller) {
        lead = (Lead) controller.getRecord();
        if(lead.id == null){
            lead = createLeadFromParameters(System.currentPageReference().getParameters());
        }
        if(lead.ownerId == null){
            lead.ownerId = UserInfo.getUserId();
        }
        
        if(lead.recordTypeId == null){
            lead.recordTypeId = [select Id from RecordType where DeveloperName = 'Sales_Generated_Lead' and SobjectType = 'Lead'].Id;
        }
        
        List<Country_Settings__c> countrySettingsList = [Select Name, Full_Name__c from Country_Settings__c where Embargoed__c=false order by Full_Name__c asc];
        selectableCountries = new List<SelectOption>();
        selectableCountries.add(new SelectOption('', 'Select a Country'));
        for(Country_Settings__c countrySetting : countrySettingsList){
            selectableCountries.add(new SelectOption(countrySetting.Name, countrySetting.Full_Name__c));
            if(lead.country != null && (lead.country.trim().toUpperCase().equals(countrySetting.Name.toUpperCase().trim()) || lead.country.toUpperCase().equals(countrySetting.Full_Name__c.toUpperCase().trim()))){
                selectedCountry = countrySetting.Name;
            }
        }
        
        showErrors = false;
        updateSelections = false;
    }
    
    private Lead createLeadFromParameters(Map<String, String> params){
        SObject sobj = new Lead();
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Lead.fields.getMap();
        
        for (String fieldName : fieldMap.keySet()) {
            try {
                SObjectField fieldToken = fieldMap.get(fieldName);
                DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                String fieldStringValue = params.get(fieldName);
                Object fieldValue = null;
                if(fieldDescribe.getSOAPType() == Schema.SoapType.Boolean)
                    fieldValue = Boolean.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Date)
                    fieldValue = Date.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.DateTime)
                    fieldValue = DateTime.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Double)
                    fieldValue = Double.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.ID)
                    fieldValue = ID.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Integer)
                    fieldValue = Integer.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.String)
                    fieldValue = fieldStringValue;
                if(fieldValue!=null)
                    sobj.put(fieldName, fieldValue);
            } catch (Exception e)    { 
                // Intentional capture to prevent from crashing
                System.debug(e.getStackTraceString());
            }
        }
        
        return (Lead) sobj;
    }
    
    public PageReference refresh(){
        System.debug('***** contact id:' + contactId);
        System.debug('***** account id:' + accountId);
        
        if(accountId != null && !accountId.equals('')){
            account = [select Name, Id from Account where Id =: accountId];
            if(account != null){
                lead.account__c = account.Id;
            }
            updateSelections = true;
        }
        
        if(contactId != null && !contactId.equals('')){
            contact = [select Name, FirstName, LastName, Id, AccountId from Contact where Id =: contactId];
            if(contact != null){
                lead.contact__c = contact.Id;
            }
            updateSelections = true;
        }
        
        if(contact != null && account != null){
            if(contact.accountId != account.Id){
                showErrors = true;
            }
        }else if(contact == null){
            showErrors = false;
        }
        
        return null;
    }
    
    public PageReference updateSelections(){
        return null;
    }
    
    public PageReference save(){
        try {
            if(runAssignmentRules != null && runAssignmentRules){
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.useDefaultRule = true; 
                lead.setOptions(dmlOpts);
                lead.status = 'Qualified';
            }else if(lead.ownerId != UserInfo.getUserId() && String.valueOf(lead.ownerId).substring(0,3).equals('005')){
                // lead is a user
                lead.status = 'Assigned';
            }else if(lead.ownerId != UserInfo.getUserId() && String.valueOf(lead.ownerId).substring(0,3).equals('00G')){
                // lead is a queue
                lead.status = 'Qualified';
            }
            
            lead.country = selectedCountry;
            
            if(lead.Id != null){
                // lead already exists, just update
                update lead;
            }else{
                // lead doesn't exist, insert it
                insert lead;
            }
        }
        catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        
        PageReference pr = new PageReference('/' + lead.Id);
        return pr;
    }
}