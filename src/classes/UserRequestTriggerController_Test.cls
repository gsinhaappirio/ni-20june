@isTest(SeeAllData=true)
public class UserRequestTriggerController_Test {

    @isTest
    public static void onBeforeInsertTest(){
        Test.startTest();
        User toClone = [Select Id, Name from User where isActive = TRUE LIMIT 1];
        List<User_Request__c> newRequestList = new List<User_Request__c>();
        for(Integer i = 0; i < 10; i++){
            User_Request__c request = new User_Request__c();
            request.Clone_User__c = toClone.Id;
            newRequestList.add(request);
        }
        insert newRequestList;
        
        UserRequestTriggerController.onBeforeInsert(newRequestList);
        Test.stopTest();
    }
    
    @isTest
    public static void onBeforeUpdateTest(){
        Test.startTest();
        User toClone = [Select Id, Name from User where isActive = TRUE AND ProfileId != null AND UserRoleId != null ORDER BY CreatedDate ASC LIMIT 1];
        User toClone2 = [Select Id, Name from User where isActive = TRUE AND ProfileId != null AND UserRoleId != null ORDER BY CreatedDate DESC LIMIT 1];
        
        String randomString = generateRandomString();
        String randomEmail = randomString + '@ni.com';
        
        HCM_Record__c hcm = new HCM_Record__c();
        hcm.Email__c = randomEmail;
        hcm.Employee_Number__c = '987654321';
        hcm.HCM_Region__c = 'US';
        hcm.First_Name__c = 'Johnny';
        hcm.Last_Name__c = randomString;
        insert hcm;
        
        Skill__c skill = new Skill__c();
        skill.Name = 'Football';
        insert skill;
        
        String queueName = [Select Name from Group where type = 'Queue' ORDER BY CreatedDate DESC LIMIT 1].Name;
        
        List<User_Request__c> newRequestList = new List<User_Request__c>();
        Map<Id, User_Request__c> oldRequestMap = new Map<Id, User_Request__c>();
        
        for(Integer i = 0; i < 10; i++){
            User_Request__c request = new User_Request__c();
            request.Email__c = randomEmail;
            request.Status__c = 'Processing';
            request.Attempt_User_Creation__c = true;
            request.Clone_User__c = toClone.Id;
            request.Queues__c = queueName;
            request.Profile__c = 'NI Sales User';
            newRequestList.add(request);
        }
        
        insert newRequestList;
        
        List<User_Request_Skill__c> skillList = new List<User_Request_Skill__c>();
        
        for(User_Request__c request : newRequestList){
            User_Request_Skill__c requestedSkill = new User_Request_Skill__c();
            requestedSkill.Skill__c = skill.Id;
            requestedSkill.Exclusive__c = requestedSkill.Exclusive__c;
            skillList.add(requestedSkill);
            requestedSkill.User_Request__c = request.Id;
            request.Clone_User__c = toClone2.Id;
            oldRequestMap.put(request.Id, request);
        }
        
        insert skillList;
        
        UserRequestTriggerController.onBeforeUpdate(newRequestList, oldRequestMap);
        UserRequestTriggerController.onAfterUpdate(newRequestList, oldRequestMap);
        Test.stopTest();
    }
    
    private static String generateRandomString() {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 24) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
}