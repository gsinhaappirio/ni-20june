@isTest(SeeAllData=true)
global class ContactOwnerSync_Test {
    
    @isTest(SeeAllData=true)
    private static void handleRecordTest(){
        
        User user1 = TestUtils.getUserForProfile('Last1', 'NI Sales User');
        insert user1;
        User user2 = TestUtils.getUserForProfile('Last2', 'NI Sales User');
        insert user2;
        Account account = TestUtils.getAccount('accountname');
        account.OwnerId = user1.Id;
        insert account;
        Contact contact = TestUtils.getContact('lastname', account.Id);
        contact.OwnerId = user2.Id;
        insert contact;
        
        String query = 'SELECT Id, OwnerId, Account.OwnerId FROM Contact WHERE Owner_Compare__c = false';
        ContactOwnerSync cos = new ContactOwnerSync();
        cos.HandleRecord((SObject) contact, new BatchUtil(query, new ContactOwnerSync()));
    }
    
    @isTest(SeeAllData=true)
    private static void handleRecordNegativeTest(){
        
        Account account = TestUtils.getAccount('accountname');
        insert account;
        
        String query = 'SELECT Id, OwnerId, Account.OwnerId FROM Contact WHERE Owner_Compare__c = false';
        ContactOwnerSync cos = new ContactOwnerSync();
        cos.HandleRecord((SObject) account, new BatchUtil(query, new ContactOwnerSync()));
    }
    
}