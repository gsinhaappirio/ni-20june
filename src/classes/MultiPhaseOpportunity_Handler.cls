public with sharing class MultiPhaseOpportunity_Handler {

	 /**
    * @author        mmurillo
    * @created       06/03/15
    */
    public static void updateAmount(Map<id,Multi_Phase_Opportunity__c> newMultiPhaseMap, Map<id,Multi_Phase_Opportunity__c> oldMultiPhaseMap){
    	
    	List<CurrencyType> currencyTypeList = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
        Map<String, CurrencyType> currencyTypes = new Map<String, CurrencyType>();
        
        for(CurrencyType currencyType : currencyTypeList){
            currencyTypes.put(currencyType.ISOCode, currencyType);
        }

        for(Multi_Phase_Opportunity__c newMultiPhase : newMultiPhaseMap.values()){
            
            Multi_Phase_Opportunity__c oldMultiPhase = oldMultiPhaseMap.get(newMultiPhase.id);
            
            //if multiphase opp currency was updated, it's needed to recalculate the amount
            if(oldMultiPhase.CurrencyIsoCode != newMultiPhase.CurrencyIsoCode  ) {
            	
            	CurrencyType oldMPCurrencyType = currencyTypes.get(oldMultiPhase.CurrencyISOCode);
	            CurrencyType newMpCurrencyType = currencyTypes.get(newMultiPhase.currencyIsoCode);
	            
	            Decimal convertedAmount = (oldMultiPhase.Current_Amount_del__c/oldMPCurrencyType.ConversionRate) * newMpCurrencyType.ConversionRate;
		            
	            newMultiPhase.Current_Amount_del__c = convertedAmount;
             }

        }

    }

}