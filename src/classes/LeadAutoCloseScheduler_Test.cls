@isTest
private class LeadAutoCloseScheduler_Test {

    @isTest(Seealldata=true)
    static void test() {
        Test.startTest();
        
        Lead lead = new Lead();
        lead.firstName = 'Test';
        lead.lastName = 'Lead';
        lead.Company = 'NI';
        lead.status = 'Qualified';
        lead.Country = 'US';
        lead.CreatedDate = System.today()-60;
        insert lead;
        Lead lead2 = new Lead();
        lead2.firstName = 'Test';
        lead2.lastName = 'Lead';
        lead2.Company = 'NI';
        lead2.status = 'Assigned';
        lead2.Country = 'US';
        lead2.CreatedDate = System.today()-60;
        insert lead2;
        
        // Schedule the test job
        String jobId = System.schedule('testBasicScheduledApex', LeadAutoCloseScheduler.CRON_EXP, new LeadAutoCloseScheduler());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(LeadAutoCloseScheduler.CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime)); 
    }
}