@isTest(SeeAllData=true)
public class LeadTriggerController_Test {
    
    @isTest
    public static void onBeforeInsertTest(){
        
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.lastName = 'Test' + String.valueOf(i);
            lead.LeadSource = 'Sales - Sales';
            lead.Company = 'NI';
            lead.Country = 'US';
            leads.add(lead);
        }
        
        LeadTriggerController.onBeforeInsert(leads);
        Test.stopTest();
        
    }
    
    @isTest
    public static void onAfterInsertTest(){
        
        Test.startTest();
        
        Skill__c skill = new Skill__c();
        skill.Name = 'Academic';
        skill.Type__c = 'Lead';
        insert skill;
        
        Skill__c skill2 = new Skill__c();
        skill.Name = 'American English';
        skill.Type__c = 'Lead';
        insert skill2;
        
        Group queue = [Select Id, Name from Group where Type = 'Queue' AND (NOT Name Like '%RH%') LIMIT 1];
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.lastName = 'Test' + String.valueOf(i);
            lead.LeadSource = 'Sales - Sales';
            lead.Company = 'NI';
            lead.Country = 'US';
            lead.OwnerId = queue.Id;
            lead.Language__c = 'American English';
            lead.Industry_Segment__c = 'Academic';
            leads.add(lead);
        }
        
        insert leads;
        
        Map<Id, Lead> leadsMap = new Map<Id, Lead>();
        for(Lead lead : leads){
            lead.OwnerId = queue.Id;
            leadsMap.put(lead.Id, lead);
        }
        
        LeadTriggerController.onAfterInsert(leadsMap);
        Test.stopTest();
        
    }
    
    @isTest
    public static void onBeforeUpdateTest(){
        
        Test.startTest();
        
        Skill__c skill = new Skill__c();
        skill.Name = 'Academic';
        skill.Type__c = 'Lead';
        insert skill;
        
        Skill__c skill2 = new Skill__c();
        skill.Name = 'American English';
        skill.Type__c = 'Lead';
        insert skill2;
        
        Group queue = [Select Id, Name from Group where Type = 'Queue' AND (NOT Name Like '%RH%') LIMIT 1];
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.lastName = 'Test' + String.valueOf(i);
            lead.LeadSource = 'Sales - Sales';
            lead.Company = 'NI';
            lead.Country = 'US';
            lead.OwnerId = queue.Id;
            lead.Language__c = 'American English';
            leads.add(lead);
        }
        
        insert leads;
        
        Map<Id, Lead> newLeads = new Map<Id, Lead>();
        Map<Id, Lead> oldLeadMap = new Map<Id, Lead>();
        for(Lead lead : leads){
            oldLeadMap.put(lead.Id,lead);
            lead.Industry_Segment__c = 'Academic';
            newLeads.put(lead.Id,lead);
        }
        
        LeadTriggerController.onBeforeUpdate(newLeads, oldLeadMap);
        Test.stopTest();
        
    }
    
    @isTest
    public static void onAfterUpdateTest(){
        
        Test.startTest();
        List<Lead> oldLeads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.lastName = 'Test' + String.valueOf(i);
            lead.LeadSource = 'Sales - Sales';
            lead.Company = 'NI';
            lead.Country = 'US';
            lead.Language__c = 'American English';
            oldLeads.add(lead);
        }
        
        insert oldLeads;
        
        Map<Id, Lead> newLeads = new Map<Id, Lead>();
        Map<Id, Lead> oldLeadMap = new Map<Id, Lead>();
        for(Lead lead : oldLeads){
            oldLeadMap.put(lead.Id,lead);
            lead.Industry_Segment__c = 'Academic';
            newLeads.put(lead.Id,lead);
        }
        
        LeadTriggerController.onBeforeUpdate(newLeads, oldLeadMap);
        Test.stopTest();
        
    }

}