@isTest
public class NI_DispatcherSRViewExtension_Test{

    @isTest
    public static void redirectToExternalObjectTest() {
        
        NI_Service_Request__c sr  = new NI_Service_Request__c(site__c = 'NIC', oracle_Service_Request_Id__c = 123456);
        insert sr;
        
        NI_DispatcherSRViewExtension dispatcher = new NI_DispatcherSRViewExtension(new ApexPages.StandardController(sr));
        apexpages.currentpage().getparameters().put('id' , sr.id);
        
        dispatcher.redirectToExternalObject();
    }

}