/**
*
*Name        : NI_ArticleUtils 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 07th June, 2017
*Purpose     : Invocable Apex Class to be used in Processes
*
**/

public with sharing class NI_ArticleUtils {  
    /**
    * @method : publishArticle
    * @purpose : Invocable method to be called from Process for publishing the article
    *
    */
    @InvocableMethod
    public static void publishArticle(List<String> articleIdList)
    {
        List<Knowledge__kav> upadteList = new List<Knowledge__kav>();
        for(Id artId : articleIdList){
            Knowledge__kav art = new Knowledge__kav(Id=artId, Status__c=NI_Constants.NI_Published_Status, Submit_For_Approval__c=false);
            upadteList.add(art);
        }
        if(upadteList.size()>0){
            update upadteList;
        }
        for(knowledge__kav  articleId : [Select id,knowledgearticleid,PublishStatus from knowledge__kav where id in : articleIdList]){
      		KbManagement.PublishingService.publishArticle(articleId.knowledgearticleid, true);
        } 
   }
}