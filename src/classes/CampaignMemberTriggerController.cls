public class CampaignMemberTriggerController {
    
    public static void onAfterInsert(Map<id,CampaignMember> newCampaignMembers){
        Set<Id> campaignMemberIds = newCampaignMembers.keySet();
        CampaignMemberHandler.updateLeads(campaignMemberIds);
    }
    
}