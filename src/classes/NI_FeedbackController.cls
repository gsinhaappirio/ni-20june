/**
*
*Name        : NI_FeedbackController 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 18th May, 2017
*Purpose     : Controller for Article Feedback
*
**/

public class NI_FeedbackController {
    
    /**
    * @method : insertArticleFeedback
    * @purpose : inserting feedback for a article.
    *
    */
    @AuraEnabled
    public static String insertArticleFeedback(String recordId, Integer rating, String summary, String details){
        try{
            KnowledgeArticleVersion artcl = [SELECT Title, KnowledgeArticleId, OwnerId FROM KnowledgeArticleVersion where id =:recordId LIMIT 1];
            Article_Feedback__c articleFeedback = new Article_Feedback__c();
            articleFeedback.Rating__c = ''+rating;
            articleFeedback.Article_Id__c = recordId;
            articleFeedback.Summary__c = summary;
            articleFeedback.Details__c = details;
            articleFeedback.Knowledge_Article_Version_Id__c = artcl.KnowledgeArticleId;
            if(UserInfo.getUserId() != null){
            	articleFeedback.OwnerId = UserInfo.getUserId();
            }
            articleFeedback.Article_Name__c = artcl.Title;
            insert articleFeedback;
        	return 'SUCCESS';
        }catch(Exception e){
            return 'FAILURE';
        }
    }
    
    /**
    * @method : getPicklist
    * @purpose : getting picklist values for summary field on article feedback.
    *
    */
    @AuraEnabled
    public static list<string> getPicklist(){
		Schema.DescribeFieldResult fieldResult = Article_Feedback__c.Summary__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> summaryValues = new List<String>();
        for( Schema.PicklistEntry f : ple){
  			summaryValues.add(f.getValue());
        }
        return summaryValues;   
    }
}