global class ContactOwnerSyncScheduler implements Schedulable {

    public static String CRON_EXP = '0 0 0 3 9 ? 2022';

    global void execute(SchedulableContext ctx) {
        String query = 'SELECT Id, Account.OwnerId FROM Contact WHERE AccountId != null AND Owner_Compare__c = false';
        Database.executeBatch(new BatchUtil(query, new ContactOwnerSync()), 2000);
    }
}