public class Ni_Sales_Contact_Phone_Extension {

    public Contact contact{get;set;}
    public Boolean editing{get;set;}
    public String phoneType{get;set;}
    ApexPages.StandardController controller{get;set;}

    public Ni_Sales_Contact_Phone_Extension(ApexPages.StandardController controller){
        contact = (Contact) controller.getRecord();
        editing = false;
        this.controller = controller;
    }
    
    public PageReference edit(){
        editing = true;
        contact = (Contact) controller.getRecord();
        return null;
    }
    
    public PageReference save(){
        editing = false;
        controller.save();
        contact = (Contact) controller.getRecord();
        return null;
    }
    
    public PageReference cancel(){
        editing = false;
        contact = (Contact) controller.getRecord();
        return null;
    }

}