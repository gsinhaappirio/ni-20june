/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            AccountTriggerController
Description:     This class invokes methods of AccountHandler. 

Date                            Version            Author                                    Summary of Changes 
---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class AccountTriggerController{
    private Boolean m_isExecuting = false;
    private Integer batchSize = 0;
    
    
    /*Constructor*/
    public AccountTriggerController (Boolean isExecuting, Integer size){
        m_isExecuting = isExecuting;
        batchSize = size;
    }
    
    public static void onBeforeInsert(List<Account> accounts){
        AccountHandler.updateCountry(accounts);
    }
    
    public static void onBeforeUpdate(List<Account> accounts, Map<Id, Account> oldAccountMap){
        List<Account> accountList = new List<Account>();
        for(Account account : accounts){
            Account oldAccount = oldAccountMap.get(account.Id);
            if(account.ShippingCountry != oldAccount.ShippingCountry){
                accountList.add(account);
            }
        }
        AccountHandler.updateCountry(accounts);
    }
    
    /**********************************************************************************************************************
* Author: 
* Date: 
* Param: List<Account> (Trigger.New),Map<Id,Account> (Trigger.OldMap)
* Return: void
* Description: This method Invokes all after update methods of AccountHandler.
* Summary of Changes :   - Initial version
**********************************************************************************************************************/
    public static void onAfterUpdate(List<Account> newAccountList, Map<Id,Account> oldAccountMap){
        String errorDetail = system.label.Space;
        
        Set<Id> ownerAccountIds = new Set<Id>();
        for(Account account : newAccountList){
            Account oldAccount = oldAccountMap.get(account.Id);
            if(account.ownerId != oldAccount.ownerId){
                ownerAccountIds.add(account.Id);
            }
        }
        
        if(!ownerAccountIds.isEmpty()){
            AccountHandler.syncContactOwnersWithAccountOwners(ownerAccountIds);
        }
        
    }
    
    /**********************************************************************************************************************
* Author: 
* Date: 
* Param: List<Account> (Trigger.New)
* Return: void
* Description: This method Invokes all after undelete methods of AccountHandler.
* Summary of Changes :   - Initial version
**********************************************************************************************************************/
    public static void onAfterUndelete(List<Account> newAccountList){
        set<ID> AccountId = new set<Id>();
        for(Account acc : newAccountList)
        {
            AccountId.add(acc.Id);
        }
        
        string errorDetail = system.label.Space;
        try{  
            AccountHandler.UndeleteAccount(AccountId); 
        }Catch(Exception e){errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
                            System.debug(errorDetail);}
    }
    
    public static void onAfterDelete(Map <Id, Account> oldAccountMap){
        Set<Id> accountIds = new Set<Id>();
        for(Account account : oldAccountMap.values() ){
            if(account.masterrecordid != null){
                accountIds.add(account.masterrecordid);
            }
        }
        AccountHandler.syncContactOwnersWithAccountOwners(accountIds);
    }
}