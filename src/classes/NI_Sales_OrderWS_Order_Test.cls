@isTest
public with sharing class NI_Sales_OrderWS_Order_Test {
    
    @isTest(seeAllData=true)
    static void validateGetOrder(){
        
        Test.setMock(WebServiceMock.class, new NI_Sales_OrderWS_OrderRequest_MockImpl());
        String orderId = '123';
        NI_Sales_OrderWS_OrderRequest.GetOrderBindingQSPort service = new NI_Sales_OrderWS_OrderRequest.GetOrderBindingQSPort();
        NI_Sales_OrderWS_Order.GetOrderResponse_element response = service.getOrder(orderId);
        
        System.assertEquals('S', response.responseStatus);
        System.assertEquals(123, response.OrderInformation.OrderHeader.headerId);
        System.assertEquals('partNumber1', response.OrderInformation.orderLines.orderLine[0].partNumber);
        System.assertEquals('partNumber2', response.OrderInformation.orderLines.orderLine[1].partNumber);
        
    }

}