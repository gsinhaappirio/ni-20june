public class NewRequestHelpTriggerController {

    public static void onBeforeInsert(List<New_Request_Help__c> newRequestHelpList){
        NewRequestHelpHandler.determineDestinationEmail(newRequestHelpList);
        NewRequestHelpHandler.populateRequesterLocale(newRequestHelpList);
    }
    
    public static void onAfterInsert(List<New_Request_Help__c> newRequestHelpList){
        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
        Boolean useJiraApi = rhs.Use_Jira_API__c;
        if(useJiraApi){
            NewRequestHelpHandler.createJiraTicket(newRequestHelpList);
        }
    }
    
    public static void onBeforeUpdate(List<New_Request_Help__c> newRequestHelpList, Map<Id, New_Request_Help__c> oldRequestHelpMap){
        List<New_Request_Help__c> rhList = new List<New_Request_Help__c>();
        for(New_Request_Help__c requestHelp : newRequestHelpList){
            // category changed, update destination email
            if(requestHelp.Category__c != oldRequestHelpMap.get(requestHelp.Id).category__c){
                rhList.add(requestHelp);
            }
        }
        if(!rhList.isEmpty()){
            NewRequestHelpHandler.determineDestinationEmail(rhList);
        }
    }
    
    public static void onAfterUpdate(List<New_Request_Help__c> newRequestHelpList, Map<Id, New_Request_Help__c> oldRequestHelpMap){
        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
        Boolean useJiraApi = rhs.Use_Jira_API__c;
        if(useJiraApi){
            NewRequestHelpHandler.synchronizeJiraTicket(newRequestHelpList);
        }
    }
    
}