/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NI_Order_Extension_Test {

    @isTest(seeAllData=true)
    static void validateGetOrderLineList(){
        Test.setMock(WebServiceMock.class, new NI_Sales_OrderWS_OrderRequest_MockImpl());   
        NI_Order__c order = new NI_Order__c();
        order.oracle_Header_Id__c = 123;
        List<NI_Sales_Sales_OrderLineDTO> orderLinesList;
        NI_Order_Extension orderExt = new NI_Order_Extension(new ApexPages.StandardController(order));
        orderLinesList = orderExt.getOrderLineList();
        System.assertEquals('partNumber1', orderLinesList[0].partNumber);
        System.assertEquals('partNumber2', orderLinesList[1].partNumber);
    }
}