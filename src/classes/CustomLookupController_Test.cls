@isTest(SeeAllData=true)
public with sharing class CustomLookupController_Test {

    @isTest(SeeAllData=true)
    public static void getOpportunitiesTest() {
        
        CustomLookupController clc = new CustomLookupController(null);
        
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        insert account1;
        
        Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
        insert opp;
        
        List<Opportunity> opps = CustomLookupController.getOpportunities('Test');
        System.assertNotEquals(opps.size(), 0);
        
        List<Opportunity> opps2 = CustomLookupController.getOpportunities('ThereShouldntbeanyoppwiththisvalue');
        System.assertEquals(opps2.size(), 0);
        
        List<Opportunity> opps3 = CustomLookupController.getOpportunities('');
        System.assertEquals(opps3, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void getLeadsTest() {
        Lead lead = new Lead(FirstName='Test', LastName='Test', Company='Test Company', LeadSource='Sales - Sales', Status='Accepted', Country='US',
                             Phone='123456789', Description='Test Description', State='TX', Street='Street 1', City='City 1', postalCode = 'PC1');
        insert lead; 
        
        List<Lead> leads = CustomLookupController.getLeads('Test');
        System.assertNotEquals(leads.size(), 0);
        
        List<Lead> leads2 = CustomLookupController.getLeads('ThereShouldntbeanyleadwiththisvalue');
        System.assertEquals(leads2.size(), 0);
        
        List<Lead> leads3 = CustomLookupController.getLeads('');
        System.assertEquals(leads3, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void getContactsTest() {
    
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        insert account1;
        
        Contact contact = new Contact(FirstName='Test', LastName='Test', Account=account1, Email='test@emailtest.com',
                             Phone='123456789');
        insert contact; 
        
        List<Contact> contacts1 = CustomLookupController.getContacts('Test');
        System.assertNotEquals(contacts1.size(), 0);
        
        List<Contact> contacts2 = CustomLookupController.getContacts('ThereShouldntbeanycontactwiththisvalue');
        System.assertEquals(contacts2 .size(), 0);
        
        List<Contact> contacts3 = CustomLookupController.getContacts('');
        System.assertEquals(contacts3 , null);
        
    }
}