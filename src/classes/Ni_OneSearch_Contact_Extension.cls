public class Ni_OneSearch_Contact_Extension {
    
    public Lead lead{get;set;}
    
    public List<Ni_Cmdm_contact_Search_Result> results{get;set;}
    public Contact contact{get;set;}
    public String accountId{get;set;}
    public String contactId{get;set;}
    
    public String selectedIndex{get;set;}
    
    public Boolean editing{get;set;}
    public Boolean readyToSearch{get;set;}
    public Boolean firedSearch{get;set;}
    public Boolean firedCreate{get;set;}
    
    public List<Schema.FieldSetMember> leftFields{get;set;}
    public List<Schema.FieldSetMember> rightFields{get;set;}
    
    public String client{get;set;}
    public String screenWidthText{get;set;}
    public Boolean showSearchForm{get;set;}
    
    public String errorTitle{get;set;}
    public String errorMessage{get;set;}
    public Boolean done{get;set;}
    public String temp{get;set;}
    public String returnUrl{get;set;}
    public String type{get;set;}
    
    private Integer maxLocalResults = 5;
    private Integer maxCmdmResults = 15;
    
    public Boolean showDuplicates{get;set;}
    public List<Contact> duplicateContacts{get;set;}
    public String duplicateMessage{get;set;}
    public Boolean allowSave{get;set;}
    
    public Ni_OneSearch_Contact_Extension(ApexPages.StandardController controller) {
        
        lead = (Lead) controller.getRecord();
        contact = (Contact) createObjectFromParameters(System.currentPageReference().getParameters());
        if(contact.account == null){
            contact.account = new Account();
        }
        
        showDuplicates = false;
        client = System.currentPageReference().getParameters().get('client');
        allowSave = true;
        type = 'contact';
        
        String importParameter = System.currentPageReference().getParameters().get('import');
        
        if(importParameter != null && (importParameter.toLowerCase().trim().equals('true') || importParameter.toLowerCase().trim().equals('yes') || importParameter.toLowerCase().trim().equals('1'))){
            editing = true;
            gatherFieldSetForImport();
        }else {
            readyToSearch = readyToSearch(contact);
        }
        
    }
    
    public PageReference performSearch(){
        List<Ni_Cmdm_Contact_Search_Result> localResults = localSearch(contact);
        List<Ni_Cmdm_Contact_Search_Result> cmdmResults = cmdmSearch(contact);
        results = mergeResults(cmdmResults, localResults);
        readyToSearch = false;
        return null;
    }
    
    private void gatherFieldSetForImport(){
        List<Schema.FieldSetMember> fields;
        fields = SObjectType.Contact.FieldSets.OneSearch.getFields();
        
        Double half = fields.size()/2.0;
        Integer leftSize;
        if(Math.floor(half) == half){
            leftSize = half.intValue();
        }else{
            leftSize = Math.ceil(Double.valueOf(half)).intValue();
        }
        
        Integer rightSize = fields.size() - leftSize;
        leftFields = new List<Schema.FieldSetMember>();
        rightFields = new List<Schema.FieldSetMember>();
        
        for(Integer i = 0; i < leftSize; i++){
            leftFields.add(fields.get(i));
        }
        
        for(Integer i = leftSize; i < fields.size(); i++){
            rightFields.add(fields.get(i));
        }
    }
    
    private Boolean readyToSearch(SObject sobj){
        
        if(sobj == null){
            return false;
        }
        
        Contact c = (Contact) sobj;
        
        if(c.email != null && !c.email.equals('')){
            return true;
        }else if(c.firstName != null && !c.firstName.equals('') && c.lastName != null && !c.lastName.equals('')){
            return true;
        }else if(c.firstName != null && !c.firstName.equals('') && c.account != null && c.account.name != null && !c.account.name.equals('')){
            return true;
        }else if(c.lastName != null && !c.lastName.equals('') && c.account != null && c.account.name != null && !c.account.name.equals('')){
            return true;
        }else if(c.phone != null && !c.phone.equals('')){
            return true;
        }
        
        return false;
    }
    
    private Map<String, String> createParameterMap(sObject sobj, Boolean importing){
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Contact.fields.getMap();
        Map<String, String> params = new Map<String, String>();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                if(sobj.get(fieldName) != null && !sobj.get(fieldName).equals('') && !fieldName.toLowerCase().equals('id')){
                    params.put(fieldName, String.valueOf(sobj.get(fieldName))); 
                }
                
            } catch (Exception e)    { 
                System.debug(e);
            }
        }
        
        Contact c = (Contact) sobj;
        sobj = c.account;
        
        schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        for (String fieldName : schemaFieldMap.keySet()) {
            try {
                if(sobj.get(fieldName) != null && !sobj.get(fieldName).equals('') && !fieldName.toLowerCase().equals('id') && !fieldName.toLowerCase().equals('phone')){
                    params.put(fieldName, String.valueOf(sobj.get(fieldName))); 
                }
                
            } catch (Exception e)    { 
                System.debug(e);
            }
        }
        
        // have to get the account info also
        //Contact c = (Contact) sobj;
        
        if(importing){
            params.put('import', 'true');
        }
        
        if(client != null && !client.equals('')){
            params.put('client', client);
        }
        
        if(lead != null && lead.Id != null){
            params.put('id', lead.Id);
        }
        
        return params;
    }
    
    private SObject createObjectFromParameters(Map<String, String> params){
        
        SObject sobj = new Contact();
        
        if(params == null || params.isEmpty()){
            return sobj;
        }
        
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Contact.fields.getMap();
        
        for (String fieldName : fieldMap.keySet()) {
            try {
                SObjectField fieldToken = fieldMap.get(fieldName);
                DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                String fieldStringValue = params.get(fieldName);
                Object fieldValue = null;
                if(fieldDescribe.getSOAPType() == Schema.SoapType.Boolean)
                    fieldValue = Boolean.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Date)
                    fieldValue = Date.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.DateTime)
                    fieldValue = DateTime.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Double)
                    fieldValue = Double.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Integer)
                    fieldValue = Integer.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Id)
                    fieldValue = Id.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.String)
                    fieldValue = fieldStringValue;
                if(fieldValue!=null){
                    if(!fieldName.toLowerCase().trim().equals('recordtypeid')){
                        sobj.put(fieldName, fieldValue);
                    }
                }
            } catch (Exception e)    { 
                // Intentional capture to prevent from crashing
                System.debug(e);
            }
        }
        
        fieldMap = Schema.SObjectType.Account.fields.getMap();
        SObject saccount = new Account();
        
        for (String fieldName : fieldMap.keySet()) {
            try {
                SObjectField fieldToken = fieldMap.get(fieldName);
                DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                String fieldStringValue = params.get(fieldName);
                Object fieldValue = null;
                if(fieldDescribe.getSOAPType() == Schema.SoapType.Boolean)
                    fieldValue = Boolean.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Date)
                    fieldValue = Date.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.DateTime)
                    fieldValue = DateTime.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Double)
                    fieldValue = Double.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Integer)
                    fieldValue = Integer.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.Id)
                    fieldValue = Id.valueOf(fieldStringValue);
                else if(fieldDescribe.getSOAPType() == Schema.SoapType.String)
                    fieldValue = fieldStringValue;
                if(fieldValue!=null)
                    saccount.put(fieldName, fieldValue);
            } catch (Exception e)    { 
                // Intentional capture to prevent from crashing
                System.debug(e);
            }
        }
        
        Contact c = (Contact) sobj;
        c.account = (Account) saccount;
        
        // now we have to handle the screenpop params
        // these should be changed to the standard names in the future
        // and this section of code should be removed
        if(c.firstName == null || c.firstName.equals('')){
            if(params.containsKey('first_name')){
                c.firstName = System.currentPageReference().getParameters().get('first_name');
            }
        }
        
        if(c.lastName == null || c.lastName.equals('')){
            if(params.containsKey('last_name')){
                c.lastName = System.currentPageReference().getParameters().get('last_name');
            }
        }
        
        //email_address
        if(c.email == null || c.email.equals('')){
            if(params.containsKey('email_address')){
                c.email = params.get('email_address');
            }
        }
        
        if(params.containsKey('account_site_name')){
            if(c.account.name == null || c.account.name.equals('')){
                c.account.name = params.get('account_site_name');
            }
        }
        
        if(params.containsKey('city')){
            if(c.account.ShippingCity == null || c.account.ShippingCity.equals('')){
                c.account.ShippingCity = params.get('city');
            }
        }
        
        if(params.containsKey('postal_code')){
            if(c.account.ShippingPostalCode == null || c.account.ShippingPostalCode.equals('')){
                c.account.ShippingPostalCode = params.get('postal_code');
            }
        }
        
        if(params.containsKey('phone_country')){
            if(c.phone_country_code__c == null || c.phone_country_code__c.equals('')){
                c.phone_country_code__c = params.get('phone_country');
            }
        }
        
        if(params.containsKey('phone_area')){
            if(c.phone_area_code__c == null || c.phone_area_code__c.equals('')){
                c.phone_area_code__c = params.get('phone_area');
            }
        }
        
        if(params.containsKey('phone_number')){
            if(c.phone_number__c == null || c.phone_number__c.equals('')){
                c.phone_number__c = params.get('phone_number');
            }
        }
        
        if(params.containsKey('phone_ext')){
            if(c.phone_extension__c == null || c.phone_extension__c.equals('')){
                c.phone_extension__c = params.get('phone_ext');
            }
        }
        
        /*
        c.phone = '';
        c.phone += c.phone_country_code__c != null && !c.phone_country_code__c.equals('') ? '+' + c.phone_country_code__c + ' ' : '';
        c.phone += c.phone_area_code__c != null && !c.phone_area_code__c.equals('') ? c.phone_area_code__c + ' ' : '';
        c.phone += c.phone_number__c != null && !c.phone_number__c.equals('') ? c.phone_number__c + ' ' : '';
        c.phone += c.phone_extension__c != null && !c.phone_extension__c.equals('') ? c.phone_extension__c + ' ' : '';
        c.phone = c.phone.trim();
        
        if(c.phone.equals('')){
            c.phone = null;
        }
        */
        //contact.phone = null;
        
        return c;
    }
    
    public PageReference selectObject(){
        if(lead != null){
            lead.contact__c = contactId != null ? Id.valueOf(contactId) : null;
            lead.account__c = accountId != null ? Id.valueOf(accountId) : null;
            try {
                update lead;
            } catch (DmlException e) {
                System.debug(e);
            }
        }
        return null;
    }
    
    public PageReference updateLead(){
        if(lead != null){
            lead.contact__c = contactId != null ? Id.valueOf(contactId) : null;
            lead.account__c = accountId != null ? Id.valueOf(accountId) : null;
            try {
                update lead;
            } catch (DmlException e) {
                System.debug(e);
            }
        }
        return null;
    }
    
    public PageReference selectAccount() {
        if(accountId != null && !accountId.equals('')){
            Account account = [Select Id, Name, ShippingStreet, ShippingCountry from Account where Id = :accountId];
            if(account != null && account.Id != null){
                contact.accountId = accountId;
            }
        }
        
        return null;
    }
    
    public void hideDuplicates(){
        firedCreate = false;
        done = false;
        showDuplicates = false;
    }
    
    public PageReference createObject(){
    
        contact.id = null;
        contact.account = null;
        
        Database.SaveResult result = Database.insert(contact, false);
        
        if (!result.isSuccess()) {
            
            // If the duplicate rule is an alert rule, we can try to bypass it
            if(showDuplicates){
                firedCreate = true;
                Database.DMLOptions dml = new Database.DMLOptions(); 
                dml.DuplicateRuleHeader.AllowSave = true;
                Database.SaveResult sr2 = Database.insert(contact, dml);
                if (sr2.isSuccess()) {
                    done = true;
                }
            }else{
                List<Id> duplicateIds = new List<Id>();
                Datacloud.DuplicateResult duplicateResult;
                // Insertion failed due to duplicate detected
                for(Database.Error error : result.getErrors()){
                    if (error instanceof Database.DuplicateError){
                        firedCreate = true;
                        duplicateResult = ((Database.DuplicateError) error).getDuplicateResult();
                        duplicateMessage = duplicateResult.getErrorMessage();
                        allowSave = duplicateResult.getDuplicateRule().equalsIgnoreCase('exact_email') ? false : true;
                        for(Datacloud.MatchResult duplicateMatchResult : duplicateResult.getMatchResults()) {
                            for(Datacloud.MatchRecord duplicateMatchRecord : duplicateMatchResult.getMatchRecords()) {
                                duplicateIds.add(duplicateMatchRecord.getRecord().Id);
                            }
                        }
                        
                        duplicateContacts = [Select Id, Name, AccountId, Account.Name, Account.ShippingStreet, Account.ShippingCity, Account.ShippingState, Account.ShippingCountry, Owner.Name, OwnerId from Contact where Id in :duplicateIds];
                        if(duplicateContacts != null && !duplicateContacts.isEmpty()){
                            showDuplicates = true;
                        }
                    }else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error.getMessage()));
                    }
                }
            }
            
        }else{
            done = true;
        }
        
        return null;
        
    }
    
    public PageReference importObject() {
        editing = true;
        
        // if this is populated, it was specifically passed in. we want to carry it along
        // eg, lead sending its account to prepopulate newly created contacts
        String accountId;
        if(contact.accountId != null){
            accountId = contact.accountId;
        }
        
        if(selectedIndex != null && !selectedIndex.equals('')){
            contact = results.get(Integer.valueOf(selectedIndex)).contact;
            if(accountId != null && !accountId.equals('')){
                contact.accountId = accountId;
            }
        }
        
        Map<String,String> params = createParameterMap(contact, true);
        
        PageReference pr = new PageReference('/apex/onesearch_contacts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference addContact(){
        editing = true;
        return null;
    }
    
    public PageReference search() {
        Map<String,String> params = createParameterMap(contact, false);
        PageReference pr = new PageReference('/apex/onesearch_contacts');
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));
        }
        pr.setRedirect(true);
        return pr;
    }
    
    private List<Ni_Cmdm_Contact_Search_Result> cmdmSearch(Contact contact){
        
        if(!readyToSearch(contact)){
            return null;
        }
        
        firedSearch = true;
        
        try{
            Ni_Cmdm_Search_Api cmdm = new Ni_Cmdm_Search_Api();
            results = cmdm.searchForContact(contact, 'ALL', 10, false, true);
        }catch(Exception e){
            results = null;
        }
        
        return results;
    }
    
    private List<Ni_Cmdm_Contact_Search_Result> localSearch(SObject sobj){
        firedSearch = true;
        
        String searchQuery = '';
        
        // phone and account name don't produce great results. full name + city + email seems to be the best combo
        Contact c = (Contact)sobj;
        searchQuery += c.FirstName != null ? c.FirstName + ' ' : '';
        searchQuery += c.LastName != null ? c.LastName + ' ' : '';
        searchQuery += c.MailingCity != null ? c.MailingCity + ' ' : '';
        searchQuery += c.Email != null ? c.Email + ' ' : '';
        searchQuery = searchQuery.trim();
        
        // can't perform search if the string is one character or less
        if(searchQuery.replace('*','').length() <= 1){
            return null;
        }
        
        String query = 'FIND :searchQuery IN ALL FIELDS RETURNING Contact (Id, FirstName, LastName, Department, Email, Phone_Country_Code__c, Phone_Area_Code__c, Phone_Number__c, Phone_Extension__c, Account.Name, Account.ShippingStreet, Account.ShippingCity, Account.ShippingState, Account.ShippingCountry, Account.ShippingPostalCode, Phone where Status__c = \'Active\')';
        List<List<SObject>> results = search.query(query);
        List<Contact> contacts = (List<Contact>)results[0];
        
        List<Ni_Cmdm_Contact_Search_Result> localResults = new List<Ni_Cmdm_Contact_Search_Result>();
        
        for(Contact contact : contacts){
            Ni_Cmdm_Contact_Search_Result r = new Ni_Cmdm_Contact_Search_Result();
            r.contact = contact;
            r.sourceSystem = 'Salesforce';
            r.score = 0.0;
            r.sourceId = String.valueOf(contact.Id);
            localResults.add(r);
        }
        
        return localResults;
    }
    
    private List<Ni_Cmdm_Contact_Search_Result> mergeResults(List<Ni_Cmdm_Contact_Search_Result> cmdmResults, List<Ni_Cmdm_Contact_Search_Result> localResults){
        
        if((cmdmResults == null || cmdmResults.isEmpty()) && (localResults == null || localResults.isEmpty())){
            return null;
        }
        
        List<Ni_Cmdm_Contact_Search_Result> mergedResults = new List<Ni_Cmdm_Contact_Search_Result>();
        Map<String, Ni_Cmdm_Contact_Search_Result> mergedResultsMap = new Map<String, Ni_Cmdm_Contact_Search_Result>();
        Integer localCount = 0;
        Integer cmdmCount = 0;
        
        if(cmdmResults != null && !cmdmResults.isEmpty()){
            for(Ni_Cmdm_Contact_Search_Result cmdmResult : cmdmResults){
                if(cmdmResult.sourceSystem != null && cmdmResult.sourceSystem.toLowerCase().equals('salesforce') && localCount < maxLocalResults){
                    localCount++;
                    mergedResults.add(cmdmResult);
                    mergedResultsMap.put(String.valueOf(cmdmResult.contact.id), cmdmResult);
                }else{
                    // all sfdc records will be first, so if we hit this, we can break
                    break;
                }
            }
        }else{
            maxLocalResults = 20;
        }
        
        if(localResults != null && !localResults.isEmpty()){
            for(Ni_Cmdm_Contact_Search_Result localResult : localResults){
                if(localCount < maxLocalResults){
                    if(localResult.sourceSystem != null && localResult.sourceSystem.toLowerCase().equals('salesforce') && localCount < maxLocalResults){
                        if(mergedResultsMap.get(String.valueOf(localResult.contact.id)) == null){
                            localCount++;
                            mergedResults.add(localResult);
                        }
                    }
                }else{
                    break;
                }
            }
        }
        
        if(cmdmResults != null && !cmdmResults.isEmpty()){
            for(Ni_Cmdm_Contact_Search_Result cmdmResult : cmdmResults){
                if(cmdmCount < maxCmdmResults){
                    if(cmdmResult.sourceSystem != null && !cmdmResult.sourceSystem.toLowerCase().equals('salesforce')){
                        cmdmCount++;
                        mergedResults.add(cmdmResult);
                    }
                }else{
                    break;
                }
            }
        }
        
        return mergedResults == null || mergedResults.size() == 0 ? null : mergedResults;
    }
    
}