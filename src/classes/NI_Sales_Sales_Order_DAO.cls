public with sharing class NI_Sales_Sales_Order_DAO {

    public List<NI_Sales_Sales_OrderLineDTO> getOrderLines(String orderId){
        List<NI_Sales_Sales_OrderLineDTO> orderLinesList = new List<NI_Sales_Sales_OrderLineDTO>();
        NI_Sales_OrderWS_OrderRequest.GetOrderBindingQSPort service = new NI_Sales_OrderWS_OrderRequest.GetOrderBindingQSPort();
        
        try {
            NI_Sales_OrderWS_Order.GetOrderResponse_element response = service.getOrder(orderId);
            
            if (response.responseStatus != 'S'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Order Lines ' + response.responseMessage));
                System.debug('Webservice error : ' + response.responseMessage);
            }
            else{
                NI_Sales_OrderWS_Order.OrderLines lines = response.OrderInformation.orderLines;
                
                if (lines.OrderLine != null){
                    for(Integer i = 0; i < lines.OrderLine.size(); i++) {
                        orderLinesList.add(mapOrderLineToDTO(lines.OrderLine[i]));
                    }
                }
            }
            
            
        } catch (System.CalloutException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Order Lines'));
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        
        return orderLinesList;
    }
    
    @testVisible
    private NI_Sales_Sales_OrderLineDTO mapOrderLineToDTO(NI_Sales_OrderWS_Order.OrderLine orderLine){
        
        return new NI_Sales_Sales_OrderLineDTO( orderLine.partNumber, orderLine.partDescription, orderLine.orderedQuantity,
                                                orderLine.unitListPrice.setScale(2), orderLine.unitSellingPrice.setScale(2));
    }    

}