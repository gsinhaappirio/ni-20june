/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            NI_NoteHandler
   Description:     This class contains support actions for triggers on Note
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   30th September 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class NI_NoteHandler {
        
    public static final string TRGRNAME='Note_Trigger';
    public static final string OPPORTUNITY ='006';
    public static final string WON = system.label.won;
     public static final string LOST = system.label.lost;
     public static final string ERRMSG = system.label.Opportunity_Errormsg ;
    
    /**********************************************************************************************************************
    * Author: 
    * Date:
    * Param: List<Note> (Trigger.New)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Insert
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/

      public static void OppLastActiveDate(List<Note> newNoteList){
          List<Opportunity> oppsToUpdate = new List<Opportunity>();
          List<Database.SaveResult> dbSaveList = new List<Database.SaveResult> ();
          List<Opportunity> opplist = new list<Opportunity>();
          List<Id> Oppidlist = new list<Id>();
       
          try{   
              for (Note nRecord : newNoteList) {
                  String nrecordid = nRecord.ParentId;
                  if( nrecordid.startsWithIgnoreCase(OPPORTUNITY)){
                      Oppidlist.add(nRecord.ParentId);
                  }       
              }
              opplist =[select Last_Activity_Date_Plain__c,Stagename,Close_Reason__c,Target_Application_Value__c from  Opportunity where id IN : Oppidlist];
              for (Note nRecord : newNoteList) {
                  for (Opportunity opp : opplist)
                  {
                     /*------- Check if the opportunity record meets the desired validation rule -------- */
                     if(((opp.StageName==WON)||(opp.StageName==Lost))&&((opp.Close_Reason__c==null)||(opp.Target_Application_Value__c==null))){
                         nRecord.addError(ERRMSG );
                     }
                     else{
                        opp.Last_Activity_Date_Plain__c=Date.TODAY();
                        oppsToUpdate.add(opp); 
                     }
                   
                  }
             }
                dbSaveList = database.update(oppsToUpdate,false); }catch(Exception e){system.LoggingLevel newLoglevel = LoggingLevel.ERROR;if(e.getMessage().contains('Update failed. First exception on row 0;') ){UTIL_LoggingService.logDmlResults(dbSaveList,null,oppsToUpdate,'Sales','NI_NoteHandler','OppLastActiveDate',TRGRNAME,NEWLOGLEVEL); }                                }       
               
      } 
}