@isTest(SeeAllData=true)
public class NI_Attachment_Trigger_Test
{
    
    static testmethod void testAttachment_Trigger()
    {
        test.starttest();
        Boolean m_isExecuting = false;
        Integer batchSize = 0;
        //---------- Insert Account---------------
        List<account> acclist = new List<Account>();
        account acc= new account(name= 'test',ownerId = UserInfo.getUserId(),shippingCountry = 'United States',shippingStreet = '1/AX39',shippingCity = 'New York',shippingState = 'New York',shippingPostalCode = '12119');
        acclist.add(acc);
        insert acclist;
        //---------- Insert Opportunity---------------
        List<opportunity> opplist = new List<opportunity>();
        Opportunity opp4 = new opportunity(name='test2',accountId=acclist[0].id,Multi_Phase_Opportunity__c=null,stageName='Qualify',forecastCategoryName='funnel',closeDate=System.today(),amount = null);
        opplist.add(opp4);
        insert opplist;
        //---------- Insert Case---------------
        Case cse=new case();
        insert cse;
        //---------- Insert Attachment---------------    
        List<Attachment> attachlist = new List<Attachment>();
        Attachment attach=new attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        attachlist.add(attach);
        insert attachlist;
        update attachlist;
        //---------- Attachment Handler Class methods---------------
        
        NI_AttachmentTriggerController atc=new NI_AttachmentTriggerController(m_isExecuting,batchSize);
        test.stoptest();
    }
}