@isTest
public with sharing class NI_Sales_Opp_Quote_WS_Test {

    @isTest(seeAllData=true)
    static void validateOppQuoteLink(){
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        String sfdcOppId = '843Ads989ASD90s';
        Decimal sfdcQuoteID = 123;
        Decimal sfdcOrgID = 1;
        NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP service = new NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP();
        NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = service.OppQuoteLink(sfdcOppId, sfdcQuoteID, sfdcOrgID);
        
        System.assertEquals('S', response.status);
        
    }   

}