@isTest(SeeAllData=true)
public class EventTriggerController_Test {

    @isTest(SeeAllData=true)
    private static void constructorTest(){
        EventTriggerController controller = new EventTriggerController (false, 1);
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest(){
        Test.startTest();
        List<Event> events = new List<Event>();
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp';
        opp.StageName = 'Advance';
        opp.CloseDate = System.today() + 10;
        insert opp;
        Event event = new Event();
        event.whatId = opp.Id;
        events.add(event);
        EventTriggerController.onAfterInsert(events);
        Test.stopTest();
    }
    
}