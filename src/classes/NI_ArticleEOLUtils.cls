/**
*
*Name        : NI_ArticleEOLUtils 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 19th June, 2017
*Purpose     : Invocable Apex Class to be used in Processes for EOL Approval
*
**/

public with sharing class NI_ArticleEOLUtils { 
    /**
    * @method : publishArticle
    * @purpose : Invocable method to be called from Process for publishing the article to EOL Archive
    *
    */
    @InvocableMethod
    public static void publishArticle(List<String> articleIdList)
    {
        List<Knowledge__kav> upadteList = new List<Knowledge__kav>();
        for(Id artId : articleIdList){
            Knowledge__kav art = new Knowledge__kav(Id=artId, EOL_Archive_Date__c=Date.today(), Status__c=NI_Constants.NI_PublishedEOL_Status, EOL_Archive__c=false);
            upadteList.add(art);
        }
        if(upadteList.size()>0){
            // TODO: implement save results.  Question for Casey - fail all or one?
            update upadteList;
        }
        for(knowledge__kav  articleId : [Select id,knowledgearticleid,PublishStatus from knowledge__kav where id in : articleIdList]){
      		KbManagement.PublishingService.publishArticle(articleId.knowledgearticleid, true);
        } 
   }
}