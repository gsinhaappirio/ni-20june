/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            NI_AttachmentTriggerController
   Description:     This class invokes methods of NI_AttachmentHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   30th September           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class NI_AttachmentTriggerController{
    private Boolean m_isExecuting = false;
    private Integer batchSize = 0;
    
    /*Constructor*/
    public NI_AttachmentTriggerController (Boolean isExecuting, Integer size){
        m_isExecuting = isExecuting;
        batchSize = size;
    }

    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Attachment> (Trigger.New),Map<Id,Attachment> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Insert methods of NI_AttachmentHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onBeforeInsert(List<Attachment> newAttachmentList){
         System.debug('Nothing to call in onBeforeInsert method' );
       
    }
    
    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Attachment> (Trigger.New),Map<Id,Attachment> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Update methods of NI_AttachmentHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
     public static void onBeforeUpdate(List<Attachment> newAttachmentList,Map<Id,Attachment> oldAttachmentMap){   
        System.debug('Nothing to call in onBeforeUpdate method' );
       
    }
    
    /**********************************************************************************************************************
    * Author:
    * Date: 
    * Param: List<Attachment> (Trigger.New),Map<Id,Attachment> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after insert methods of NI_AttachmentHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert (List<Attachment> newAttachmentList){
      
        NI_AttachmentHandler.OppLastActiveDate(newAttachmentList);
              
    }
    
    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Attachment> (Trigger.New),Map<Id,Attachment> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after update methods of NI_AttachmentHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterUpdate(List<Attachment> newAttachmentList,Map<Id,Attachment> oldAttachmentMap){
    	
    	System.debug('Nothing to call in onAfterUpdate method' );
    
    }    
}