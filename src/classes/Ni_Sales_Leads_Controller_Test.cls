@isTest
public class Ni_Sales_Leads_Controller_Test {
    
    @isTest(SeeAllData=true)
    public static void refreshTest(){
        PageReference pageRef = new PageReference('/apex/ni_sales_leads');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.FirstName = 'Lead';
            lead.LastName = String.valueOf(i);
            leads.add(lead);
        }
        Ni_Sales_Leads_Controller controller = new Ni_Sales_Leads_Controller(new ApexPages.StandardSetController(leads));
        /**
        controller.sortType = 'boolean';
        controller.sortDirection= 'asc';
        controller.sortField = 'Date Accepted';
        controller.refreshTable();
        System.assertEquals(controller.sortField, 'Budget');
        
        controller.sortType = 'string';
        controller.sortDirection= 'desc';
        controller.sortField = 'Date Accepted';
        controller.refreshTable();
        System.assertEquals(controller.sortField, 'First Name');
        
        controller.sortType = 'date';
        controller.sortDirection= 'desc';
        controller.sortField = 'Date Accepted';
        controller.refreshTable();
        System.assertEquals(controller.sortField, 'Date Accepted'); 
    **/
        Test.stopTest();
        
    }
    
    @isTest (SeeAllData=true)
    public static void compareToTest(){
        Test.startTest();
        
        Lead lead1 =new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert lead1;    
        
        lead1 = [SELECT id, lead_number__c, name, lead.owner.name, budget__c, timeframe__c, company, address, country, street, city, state, postalCode, phone, email, cREQ_Date__c, CreatedDate, description 
                FROM Lead 
                WHERE id = :lead1.id ];
                
        Ni_Sales_Leads_Controller.SortedLead sl1 = new Ni_Sales_Leads_Controller.SortedLead(lead1);
        
        
        Lead lead2 = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert lead2;    
        
        lead1 = [SELECT id, lead_number__c, name, lead.owner.name, budget__c, timeframe__c, company, address, country, street, city, state, postalCode, phone, email, cREQ_Date__c, CreatedDate, description 
                FROM Lead 
                WHERE id = :lead2.id ];
                
        Ni_Sales_Leads_Controller.SortedLead sl2 = new Ni_Sales_Leads_Controller.SortedLead(lead2);
        
        Lead lead3 = new  Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert lead3;    
        
        lead1 = [SELECT id, lead_number__c, name, lead.owner.name, budget__c, timeframe__c, company, address, country, street, city, state, postalCode, phone, email, cREQ_Date__c, CreatedDate, description 
                FROM Lead 
                WHERE id = :lead3.id ];
                
        Ni_Sales_Leads_Controller.SortedLead sl3 = new Ni_Sales_Leads_Controller.SortedLead(lead3);
        
        //Integer compare = sl2.compareTo(sl1);
        //System.assertEquals(compare, 0);
        
        //compare = sl3.compareTo(sl1);
       // System.assertNotEquals(compare, 0);
        
        Test.stopTest();
    }
    
    
    
    
    @isTest(SeeAllData=true)
    public static void getNextLeadTest(){
        PageReference pageRef = new PageReference('/apex/ni_sales_leads');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.FirstName = 'Lead';
            lead.LastName = String.valueOf(i);
            leads.add(lead);
        }
        Ni_Sales_Leads_Controller controller = new Ni_Sales_Leads_Controller(new ApexPages.StandardSetController(leads));
        
        PageReference pr = controller.getNextLead();
        System.assertNotEquals(pr, null);
        pr=controller.refreshTable();
        
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=true)
    public static void getNextLeadTest2(){
        PageReference pageRef = new PageReference('/apex/ni_sales_leads');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            lead.FirstName = 'Lead';
            lead.LastName = String.valueOf(i);
            leads.add(lead);
        }
        Ni_Sales_Leads_Controller controller = new Ni_Sales_Leads_Controller(new ApexPages.StandardSetController(leads));
        
        PageReference pr = controller.getNextLead();
        System.assertNotEquals(pr, null);
        
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, i, 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest2(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, String.valueOf(i), 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest3(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, String.valueOf(i), 'desc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest4(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, null, 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest5(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, null, 'desc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest6(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, true, 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest7(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, true, 'desc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest8(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, Date.today(), 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest9(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, Date.today(), 'desc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest10(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, Decimal.valueOf(i), 'asc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
    @isTest(SeeAllData=true)
    public static void sortableLeadsTest11(){
        List<Ni_Sales_Leads_Controller.SortedLeadColumn> sortedLeads = new List<Ni_Sales_Leads_Controller.SortedLeadColumn>();
        for(integer i = 0; i < 10; i++){
            Lead lead = new Lead();
            Ni_Sales_Leads_Controller.SortedLeadColumn sl = new Ni_Sales_Leads_Controller.SortedLeadColumn(lead, Decimal.valueOf(i), 'desc');
            sortedLeads.add(sl);
        }
        sortedLeads.sort();
    }
    
}