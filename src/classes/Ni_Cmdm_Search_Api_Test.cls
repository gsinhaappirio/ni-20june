@isTest
public class Ni_Cmdm_Search_Api_Test {
    
    @isTest(seeAllData=true)
    public static void searchForContactTest(){
        
        Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Contact_Mock());
        
        Ni_Cmdm_Search_Api api = new Ni_Cmdm_Search_Api();
        Test.startTest();
        Contact contact = new Contact();
        contact.firstName = 'Tyler';
        contact.lastName = 'Hobbs';
        contact.account = new Account();
        contact.account.name = 'National Instruments';
        contact.phone = '5126830000';
        List<Ni_Cmdm_Contact_Search_Result> results = api.searchForContact(contact, 'ALL', 10, false, true);
        Test.stopTest();
        System.assertNotEquals(results, null);
    }
    
    @isTest(seeAllData=true)
    public static void searchForAccountTest(){
        
        Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Account_Mock());
        
        Ni_Cmdm_Search_Api api = new Ni_Cmdm_Search_Api();
        Test.startTest();
        Account account = new Account();
        account.name = 'National Instruments';
        account.ShippingCity = 'Austin';
        account.ShippingPostalCode = '78759';
        List<Ni_Cmdm_Account_Search_Result> results = api.searchForAccount(account, 'ALL', 10, false, true);
        Test.stopTest();
        System.assertNotEquals(results, null);
    }
    
}