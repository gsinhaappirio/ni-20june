@isTest
public with sharing class Ni_Cmdm_Search_Account_Mock implements WebServiceMock {

	public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
                   
                   Ni_Cmdm_Search_Result.AccountSearchResultType response_x = new Ni_Cmdm_Search_Result.AccountSearchResultType();
                   
                   response_x.entryList = new Ni_Cmdm_Search_Result.entryList_element();
                   Ni_Cmdm_Search_Result.entry_element entry = new Ni_Cmdm_Search_Result.entry_element();
                   response_x.entryList.entry = new List<Ni_Cmdm_Search_Result.entry_element>();
                   
                   Ni_Cmdm_Search_Result.entry_element entryElement = new Ni_Cmdm_Search_Result.entry_element();
                   entryElement.score = 0;
                   entryElement.sourceRef = new Ni_Cmdm_Search_Result.sourceRef_element();
                   entryElement.sourceRef.sourceSystem = 'Salesforce';
                   
                   entryElement.data = new Ni_Cmdm_Search_Result.data_element();
                   
                   entryElement.data.Account = new Ni_Cmdm_Search_Enterprise_Object.Account();
                   entryElement.data.Account.name = 'National Instruments';
                   
                   response_x.entryList.entry.add(entryElement);
                   
                   response.put('response_x', response_x);
	}
}