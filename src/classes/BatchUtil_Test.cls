@isTest
public class BatchUtil_Test implements BatchUtil.BatchUtilable {
    
    
    
    public boolean HandleRecord(sObject s, BatchUtil callee) {
        string log = '';
        if (s.get('LastName') == 'testing1') {
            s.put('Company', 'updated');
            callee.Log('Found ' + s.get('LastName') + ' and updated company to: "' + s.get('Company') + '"\n'); 
            return true;
        } else {
            callee.Log('Ignoring ' + s.get('LastName'));
            return false;
        }
    }

    @isTest(SeeAllData=true)
    static void Test() {
        Lead l1 = new Lead();
        l1.LastName = 'testing1';
        l1.Company = 'apex_test_company';
        l1.Country = 'US';
        l1.leadSource = 'Sales - Sales';
        insert l1;
        l1 = [select LastName, Company, LastModifiedDate from Lead where Id = :l1.Id];
        Lead l2 = new Lead();
        l2.LastName = 'testing2';
        l2.Company = 'apex_test_company';
        l2.Country = 'US';
        l2.leadSource = 'Sales - Sales';
        insert l2;
        l2 = [select LastName, Company, LastModifiedDate from Lead where Id = :l2.Id];

        Test.startTest();
        Database.executeBatch(new BatchUtil('select lastname, company from lead where company = \'apex_test_company\'', new BatchUtil_Test()), 10);
        Test.stopTest();

        //Lead l1refreshed = [select LastName, Company, LastModifiedDate from Lead where Id = :l1.Id];
        //system.assertEquals('updated', l1refreshed.Company);
        //system.assertNotEquals(l1.LastModifiedDate, l1refreshed.LastModifiedDate);
        //Lead l2refreshed = [select LastName, Company, LastModifiedDate from Lead where Id = :l2.Id];
        //system.assertEquals('testing2', l2refreshed.Company);
        //system.assertEquals(l2.LastModifiedDate, l2refreshed.LastModifiedDate);
    }
}