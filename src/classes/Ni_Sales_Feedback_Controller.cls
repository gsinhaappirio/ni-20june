public class Ni_Sales_Feedback_Controller {

    public String emailLink{get;set;}
    public String currentUrl{get;set;}
    public String explanation{get;set;}
    public Integer step{get;set;}
    public transient Blob bl{get;set;}
    public String filename{get;set;}
    public String contentType{get;set;}
    public Boolean doneSendingEmail{get;set;}
    public Boolean success{get;set;}
    
    public String recipient{get;set;}

    public Ni_Sales_Feedback_Controller(){
        step = 0;
        emailLink = 'mailto:crmassist.help@ni.com?subject=Feedback&body=body+text+here';
        currentUrl = ApexPages.currentPage().getHeaders().get('referer');
        Ni_Feedback__c feedbackSettings = Ni_Feedback__c.getInstance(UserInfo.getUserID());
    }

    private String generateUserInfoString(){
    
        User user = [Select Region_HCM__c from User where Id = :UserInfo.getUserId()];

        String profileName = '';
        if(UserInfo.getProfileId() != null && !UserInfo.getProfileId().equals('')){
            Profile profile = [Select Id, Name from Profile where id = :UserInfo.getProfileId()];
            if(profile != null && profile.name != null){
                profileName = profile.name;
            }
        }

        String roleName = '';
        if(UserInfo.getUserRoleId() != null && !UserInfo.getUserRoleId().equals('')){
            UserRole role = [Select Id, Name from UserRole where id = :UserInfo.getUserRoleId()];
            if(role != null && role.name != null){
                roleName = role.name;
            }
        }

        String bodyString = '';
        bodyString += UserInfo.getName() != null ? 'Name = ' + UserInfo.getName() + '\r\n' : '';
        bodyString += UserInfo.getUserEmail() != null ? 'Email = ' + UserInfo.getUserEmail() + '\r\n' : '';
        bodyString += profileName != null ? 'Profile = ' + profileName + '\r\n' : '';
        bodyString += roleName != null ? 'Role = ' + roleName + '\r\n' : '';
        bodyString += user.Region_HCM__c != null ? 'Region = ' + user.Region_HCM__c + '\r\n' : '';
        bodyString += UserInfo.getTimeZone() != null ? 'Time Zone = ' + String.valueOf(UserInfo.getTimeZone()) + '\r\n' : '';

        String currentlyViewingId = ApexPages.currentPage().getParameters().get('id') != null ? ApexPages.currentPage().getParameters().get('id') : '';
        if(!currentlyViewingId.equals('')){
            bodyString += ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ? 'Last URL = ' + ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') + '?id=' + currentlyViewingId + '\r\n' : '';
        }else{
            bodyString += ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ? 'Last URL = ' + ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') + '\r\n' : '';
        }


        bodyString += ApexPages.currentPage().getHeaders().get('User-Agent') != null ? 'User Agent = ' + ApexPages.currentPage().getHeaders().get('User-Agent') + '\r\n' : '';

        return bodyString;
    }
    
    public PageReference restart(){
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }

    public PageReference sendEmail() {
    
        
        //*
    
        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after

        // the current transaction is committed.
        if(!Test.isRunningTest()){
            Messaging.reserveSingleEmailCapacity(2);
        }

        // Processes and actions involved in the Apex transaction occur next,
        // which conclude with sending a single email.

        // Now create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        if(bl != null){
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setFileName(filename);
            attachment.setBody(bl);
            attachment.setContentType(contentType);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
        }

        // Strings to hold the email addresses to which you are sending the email.
        Ni_Feedback__c feedbackSettings = Ni_Feedback__c.getInstance(UserInfo.getUserID());
        
        String toEmail = '';
        
        if(recipient.equalsIgnoreCase('sfdc')){
            toEmail = feedbackSettings.Account_Destination_Email__c;
        }else{
            toEmail = feedbackSettings.Destination_Email__c;
        }

        String[] toAddresses = new String[] {toEmail};
        String[] ccAddresses = new String[] {UserInfo.getUserEmail()};


        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);
        mail.setCcAddresses(ccAddresses);

        // Specify the address used when the recipients reply to the email.
        //mail.setReplyTo('support@ni.com');

        // Specify the name used as the display name.
        //mail.setSenderDisplayName('SFDC Support');
        mail.setSenderDisplayName(feedbackSettings.Sender_Display_Name__c);

        // Specify the subject line for your email address.
        //mail.setSubject('Support Requested from ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
        String subject = feedbackSettings != null && feedbackSettings.Subject__c != null ? feedbackSettings.Subject__c : 'Request for Help';
        Integer subjectSize = feedbackSettings != null && feedbackSettings.Subject_size__c != null ? feedbackSettings.Subject_size__c.intValue() : 100;
        
        if(explanation != null){
            if (explanation.length() > subjectSize){
               subject = subject + ': ' + explanation.substring(0,subjectSize-3) + '...';
            }
            else{
                subject = subject + ': ' + explanation;
            }
        }
                
        mail.setSubject(subject);

        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);

        // Optionally append the salesforce.com email signature to the email.
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(false);

        // Specify the text content of the email.
        if(explanation != null){
            mail.setPlainTextBody(generateUserInfoString() + '\r\n\r\n' + explanation + '\r\n\r\n');
        }

        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            success = true;
        }catch(Exception e){
            success = false;
        }

        doneSendingEmail = true;
        //*/
        return null;
    }
}