@isTest
public class Ni_Cmdm_Search_Test {

    @isTest(SeeAllData=true)
    public static void QbeContactSearchTest(){
        Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Contact_Mock());
        Test.startTest();
        Ni_Cmdm_Search.SearchSOAP11BindingQSPort port = new Ni_Cmdm_Search.SearchSOAP11BindingQSPort();
        port.QbeContactSearch(new Ni_Cmdm_Search_Request.QbeFilterType(), new Ni_Cmdm_Search_Enterprise_Object.Contact());
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    public static void QbeAccountSearchTest(){
        Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Account_Mock());
        Test.startTest();
        Ni_Cmdm_Search.SearchSOAP11BindingQSPort port = new Ni_Cmdm_Search.SearchSOAP11BindingQSPort();
        port.QbeAccountSearch(new Ni_Cmdm_Search_Request.QbeFilterType(), new Ni_Cmdm_Search_Enterprise_Object.Account());
        Test.stopTest();
    }
    
}