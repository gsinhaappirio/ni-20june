/*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* Description:
*
* Global class to return the options for typeahead functionality
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* Modification History:
*
* Date 				Author 				Modifications
* 06-JUN-2017 		Meghana Agarwal 	Created
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/

global class TypeaheadController {
    
    /*
    * ──────────────────────────────────────────────────────────────────────────────────────────────────   
    * Method to retrieve the TypeaheadOption List
    * ──────────────────────────────────────────────────────────────────────────────────────────────────    
    * @param searchText String
    * @param searchObject String
    * @param searchField String
    * @param returnFields String
    * @param orderBy String
    * @param searchText String
    * @param searchText String
    * @return List<TypeaheadOption>
    * ──────────────────────────────────────────────────────────────────────────────────────────────────
    */
	@AuraEnabled
    global static List<TypeaheadOption> getOptionsData(String searchText,String searchObject,String searchField,String returnFields,String orderBy,Integer limitRec, String condition) {
        String query = ' Select ' + returnFields + ' from ' +  searchObject;
 		if(!String.isBlank(searchText)){
           query += ' where ' + searchField + ' like \'%' + searchText + '%\'';

 		}
        
        if(condition != null && !String.isBlank(condition)){
            query += ' and '+ condition;
        }
        
        if(orderBy != null){
            query += ' Order By '+ searchField + ' ' + orderBy;
        }
        
        if(limitRec != null){
            query += ' Limit '+ limitRec;
        }

 		List<TypeaheadOption> tOptions = new List<TypeaheadOption>();
 		for(User us : Database.query(query)){
 			TypeaheadOption option = new TypeaheadOption();
 			option.label = us.name;
 			option.value = us.id;
 			tOptions.add(option);
 		}
 		return tOptions;
    }
    
    /*******************************************************************************************************
    * @description Wrapper Class of TypeaheadOption
    */  
    global class TypeaheadOption{
        /*******************************************************************************************************
        * @description get and set components.
        * @return label
        */
        @AuraEnabled
        global String label{get;set;}
        /*******************************************************************************************************
        * @description get and set components.
        * @return value
        */
        @AuraEnabled
        global String value{get;set;}
    }
}