@isTest
public class Ni_Cmdm_Contact_Search_Result_Test {
	@isTest
    public static void noParametersTest(){
        Contact contact = new Contact();
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        Ni_Cmdm_Contact_Search_Result contactResult = new Ni_Cmdm_Contact_Search_Result(contact, score, sourceSystem);
        System.assertNotEquals(contactResult.contact, null);
        
        contactResult = new Ni_Cmdm_Contact_Search_Result();
    }
    
    @isTest
    public static void ascendingSortTest(){
        
        List<Ni_Cmdm_Contact_Search_Result> contacts = new List<Ni_Cmdm_Contact_Search_Result>();
        for(Integer i = 0; i < 10; i++){
            Contact contact = new Contact();
            String sourceSystem = 'Salesforce';
            Ni_Cmdm_Contact_Search_Result contactsResult = new Ni_Cmdm_Contact_Search_Result(contact, i, sourceSystem);
            contacts.add(contactsResult);
        }
        
        contacts.sort();
        
        for(Integer i = 0; i < contacts.size(); i++){
            if(i < contacts.size()-1){
                System.assert(contacts.get(i+1).score < contacts.get(i).score);
            }
        }

    }
    
    @isTest
    public static void descendingSortTest(){
        
        List<Ni_Cmdm_Contact_Search_Result> contacts = new List<Ni_Cmdm_Contact_Search_Result>();
        for(Integer i = 9; i >= 0; i--){
            Contact contact = new Contact();
            String sourceSystem = 'Salesforce';
            Ni_Cmdm_Contact_Search_Result contactsResult = new Ni_Cmdm_Contact_Search_Result(contact, i, sourceSystem);
            contacts.add(contactsResult);
        }
        
        contacts.sort();
        
        for(Integer i = 0; i < contacts.size(); i++){
            if(i < contacts.size()-1){
                System.assert(contacts.get(i+1).score < contacts.get(i).score);
            }
        }

    }
}