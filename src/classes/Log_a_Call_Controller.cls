public class Log_a_Call_Controller {

    private Id fromId {get;set;}
    private Id recordTypeId {get;set;}
    
    public Log_a_Call_Controller() {
        
        String fromIdParameter = System.currentPageReference().getParameters().get('fromId');
        
        if(fromIdParameter != null){
            fromId = (Id) fromIdParameter;
            //RecordType recordType = [SELECT Id FROM RecordType WHERE Name = 'Interaction Record Type'];
            //recordTypeId = recordType.Id;
        }
        
    }
    
    public PageReference forwardToTaskCreation(){
        String url = '/00T/e';
        PageReference pr = new PageReference(url);
        
        if(fromId.getSObjectType() == Schema.Multi_Phase_Opportunity__c.SObjectType || 
           fromId.getSObjectType() == Schema.Account.SObjectType)
        {
           pr.getParameters().put('what_id', fromId);
        }else if(fromId.getSObjectType() == Schema.Opportunity.SObjectType)
        {
            pr.getParameters().put('what_id', fromId);
            // save query result to a list instead of an object to prevent query zero exception
            List<OpportunityContactRole> ocrList = [Select ContactId from OpportunityContactRole where OpportunityId = :fromId and isPrimary = true LIMIT 1];
            if(ocrList != null && !ocrList.isEmpty()){
                pr.getParameters().put('who_id', ocrList.get(0).ContactId);
            }
        }else if(fromId.getSObjectType() == Schema.Lead.SObjectType || 
                 fromId.getSObjectType() == Schema.Contact.SObjectType)
        {
           pr.getParameters().put('who_id', fromId);
        }
        
        pr.getParameters().put('followup', '1');
        pr.getParameters().put('retURL', fromId);
        pr.getParameters().put('00Ni000000FoVm9','Interaction');
        //pr.getParameters().put('RecordType', recordTypeId);
        
        return pr;
    }
    
}