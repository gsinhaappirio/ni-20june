public with sharing class Ni_Merge_Accounts_Extension {
    
    public List<AccountWrapper> results{get;set;}
    public String searchQuery{get;set;}
    public String url{get;set;}
    public String errorMessage{get;set;}
    
    public Ni_Merge_Accounts_Extension(ApexPages.StandardSetController controller){
        url = null;
        String query = System.currentPageReference().getParameters().get('q');
        if(query != null && !query.equals('')){
            searchQuery = query;
            results = localSearch();
        }
    }
    
    public PageReference searchForAccounts(){
        results = localSearch();
        return null;
    }
    
    public List<AccountWrapper> localSearch(){

        String query = searchQuery.trim();
        
        // can't perform search if the string is one character or less
        if(query.replace('*','').length() <= 1){
            return null;
        }

        query = String.escapeSingleQuotes(searchQuery);
        
        query += '*';
        
        String q = 'FIND :query IN ALL FIELDS RETURNING Account (';
        
        //confirm that all the necessary fields are available to search
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Account.fields.getMap();
        for (String fieldName : fieldMap.keySet()) {
            q += fieldName + ',';
        }

        // add all of the user fields for the owner
        Map<String, Schema.SObjectField> userFieldMap = Schema.SObjectType.User.fields.getMap();
        for (String fieldName : userFieldMap.keySet()) {
            q += 'Owner.' + fieldName + ',';
        }
        
        // remove the last comma
        q = q.substring(0, q.length()-1);
        
        q += ')';

        List<List<SObject>> results = search.query(q);
        List<Account> accounts = (List<Account>)results[0];
        List<AccountWrapper> wrapperList = new List<AccountWrapper>();
        
        if(Test.isRunningTest()){
             //names to avoid duplicate accounts
            String [] names = new List<String> {'first','second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 
                                  'eighth', 'ninth', 'tenth'};
            for(Integer i = 0; i < 10; i++){
                Account account = new Account();
                account.name = names[i];
                account.shippingstreet = names[i];
                account.shippingcity = names[i];
                account.shippingcountry = 'US';
                insert account;
                accounts.add(account);
            }
        }
        
        for(Account account : accounts){
            AccountWrapper aw = new AccountWrapper(account, false);
            wrapperList.add(aw);
        }
        return wrapperList;
    }
    
    public PageReference mergeAccounts(){
        url = '/merge/accmergewizard.jsp?goNext=+Next+';
        Integer selectedAccounts = 0;
        for(AccountWrapper result : results){
            if(result.selected){
                selectedAccounts++;
                url += '&cid=' + result.account.Id;
            }
        }
        
        if(selectedAccounts == 0){
            errorMessage = 'You must select at least 2 accounts to continue.';
            url = null;
        }else if(selectedAccounts == 1){
            errorMessage = 'You must select more than 1 account to continue.';
            url = null;
        }
        
        return null;
    }
    
    public class AccountWrapper {
    
        public Boolean selected{get;set;}
        public Account account{get;set;}
        
        public AccountWrapper(Account account, Boolean selected){
            this.account = account;
            this.selected = selected;
        }
    }
    
}