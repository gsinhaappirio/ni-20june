@isTest
public class NI_DispatcherQuoteViewExtension_Test{

    @isTest
    public static void redirectToExternalObjectTest() {
        
        NI_Quote__c quote = new NI_Quote__c(site__c = 'NIC', oracle_header_id__c = 123456);
        insert quote ;
        
        NI_DispatcherQuoteViewExtension dispatcher = new NI_DispatcherQuoteViewExtension(new ApexPages.StandardController(quote));
        apexpages.currentpage().getparameters().put('id' , quote.id);
        
        dispatcher.redirectToExternalObject();
    }
    

}