/**************************************************************************************************
*                                                                                                 *
*                     RT LIBRARY – Technical Goods                                                *
*    <<< BC.K.000L – Adding/Editing/Remove Multiple Products on Opportunity via single page >>>   *
*                                                                                                 *
*                                                                                                 *
*-------------------------------------------------------------------------------------------------*
*                                                                                                 *
*                            Version 1.00                                                         *
*                                                                                                 *
* Creation Date: 15.Feb.2014                    Reetika Budhiraja                                 *
* Last Modified: 18.Apr.2014                    Sarbpreet Kaur                                    *
* Last Modified: 09.Sept.2014                   Sandeep Wategave                                    *
                                                                                                  *
*                             © 2014 - Accenture - All Rights Reserved                            *
***************************************************************************************************/

/*Purpose:  This is a Controller Class for Product Search and Add Product Page . Where we search,Add as many
 number of Products, Save,Delete and also Cancel the event.
 ===========================================================================================================
 History 
----------------------- 
VERSION     AUTHOR              DATE        DETAIL 
   1.0 -    Sarbpreet,Reetika   18/4/2014   Created Controller class
 =========================================================================================================== */
public with sharing class CRUDOPP_OpportunityProductController implements CRUDOPP_ObjectPaginatorListener {

    private ApexPages.StandardController std;
    private string recordIds = ConstantUtility.STR_Blank;
    private string pricebookId = ConstantUtility.STR_Blank;
    private String soql ;
    private map<id, OpportunityLineItem> oliMap = new Map<id, OpportunityLineItem>();
    private  List<OpportunityLineItem> list_opptyLineItemsToDel = new List<OpportunityLineItem>();
       private String OppCurrency='';    
    public CRUDOPP_ObjectPaginator paginator {get;private set;}
    public Boolean isSearchHaveValue{get;set;}  
    public Boolean isMore{get;set;} 

    public Opportunity opp {get;set;}
    public Opportunity theOpp {get;set;}        
    public  string STR_searchString {get;set;}        
     
    public string pricebookName{get;set;}
    public String selected {get; set;} 
    public String unselect {get; set;}    

    public Map<String,CRUDOPP_searchFieldSettings__c> optionFieldMap = CRUDOPP_searchFieldSettings__c.getall();
    public Map<String,CRUDOPP_searchOperatorSettings__c> optionOprMap = CRUDOPP_searchOperatorSettings__c.getall();       
    
    public list<SearchWrapper> listSearchWrap {get;set;}
    public Integer lastIndex {get;set;}
    public Integer removeIndex {get; set;}  
   
    public List<PricebookEntry> priceBookEntries { get; set;}     
    public List<PriceBookEntryWrapper> list_priceBookEntry {
        get {
            if (list_priceBookEntry == null) {
                list_priceBookEntry = new List<PriceBookEntryWrapper>();
            }
            return list_priceBookEntry;
        }
        set;
    }
    
    public List<PriceBookEntryWrapper> list_priceBookEntryAll { 
        get {
                if (list_priceBookEntryAll == null) {
                    list_priceBookEntryAll = new List<PriceBookEntryWrapper>();
                }
                return list_priceBookEntryAll;
            }
        set;
    }     

    public List<OpportunityLineItem> list_opptyLineItemsSel {
        get {
                if (list_opptyLineItemsSel == null) {
                    list_opptyLineItemsSel = new List<OpportunityLineItem>();
                }
                return list_opptyLineItemsSel;
            }
        set;            
    }    
             
    // the current sort direction. defaults to asc
    public String sortDir {
        get  { 
                if (sortDir == null)
                {  
                    sortDir = ConstantUtility.STR_SortAsc;
                } 
                return sortDir; 
            }
        set;
    }
 
    // the current field to sort by. defaults to last name
    public String sortField {
        get  { 
                if (sortField == null)
                {
                    sortField = 'Product2.Name'; 
                } 
                return sortField;
            }
        set;
    }
 
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { 
                return soql + ConstantUtility.STR_OrderBy + sortField + ' ' + sortDir ; //+ ' limit 10'
        }
        set;
    }
     
    // ============================================================================
    // = C.O.N.S.T.R.U.C.T.O.R
    // ============================================================================
    public CRUDOPP_OpportunityProductController(ApexPages.StandardController controller) { //The constructor retreives the current opportunity, its related Opportunity Products and a list of all the products.
        if(ApexPages.currentPage().getParameters().get('id') !=null){
                isSearchHaveValue=true;               
                listSearchWrap= new List<SearchWrapper>();
                //Initialize a new wrapper
                listSearchWrap.add(new SearchWrapper(null,0));
                lastIndex = 1;
                
                recordIds = ApexPages.currentPage().getParameters().get('id');
                Id pbid = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
                list<opportunity> opptyObjects = [select id,Pricebook2Id,Pricebook2.Name,CurrencyIsoCode from opportunity where id= :recordIds LIMIT 1];
                
                if(opptyObjects.size()> 0){
                    for(Opportunity op:opptyObjects){
                        pricebookId = op.Pricebook2Id;
                        pricebookName= op.Pricebook2.Name ;
                        OppCurrency=op.currencyISOCode;
                        for(OpportunityLineItem oli : [Select id,PricebookEntryid,PricebookEntry.Product2.Name,UnitPrice, 
                        quantity from OpportunityLineItem where OpportunityID= :op.id]) {
                            list_opptyLineItemsSel.add(oli);
                        }
                    }      
                    if(PricebookId !=null){
                        soql = 'Select UnitPrice, ProductCode, Product2.Family, Product2.Description,Product2.ProductCode,Product2.Name, Product2Id,CurrencyIsoCode , Pricebook2Id, Name, IsActive, Id From PricebookEntry  where Pricebook2Id=\''+pricebookId+'\' and CurrencyIsoCode = :oppCurrency  and Product2.IsActive=true and IsActive=true ' ;
                        unselectFields();
                        // run the query again
                        runQuery();
                            
                    }else{
                        // ------------------------  New Code ----------------------------------
                        //List<DefaultPriceBook__c> mcs = DefaultPriceBook__c.getall().values();
                        Id standardPriceBookId = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
                        //pricebookId = mcs[0].Name;

                        soql = 'Select UnitPrice, ProductCode, Product2.Family, Product2.Description,Product2.ProductCode,Product2.Name,CurrencyIsoCode , Product2Id, Pricebook2Id, Name, IsActive, Id From PricebookEntry  where Pricebook2Id=\''+standardPriceBookId+'\' and CurrencyIsoCode = :oppCurrency and Product2.IsActive=true and IsActive=true ' ;

                        unselectFields();
                       
                        // run the query again
                        runQuery();
                        
                            //ErrorUtil.LogError('No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.');
                           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.'));
                    }                 
                    opptyObjects.clear();                       
                }                                  
        }
    }
    
    // ============================================================================
    // = M.E.T.H.O.D.S  
    // ============================================================================

    
    /*
     * @Description : This method toggles the sorting of query from asc<-->desc     
     * @ Args       : null     
     * @ Return     : void
    */ 
    public void toggleSort() {
        try {
                // simply toggle the direction
                sortDir = sortDir.equals(ConstantUtility.STR_SortAsc) ? ConstantUtility.STR_SortDesc : ConstantUtility.STR_SortAsc;
            } catch (Exception ex) {
                
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            }
        // run the query again
        runSearch();
    }
 
    /*
     * @Description : This method runs the actual query.
     * @ Args       : null
     * @ Return     : void
    */
    public void runQuery() {
        try {
                list_priceBookEntry.clear();
                priceBookEntries=Database.query(soql + ConstantUtility.STR_OrderBy + sortField + ' ' + sortDir );
                List<PriceBookEntryWrapper> paginatePBEntry=new List<PriceBookEntryWrapper>();
                if( priceBookEntries !=null &&  priceBookEntries.size()>0){
                    isSearchHaveValue=false;
                    for(PricebookEntry pbe :  priceBookEntries ) {
                        paginatePBEntry.add(new PriceBookEntryWrapper(pbe));
                    }
                    //15 is pageSize, this refers to this class which acts as listener to paginator
                    paginator = new CRUDOPP_ObjectPaginator(200,this);
                    paginator.setRecords(paginatePBEntry);
                }
                else{
                        isSearchHaveValue=true;
                }
        } catch (Exception ex) {
        
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }
    }
  
  
    /*
     * @Description : This method removes the Products from the Product's list which are already added to the Opportunity
     * @ Args       : null
     * @ Return     : void 
    */
    public void unselectFields() {
        Set<ID> opptyPricebookEntryIds = new Set<ID>();
                        for(OpportunityLineItem oli: list_opptyLineItemsSel)
                        {
                            opptyPricebookEntryIds.add(oli.PricebookEntryId);
                        } 
                        if(list_opptyLineItemsSel.size()>0)
                        {
                            String notProdIds = ' and Id not in (';
                            for(Id opeid : opptyPricebookEntryIds){
                                notProdIds+= '\'' + (String)opeid + '\',';             
                            }

                            String prodRemove = notProdIds.substring(0,notProdIds.length()-1);
                            prodRemove+= ')';
                            soql+= prodRemove; 
                        }
                          
    }
  
    /*
     * @Description : This method is used for pagination.
     * @ Args       : null
     * @ Return     : void 
    */
    public void handlePageChange(List<Object> newPage){
        try {
                list_priceBookEntry.clear();
                if(newPage != null){
                    for(Object pbEntreyWrapper : newPage){
                        list_priceBookEntry.add((PriceBookEntryWrapper)pbEntreyWrapper);
                        list_priceBookEntryAll.add((PriceBookEntryWrapper)pbEntreyWrapper);
                    }
                }
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        } 
        
    }    
    
    /*
     * @Description : This method clears the search filters.     
     * @ Args       : null     
     * @ Return     : void
    */
    public void clearSearch() {
        Integer inc=0;
        try {
            if(STR_searchString != null) {
                STR_searchString = '';
            }
                                
            for(inc=0; inc< listSearchWrap.size();inc++) { 
                    listSearchWrap.get(inc).searchField ='';                              
                    listSearchWrap.get(inc).searchValue ='';                       
                    listSearchWrap.get(inc).searchOpr ='';                                
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }        
        runSearch();
    } 
   

    /*
     * @Description : This method filters and searches for a product    
     * @ Args       : null   
     * @ Return     : PageReference 
    */
    public PageReference runSearch() {
        Integer inc = 0;
      
         soql = 'Select UnitPrice, ProductCode, Product2.Family, Product2.Description,Product2.ProductCode,Product2.Name ,Product2Id, Pricebook2Id, Name, IsActive,CurrencyIsoCode , Id From PricebookEntry  where Pricebook2Id=\''+pricebookId+'\' and CurrencyIsoCode = :oppCurrency and Product2.IsActive=true and IsActive=true ' ;   
                    
        try {           
                if(pricebookId !=null){  
                     List<String> fieldSearch = new List<String>();
                     fieldSearch.addAll(optionFieldMap.keySet()); 
                     if (STR_searchString !=null ){
                        String anding = ' and  (';
                        for(String src : fieldSearch)
                     { 
                        CRUDOPP_searchFieldSettings__c srcField = optionFieldMap.get(src); 
                        anding += srcField.FieldValue__c+ ' LIKE \'%' + String.escapeSingleQuotes(STR_searchString)+ '%\' OR ';
                     }
                     String extraOR = anding.substring(0,anding.length()-3);
                            extraOR += ')';
                            soql+= extraOR;
                     }                                                            
                                                         
                    for(inc=0; inc< listSearchWrap.size();inc++) {              
                            String str_field = '';
                            String str_value = '';                            
                            String str_opr = '';
                           
                            str_field = listSearchWrap.get(inc).searchField;                              
                            str_value = listSearchWrap.get(inc).searchValue;                       
                            str_opr = listSearchWrap.get(inc).searchOpr;
                                                                                     
                            moreSearchFilters(str_field,str_opr,str_value);
                    }
                     
                    unselectFields();
                   
                    // run the query again
                    runQuery();               
                }
                else{
                    ErrorUtil.LogError('No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.');
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 
                    //'No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.'));
                }
        } catch (Exception ex) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        return null;
    }
 
  
    /*
     * @Description : This method created a new Opportunityline item when a product is selected and displayes it in 
                    Product entry section.
     * @ Args       : null     
     * @ Return     : PageReference     
    */
    public PageReference selectPBE() {
        try {

                for(PriceBookEntry pb :  priceBookEntries) {
                    
                    if((String)pb.Id==selected){
                        OpportunityLineItem opptyLineItemObj= new OpportunityLineItem(OpportunityId=recordIds,
                        PriceBookEntry=pb,PricebookEntryId=pb.Id,UnitPrice=pb.UnitPrice, Quantity = 1 );
                        list_opptyLineItemsSel.add(opptyLineItemObj);
                        break;  
                    }
                }
   
                PageReference pageRef = ApexPages.currentPage(); 
            } catch (Exception ex) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            }  
            
            unselectFields();
            runQuery();                        
            return null;
    }  
             
    /*
     * @Description : This method is called when Cancel button is clicked.
     * @ Args       : null 
     * @ Return     : PageReference 
    */
    public PageReference cancelButton() {
        PageReference pageReference = null;
            try {
                pageReference = new PageReference('/' + recordIds);
                 
            }catch (Exception ex) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
                    pageReference = null;
            }
            return pageReference;
    }
       
    /*
     * @Description : This method deletes an Opportunity Product.  
     * @ Args       : null 
     * @ Return     : PageReference 
    */
    public PageReference deleteButton() {
        Integer count = 0;
        try{
            for(OpportunityLineItem olidel : list_opptyLineItemsSel)
            {      
                if((String)olidel.PriceBookEntryId==unselect){
                    if(olidel.id!=null) {
                        list_opptyLineItemsToDel.add(olidel);
                    }       
                    list_opptyLineItemsSel.remove(count);     
                    break;
                } 
                count++;
            }
        }catch(System.DmlException e){
            PageReference pageRef = null;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
            pageRef=null;
        }
        runSearch();
        
        return null; 
    }  

    /*
     * @Description : This method saves the Opportunity Product and returns to Opportunity page.  
     * @ Args       : null 
     * @ Return     : PageReference 
    */
    public PageReference saveButton(){
        PageReference pageReference=null;
        try{
                upsert list_opptyLineItemsSel;
                if( list_opptyLineItemsToDel.size()>0) {
                    delete(list_opptyLineItemsToDel);
                }
                
                pageReference = new PageReference('/' + recordIds);
        }catch(System.DmlException e){       
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
            pageReference=null;
        }
        return pageReference;
    }
   
  
    /* Wrapper class for PriceBookEntry*/
    public class PriceBookEntryWrapper { 
        public Boolean checked{ get; set; }
        public PricebookEntry priceBE {get; set;} 
        // Method for retreiveing PriceBookEntries
        public PriceBookEntryWrapper(PricebookEntry pbe){
            priceBE = pbe;        
            checked = false;
        }
 
    }   
    

 
    /* Wrapper class for Search*/
    public class SearchWrapper {         
         public String searchValue {get;set;}                              
         public String searchField {get;set;}
         public String searchOpr {get;set;}        
         public Integer index {get;set;}   
         
         public SearchWrapper(String value, Integer count  ) {
            searchValue = value;         
            index  = count;                     
         }       
    }   
   
    public List<SelectOption> getSearchField(){
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('', '-- None --'));        
         List<String> fieldNames = new List<String>();
         fieldNames.addAll(optionFieldMap.keySet());
         
         for(String item : fieldNames){
           CRUDOPP_searchFieldSettings__c srcField = optionFieldMap.get(item);          
           options.add(new SelectOption(srcField.FieldValue__c,srcField.Name));        
         }
         return options;
    }
    
    public List<SelectOption> getSearchOpr(){
        List<SelectOption> options = new List<SelectOption>();       
        options.add(new SelectOption('', '-- None --'));      
        List<String> oprNames = new List<String>();
        oprNames.addAll(optionOprMap.keySet());
         
        for(String item : oprNames){
           CRUDOPP_searchOperatorSettings__c srcOpr = optionOprMap.get(item);           
           options.add(new SelectOption(srcOpr.Name,srcOpr.Name));        
        }   
        return options;        
    }
         
   
     /* @Description : This method is used to add multiple rows
     * @ Args       : none
     * @ Return     : void
     */
    public void addSearchRow(){     
        //Adding another row        
        listSearchWrap.add(new SearchWrapper(null,lastIndex++));    
    }
    
    
     /*
     * @Description : This method is used to remove a row 
     * @ Args       : none
     * @ Return     : void
     */
    public void removeSearchRow()
    {
        //iterate on wrapper list
        for(Integer cnt = 0; cnt < listSearchWrap.size(); cnt++){
            if( listSearchWrap[cnt].index == removeIndex){
                    listSearchWrap.remove(cnt);
            }
        }
    }
    
   /*
     * @Description : This method is used for filtering the records.
     * @ Args       : String field, String opr, String value
     * @ Return     : void
     */
   public void moreSearchFilters( String field, String opr, String value) {
                if(  value!=null  )
                {           
                    if(ConstantUtility.STR_Equals.equals(opr))   
                                      
                        soql += ' and (' +field+ ' = '+ '\'' + String.escapeSingleQuotes(value)+ '\')';
                      
                    else if(ConstantUtility.STR_StartsWith.equals( opr))    
                        soql += ' and (' +field+ ' LIKE '+ '\'' + String.escapeSingleQuotes(value)+ '%\')';
                        
                    else if(ConstantUtility.STR_Contains.equals( opr))  
                        soql += ' and (' +field+ ' LIKE '+  '\'%' + String.escapeSingleQuotes(value)+ '%\')';                                                                                                                          
                                           
                                         
                } 
                                                         
    }
 
 } 

/***************************************************************************************************
*                     RT LIBRARY – Technical Goods                                                 *
*                                                                                                  *
*                             © 2014 - Accenture - All Rights Reserved                             *
****************************************************************************************************/