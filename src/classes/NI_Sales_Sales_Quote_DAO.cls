public with sharing class NI_Sales_Sales_Quote_DAO {

   public NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element linkOppToQuote(String sfdcId, double oracleQuoteId, double orgId){
        
        NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP quotesService = new NI_Sales_Opp_Quote_WS.OppQuote_WSSOAP();

        NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element response = new NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element(); 
                
        try{
            Opportunity opp = [Select id, Opportunity_Number__c from opportunity where id = :sfdcId];
            //System.debug('Opp number: ' + opp.Opportunity_Number__c);
            response = quotesService.OppQuoteLink(opp.Opportunity_Number__c, oracleQuoteId, orgId);
        }
        catch(System.CalloutException e){
            response.status = 'E';
            response.message = e.getTypeName();
            //System.debug ('Error calling webservice quoteId: ' + String.valueOf(oracleQuoteId) + ' Callout Exception: ' + e);
        }
        catch(Exception e){
            response.status = 'E';
            response.message = e.getTypeName();
            //System.debug ('Generic exception quoteId: ' + String.valueOf(oracleQuoteId) + ' Exception: ' + e);
        }       
        
        //System.debug ('Status: ' + response.status + '  Message: ' + response.message);
        
        return response;
        
    }  
    
   public NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element linkLeadToQuote(String sfdcId, double oracleQuoteId, double orgId){
        
        NI_Sales_Lead_Quote_WS.LeadQuote_WSSOAP quotesService = new NI_Sales_Lead_Quote_WS.LeadQuote_WSSOAP();

        NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element response = new NI_Sales_Lead_Quote_WS.LeadQuoteLinkResponse_element(); 
                
        try{
            Lead lead = [Select id, Lead_Number__c from lead where id = :sfdcId];
            //System.debug('Lead number: ' + lead.Lead_Number__c);
            response = quotesService.LeadQuoteLink(lead.Lead_Number__c, oracleQuoteId, orgId);
        }
        catch(System.CalloutException e){
            response.status = 'E';
            response.message = e.getTypeName();
            //System.debug ('Error calling webservice quoteId: ' + String.valueOf(oracleQuoteId) + ' Callout Exception: ' + e);
        }
        catch(Exception e){
            response.status = 'E';
            response.message = e.getTypeName();
            //System.debug ('Generic exception quoteId: ' + String.valueOf(oracleQuoteId) + ' Exception: ' + e);
        }       
        
        //System.debug ('Status: ' + response.status + '  Message: ' + response.message);
        
        return response;
        
    }      
    
    public List<NI_Sales_Sales_QuoteLineDTO> getQuoteLines(String quoteId){
        List<NI_Sales_Sales_QuoteLineDTO> quoteLinesList = new List<NI_Sales_Sales_QuoteLineDTO>();
        NI_Sales_QuoteWS_QuoteRequest.Get_Quote_XMLBindingQSPort service = new NI_Sales_QuoteWS_QuoteRequest.Get_Quote_XMLBindingQSPort();
        
        try {
            NI_Sales_QuoteWS_Quote.GetQuoteResponse_element response = service.getQuote(quoteId);
            
            if (response.responseStatus != 'S'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Order Lines ' + response.responseMessage));
                //System.debug('Webservice error : ' + response.responseMessage);
            }
            else{
            
                NI_Sales_QuoteWS_Quote.QuoteLines lines = response.QuoteInformation.quoteLines;
                
                if (lines.QuoteLine != null){
                    for(Integer i = 0; i < lines.QuoteLine.size(); i++) {
                        quoteLinesList.add(mapQuoteLineToDTO(lines.QuoteLine[i]));
                    }
                }
            }
            
        } catch (System.CalloutException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Quote Lines'));
            //System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Quote Lines'));
            //System.debug(e.getTypeName() + ': ' + e.getMessage());
        }       
        return quoteLinesList;
    }
    
    private NI_Sales_Sales_QuoteLineDTO mapQuoteLineToDTO(NI_Sales_QuoteWS_Quote.QuoteLine quoteLine){
        
        return new NI_Sales_Sales_QuoteLineDTO( quoteLine.lineNumber, quoteLine.bookingClass, quoteLine.partNumber, quoteLine.partDescription, quoteLine.quantity,
                                                quoteLine.lineListPrice.setScale(2), quoteLine.currencyCode, quoteLine.lineAdjustedAmount.setScale(2), quoteLine.lineAdjustedPercent.setScale(2),
                                                quoteLine.lineQuotePrice.setScale(2), quoteLine.lineTotalPrice.setScale(2));
    }    

}