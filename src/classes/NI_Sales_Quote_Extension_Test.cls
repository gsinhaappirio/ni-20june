/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NI_Sales_Quote_Extension_Test {
    
    @isTest(seeAllData=true)
    static void validateGetQuoteLineList(){
        Test.setMock(HttpCalloutMock.class, new Ni_Http_Response_Mock());
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.oracle_Header_Id__c = 123;
        List<NI_Sales_Sales_QuoteLineDTO> quoteLinesList;
        NI_Sales_Quote_Extension quoteExt = new NI_Sales_Quote_Extension(new ApexPages.StandardController(quote));
        Test.setMock(WebServiceMock.class, new NI_Sales_QuoteWS_QuoteRequest_MockImpl());
        quoteLinesList = quoteExt.getQuoteLineList();
        System.assertEquals('1', quoteLinesList[0].partNumber);
        System.assertEquals('2', quoteLinesList[1].partNumber);
    }
    
}