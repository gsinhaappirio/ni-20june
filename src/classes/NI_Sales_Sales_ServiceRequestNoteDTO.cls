public with sharing class NI_Sales_Sales_ServiceRequestNoteDTO {
    
    public String createdBy { get; set; }
    public DateTime creationDate { get; set; }
    public String type { get; set; }
    public String comments { get; set; }
	public String creationDateString { get; set; } 
    
    public NI_Sales_Sales_ServiceRequestNoteDTO(){}
    
    /**
    * Constructor.
    * @param createdBy name of this note's author
    * @param creationDate date this note was created on
    * @param type type of this note
    * @param comments contents of this note
    */
    public NI_Sales_Sales_ServiceRequestNoteDTO(String createdBy, DateTime creationDate, String type, String comments){
        this.createdBy = createdBy;
        this.creationDate = creationDate;
        this.type = type;
        this.comments = comments;
		this.creationDateString = date.newinstance(creationDate.year(), creationDate.month(), creationDate.day()).format();
    }
    
}