/**
*
*Name        : NI_Global_Wrapper 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 06th June, 2017
*Purpose     : Global Wrapper Class
*
**/

public with sharing class NI_Global_Wrapper{
    
    //Variables
    @AuraEnabled
	public String articleStatus 			{get;set;}
    @AuraEnabled
    public Id articleApproverId 			{get;set;}
    @AuraEnabled
    public Boolean isVisible    			{get;set;}
    @AuraEnabled
    public Boolean isEditable   			{get;set;}
    @AuraEnabled
    public Integer totalScore   			{get;set;}
    @AuraEnabled
    public String comments		   			{get;set;}
    @AuraEnabled
    public String rejectReason		   		{get;set;}
    @AuraEnabled
    public Boolean isReadOnly       		{get;set;}
    @AuraEnabled
    public Boolean isReject         		{get;set;}
    @AuraEnabled
    public Boolean isRejectedArticle         		{get;set;}
    @AuraEnabled
    public Boolean displayButtons   		{get;set;}
    @AuraEnabled
    public Boolean isSubmitter				{get;set;}
    @AuraEnabled
    public Boolean isPublisher				{get;set;}
    @AuraEnabled
    public Boolean isAvailable				{get;set;}
    @AuraEnabled
    public Boolean isPublishPermission		{get;set;}
    @AuraEnabled
    public Boolean isEOLArchive				{get;set;}
    @AuraEnabled
    public Boolean isSetEOLApprover			{get;set;}
    @AuraEnabled
    public Boolean isEOLArchiveApprover		{get;set;}
    @AuraEnabled
    public Boolean EOLApprover				{get;set;}
    @AuraEnabled
    public String label					{get;set;}
    @AuraEnabled
    public Object value					{get;set;}
    
    //Constructor
    public NI_Global_Wrapper(){
    }
}