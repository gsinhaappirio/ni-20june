@isTest(SeeAllData=true)
Private class NI_Note_Trigger_Test
{
    
    static testMethod void testNote_Trigger()
    {
         test.starttest();
         Boolean m_isExecuting = false;
         Integer batchSize = 0;
    
         //-----------Insert Account---------
         list<account> acclist = new list<Account>();
         account acc= new account(name= 'test',OwnerId = UserInfo.getUserId(),ShippingCountry = 'United States',ShippingStreet = '1/AX39',ShippingCity = 'New York',ShippingState = 'New York',ShippingPostalCode = '12119');
         acclist.add(acc);
         insert acclist;
        
         //-----------Insert Opportunity---------
         list<opportunity> opplist =  new list<opportunity>();
         Opportunity opp = new opportunity(name='test2',AccountId=acclist[0].id,Multi_Phase_Opportunity__c=null,StageName='Qualify',ForecastCategoryName='funnel',CloseDate=system.today(),Amount = 1000  );
         opplist.add(opp);
         insert opplist;
        
         //-----------Insert Case---------
         Case cse=new Case();
         insert cse;
    
         //-----------Insert Note---------
         list<Note> nlist = new list<note>();
         Note note = new Note();
         note.title='Test';
         note.parentId=opplist[0].id;
         nlist.add(note);
         Insert nlist;
         update nlist;
         
         //-----------Note Handler Class Method---------
         NI_NoteTriggerController ntc=new NI_NoteTriggerController(m_isExecuting,batchSize);
         
         test.stoptest();
    }
}