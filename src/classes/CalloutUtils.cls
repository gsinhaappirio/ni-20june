global class CalloutUtils {
    
    public static HttpRequest getPUTRequest(String url){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('PUT');
        return req;
    }
    
    public static HttpRequest getPUTRequest(String url, String username, String password){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        String auth = generateAuthHeaderValue(username, password);
        req.setHeader('Authorization', auth);
        req.setMethod('PUT');
        return req;
    }
    
    public static HttpRequest getPOSTRequest(String url){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        return req;
    }
    
    public static HttpRequest getPOSTRequest(String url, String body){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        if(body != null){
            req.setBody(body);
        }
        return req;
    }
    
    public static HttpRequest getPOSTRequest(String url, String username, String password, String body){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        String auth = generateAuthHeaderValue(username, password);
        req.setHeader('Authorization', auth);
        req.setMethod('POST');
        if(body != null){
            req.setBody(body);
        }
        return req;
    }
    
    // generates the value for a basic authentication header
    public static String generateAuthHeaderValue(String username, String password) {
        Blob headerValue = Blob.valueOf(username + ':' + password);
        return 'Basic ' + EncodingUtil.base64Encode(headerValue);
    }
    
    // send a request and return the response
    public static HttpResponse sendRequest(HttpRequest req) {
        Http http = new Http();
        return http.send(req);
    }

}