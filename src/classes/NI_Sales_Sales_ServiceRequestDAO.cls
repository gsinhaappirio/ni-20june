public with sharing class NI_Sales_Sales_ServiceRequestDAO {

    /**
    * Retrieve the Notes associated to a specific Service Request.
    * @param serviceRequestId the Service Request's Id
    */
    public List<NI_Sales_Sales_ServiceRequestNoteDTO> getServiceRequestNotes(Decimal serviceRequestId){
        List<NI_Sales_Sales_ServiceRequestNoteDTO> noteList = new List<NI_Sales_Sales_ServiceRequestNoteDTO>();
        Oracle_ServiceRequestNote.SRNotes360SOAP srn = new Oracle_ServiceRequestNote.SRNotes360SOAP();
        
        try {
            Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element[] response = srn.getSRNotes((Long) serviceRequestId);
            if(response != null){
                for(Integer i = 0; i < response.size(); i++) {
                    noteList.add(mapNoteToDTO(response[i]));
                }
            }
            
        } catch (System.CalloutException e) {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Service Request Notes: Error calling web service'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Service Request Notes: Unable to reach service'));
            System.debug(e.getTypeName() + ': ' + e.getMessage());
            
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve Service Request Notes'));
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        
        return noteList;
    }
    
    /**
    * Populates a ServiceRequestNoteDTO object.
    * @param note the source Service Request Note object
    * @return the populated DTO
    */
    @TestVisible
    private NI_Sales_Sales_ServiceRequestNoteDTO mapNoteToDTO(Oracle_ServiceRequestNoteResponse.ServiceRequestNote_element note){
        // noteDate is stored in epoch seconds
        return new NI_Sales_Sales_ServiceRequestNoteDTO(note.noteCreatedBy, DateTime.newInstance( ((Long) note.noteDate) * 1000), note.noteType, note.noteComments);
    }
    
}