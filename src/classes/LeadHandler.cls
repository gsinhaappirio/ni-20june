/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            LeadHandler
   Description:     This class contains support actions for triggers on Lead

   Date                            Version            Author                                    Summary of Changes
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   29th September 2014               1.0                OffshoreTeam_Accenture                    Initial Release
   17th February 2015                1.1                Ricardo Vargas (ricardo.vargas@ni.com)    USZipCodeStateMappingCustomSetting__c instance calls removed
   26th August    2015               1.2                Gabriel Mora                              Created asignTaskBackToOwner
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class LeadHandler {

    final static string Insert_FailedLbl = system.label.Insert_Failed;
    final static string SpaceLbl = system.label.Space;
    final static string APPNAMELBL = system.label.App_Name;
    final static string CLASSNAME = 'LeadHandler';
    final static string METHODNAME = 'insertState';
    final static string trgrName = 'Lead_Trigger';
    final static string Closed_Converted_to_OpportunityLbl = system.label.Closed_Converted_to_Opportunity;
    final static string Converted_to_OpportunityLbl = system.label.Converted_to_Opportunity;
    final static string QUALIFIED = system.label.qualified;
    final static string ASSIGNED = system.label.assigned;
    
    public static void updateAccountFromContact(List<Lead> leads){
        
        List<Id> contactIds = new List<Id>();
        for(Lead lead : leads){
            if(lead.contact__c != null){
                contactIds.add(lead.Contact__c);
            }
        }
        
        Map<Id, Contact> contacts = new Map<Id, Contact>([Select Id, AccountId from Contact where Id in :contactIds]);
        for(Lead lead : leads){
            if(lead.contact__c != null){
                Contact contact = contacts.get(lead.contact__c);
                lead.account__c = contact.AccountId;
            }
            
        }
    }
    
    public static void deleteContact(List<Lead> leads){
        
        for(Lead lead : leads){
            if(lead.contact__c != null){
                lead.contact__c = null;
            }
            
        }
    }
    
    
    public static void updateCountry(List<Lead> leads){
        List<Country__c> countries = [Select Id, Full_Name__c, Name, Three_Digit_ISO_Code__c, Other_Names__c, Override_Lead_Language__c, Primary_Language__c from Country__c];
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        for(Lead lead : leads){
            if(lead.Country != null){
                Country__c country = countryMap.get(lead.Country.trim().touppercase());
                if(country != null){
                    lead.Country = country.Name;
                    lead.Country__c = country.Id;
                    if(country.Override_Lead_Language__c && lead.Language__c != country.Primary_Language__c){
                        lead.Language__c = country.Primary_Language__c;
                    }
                }
            }
        }
    }

    @future
    public static void assignSkills(Set<Id> leadIds){
        deleteSkills(leadIds);
        addSkills(leadIds);
    }
    
    private static void deleteSkills(Set<Id> leadIds){
        List<Lead_Skill__c> leadSkills = [Select Id from Lead_Skill__c where Lead__c in :leadIds];
        if(leadSkills != null && !leadSkills.isEmpty()){
            Database.delete(leadSkills, false);   
        }
    }
    
    private static void addSkills(Set<Id> leadIds){
        List<Lead> leads = [Select Id, Name, Country, Language__c, Industry_Segment__c, OwnerId  from Lead where Id in :leadIds];
        
        List<Skill__c> skills = new List<Skill__c>([Select Id, Name from Skill__c]);
        Map<String, Skill__c> skillMap = new Map<String, Skill__c>();
        for(Skill__c skill : skills){
            skillMap.put(skill.Name.trim().toUpperCase(), skill);
        }
        
        Map<Id, Group> groups = new Map<Id, Group>([SELECT Id, Name, DeveloperName FROM Group WHERE Type = 'Queue']);
        Map<String, Queue_Settings__c> queueSettings = Queue_Settings__c.getall();
        
        List<Country__c> countries = [Select Id, Name, Full_Name__c, Three_Digit_ISO_Code__c, Other_Names__c, Override_Lead_Language__c, Primary_Language__c, Lead_Language_Skill_Required__c from Country__c];
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        List<Lead_Skill__c> skillsToInsert = new List<Lead_Skill__c>();
        
        // language skills
        for(Lead lead : leads){
            Country__c country;
            if(lead.country != null){
                country = countryMap.get(lead.country.trim().touppercase());
            }
            
            // language skill
            if(lead.Language__c != null){
                Lead_Skill__c leadSkill = new Lead_Skill__c();
                leadSkill.Lead__c = lead.Id;
                Skill__c skill = skillMap.get(lead.Language__c.trim().toUpperCase());
                if(skill != null){
                    leadSkill.Skill__c = skill.Id;
                    leadSkill.Required__c = country != null ? country.Lead_Language_Skill_Required__c : false;
                    skillsToInsert.add(leadSkill);
                }
            }
            
            // academic skill
            if(lead.Industry_Segment__c != null && lead.Industry_Segment__c.toLowerCase().contains('academic')){
                Lead_Skill__c leadSkill = new Lead_Skill__c();
                leadSkill.Lead__c = lead.Id;
                Skill__c skill = skillMap.get('ACADEMIC');
                leadSkill.Skill__c = skill.Id;
                leadSkill.Required__c = false;
                Group g = groups.get(lead.ownerId);
                if(g != null){
                    Queue_Settings__c settings = queueSettings.get(g.DeveloperName);
                    leadSkill.Required__c = settings != null ? settings.Academic_Skills_Required__c : false;
                }
                skillsToInsert.add(leadSkill);
            }
        }
        
        if(skillsToInsert != null && !skillsToInsert.isEmpty()){
            Database.insert(skillsToInsert, false);
        }
        
    }

    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: List<Lead> (Trigger.New)
    * Return: void
    * Description:
    ***********************************************************************************************************************
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Not Applicable
    * Trigger Events : Before Insert, Before Update
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/
    public static void updatestatus(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
        string errorDetail = system.label.Space;
        list<QueueSobject> QueueList = new list<QueueSobject>();
        set<ID> QueueId= new set<ID>();

        try{

            for(lead newlead : newLeadMap.values()){
                Lead oldLead = oldLeadMap.get(newlead.id);

                if(String.valueOf(oldLead.ownerId).startsWith('00G') &&  String.valueOf(newLead.ownerId).startsWith('005') && newlead.status == QUALIFIED){
                    // if the lead is being assigned from a queue to a user, set the status as assigned
                    newlead.status=ASSIGNED;
                }else if(newLead != null && newLead.ownerId != null && String.valueOf(newLead.ownerId).startsWith('00G') && oldLead != null && oldLead.ownerId != null && !String.valueOf(oldLead.ownerId).startsWith('00G')){
                    //if we're going back to a queue, set the status as qualified so that it gets picked up by a user
                    newlead.status = QUALIFIED;
                }

                // Set lead Close Reason on conversion
                if(newlead.IsConverted==true && newlead.status==Closed_Converted_to_OpportunityLbl){
                    newlead.Close_Reason__c=Converted_to_OpportunityLbl;
                }
            }
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }

    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: List<Lead> (Trigger.New),Map<Id,Lead> (Trigger.OldMap)
    * Return: void
    * Description:
    ***********************************************************************************************************************
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : Before Update
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
     public static void validateCMDM(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){

        for(lead newlead : newLeadMap.values()){
            System.debug('UserInfo.getName() :' +UserInfo.getName());
            Lead oldlead = oldLeadMap.get(newlead.id);
            if( (oldlead.account__c != null || oldlead.contact__c != null ) && UserInfo.getuserName() == System.label.Integration_User){
                 newlead.account__c= oldlead.account__c;
                 newlead.contact__c = oldlead.contact__c;
             }

        }

    }

    public static void copyLanguageFromContactToLead(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
        // if lead's language is null, copy the language from the contact (if it exists)
        
        List<String> contactIds = new List<String>();
        for(Lead newLead : newLeadMap.values()){
            contactIds.add(newLead.contact__c);
        }
        
        Map<Id, Contact> contacts = new Map<Id, Contact>([Select Id, Language__c, Name from Contact where Id in :contactIds]);
        
        for(Lead newLead : newLeadMap.values()){
            Lead oldLead = oldLeadMap.get(newLead.Id);
            Contact contact = contacts.get(newLead.contact__c);
            if(newLead.Cmdm_Resolution_Complete__c && !oldLead.Cmdm_Resolution_Complete__c){
                System.debug('***** resolution is complete');
                if(newLead.language__c == null && contact != null && contact.language__c != null){
                    newLead.language__c = contact.language__c;
                }
            }
        }
    }
    
     /**
    * @author        tyler hobbs
    * @created       1/12/15
    * @param         newLeads: map of the new leads. (trigger.newmap)
    */
    public static void updateTasksOwner(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
        Set<ID> newIds = newLeadMap.keySet();
        Set<ID> oldIds = oldLeadMap.keySet();
        
        List<Task> tasks = [Select Id, whoId, ownerId from Task where IsClosed = FALSE and whoId in :newIds];
        List<Task> tasksToUpdate = new List<Task>();
        
        for(Task task : tasks){
            
            Lead newLead = newLeadMap.get(task.whoId);
            Lead oldLead = oldLeadMap.get(task.whoId);
            
            if(task.ownerId != null && newLead.ownerId != null && oldLead.ownerId == newLead.OwnerId && String.valueOf(task.OwnerId).startsWith('005')){
                // lead owner changed to a user (id starts with 005)
                tasksToUpdate.add(task);
            }else{
            
                if(task.ownerId != null && newLead.ownerId != null && task.ownerId != newLead.OwnerId && String.valueOf(newLead.OwnerId).startsWith('005')){
                    // lead owner changed to a user (id starts with 005)
                    task.ownerId = newLead.OwnerId;
                    tasksToUpdate.add(task);
                }
            }
        }
        if(tasksToUpdate != null && !tasksToUpdate.isEmpty()){
            update tasksToUpdate;
        }
    }
    
     /**
    * @author        Gabriel Mora
    * @created       08/27/2015
    * @param         newLeads: map of the new leads. (trigger.newmap)  
    */
    public static void updateEventsOwner(Map<id,Lead> newLeadMap, Map<id,Lead> oldLeadMap){
        
        Set<ID> newIds = newLeadMap.keySet();
        Set<ID> oldIds = oldLeadMap.keySet();


        //We get all events from the lead
        DateTime sysDateTime = DateTime.now();
        List<Event> events = [Select Id, whoId, ownerId, Type__c from Event where EndDateTime >=: sysDateTime and whoId in :newIds and IsChild = false];
        List<Event> eventsToUpdate = new List<Event>();
        
        //We assign future events
         if(events != null && !events.isEmpty()){

            for(Event event : events){
                    System.debug('updateEventsOwner event.Id: '+event.Id);
                    System.debug('updateEventsOwner event.ownerId: '+event.ownerId);

                    Lead newLead = newLeadMap.get(event.whoId);
                    Lead oldLead = oldLeadMap.get(event.whoId);
                
                    if(event.ownerId != null && newLead.ownerId != null && event.ownerId != newLead.OwnerId && String.valueOf(newLead.OwnerId).startsWith('005')){
                        // lead owner changed to a user (id starts with 005)
                        event.ownerId = newLead.OwnerId;
                        eventsToUpdate.add(event);
                    }
                
            }
            
            if(eventsToUpdate != null && !eventsToUpdate.isEmpty()){
                //Updating all events
                //try{
                update eventsToUpdate;
               /* }catch(DmlException ex){
                    //System.debug('System.DmlException ex.getDmlFieldNames(): '+ex.getDmlFieldNames());
                    System.debug('System.DmlException ex.getNumDml: '+ex.getNumDml() );
                    for (Integer i=0; i<ex.getNumDml(); i++)
                        System.debug('System.DmlException ex.getDmlId('+i+'): '+ex.getDmlId(i) );
                    
                }/**/
            }
        }

    }  

   /**********
    /*********************************************************************************************************************
    * Author:
    * Date:13/10/2014
    * Param: List<Lead> (Trigger.New)
    * Return: void
    * Description: This method is not being called from trigger any more as whole lead routing logic changed. No more account based routing
    ***********************************************************************************************************************
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Update
    * Summary of Changes :  - Initial version
    *********************************************************************************************************************
    public static void assignAssignmentRule(List<Lead> newLeadList, Map<Id,Lead> oldLeadMap){
        string errorDetail = system.label.Space;
        List<Id> lIds=new List<id>();
        try{
            for (lead l:newLeadList){
                if (l.Cmdm_Resolution_Complete__c){
                    lIds.add(l.Id);
                }
            }

            if (AssignAssignmentRule.assignAlreadyCalled()==FALSE)
            {
                AssignAssignmentRule.Assign(lIds);
            }
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }
    ****/

}