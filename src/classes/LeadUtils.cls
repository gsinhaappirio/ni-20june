global class LeadUtils implements BatchUtil.BatchUtilable {

    String QUALIFIED = Label.Qualified.trim().toUpperCase();
    String ASSIGNED = Label.Assigned.trim().toUpperCase();
    String ACCEPTED = Label.Accepted.trim().toUpperCase();

    public boolean HandleRecord(sObject s, BatchUtil callee) {
        String log = '';

        //if updating a contact
        if (s.getSObjectType().getDescribe().getName().toUpperCase() == 'LEAD'){

            callee.Log('Closing lead: ' + s.get('Id') + '\n');
            Lead lead = (Lead) s;

            Date today = System.now().dateGMT();
            Date createdDate = lead.createdDate.dateGMT();

            if(lead.cREQ_Date__c == null && lead.Status.trim().toUpperCase().equals(QUALIFIED)){
                Lead_Settings__c settings = Lead_Settings__c.getOrgDefaults();
                Integer slaDays = settings.Acceptance_SLA__c != null ? settings.Acceptance_SLA__c.intValue() : 7;
                Date dueDate = createdDate.addDays(slaDays);
                Integer delta = today.daysBetween(dueDate);

                // by setting it to < 0 (and not <=), we're closing it the morning after it the SLA date. So, if SLA is createdDate+7. it will close on the morning of createdData+8
                if(delta < 0){
                    s.put('Status', 'Closed - Aged');
                    s.put('Close_Reason__c', 'Aged Lead – Never Assigned');
                    return true;
                }

            }else if(lead.cREQ_Date__c == null && lead.Status.trim().toUpperCase().equals(ASSIGNED)){
                Lead_Settings__c settings = Lead_Settings__c.getOrgDefaults();
                Integer slaDays = settings.Acceptance_SLA__c != null ? settings.Acceptance_SLA__c.intValue() : 7;
                Date dueDate = createdDate.addDays(slaDays);
                Integer delta = today.daysBetween(dueDate);

                // by setting it to < 0 (and not <=), we're closing it the morning after it the SLA date. So, if SLA is createdDate+7. it will close on the morning of createdData+8
                if(delta < 0){
                    s.put('Status', 'Closed - Aged');
                    s.put('Close_Reason__c', 'Aged Lead – Never Accepted/Rejected');
                    return true;
                }

            }else if(lead.cREQ_Date__c == null && lead.Status != null && lead.Status.trim().toUpperCase().equals(ACCEPTED)){
                Lead_Settings__c settings = Lead_Settings__c.getOrgDefaults();
                Integer slaDays = settings.Qualification_SLA__c != null ? settings.Qualification_SLA__c.intValue() : 30;
                Date dueDate = createdDate.addDays(slaDays);
                Integer delta = today.daysBetween(dueDate);

                // by setting it to < 0 (and not <=), we're closing it the morning after it the SLA date. So, if SLA is createdDate+30. it will close on the morning of createdData+31
                if(delta < 0){
                    s.put('Status', 'Closed - Aged');
                    s.put('Close_Reason__c', 'Aged Lead – Never Converted/Disqualified');
                    return true;
                }

            }
            return false;
        }else{
            callee.Log('Not a lead, do nothing \n');
            return false;
        }
    }

    webservice static String getNewLeadId(String userId){

        List<Queue_Settings__c> assignments = Queue_Settings__c.getall().values();
        Map<String, String> assignmentMap = new Map<String, String>();
        for(Queue_Settings__c assignment : assignments){
            assignmentMap.put(assignment.Group_Name__c, assignment.Name);
        }

        List<GroupMember> groups = [SELECT Group.Id, Group.Name, Group.Type, UserOrGroupId, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: userId and Group.Type = 'Regular'];

        List<String> queueNames = new List<String>();
        for(GroupMember gm : groups){
            queueNames.add(assignmentMap.get(gm.group.developerName));
        }

        List<Group> queues = [SELECT Id, Name FROM Group WHERE developerName in :queueNames];
        if(queues != null && queues.size() == 0){
            queues = null;
        }

        User user = [Select Id, FirstName, LastName, SmallPhotoUrl, FullPhotoUrl, profile.name, (Select Id, Name, Exclusive__c, Skill__c, Skill__r.Name, Skill__r.Id From User_Skills__r) From User where Id = :UserInfo.getUserId() LIMIT 1];
        List<User_Skill__c> skillSets = user.User_Skills__r;
        List<User_Skill__c> userSkillSetList = new List<User_Skill__c>();
        List<Skill__c> exclusiveSkills = new List<Skill__c>();
        Map<Id, Skill__c> userSkills = new Map<Id, Skill__c>();

        // check if any skills are exclusive
        if(skillSets != null && skillSets.size() > 0){
            exclusiveSkills = new List<Skill__c>();
            userSkillSetList = new List<User_Skill__c>();
            userSkills = new Map<Id, Skill__c>();
            for(User_Skill__c skillSet : skillSets){
                if(skillSet.Exclusive__c){
                    exclusiveSkills.add(skillSet.skill__r);
                    System.debug('***** exclusive skill: ' + skillSet.skill__r.name);
                }
                userSkillSetList.add(skillSet);
                userSkills.put(skillSet.Skill__r.Id, skillSet.Skill__r);
            }
        }

        if(exclusiveSkills == null || exclusiveSkills.size() == 0){
            exclusiveSkills = null;
        }

        List<Id> queueIds = new List<Id>();
        if(queues != null && !queues.isEmpty()){
            for(Group g : queues){
                System.debug('***** queue id: ' + g.Id);
                queueIds.add(g.Id);
            }
        }

        List<Lead> leads;
        Lead leadToAssign;

        if(exclusiveSkills != null && !exclusiveSkills.isEmpty()){
            // there are exclusive skills. only query for leads that have this skill
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true)
                    from Lead
                    where ownerId in :queueIds
                    and status = 'Qualified'
                    and Ready_to_be_Assigned__c = true
                    and isConverted = false
                    and Id in
                        (Select Lead__c from Lead_Skill__c where Skill__c in :exclusiveSkills)
                    ORDER BY CreatedDate DESC];
            System.debug('***** number of leads with skill: ' + leads.size());

        }else{
            // query leads but only get required skills for that lead
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true)
                    from Lead
                    where ownerId in :queueIds
                    and status = 'Qualified'
                    and Ready_to_be_Assigned__c = true
                    and isConverted = false
                    ORDER BY CreatedDate DESC];
            System.debug('***** number of leads: ' + leads.size());

        }

        List<SortedLead> sortedLeads = new List<SortedLead>();

        for(Lead lead : leads){
            SortedLead sl = new SortedLead(lead);
            sortedLeads.add(sl);
        }

        sortedLeads.sort();
	
        for ( SortedLead sl : sortedLeads )
        {
            Lead lead = sl.lead;
            System.debug('***** iterating over: ' + lead.name + ' - ' + lead.Id);
            if ( lead.lead_skills__r!=null && !lead.lead_skills__r.isEmpty() )
            {
                // the lead has required skills. loop through to verify the user has a match
                // TODO - how can we get rid of this n^2 runtime?
                //for ( Integer i=0 ; i<lead.lead_skills__r.size() ; i++ )
                for ( Integer i=lead.lead_skills__r.size()-1 ; i>=0 ; i-- )
                {
                    Lead_Skill__c leadSkill = lead.lead_skills__r.get(i);
                    if( leadSkill.Required__c )
                    {
                    	
                        System.debug('+++++ *required skill on lead: ' + leadSkill.Skill__r.Name);
                        if ( userSkills!=null && userSkills.get(leadSkill.skill__r.id)!=null )
                        {
                            System.debug('+++++ has a required skill: ' + userSkills.get(leadSkill.skill__r.id).Name);
                            leadToAssign = lead;
                            if ( leadSkill.Skill__r.Name.equals('Academic') )
								continue;
                            else
	                            break;
                        }
                        else
                        {
                            System.debug('+++++ user does not have required skill: ' + leadSkill.Skill__r.Name);
                            leadToAssign = null;
                            break;
                        }
                    }

                    //if ( i == lead.lead_skills__r.size()-1 )
                    if ( i == 0 )
                    {
                        leadToAssign = lead;
                        break;
                    }

                }
            }
            else
            {
                leadToAssign = lead;
            }

            if(leadToAssign != null)
            {
                break;
            }

        }

        if(leadToAssign != null){
            leadToAssign.ownerId = userId;
            leadToAssign.Status = 'Assigned';
            update leadToAssign;
            return String.valueOf(leadToAssign.Id);
        }else{
            return null;
        }

    }

    public class SortedLead implements Comparable {
        public Lead lead{get;set;}
        
        public SortedLead(Lead lead){
            this.lead = lead;
        }
        
        public Integer compareTo(Object compareTo) {
            SortedLead other = (SortedLead)compareTo;
            
            // this.lead is the one that is getting ordered.
            // "other" is the lead that it is being compared to
            // -1 pushes lead to the top (first), 1 pushes it down (last)
            
            if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c == null){
                // creq, other is not
                return -1;
            }else if(lead.cREQ_Date__c == null && other.lead.cREQ_Date__c != null){
                // not creq, other is
                return 1;
            }else if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c != null){
                // they're both cReqs, this should stay FIFO for cReqs
                if(lead.createdDate > other.lead.createdDate){
                    return 1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return -1;
                }
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') &&
                    !other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // sgl, other is not
                return -1;
            }else if(!lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // not sgl, other is
                return 1;
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // they're both sgl's, this should stay LIFO
                if(lead.createdDate > other.lead.createdDate){
                    return -1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return 1;
                }
            }else if(lead.createdDate > other.lead.createdDate){
                return -1;
            }else if(lead.createdDate < other.lead.createdDate){
                return 1;
            }
            
            return 0;
            
        }
        
    }

}