@isTest(seeAllData=true)
public class AccountTriggerController_Test {
    
    @isTest(seeAllData=true)
    private static void onBeforeInsertTest(){
        List<Account> accounts = new List<Account>();
        Test.startTest();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = 'Test';
            account.ShippingStreet = '123 Fake';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            accounts.add(account);
        }
        insert accounts;
        
        AccountTriggerController.onBeforeInsert(accounts);
        
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    private static void onBeforeUpdateTest(){
        List<Account> accounts = new List<Account>();
        Test.startTest();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = 'Test';
            account.ShippingStreet = '123 Fake';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            accounts.add(account);
        }
        insert accounts;
        
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for(Account account : accounts){
            account.ShippingCountry = 'GB';
            accountMap.put(account.Id, account);
        }
        
        AccountTriggerController.onBeforeUpdate(accounts, accountMap);
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    private static void onAfterUndeleteTest(){
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = 'Test';
            account.ShippingStreet = '123 Fake';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            accounts.add(account);
        }

        AccountTriggerController.onAfterUndelete(accounts);
    }
    
    
    @isTest(seeAllData=true)
    private static void onAfterDeleteTest(){
        List<Account> accounts = new List<Account>();
        Test.startTest();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = 'Test';
            account.ShippingStreet = '123 Fake';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            accounts.add(account);
        }
        insert accounts;
        
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for(Account account : accounts){
            accountMap.put(account.Id, account);
        }
        
        AccountTriggerController.onAfterDelete(accountMap);
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    private static void onAfterUpdate(){
        List<Account> accounts = new List<Account>();
        Test.startTest();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = 'Test';
            account.ShippingStreet = '123 Fake';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            accounts.add(account);
        }
        insert accounts;
        
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for(Account account : accounts){
            account.ShippingCountry = 'GB';
            accountMap.put(account.Id, account);
        }
        
        AccountTriggerController.onAfterUpdate(accounts, accountMap);
        Test.stopTest();
    }
    
    
}