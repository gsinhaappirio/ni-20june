@isTest(SeeAllData=true)
public class Ni_Merge_Accounts_Test {

    @isTest
    public static void initTest(){
        PageReference pageRef = new PageReference('/apex/ni_merge_accounts');
        Test.setCurrentPage(pageRef);
        
        //names to avoid duplicate accounts
        String [] names = new List<String> {'first','second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 
                                            'eighth', 'ninth', 'tenth'};
        
        Test.startTest();
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = names [i];
            account.shippingstreet = '123 Fake St';
            account.shippingcity = 'Austin';
            account.shippingcountry = 'US';
            accounts.add(account);
        }
        Ni_Merge_Accounts_Extension controller = new Ni_Merge_Accounts_Extension(new ApexPages.StandardSetController(accounts));
        
        controller.searchquery = 'tyler';
        controller.searchForAccounts();
        System.assertNotEquals(controller.results, null);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void mergeTest(){
        PageReference pageRef = new PageReference('/apex/ni_merge_accounts');
        Test.setCurrentPage(pageRef);
        
        //names to avoid duplicate accounts
        String [] names = new List<String> {'first','second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 
                                            'eighth', 'ninth', 'tenth'};
        
        Test.startTest();
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < 10; i++){
            Account account = new Account();
            account.name = names[i];
            account.shippingstreet = '123 Fake St';
            account.shippingcity = 'Austin';
            account.shippingcountry = 'US';
            insert account;
            accounts.add(account);
        }
        Ni_Merge_Accounts_Extension controller = new Ni_Merge_Accounts_Extension(new ApexPages.StandardSetController(accounts));
        
        controller.searchquery = 'Account';
        controller.searchForAccounts();
        controller.mergeAccounts();
        
        Test.stopTest();
        
    }
    
}