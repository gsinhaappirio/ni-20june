@isTest
public class TaskTriggerController_Test {

    @isTest(SeeAllData=true)
    private static void onBeforeInsertTest(){
        Test.startTest();
        TaskTriggerController.onBeforeInsert(null);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest(){
        
        Test.startTest();
        Lead lead = new Lead();
        lead.FirstName = 'Tyler';
        lead.lastName = 'Hobbs';
        lead.Company = 'NI';
        lead.LeadSource = 'Sales - Sales';
        lead.Country = 'US';
        insert lead;
        
        List<Task> newTaskList = new List<Task>();
        DateTime now = System.now();
        for(Integer i = 0; i < 10; i++){
            Task task = new Task();
            task.ReminderDateTime = now+i;
            task.whoId = lead.Id;
            task.Status = 'Not Started';
            newTaskList.add(task);
        }
        
        insert newTaskList;
        
        Map<Id, Task> taskMap = new Map<Id, Task>();
        for(Task task : newTaskList){
            taskMap.put(task.Id,task);
        }
        
        TaskTriggerController.onAfterInsert(taskMap);
        
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest2(){
        
        Test.startTest();
        Lead lead = new Lead();
        lead.FirstName = 'Tyler';
        lead.lastName = 'Hobbs';
        lead.Company = 'NI';
        lead.LeadSource = 'Sales - Sales';
        lead.Country = 'US';
        insert lead;
        
        Map<Id, Task> taskMap = new Map<Id, Task>();
        
        TaskTriggerController.onAfterInsert(taskMap);
        
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest3(){
        
        Test.startTest();
        Opportunity opp = new Opportunity();
        opp.Name = 'Testing';
        opp.stageName = 'Qualify';
        opp.CloseDate = System.today()+10;
        insert opp;
        
        List<Task> newTaskList = new List<Task>();
        DateTime now = System.now();
        for(Integer i = 0; i < 10; i++){
            Task task = new Task();
            task.ReminderDateTime = now+i;
            task.whatId = opp.Id;
            task.Status = 'Not Started';
            newTaskList.add(task);
        }
        
        insert newTaskList;
        
        Map<Id, Task> taskMap = new Map<Id, Task>();
        for(Task task : newTaskList){
            taskMap.put(task.Id,task);
        }
        TaskTriggerController.onAfterInsert(taskMap);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterInsertTest4(){
        Test.startTest();
        TaskTriggerController.onAfterInsert(null);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterUpdateTest(){
        Test.startTest();
        Opportunity opp = new Opportunity();
        opp.Name = 'Testing';
        opp.stageName = 'Qualify';
        opp.CloseDate = System.today()+10;
        insert opp;
        
        List<Task> newTaskList = new List<Task>();
        DateTime now = System.now();
        for(Integer i = 0; i < 10; i++){
            Task task = new Task();
            task.ReminderDateTime = now+i;
            task.whatId = opp.Id;
            task.Status = 'Not Started';
            newTaskList.add(task);
        }
        
        Map<Id, Task> taskMap = new Map<Id, Task>();
        for(Task task : newTaskList){
            taskMap.put(task.Id,task);
        }
        
        TaskTriggerController.onAfterUpdate(taskMap, taskMap);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterUpdateTest2(){
        Test.startTest();
        TaskTriggerController.onAfterUpdate(null, null);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onBeforeUpdateTest(){
        Test.startTest();
        TaskTriggerController.onBeforeUpdate(null, null);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void onAfterDeleteTest(){
        Test.startTest();
        TaskTriggerController.onAfterDelete(null);
        Test.stopTest();
    }
    
}