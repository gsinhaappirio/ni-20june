global class SandboxRefreshSetups implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        updateServerUrls(context.sandboxName().tolowerCase());
        updateBaseUrls(context.sandboxName().tolowerCase());
        updateEmails();
    }
    
    global void updateBaseUrls(String sandboxName){
        
        String objectName = 'Base_Urls__c';
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        String query = 'SELECT';
        for(String s : fields.keySet()) {
            query += ' ' + s + ',';
        }
        query = query.substring(0, query.length()-1);
        query += ' FROM Base_Urls__c';
        
        List<sObject> sobjs = Database.query(query);
        
        for(sObject sobj : sobjs){
            for(String s : fields.keySet()){
                DisplayType fieldType = fields.get(s).getDescribe().getType();
                if(fieldType == Schema.DisplayType.URL){
                    String url = (String) sobj.get(s);
                    if(url != null){
                        url = url.replaceAll('\\.natinst\\.', '-' + sandboxName + '.natinst.');
                        url = url.replaceAll('\\.ni\\.', '-' + sandboxName + '.ni.');
                        sobj.put(s, url);
                    }
                }
            }
        }
        
        Database.update(sobjs);
    }
    
    global void updateServerUrls(String sandboxName){
        List<NI_Server_URL_List__c> serverList = [Select Name, Server_URL__c from NI_Server_URL_List__c];
        for(NI_Server_URL_List__c server : serverList){
            server.Server_URL__c = server.Server_URL__c.replaceAll('parsec.*\\.ni','parsec-' + sandboxName + '.ni');
        }
        update serverList;
    }
    
    global void updateEmails(){
        // only reset emails for NI System Administrator, NI Business Support User, NI Developer User, and NI API Only User profiles
        List<User> users = [Select Id, Email from User where ProfileId in ('00ei0000001FDk8','00ei0000001FDyu','00ei0000001FDje','00ei0000001FDjK','00e31000001FROd')];
        for(User user : users){
            String email = user.Email;
            if(email.contains('=')){
                Integer ndx = email.indexOf('@');
                if(ndx > -1){
                    user.Email = email.substring(0, ndx).replace('=','@');
                }
            }
        }
        Database.update(users, false);
    }
    
}