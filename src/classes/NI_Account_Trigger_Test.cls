/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for Account_Trigger class.
  */
@isTest(SeeAllData=true)
Private class NI_Account_Trigger_Test{
    // Start of Constants for Test Methods
    private static final String ALIAS = 'sch1';
    private static final String SYSTEM_ADMIN = 'System Administrator';
    
    /**
    * @description       Test method to verify account trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */
    @isTest(SeeAllData=true)
    private static void testAccount_Trigger_Test(){
    
        User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
        test.starttest();
        Boolean bln = false;
        Integer ints = 0;
        
        List<Account> accList = new List<Account>();
        Account accObj = new account();
        
        accObj.Name = 'TestAccountTrigger';
        accObj.OwnerId = UserInfo.getUserId();
        accObj.ShippingCountry = 'United States';
        accObj.ShippingStreet = '1/AX39';
        accObj.ShippingCity = 'New York';
        accObj.ShippingState = 'New York';
        accObj.ShippingPostalCode = '12119';
        accObj.BillingCountry = 'United States';
        accObj.BillingStreet = '1/AX39';
        accObj.BillingCity = 'New York';
        accObj.BillingState = 'New York';
        accObj.BillingPostalCode = '12119';
        //accObj.recordtypeid = [select id from recordtype where Name = 'Provisional' limit 1].Id;
        accObj.Fax_Country_Code__c = '12345';
        accObj.Fax_Area_Code__c = '56789';
        accObj.Fax_Number__c = '1234567891';
        accObj.Phone_Country_Code__c = '223';
        accObj.Phone_Area_Code__c = '586321';
        accObj.Phone_Number__c = '4561237892';
        accObj.Fax='53545';
        accObj.phone='5454';
        accObj.Alias1__c='abc';
        accObj.Alias2__c='d';
        accObj.Alias3__c='ef';
        accObj.Account_Name_Phonetic__c='SA';
        accObj.Account_Name_Roman__c='rm';
        
        accList.add(accObj);
        insert accList;
        
        Contact con= new Contact();
        con.AccountId=accObj.Id;
        con.LastName='yxz';
        
        insert con;
        
        List<Account> aList = new List<Account> ();
        for(Account accListVar : accList){
            accListVar.Phone = '1112119111';
            accListVar.Fax = '1234567890';
            accObj.BillingCountry = 'United Statesa';
            accObj.BillingStreet = '7-1335';
            accObj.BillingCity = 'Newk';
            accObj.BillingState = 'Nerk';
            accObj.BillingPostalCode = '012119';
            
            aList.add(accListVar);
        }
        update aList; 
        delete accObj;
        undelete accObj;
        
        //----------------------  Account Trigger Controller Class Methods -----------------------
        AccountTriggerController atc= new AccountTriggerController(bln,ints);
        
        
        //----------------------  Account Handler Class Methods -----------------------
        //AccountHandler.ChangeContactFields(accList,aList);
        //AccountHandler.ChangeContactFields(null,null);
         
        
        System.runAs(runUser) {
            // The following code runs as user 'runUser' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
        }
        
       test.stoptest();
    }
}