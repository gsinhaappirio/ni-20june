@isTest(SeeAllData=true)
public class NI_WebOrder_Extension_Test {

    @isTest
    public static void initTest() {
        NI_OD_ORDER_HDR_ALL_V__x order = new NI_OD_ORDER_HDR_ALL_V__x();
        order.oracle_header_id__c = '1234567';
        order.PO_NUMBER__c = '1234567';
        ApexPages.StandardController sc = new ApexPages.standardController(order);
        NI_WebOrder_Extension controller = new NI_WebOrder_Extension(sc);
        controller.redirectToWebOrderPage();
    }
   
}