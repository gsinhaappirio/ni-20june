public class HCMRecordTriggerController {

    public static void onAfterInsert(List<HCM_Record__c> newList){
        HCMRecordHandler.updateApprovedUserRequests(newList);
        HCMRecordHandler.updateUsers(newList);
    }
    
    public static void onAfterUpdate(List<HCM_Record__c> newList){
        HCMRecordHandler.updateUsers(newList);
    }
    
}