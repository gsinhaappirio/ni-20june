public class NI_WebQuote_Extension{

    private ApexPages.StandardController sc;
    private final NI_OD_QUOTE_HDR_ALL_V__x quote;
    
    public NI_WebQuote_Extension (ApexPages.StandardController controller) {
        this.sc = controller;
        this.quote= (NI_OD_QUOTE_HDR_ALL_V__x)sc.getRecord();
    }

    public PageReference redirectToWebQuotePage() {
            
        String oracleHeaderId = String.valueOf(quote.oracle_header_id__c);
        
        NI_OD_QUOTE_CNT_ALL_V__x [] contacts = [select id, SFDC_CONTACT_ID__r.email from NI_OD_QUOTE_CNT_ALL_V__x where ORACLE_HEADER_ID__c = :oracleHeaderId ];

        if(Test.isRunningTest()){
            
            this.quote.quote_number__c = '1234';
            Account account = new Account();
            account.name = 'Fake Account';
            account.ShippingStreet = '123 Fake St';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            insert account;
            
            Contact contact = new Contact();
            contact.firstName = 'Fake';
            contact.LastName = 'Contact';
            contact.Email = 'fake@ni.com';
            insert contact;
            
            NI_OD_QUOTE_CNT_ALL_V__x c = new NI_OD_QUOTE_CNT_ALL_V__x();
            c.SFDC_CONTACT_ID__c = contact.Id;
            
            contacts.add(c);
        }
        
        if(contacts.size() > 0){
        
            NI_Server_URL_List__c urls = NI_Server_URL_List__c.getInstance('SineURL');
            String url = urls.Server_URL__c + '/apps/utf8/niwq.rq_encrypt?p_qno=';
            //remove site from quote number
            url += quote.quote_number__c;
            url += '&p_secondary_type=email&p_secondary_data=' + contacts[0].SFDC_CONTACT_ID__r.email;
            
            System.debug('******'+url);
            
            PageReference pageRef = new PageReference(url);
            return pageRef.setRedirect(true);
        }
        else{
            Ni_Quote_Settings__c quoteSettings = Ni_Quote_Settings__c.getInstance(UserInfo.getUserID());
            String webLinkError = quoteSettings.Web_Quote_Link_Error__c;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, webLinkError ));
            return null;
        }

    }

}