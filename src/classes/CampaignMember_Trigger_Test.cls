@isTest
public class CampaignMember_Trigger_Test {

    @isTest(SeeAllData=true)
    private static void insertTest(){
        
        Test.startTest();
        Campaign campaign = new Campaign();
        campaign.Name = 'Test Campaign';
        campaign.Application__c = 'Datalogging'; 
        insert campaign;
        
        Lead lead = new Lead();
        lead.FirstName = 'First';
        lead.LastName = 'Last';
        lead.LeadSource = 'Sales - Sales';
        lead.Country = 'US';
        lead.Company = 'NI';
        insert lead;
        
        CampaignMember cm = new CampaignMember();
        cm.campaignId = campaign.Id;
        cm.LeadId = lead.Id;
        
        insert cm;
        
        Test.stopTest();
        
    }
    
}