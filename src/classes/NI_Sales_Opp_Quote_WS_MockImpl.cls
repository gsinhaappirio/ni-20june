@isTest	
public with sharing class NI_Sales_Opp_Quote_WS_MockImpl implements WebServiceMock{

	public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	 
	       NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element respElement = 
	           new NI_Sales_Opp_Quote_WS.OppQuoteLinkResponse_element();
	           
	       respElement.status = 'S';	           
	       response.put('response_x', respElement); 
	       
	
	}
}