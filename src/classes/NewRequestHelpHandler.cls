public class NewRequestHelpHandler {
    
    public static void populateRequesterLocale(List<New_Request_Help__c> requestHelpList){
        for(New_Request_Help__c requestHelp : requestHelpList){
            requestHelp.Requester_Locale__c = UserInfo.getLocale();
        }
    }
    
    public static void createJiraTicket(List<New_Request_Help__c> requestHelpList){
        String objectType = 'New_Request_Help__c';	// the type of sfdc object
        
        // create the issue - 100 callouts per session
        for(New_Request_Help__c requestHelp : requestHelpList){
            // this will only fire if the destination email is null and the Jira routing data exists
            if(requestHelp.Jira_Project_Key__c != null && !requestHelp.Jira_Project_Key__c.equals('') && 
               requestHelp.Jira_Mapping_Id__c != null && requestHelp.Jira_Mapping_Id__c != -1 && 
               requestHelp.Jira_Issue_Type_Id__c != null && requestHelp.Jira_Issue_Type_Id__c != -1){
                   JiraUtils.createIssue(String.valueOf(requestHelp.Jira_Mapping_Id__c), objectType, requestHelp.Id, requestHelp.Jira_Project_Key__c, String.valueOf(requestHelp.Jira_Issue_Type_Id__c));
               }
        }
    }
    
    public static void synchronizeJiraTicket(List<New_Request_Help__c> requestHelpList){
        String objectType = 'New_Request_Help__c';	// the type of sfdc object
        
        // sync the issue - 100 callouts per session
        for(New_Request_Help__c requestHelp : requestHelpList){
            // this will only fire if the destination email is null and the Jira routing data exists
            if(requestHelp.Jira_Project_Key__c != null && !requestHelp.Jira_Project_Key__c.equals('') && 
               requestHelp.Jira_Mapping_Id__c != null && requestHelp.Jira_Mapping_Id__c != -1 && 
               requestHelp.Jira_Issue_Type_Id__c != null && requestHelp.Jira_Issue_Type_Id__c != -1 &&
               requestHelp.Jira_Issue_Key__c != null && !requestHelp.Jira_Issue_Key__c.equals('')
               && JiraUtils.currentUserIsNotJiraAgent()){
                   JiraUtils.synchronizeIssue(String.valueOf(requestHelp.Jira_Mapping_Id__c), objectType, requestHelp.Id);
               }
        }
    }
    
    public static void determineDestinationEmail(List<New_Request_Help__c> newRequestHelpList){
        
        //query the catch all queue
        Group catchAllQueue;
        
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Country__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String query = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues){
            query += s.getDescribe().getName() + ',';
        }
        // Trim last comma
        query = query.subString(0, query.length() - 1);
        query += ' FROM Country__c';
        List<Country__c> countries = Database.query(query);
        
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        // get the account ids to query
        List<Id> accountIds = new List<Id>();
        for(New_Request_Help__c requestHelp : newRequestHelpList){
            accountIds.add(requestHelp.Account__c);
        }
        
        // query the country for the accounts
        Map<Id, Account> accounts = new Map<Id, Account>([Select Id, ShippingCountry from Account where Id = :accountIds]);
        // loop through each request help, adding the email from the country settings
        for(New_Request_Help__c requestHelp : newRequestHelpList){
            
            Account account = accounts.get(requestHelp.Account__c);
            Country__c country = account != null && account.ShippingCountry != null ? countryMap.get(account.shippingcountry.trim().toUpperCase()) : null;
            
            if(country != null && country.Request_Help_Active__c == true){
                String email;
                if(requestHelp.Category__c.equalsIgnoreCase('loaners')){
                    // it's a loan request, get the loaner email from the country settings
                    email = country != null && country.Loan_Request_Email__c != null ? country.Loan_Request_Email__c : null;
                    if(email == null || email.equals('')){
                        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
                        email = rhs.Default_Loan_Email__c;
                    }
                }else if(requestHelp.Category__c.equalsIgnoreCase('finance')){
                    email = country != null && country.Finance_Request_Email__c != null ? country.Finance_Request_Email__c : null;
                    if(email == null || email.equals('')){
                        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
                        email = rhs.Default_Finance_Email__c;
                    }
                }else if(requestHelp.Category__c.equalsIgnoreCase('sales operations')){
                    email = country != null && country.Sales_Operations_Request_Email__c != null ? country.Sales_Operations_Request_Email__c : null;
                    if(email == null || email.equals('')){
                        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
                        email = rhs.Default_Sales_Operations_Email__c;
                    }
                }else if(requestHelp.Category__c.equalsIgnoreCase('contracts & deal desk')){
                    email = country != null && country.Contracts_Deal_Desk_Request_Email__c != null ? country.Contracts_Deal_Desk_Request_Email__c : null;
                    if(email == null || email.equals('')){
                        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
                        email = rhs.Default_Contracts_Deal_Desk_Email__c;
                    }
                }else if(requestHelp.Category__c.equalsIgnoreCase('support-close opportunity') ||requestHelp.Category__c.equalsIgnoreCase('support-support customer')){
                    email = country != null && country.Support_Request_Email__c != null ? country.Support_Request_Email__c : null;
                    if(email == null || email.equals('')){
                        Request_Help_Settings__c rhs = Request_Help_Settings__c.getInstance();
                        email = rhs.Default_Support_Request_Email__c;
                    }
                }
                requestHelp.Destination_Email__c = email;
                requestHelp.Country__c = country.Name != null ? country.Name : null;
                
            }else{
                // couldn't find the country in the country settings, assign to catch all
                if(catchAllQueue == null){
                    catchAllQueue = [Select Id, DeveloperName from Group where DeveloperName = 'RH_Catch_All'];
                }
                requestHelp.ownerId = catchAllQueue.Id;
            }
        }
        
    }
}