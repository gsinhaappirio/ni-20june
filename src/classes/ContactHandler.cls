/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            ContactHandler
   Description:     This class contains support actions for triggers on Contact  
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class ContactHandler {

/**********************************************************************************************************************
    * Author: 
    * Date:
    * Param: set<Id> (ContactId)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Undelete
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/
   
   
  @future
  public static void undeleteContact(set<Id> ContactId){
      list<Contact> newContactList = new list<Contact>();
      list<Contact> updatedContactList= new list<Contact>();
      newContactList =[select id,LastUndeletedDate__c from Contact where id in:ContactId ];
      for(Contact newacc :newContactList ){
           newacc.LastUndeletedDate__c=system.now();
           updatedContactList.add(newacc);
      } 
      
      update updatedContactList;
   }

    @future
    public static void setOwnerFromAccount(Set<Id> contactIds){
        
        List<Contact> contacts = [Select Id, OwnerId, AccountId, Account.OwnerId from Contact where Id in :contactIds];
        List<Contact> toUpdate = new List<Contact>();
        
        for(Contact contact : contacts){
            if(contact.accountId != null && contact.account.ownerId != null && contact.ownerId != contact.account.ownerId){
                contact.ownerId = contact.account.ownerId;
                toUpdate.add(contact);
            }
        }
        
        if(!toUpdate.isEmpty()){
            Database.update(toUpdate, false);
        }
    }
        
}