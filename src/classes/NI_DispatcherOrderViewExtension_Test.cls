@isTest
public class NI_DispatcherOrderViewExtension_Test{

    @isTest
    public static void redirectToExternalObjectTest() {
        
        NI_ORDER__c order  = new NI_ORDER__c(site__c = 'NIC', oracle_header_id__c = 123456);
        insert order;
        
        NI_DispatcherOrderViewExtension dispatcher = new NI_DispatcherOrderViewExtension(new ApexPages.StandardController(order));
        apexpages.currentpage().getparameters().put('id' , order.id);
        
        dispatcher.redirectToExternalObject();
    }
    
}