@isTest
public class LeadUtils_Test {

    @isTest(seeAllData=true)
    public static void getNewLeadIdTest(){
        Test.startTest();
        List<SObject> toInsert = new List<SObject>();
        List<SObject> leads = TestUtils.getLeads(10);
        toInsert.addAll(leads);
        User user = TestUtils.getUserForProfile('Hobbs','NI Sales User');
        toInsert.add(user);
        insert toInsert;
        LeadUtils.getNewLeadId(user.Id);
        
        Test.stopTest();
    }
    
    
    

}