@isTest
public class Logger_Test {

    @isTest
    private static void debugTest(){
        Test.startTest();
        Logger.debug('testing');
        Logger.debug('location','message');
        Test.stopTest();
    }
    
    @isTest
    private static void errorTest(){
        Test.startTest();
        Logger.error('location','message');
        Logger.error('message');
        Test.stopTest();
    }
    
}