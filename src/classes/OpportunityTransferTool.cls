public class OpportunityTransferTool {

    public Id fromUserId{get;set;}
    public Id toUserId{get;set;}
    public User fromUser{get;set;}
    public Opportunity opportunityFrom{get;set;}
    public Opportunity opportunityTo{get;set;}
    public List<Opportunity> oppList{get;set;}
    public List<OpportunityWrapper> opps{get;set;}
    public String test{get;set;}
    public Integer numberOfSuccesses{get;set;}
    public Integer numberOfFailures{get;set;}

    public OpportunityTransferTool(ApexPages.StandardController controller) {
        this.opportunityFrom = new Opportunity();
        this.opportunityTo = new Opportunity();
        
        fromUserId = System.currentPageReference().getParameters().get('fromId');
        toUserId = System.currentPageReference().getParameters().get('toId');
        
        String successCount = System.currentPageReference().getParameters().get('success');
        numberOfSuccesses = successCount != null ? Integer.valueOf(successCount) : null;
        
        String errorCount = System.currentPageReference().getParameters().get('error');
        numberOfFailures = errorCount != null ? Integer.valueOf(errorCount) : null;
        
        if(fromUserId != null){
            opportunityFrom.ownerId = fromUserId;
        }
        
        if(toUserId != null){
            opportunityTo.ownerId = toUserId;
        }
        
        opps = new List<OpportunityWrapper>();
        
        if(opportunityFrom.ownerId != null && opportunityTo.ownerId != null){
            opps = getOpportunitiesToTransfer();
        }
    }
    
    public PageReference navigateToList(){
        PageReference pr = new PageReference('/apex/Opportunity_Transfer_Tool');
        pr.getParameters().put('fromId', opportunityFrom.ownerId);
        pr.getParameters().put('toId', opportunityTo.ownerId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference switchOwners(){
        PageReference pr = new PageReference('/apex/Opportunity_Transfer_Tool');
        pr.getParameters().put('fromId', opportunityTo.ownerId);
        pr.getParameters().put('toId', opportunityFrom.ownerId);
        pr.setRedirect(true);
        return pr;
    }
    
    public List<OpportunityWrapper> getOpportunitiesToTransfer(){
        if(opportunityFrom != null && opportunityFrom.ownerId != null){
            
            String query = 'SELECT ';
            
            for(Schema.FieldSetMember f : SObjectType.Opportunity.FieldSets.Mass_Transfer_Fields.getFields()) {
                query += f.getFieldPath() + ',';
            }
            
            query += 'OwnerId,';
            
            // Trim last comma
            query = query.subString(0, query.length() - 1);
            query += ' FROM Opportunity where ownerId = ' + '\'' + opportunityFrom.ownerId + '\'';
            query += ' AND StageName in (\'Qualify\',\'Advance\',\'Closing\')';
            oppList = Database.query(query);
            
            List<OpportunityWrapper> oppWrappers = new List<OpportunityWrapper>();
            for(Opportunity opp : oppList){
                OpportunityWrapper ow = new OpportunityWrapper();
                ow.opp = opp;
                ow.transfer = false;
                oppWrappers.add(ow);
            }
            
            return oppWrappers;
            
        }
        
        return null;
        
    }
    
    public PageReference transfer(){
        List<Opportunity> toUpdate = new List<Opportunity>();
        for(OpportunityWrapper ow : opps){
            if(ow.transfer){
                ow.opp.ownerId = opportunityTo.ownerId;
                toUpdate.add(ow.opp);
            }
        }
        
        List<Database.SaveResult> srList = Database.update(toUpdate);
        
        Integer successCount = 0;
        Integer errorCount = 0;
        List<String> errors = new List<String>();
        for(Database.SaveResult sr : srList){
            
            if(sr.isSuccess()){
                successCount++;
            }else{
                errorCount++;
            }
        }
        
        PageReference pr = new PageReference('/apex/Opportunity_Transfer_Tool');
        pr.getParameters().put('fromId', opportunityFrom.ownerId);
        pr.getParameters().put('toId', opportunityTo.ownerId);
        pr.getParameters().put('success', String.valueOf(successCount));
        pr.getParameters().put('error', String.valueOf(errorCount));
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference toggleAllForTransfer(){
        
        Integer offCount = 0;
        Integer onCount = 0;
        for(OpportunityWrapper ow : opps){
            if(ow.transfer){
                onCount++;
            }else{
                offCount++;
            }
        }
        
        Boolean toggle = true;
        
        if(offCount < onCount){
            toggle = false;
        }
        
        for(OpportunityWrapper ow : opps){
            ow.transfer = toggle;
        }
    
        return null;
    }
    
    public class OpportunityWrapper{
        
        public Opportunity opp{get;set;}
        public Boolean transfer{get;set;}
        
        public OpportunityWrapper(){}
        
    }
}