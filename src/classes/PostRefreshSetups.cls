global class PostRefreshSetups implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        updateServerUrls(context.sandboxName().tolowerCase());
        updateEmails();
    }
    
    global void updateServerUrls(String sandboxName){
        List<NI_Server_URL_List__c> serverList = [Select Name, Server_URL__c from NI_Server_URL_List__c];
        for(NI_Server_URL_List__c server : serverList){
            server.Server_URL__c = server.Server_URL__c.replaceAll('parsec.*\\.ni','parsec-' + sandboxName + '.ni');
            server.Server_URL__c = server.Server_URL__c.replaceAll('sine.*\\.ni','sine-' + sandboxName + '.ni');
        }
        update serverList;
    }
    
    global void updateEmails(){
        List<User> users = [Select Id, Email from User where ProfileId in ('00ei0000001FDk8','00ei0000001FDyu','00ei0000001FDje','00ei0000001FDjK') and IsActive = TRUE];
        for(User user : users){
            String email = user.Email;
            if(email.contains('=')){
                Integer ndx = email.indexOf('@');
                if(ndx > -1){
                    user.Email = email.substring(0, ndx).replace('=','@');
                }
            }
        }
        Database.update(users, false);
    }
    
}