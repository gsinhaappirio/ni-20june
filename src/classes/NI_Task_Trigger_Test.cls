/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for Account_Trigger class.
  */

@isTest(SeeAllData=true)
private class NI_Task_Trigger_Test{
    
    private static final String ALIAS = 'sch1';
    private static final String SYSTEM_ADMIN = 'System Administrator';
    
    /**
    * @description       Test method to verify Task trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */ 
    
    private static testMethod void testTask_Trigger_Test()
    {
        test.starttest();
        Boolean m_isExecuting = false;
        Integer batchSize = 0;
        Boolean bln = false;
        integer ints = 0;
        
        //----------Insert Account------------
         User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
        list<account> acclist = new list<Account>();
        account acc= new account(name= 'test',OwnerId = UserInfo.getUserId(),ShippingCountry = 'United States',ShippingStreet = '1/AX39',ShippingCity = 'New York',ShippingState = 'New York',ShippingPostalCode = '12119');
        acclist.add(acc);
        insert acclist;
        
        //----------Insert Opportunity------------
        list<opportunity> opplist =  new list<opportunity>();
        Opportunity opp = new opportunity(name='test2',AccountId=acclist[0].id,Multi_Phase_Opportunity__c=NULL,StageName='Qualify',ForecastCategoryName='funnel',CloseDate=system.today(),Amount = 1000  );
        opplist.add(opp);
        insert opplist; 
        
        //----------Insert Task------------
        List<Task> taskList = new List<Task>();
        Task taskObj = new Task();
        taskObj.Type = 'Task';
        taskObj.OwnerId = UserInfo.getUserId();
        taskObj.Status = 'Not Started';
        taskObj.Priority = 'Normal';
        taskObj.Subject = 'cREQ Task';
        taskObj.Type__c = 'cREQ';
        taskObj.Subtype__c = 'Callback';
        taskObj.whatid = opplist[0].id;
        taskList.add(taskObj);
        insert taskList;
        update tasklist;
         
        
        Map<Id, Task> taskMap = new Map<Id, Task>();
        for(Task task : tasklist){
            taskMap.put(task.Id, task);
        }
        
        //----------------------  Task Trigger Controller Class Methods -----------------------
         
        TaskTriggerController tk= new TaskTriggerController(bln,ints);
        
        TaskTriggerController.onBeforeInsert(taskList);
        TaskTriggerController.onAfterInsert(taskMap);
        
        //----------------------  Task Handler Class Methods -----------------------
        TaskHandler.oppLastActiveDate(taskList); 
        
        TaskTriggerController ttc=new TaskTriggerController(m_isExecuting,batchSize);
        
        test.stoptest();
    }
}