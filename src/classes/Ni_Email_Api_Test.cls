@isTest
public class Ni_Email_Api_Test {

    @isTest(seeAllData=true)
    public static void sendPlainEmailTest(){
        Ni_Email_Api api = new Ni_Email_Api();
        Test.startTest();
        List<String> toAddresses = new List<String>();
        toAddresses.add('tyler.hobbs@ni.com');
        
        List<Blob> attachments = new List<Blob>();
        String blobString = 'stringtoblob';
		Blob bl = Blob.valueof(blobString);
        attachments.add(bl);
        
        api.sendEmail('tyler.hobbs@ni.com', 'Automated Test - Do Not Reply', null, toAddresses, null, Ni_Email_Api.EmailType.PLAIN,
                          'Testing the automated test. Do not reply.', 'This is an automated test email. Do not reply', 'attachmentname', 'attachmentcontent', attachments);
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void sendHtmlEmailTest(){
        Ni_Email_Api api = new Ni_Email_Api();
        Test.startTest();
        List<String> toAddresses = new List<String>();
        toAddresses.add('tyler.hobbs@ni.com');
        
        List<Blob> attachments = new List<Blob>();
        String blobString = 'stringtoblob';
		Blob bl = Blob.valueof(blobString);
        attachments.add(bl);
        
        api.sendEmail('tyler.hobbs@ni.com', 'Automated Test - Do Not Reply', null, toAddresses, null, Ni_Email_Api.EmailType.HTML,
                          'Testing the automated test. Do not reply.', 'This is an automated test email. Do not reply', 'attachmentname', 'attachmentcontent', attachments);
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void noAttachmentNameTest(){
        Ni_Email_Api api = new Ni_Email_Api();
        Test.startTest();
        List<String> toAddresses = new List<String>();
        toAddresses.add('tyler.hobbs@ni.com');
        
        List<Blob> attachments = new List<Blob>();
        String blobString = 'stringtoblob';
		Blob bl = Blob.valueof(blobString);
        attachments.add(bl);
        
        Boolean caughtException = false;
        try{
            api.sendEmail('tyler.hobbs@ni.com', 'Automated Test - Do Not Reply', null, toAddresses, null, Ni_Email_Api.EmailType.HTML,
                          'Testing the automated test. Do not reply.', 'This is an automated test email. Do not reply', null, 'attachmentcontent', attachments);
        }catch(Ni_Email_Api.Ni_Email_Api_Exception ex){
            caughtException = true;
        }
        
        Test.stopTest();
        System.assertEquals(caughtException, true);
    }
    
    @isTest(seeAllData=true)
    public static void noAttachmentContentTypeTest(){
        Ni_Email_Api api = new Ni_Email_Api();
        Test.startTest();
        List<String> toAddresses = new List<String>();
        toAddresses.add('tyler.hobbs@ni.com');
        
        List<Blob> attachments = new List<Blob>();
        String blobString = 'stringtoblob';
		Blob bl = Blob.valueof(blobString);
        attachments.add(bl);
        
        Boolean caughtException = false;
        try{
            api.sendEmail('tyler.hobbs@ni.com', 'Automated Test - Do Not Reply', null, toAddresses, null, Ni_Email_Api.EmailType.HTML,
                          'Testing the automated test. Do not reply.', 'This is an automated test email. Do not reply', 'attachmentname', null, attachments);
        }catch(Ni_Email_Api.Ni_Email_Api_Exception ex){
            caughtException = true;
        }
        
        Test.stopTest();
        System.assertEquals(caughtException, true);
    }
    
}