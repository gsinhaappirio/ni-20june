global class LeadAutoCloseScheduler implements Schedulable {

    public static String CRON_EXP = '0 0 0 3 9 ? 2022';

    global void execute(SchedulableContext ctx) {
        String query = 'Select Id, CreatedDate, Status, Close_Reason__c, cREQ_Date__c, Description, SLA_Acceptance_Date__c, SLA_Qualification_Date__c from Lead where status in (\'Qualified\',\'Assigned\',\'Accepted\') and cREQ_Date__c = null and isConverted = false';
        Database.executeBatch(new BatchUtil(query, new LeadUtils()), 2000);
    }
}