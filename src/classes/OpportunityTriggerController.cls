/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            OpportunityTriggerController
   Description:     This class invokes methods of OpportunityHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class OpportunityTriggerController{
    private Boolean m_isExecuting = false;
    private integer batchSize = 0;
    
    /*Constructor*/
    public OpportunityTriggerController (Boolean isExecuting, integer size){
        string errorDetail = system.label.Space;
        try{
            m_isExecuting = isExecuting;
            batchSize = size;
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }

    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Opportunity> (Trigger.New),Map<Id,Opportunity> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Insert methods of OpportunityHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert(List<Opportunity> newOpportunityList){
        string errorDetail = system.label.Space;
        try{
            OpportunityHandler.updateAmtCount(newOpportunityList, null);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
        
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : newOpportunityList){
            oppIds.add(opp.Id);
        }
        
        OpportunityHandler.updateAmountUSD(oppIds);
        
    }
    
    public static void onBeforeInsert(List<Opportunity> newOpportunityList){
        string errorDetail = system.label.Space;
        try{
            OpportunityHandler.updatePriceBook(newOpportunityList);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }
    
    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Opportunity> (Trigger.New),Map<Id,Opportunity> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Update methods of OpportunityHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterUpdate(List<Opportunity> newOpportunityList, Map<Id,Opportunity> oldOpportunityMap){
        string errorDetail = system.label.Space;
        try{
            OpportunityHandler.updateAmtCount(newOpportunityList,oldOpportunityMap);
            OpportunityHandler.updatePlanOpportunities(newOpportunityList,oldOpportunityMap);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
        
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : newOpportunityList){
            Opportunity oldOpp = oldOpportunityMap.get(opp.Id);
            if(opp.Amount != oldOpp.Amount || opp.CurrencyIsoCode != oldOpp.CurrencyIsoCode){
                oppIds.add(opp.Id);
            }
        }
        
        if(oppIds != null && !oppIds.isEmpty()){
            OpportunityHandler.updateAmountUSD(oppIds);
        }
        
    }
    
    public static void onBeforeUpdate(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap){
        OpportunityHandler.updateOppPushCount(newOpportunityList, oldOpportunityMap);
        OpportunityHandler.updateLastUpdateByOwner(newOpportunityList);
    }
    
    
    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Opportunity> (Trigger.New),Map<Id,Opportunity> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after delete methods of OpportunityHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/ 
    
    public static void onAfterDelete(List<Opportunity> oldOpportunityList,Map<Id,Opportunity> oldOpportunityMap){
        string errorDetail = system.label.Space;
        try{              
            OpportunityHandler.DeleteAmtCount(oldOpportunityList);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }                
    } 
}