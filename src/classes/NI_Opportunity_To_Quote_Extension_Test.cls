@isTest(SeeAllData=true)
public with sharing class NI_Opportunity_To_Quote_Extension_Test {
    
    @isTest
    public static void controllerExtensionTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
        NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
        contactItems = oqe.contactItems;
        contactMap = oqe.contactMap;
        oqe.refresh();
    }
 
    @isTest(seeAllData=true)
    public static void saveTest() {
        //PageReference pageRef ;
        //Test.setCurrentPage();
        //PageReference nextPage;
        
        
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
        NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
        oqe.contactId = contact1.Id;
        Boolean isSF1 = oqe.isSF1;
        oqe.refresh();
        PageReference pageReference = oqe.save();
        
        // create the controller
        // call the save operation and verify returned page
        
        
        // asset page was redirected
        // was note created?
        // was email sent? *this cannot be tested, but can it be verified?*
    }
    
    @isTest
    public static void cancelTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
        NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
        PageReference pageReference = oqe.cancel();
        
        // ASSERT SOMETHING HERE !!!
    }
    
    @isTest (seeAllData=true)
    public static void mergeQuoteRequestTemplateEmailTest(){
        Account account = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        insert account;
    
        Contact contact = new Contact(FirstName='John', LastName='Doe', AccountId=account.id);
        insert contact;
        
        contact = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact.Id];
        
        Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account.Id, StageName='Qualify', CloseDate=System.today());
        insert opp;
        opp =  [SELECT Id, Account_Name__c, Opportunity_Number__c, Name, Owner.name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
        
        NI_Opportunity_To_Quote_Extension ext = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
        ext.notes = 'Test note';
        Messaging.SingleEmailMessage mail = ext.mergeQuoteRequestTemplateEmail(contact,opp);
        
        System.assertNotEquals(mail, null);
    
    } 
    
    // catch blocks not being covered
    @isTest
    public static void createQuoteRequestNoteTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.notes = 'Some notes...';
    oqe.createQuoteRequestNote();
    
    oqe.attachmentName = 'Test attachment';
    oqe.attachmentBlob = Blob.valueOf('Test Data');
    oqe.contentType = 'text/plain';
    oqe.createQuoteRequestAttachment();
    
    //System.assertNotEquals(contactMap.get(contact2.Id), null);
    }
    
    @isTest
    public static void sendQuoteRequestTemplateEmailMissingMergeFieldTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.sendQuoteRequestTemplateEmail();
    
    //System.assertNotEquals();
    }
    
    @isTest(seeAllData=true)
    public static void sendQuoteRequestTemplateEmailNoDefaultCountryTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.notes = 'Test note';
    oqe.contactMap.put(contact1.id, contact1);
    oqe.contactId = contact1.Id;
    oqe.sendQuoteRequestTemplateEmail();
    
    //System.assertNotEquals();
    }
    
    @isTest(seeAllData=true)
    public static void sendQuoteRequestTemplateEmailNewTest() {
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    // INSERT CUSTOM SETTING VALUES
    //NI_Quote_Request_Email_Routing__c
    
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.notes = 'Test note';
    oqe.contactMap.put(contact1.id, contact1);
    oqe.contactId = contact1.Id;
    
    oqe.attachmentName = 'Test attachment';
    oqe.attachmentBlob = Blob.valueOf('Test Data');
    oqe.contentType = 'text/plain';
    
    oqe.sendQuoteRequestTemplateEmail();
    
    //System.assertNotEquals();
    }
    
    @isTest
    public static void setNoteCreationErroStateTest(){
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.notes = 'Test note';
    oqe.contactMap.put(contact1.id, contact1);
    oqe.contactId = contact1.Id;
        PageReference myPage = new ApexPages.StandardController(opp).view();
        oqe.setNoteCreationErrorState(myPage, 'Error',false);
        
        
        System.assertNotEquals(myPage.getParameters().get(NI_Opportunity_To_Quote_Extension.ERROR_MESSAGE_PARAM), null);
        System.assertEquals(myPage.getParameters().get(NI_Opportunity_To_Quote_Extension.CONTACT_ID_PARAM), oqe.contactId);
        System.assertEquals(myPage.getParameters().get(NI_Opportunity_To_Quote_Extension.NOTES_PARAM), oqe.notes);
    }
    
    @isTest
    public static void getNoteCreationErroStateTest(){
        List<SelectOption> contactItems;
    Map<ID, Contact> contactMap;
    
    Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
    Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street2', ShippingCity='City2', ShippingCountry='United States', ShippingState='California', ShippingPostalCode='PostalCode2');
    insert account1;
    insert account2;
    
    Contact contact1 = new Contact(FirstName='John', LastName='Doe', AccountId=account1.id);
    Contact contact2 = new Contact(FirstName='Jane', LastName='Doe', AccountId=account2.id);
    insert contact1;
    insert contact2;
    // Retrieve auto-populated Name field
    contact1 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact1.Id];
    contact2 = [SELECT Id, Name, Account.Name, Account.shippingStreet, contact.Account.shippingCity, contact.Account.shippingState, Account.shippingPostalCode, Account.shippingCountry, Phone, Email FROM Contact WHERE Id = :contact2.Id];
    
    Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
    insert opp;
    opp =  [SELECT Id, Opportunity_Number__c, Name, Account.ShippingState, Account.ShippingCountry, CurrencyIsoCode FROM Opportunity WHERE Id = :opp.Id];
    
    OpportunityContactRole ocr1 = new OpportunityContactRole(ContactId = contact1.Id, OpportunityId = opp.Id);
    OpportunityContactRole ocr2 = new OpportunityContactRole(ContactId = contact2.Id, OpportunityId = opp.Id);
    insert ocr1;
    insert ocr2;
    
    
    NI_Opportunity_To_Quote_Extension oqe = new NI_Opportunity_To_Quote_Extension(new ApexPages.StandardController(opp));
    oqe.notes = 'Test note';
    oqe.contactMap.put(contact1.id, contact1);
    oqe.contactId = contact1.Id;
        PageReference myPage = new ApexPages.StandardController(opp).view();
        oqe.setNoteCreationErrorState(myPage, 'Error',true);
        oqe.getNoteCreationErrorState(myPage);
        
        // CHECK THIS
        System.assertEquals(oqe.contactId, myPage.getParameters().get(NI_Opportunity_To_Quote_Extension.CONTACT_ID_PARAM));
        System.assertEquals(oqe.notes, myPage.getParameters().get(NI_Opportunity_To_Quote_Extension.NOTES_PARAM));
    }
 
}