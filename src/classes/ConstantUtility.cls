/**************************************************************************************************
*                                                                                                 *
*                     RT LIBRARY – Technical Goods                      						  *
*     <<< BC.K.000L – Adding/Editing/Remove Multiple Products on Opportunity via single page>>>   *
*                                               					 							  *
*                                                                                                 *
*-------------------------------------------------------------------------------------------------*
*                                                                                                 *
*                            Version 1.00                                                 		  *
*                                                                                                 *
* Creation Date: 15.Feb.2014                    Reetika Budhiraja           					  *
* Last Modified: 18.Apr.2014                    Sarbpreet Kaur         							  *
*                                                                                                 *
*                             © 2014 - Accenture - All Rights Reserved  						  *
***************************************************************************************************/
public with sharing class ConstantUtility {

    public static final string STR_ListPrice = 'List Price';
    public static final string STR_ProductDescription = 'Product Description';  
    public static final string STR_ProductCode = 'Product Code';
    public static final string STR_ProductFamily = 'Product Family';
    public static final string STR_ProductName = 'Product Name';
    public static final string STR_StartsWith = 'Starts With';
    public static final string STR_Contains = 'Contains';
    public static final string STR_Equals = 'Equals';
    public static final string STR_SortAsc = 'asc';
    public static final string STR_SortDesc = 'desc';
    public static final string STR_OrderBy = ' order by ';
    public static final string STR_Blank = '';    
        
}
/***************************************************************************************************
*                     RT LIBRARY – Technical Goods                      						   *
*                                                                                                  *
*                             © 2014 - Accenture - All Rights Reserved  						   *
****************************************************************************************************/