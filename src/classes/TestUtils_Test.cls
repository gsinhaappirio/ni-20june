@isTest(SeeAllData=true)
public class TestUtils_Test {
    
    @isTest
    private static void getAccount(){
        Test.startTest();
        Account account = TestUtils.getAccount('accountname');
        insert account;
        
        System.assertEquals('accountname', account.name);
        Test.stopTest();
    }
    
    @isTest
    private static void getContact(){
        Test.startTest();
        Account account = TestUtils.getAccount('accountname');
        insert account;
        Contact contact = TestUtils.getContact('lastname',account.Id);
        insert contact;
        
        System.assertEquals('accountname', account.name);
        System.assertEquals('lastname', contact.LastName);
        Test.stopTest();
    }
    
    @isTest
    private static void getUserForProfileTest(){
        Test.startTest();
        User user = TestUtils.getUserForProfile('Last', 'NI Sales User');
        insert user;
        
        System.assertEquals('user.last@test.ni.com', user.Email);
        Test.stopTest();
    }
    
    @isTest
    private static void getUsersForProfileTest(){
        Test.startTest();
        Integer count = 10;
        List<User> users = TestUtils.getUsersForProfile('Last', 'NI Sales User', count);
        insert users;
        
        System.assertEquals(count, users.size());
        Test.stopTest();
    }
    
}