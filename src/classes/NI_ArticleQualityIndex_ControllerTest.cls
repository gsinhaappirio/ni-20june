@istest
public  class NI_ArticleQualityIndex_ControllerTest {
    
    
    @TestSetup
    public static void createDate(){
        Trigger_Settings__c ts = new Trigger_Settings__c(name ='User_Trigger',active__C=false);
        insert ts;
        PermissionSet ps = [select id from permissionset where name=:NI_Constants.NI_Article_ByPass_Approval_Permission];
        
        PermissionSetAssignment psa = null;
        try{
            psa = [select id from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId() and PermissionSetId =: ps.Id];
        }catch(Exception e){
            
        }
        if(psa == null ){
            psa = new PermissionSetAssignment();
            psa.AssigneeId = UserInfo.getUserId();
            psa.PermissionSetId = ps.Id;
            insert psa;
        } 
        user u = new user(id=userinfo.getUserid(),Knowledge_Reviewer__c=true);
        update u;
    }
    @istest
    private static void testQualityIndex1(){
        
        test.startTest();
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        Knowledge__kav testArticle = articleList[0];
        testArticle.Status__c = NI_Constants.NI_Draft_Status;
        testArticle.Approver__c = userinfo.getUserId();
        update testArticle;
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.submit(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.publish(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        System.assert( NI_ArticleQualityIndex_Controller.submitForArchivalApproval(articleList[0].Id,userinfo.getUserId()) == NI_Constants.NI_Success_Msg);
        
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        
        test.stopTest();
        
        
    }
    
    @istest
    private static void testQualityIndexApprove(){
        
        test.startTest();
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        Knowledge__kav testArticle = articleList[0];
        testArticle.Status__c = NI_Constants.NI_Draft_Status;
        testArticle.Approver__c = userinfo.getUserId();
        update testArticle;
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        System.assert(NI_ArticleQualityIndex_Controller.submit(articleList[0].Id)==NI_Constants.NI_Success_Msg);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.approveArticle(articleList[0].Id,10,5,14,12,12,10,'ajsdsd aushgdua ',True);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        
        test.stopTest();
        
        
    }
    
    @istest
    private static void testQualityIndexReject(){
        
        test.startTest();
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        Knowledge__kav testArticle = articleList[0];
        testArticle.Status__c = NI_Constants.NI_Draft_Status;
        testArticle.Approver__c = userinfo.getUserId();
        update testArticle;
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.submit(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.rejectArticle(articleList[0].Id,10,5,14,12,12,1,'ajsdsd aushgdua ','ajsdsd aushgdua ',True);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        
        test.stopTest();
        
        
    }
    @istest
    private static void testQualityIndexApproveEOL(){
        
        test.startTest();
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        Knowledge__kav testArticle = articleList[0];
        testArticle.Status__c = NI_Constants.NI_Draft_Status;
        testArticle.Approver__c = userinfo.getUserId();
        update testArticle;
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.submit(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.approveArticle(articleList[0].Id,10,5,14,12,12,10,'ajsdsd aushgdua ',false);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        
        test.stopTest();
        
        
    }
    
    @istest
    private static void testQualityIndexRejectEOL(){
        
        test.startTest();
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        Knowledge__kav testArticle = articleList[0];
        testArticle.Status__c = NI_Constants.NI_Draft_Status;
        testArticle.Approver__c = userinfo.getUserId();
        update testArticle;
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.submit(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        NI_ArticleQualityIndex_Controller.rejectArticle(articleList[0].Id,10,5,14,12,12,1,'ajsdsd aushgdua ','ajsdsd aushgdua ',false);
        NI_ArticleQualityIndex_Controller.getComponentVisibility(articleList[0].Id);
        
        test.stopTest();
        
        
    }
    
    
}