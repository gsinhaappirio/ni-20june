public class ReleaseNotes{
    
    public Map<Date, List<ReleaseNoteWrapper>> releaseNotes{get;set;}
    
    public ReleaseNotes(){
        
        List<Release_Note__c> releaseNotesList = [Select Rank__c, Description__c, Jira_ID__c, Release_Date__c, Summary__c, Type__c from Release_Note__c where Published__c = true ORDER BY Release_Date__c DESC];
        
        if(releaseNotesList != null && !releaseNotesList.isEmpty()){
        
            List<ReleaseNoteWrapper> wrappers = new List<ReleaseNoteWrapper>();
            for(Release_Note__c rn : releaseNotesList){
                ReleaseNoteWrapper w = new ReleaseNoteWrapper(rn);
                wrappers.add(w);
            }
            
            releaseNotes = new Map<Date, List<ReleaseNoteWrapper>>();
            for(ReleaseNoteWrapper wrapper : wrappers){
                if(releaseNotes.containsKey(wrapper.releaseNote.Release_Date__c)){
                    List<ReleaseNoteWrapper> release = releaseNotes.get(wrapper.releaseNote.Release_Date__c);
                    release.add(wrapper);
                    releaseNotes.put(wrapper.releaseNote.Release_Date__c, release);
                }else{
                    List<ReleaseNoteWrapper> release = new List<ReleaseNoteWrapper>();
                    release.add(wrapper);
                    releaseNotes.put(wrapper.releaseNote.Release_Date__c, release);
                }
            }
            
            for(List<ReleaseNoteWrapper> rnList : releaseNotes.values()){
                rnList.sort();
            }
            
            /*
            wrappers.sort();
        
            releaseNotes = new Map<Date, List<Release_Note__c>>();
            for(ReleaseNoteWrapper wrapper : wrappers){
                Release_Note__c rn = wrapper.releaseNote;
                if(releaseNotes.containsKey(rn.Release_Date__c)){
                    List<Release_Note__c> release = releaseNotes.get(rn.Release_Date__c);
                    release.add(rn);
                    releaseNotes.put(rn.Release_Date__c, release);
                }else{
                    List<Release_Note__c> release = new List<Release_Note__c>();
                    release.add(rn);
                    releaseNotes.put(rn.Release_Date__c, release);
                }
            }
            */
        }
        
    }
    
    public class ReleaseNoteWrapper implements Comparable {

        public Release_Note__c releaseNote{get;set;}
        
        // Constructor
        public ReleaseNoteWrapper(Release_Note__c rn) {
            releaseNote = rn;
        }
        
        // Compare opportunities based on the opportunity amount.
        public Integer compareTo(Object compareTo) {
            // Cast argument to OpportunityWrapper
            ReleaseNoteWrapper other = (ReleaseNoteWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if(releaseNote.rank__c == null && other.releaseNote.rank__c != null){
                returnValue = 1;
            }else if(releaseNote.rank__c != null && other.releaseNote.rank__c == null){
                returnValue = -1;
            }else if (releaseNote.rank__c > other.releaseNote.rank__c) {
                returnValue = 1;
            } else if (releaseNote.rank__c < other.releaseNote.rank__c) {
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }
    
}