@isTest
public with sharing class Ni_Sales_Lead_Details_Extension_Test {
    
    @isTest(SeeAllData=true)
    public static void noParametersTest(){
        PageReference pageRef = new PageReference('/apex/ni_sales_lead_details');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_Sales_Lead_Details_Extension controller = new Ni_Sales_Lead_Details_Extension(sc);
        
        System.assertNotEquals(controller.lead, null);
    }
    
    @isTest(SeeAllData=true)
    public static void refreshTest() {
        PageReference pageRef = new PageReference('/apex/ni_sales_lead_details');
        Test.setCurrentPage(pageRef);
        
        Account account = new Account(name='Test', ShippingStreet='123 Fake St', ShippingCity='Austin', ShippingCountry='US');
        insert account;
        
        Contact contact = new Contact(firstname='tyler', lastname='hobbs');
        insert contact;
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_Sales_Lead_Details_Extension controller = new Ni_Sales_Lead_Details_Extension(sc);
        
        controller.accountId = account.Id;
        controller.contactId = contact.Id;
        
        Test.startTest();
        PageReference pr = controller.refresh();
        Test.stopTest();
        
        System.assertNotEquals(account, null);
        System.assertNotEquals(contact, null);
    }
    
}