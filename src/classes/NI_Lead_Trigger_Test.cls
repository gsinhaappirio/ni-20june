/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for Lead_Trigger class.
  */
@isTest
private class NI_Lead_Trigger_Test{
    
    private static final String ALIAS = 'sch1';
    private static final String SYSTEM_ADMIN = 'System Administrator';
    
     /*
    * @description       Test method to verify Lead trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */
    @isTest(SeeAllData=true)
    private static void testLead_Trigger_Test(){
        test.starttest();
        Boolean bln = false;
        integer ints = 0;
        
        //----------Insert Lead----------
        
         User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
        system.LoggingLevel newLoglevel = LoggingLevel.ERROR;
        List<Lead> leadList = new List<Lead>();
        List<id> leadlistud= new List<id>();
        List<id> ldid= new List<id>();
        Lead leadObj1 = new Lead();
        leadObj1.FirstName = 'TestReqHelpTrigger';
        leadObj1.LastName = UserInfo.getUserId();
        leadObj1.Status = 'Closed - Converted to Opportunity';
        leadObj1.Close_Reason__c = 'Converted to Opportunity';
        leadObj1.recordtypeid = [select id from recordtype where Name = 'Sales Generated Lead' limit 1].Id;
        leadObj1.Company = 'Accenture';
        leadObj1.LeadSource = 'Sales-Sales';
        leadObj1.Country = 'United States';
        leadObj1.City = 'New York';
        leadObj1.PostalCode = '15563';
        leadObj1.IsConverted = true;
        
        leadList.add(leadObj1);
        ldid.add(leadobj1.id);
        
        lead leadobj2 =new lead();
        leadObj2.FirstName = 'TestReqHelpTrigger';
        leadObj2.LastName = UserInfo.getUserId();
        leadObj2.Status = 'Pending Account Match';
        leadObj2.Close_Reason__c = 'Converted to Opportunity';
        leadObj2.recordtypeid = [select id from recordtype where Name = 'Marketing Generated Lead' limit 1].Id;
        leadObj2.Company = 'Accenture';
        leadObj2.LeadSource = 'Sales-Sales';
        leadObj2.Country = 'United States';
        leadObj2.City = 'New York';
        leadObj2.PostalCode = '15563';
        
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= false;
        leadlist.add(leadObj2);
        ldid.add(leadobj2.id);
        
        lead leadobj3 =new lead();
        leadObj3.FirstName = 'TestReqHelpTrigger';
        leadObj3.LastName = UserInfo.getUserId();
        leadObj3.Status = 'Pending Account Match';
        leadObj3.Close_Reason__c = 'Matched';
        leadObj3.recordtypeid = [select id from recordtype where Name = 'Marketing Generated Lead' limit 1].Id;
        leadObj3.Company = 'Accenture';
        leadObj3.LeadSource = 'Sales-Sales';
        leadObj3.Country = 'United States';
        leadObj3.City = 'New York';
        leadObj3.PostalCode = '15563';
       
        
        Database.DMLOptions dmo1 = new Database.DMLOptions();
        dmo1.assignmentRuleHeader.useDefaultRule= false;
        leadlist.add(leadObj3);
        ldid.add(leadobj3.id);
        
        Account acc = new account(name='Account',ShippingCountry = 'United States',ShippingStreet = '1/AX39',ShippingCity = 'New York',ShippingState = 'New York',ShippingPostalCode = '12119');
        insert acc ;
        
        Contact con = new Contact();
        con.LastName='xyz';
        con.AccountId = acc.Id;
        insert con;
        
        lead leadobj4 =new lead();
        leadObj4.FirstName = 'TestReqHelpTrigger';
        leadObj4.LastName = UserInfo.getUserId();
        leadObj4.Status = 'Pending Account Match';
        leadObj4.Close_Reason__c = 'Did not return';
        leadObj4.recordtypeid = [select id from recordtype where Name = 'Marketing Generated Lead' limit 1].Id;
        leadObj4.Company = 'Accenture';
        leadObj4.LeadSource = 'Sales-Sales';
        leadObj4.Country = 'United States';
        leadObj4.City = 'New York';
        leadObj4.PostalCode = '15563';
        leadObj4.Account__c = acc.Id;
        leadObj4.Contact__c = con.Id;
        insert leadObj4;
       
       User u = Common_Class.runningUser('integ','System Administrator');
        
      /*  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'integ', Email='integrationuser@test.com', 
        EmailEncodingKey='UTF-8', LastName='Integration User', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='integrationuser@ni.com');*/
        
        System.runAs(u) {        
            leadObj4.Status='Did Not Return';
            update leadObj4;
        }
        
        lead leadobj5 =new lead();
        leadObj5.FirstName = 'TestReqHelpTrigger';
        leadObj5.LastName = UserInfo.getUserId();
        leadObj5.Status = 'Matched Account';
        //leadObj5.Close_Reason__c = 'Matched';
        leadObj5.recordtypeid = [select id from recordtype where Name = 'Marketing Generated Lead' limit 1].Id;
        leadObj5.Company = 'Accenture';
        leadObj5.LeadSource = 'Sales-Sales';
        leadObj5.Country = 'United States';
        leadObj5.City = 'New York';
        leadObj5.PostalCode = '00563';
       
        leadlist.add(leadObj5);
        insert leadObj5;
       
        //----------------------  Lead Trigger Controller Class Methods -----------------------
        //LeadTriggerController ltd= new LeadTriggerController(bln,ints);
        
        //LeadTriggerController.onBeforeInsert(leadList);
        //LeadTriggerController.onBeforeUpdate(leadList,NULL);
        //LeadTriggerController.onAfterUpdate(leadList,NULL);
       // LeadTriggerController.onAfterInsert(leadList);
        
        //----------------------  Lead Handler Class Methods -----------------------
       
        //LeadHandler.assignAssignmentRule(leadList, Null);
        //LeadHandler.QueueStatus(ldid);
        
        test.stoptest();
       
    }
}