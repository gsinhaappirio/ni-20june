/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for Contact_Trigger class.
  */
@isTest(SeeAllData=true)
Private class NI_Contact_Trigger_Test{
// Start of Constants for Test Methods
    private static final String ALIAS = 'sch1';
    private static final String SYSTEM_ADMIN = 'System Administrator';
    
    /**
    * @description       Test method to verify contact trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */
    private static testmethod void testContact_Trigger_Test(){
User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
        test.starttest();
        Boolean bln = false;
        Integer ints = 0;
        
        List<Account> accList = new List<Account>();
        Account accObj = new account();
        
        accObj.Name ='TestAccountTrigger';
        accObj.OwnerId = UserInfo.getUserId();
        accObj.ShippingCountry = 'United States';
        accObj.ShippingStreet = '1/AX39';
        accObj.ShippingCity = 'New York';
        accObj.ShippingState = 'New York';
        accObj.ShippingPostalCode = '12119';
        accObj.BillingCountry = 'United States';
        accObj.BillingStreet = '1/AX39';
        accObj.BillingCity = 'New York';
        accObj.BillingState = 'New York';
        accObj.BillingPostalCode = '12119';
        //accObj.recordtypeid = [select id from recordtype where Name = 'Provisional' limit 1].Id;
        accObj.Fax_Country_Code__c = '12345';
        accObj.Fax_Area_Code__c = '56789';
        accObj.Fax_Number__c = '1234567891';
        accObj.Phone_Country_Code__c = '223';
        accObj.Phone_Area_Code__c = '586321';
        accObj.Phone_Number__c = '4561237892';
        accObj.Fax='53545';
        accObj.phone='5454';
        accObj.Alias1__c='abc';
        accObj.Alias2__c='d';
        accObj.Alias3__c='ef';
        accObj.Account_Name_Phonetic__c='SA';
        accObj.Account_Name_Roman__c='rm';
        
        accList.add(accObj);
        insert accList;
        
        Contact con= new Contact();
        con.AccountId=accObj.Id;
        con.LastName='yxz';
        
        insert con;
        delete con;
        undelete con;
}
}