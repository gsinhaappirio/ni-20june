public without sharing class NI_MetatagController extends kb_knowledgebase {

    public List<ArticleGlobalWrapper> metawrapper {Get;set;}
    
    public NI_MetatagController(){

        metawrapper = getdaTA(knowledgeId,language);

        System.debug('===>'+metawrapper);
    }
	@auraEnabled
    public static List<ArticleGlobalWrapper> getData(String recordId, String language){
        try{
            List<ArticleGlobalWrapper> wrapperList = new List<ArticleGlobalWrapper>();
           	Map<String,String> referenceFields = new Map<String,String>();
            Set<String> referenceFieldValues = new Set<String>();
            Map<String,String> langSpecificReferenceFields = new Map<String,String>();
            Map<String,String> langSpecificFieldLangMap = new Map<String,String>();
            Set<String> langSpecificReferenceFieldValues = new Set<String>();
            Set<String> languageValues = new Set<String>();
            List<Meta_Info__mdt> metaDataRecords = [SELECT MasterLabel, Type__c, Value__c, Language_Specific__c FROM Meta_Info__mdt];
            Integer nigenYes = Database.countQuery('SELECT Count() FROM Article_Feedback__c where AnsweredQuestion__c=true AND Article_Id__c=:recordId');
            Integer nigenNo = Database.countQuery('SELECT Count() FROM Article_Feedback__c where AnsweredQuestion__c=false AND Article_Id__c=:recordId');
            
   			String docStatus = '';
   			List<String> docStatusList = new List<String>();
			// TODO : ADD Null check all over the place
   			Knowledge__kav art = [SELECT ID, Title, ValidationStatus FROM Knowledge__kav WHERE ID =: recordId];
   			String articleTitle = art.Title;
            if(articleTitle.startsWith(Constants.NI_START_WITH_EOL_ARCHIVE)){
           		docStatusList.add(Constants.NI_EOL_ARCHIVE);
            }else{
                docStatusList.add(Constants.NI_NON_EOL_ARCHIVE); 
            }
   			if(art.ValidationStatus == Constants.NI_WIP){
           		docStatusList.add(Constants.NI_WIP);
            }else{
                docStatusList.add(Constants.NI_NON_WIP); 
            }
            if(art.ValidationStatus == Constants.NI_Verified_Status){
           		docStatusList.add(Constants.NI_VERIFIED_PUBLIC);
            }else{
                docStatusList.add(Constants.NI_UNVERIFIED_PUBLIC); 
            }
            docStatus = String.join(docStatusList, ',');
            
            Map<String,Object> pageVarMap = new Map<String,Object>();
            pageVarMap.put(Constants.NI_Language,language);
            pageVarMap.put(Constants.NI_Nigen2,nigenYes);
            pageVarMap.put(Constants.NI_Nigen3,nigenNo);  
   			pageVarMap.put(Constants.NI_DocStatus,docStatus);  
            
            for(Meta_Info__mdt meta : metaDataRecords){
                if(meta.Type__c == Constants.NI_Value){
                    ArticleGlobalWrapper wrap = new ArticleGlobalWrapper();
                    wrap.label=meta.MasterLabel;
                    wrap.value=meta.Value__c;
                    wrapperList.add(wrap);
                }
                else if(meta.Type__c == Constants.NI_Reference && meta.Language_Specific__c == null){
                    referenceFieldValues.add(meta.Value__c);
                    referenceFields.put(meta.MasterLabel,meta.Value__c);
                }
                else if(meta.Type__c == Constants.NI_Reference && meta.Language_Specific__c != null){
                    langSpecificReferenceFieldValues.add(meta.Value__c);
                    langSpecificReferenceFields.put(meta.MasterLabel,meta.Value__c);
                    languageValues.add(meta.Language_Specific__c);
                    langSpecificFieldLangMap.put(meta.MasterLabel,meta.Language_Specific__c);
                }
                else if(meta.Type__c == Constants.NI_Page_Var){
                    ArticleGlobalWrapper wrap = new ArticleGlobalWrapper();
                    wrap.label=meta.MasterLabel;
                    wrap.value=pageVarMap.get(meta.MasterLabel);
                    wrapperList.add(wrap);
                }
            }

            if(referenceFieldValues.size()>0){
                String fieldString = '';
    			for(String a : referenceFieldValues){
                   fieldString += (fieldString==''?'':',')+a;
                }
                String query = 'SELECT '+ fieldString + ' FROM Knowledge__kav WHERE ID=\''+recordId+'\' LIMIT 1';
                
                for(Knowledge__kav varLoop : Database.query(query)){
    				for(String s :  referenceFields.keySet()){
                        ArticleGlobalWrapper wrap = new ArticleGlobalWrapper();
                        wrap.label = s;
                        String fieldName = referenceFields.get(s);
                        wrap.value = varLoop.get(fieldName);
                        wrapperList.add(wrap);
                    }
                }
            }
            
            if(langSpecificReferenceFieldValues.size()>0){
                String fieldString = '';
    			for(String a : langSpecificReferenceFieldValues){
                   fieldString += (fieldString==''?'':',')+a;
                }
                if(fieldString.indexOfIgnoreCase(Constants.NI_Language) < 0){
                    fieldString += Constants.NI_Add_Language;
                }
                Knowledge__kav lst = [SELECT ID, KnowledgeArticleId FROM Knowledge__kav WHERE ID =: recordId LIMIT 1];
                String query = 'SELECT '+ fieldString + ' FROM Knowledge__kav WHERE ID=\''+lst.ID+'\' AND PublishStatus=\'Online\'';
                Map<String, Object> langMap = new Map<String, String>();
                for(Knowledge__kav varLoop : Database.query(query)){
    				for(String s :  langSpecificReferenceFields.keySet()){
                        if(varLoop.Language == langSpecificFieldLangMap.get(s)){
                            String fieldName = langSpecificReferenceFields.get(s);
                            langMap.put(s,varLoop.get(fieldName));
                        }
                    }
                }
                
                for(String s :  langSpecificReferenceFields.keySet()){
                    ArticleGlobalWrapper wrap = new ArticleGlobalWrapper();
                    wrap.label = s;
                    wrap.value = langMap.get(s)!=null?langMap.get(s):'';
                    wrapperList.add(wrap);
                }
            } 
            return wrapperList;
        }
        catch(Exception e){
            System.debug('error='+e.getMessage());
            return null;
        }
    }
}