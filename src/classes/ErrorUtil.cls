public Class ErrorUtil{

//TestMethod
    public static TestMethod void TestErrorUtil()
    {
        ErrorUtil.LogSuccess('Test Log Success');
        ErrorUtil.LogError('Test Log Error');
        try
        {
            throw new testErrorException('This is test Execetion');
        }
        catch(Exception exc)
        {
            ErrorUtil.LogException(exc);
        }
    
    }
    //Following Static methods are useful for displaying userfriendly messages on various VF Pages. 
    
    public static void LogError(String str){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, str));
    }
    public static void LogException(Exception ex){
        
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
    }
    public static void LogSuccess(String str){
            
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, str));
    }
    
    public class testErrorException extends Exception{}
}