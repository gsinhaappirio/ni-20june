@isTest
public with sharing class Ni_OneSearch_Account_Extension_Test {
    
    @isTest(SeeAllData=true)
    public static void noParametersTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        
        System.assertNotEquals(controller.lead, null);
        System.assertEquals(controller.account.name, null);
        System.assertEquals(controller.client, '');
        System.assertEquals(controller.returnUrl, null);
        
        List<Account> dups = controller.duplicateAccounts;
    }
    
    @isTest(SeeAllData=true)
    public static void constructorSearchTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=averna');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        System.assertNotEquals(controller.editing, false);
    }
    
    
    @isTest(SeeAllData=true)
    public static void gatherFieldSetsTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        System.assert(controller.leftFields != null);
    }
    
    @isTest(SeeAllData=true)
    public static void createObjectTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Test.startTest();
        
        controller.account.name = 'Test Account';
        controller.account.shippingStreet = '123 Fake St';
        controller.account.shippingCity = 'Austin';
        controller.account.shippingCountry = 'US';
        controller.selectedCountry = 'US';
        PageReference pr = controller.createObject();
        
        Test.stopTest();
        
        System.assertEquals(controller.done, true);
        
        controller.hideDuplicates();
    }
    
    @isTest(SeeAllData=true)
    public static void generateSearchUrlTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Test.startTest();
        
        controller.account.name = 'Test Account';
        controller.account.shippingStreet = '123 Fake St';
        controller.account.shippingCity = 'Austin';
        controller.account.shippingCountry = 'US';
        PageReference pr = controller.generateSearchUrl();
        
        Test.stopTest();
        
        System.assertNotEquals(controller.navigateToUrl, null);
    }
    
    @isTest(SeeAllData=true)
    public static void generateImportUrlTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Test.startTest();
        
        controller.account.name = 'Test Account';
        controller.account.shippingStreet = '123 Fake St';
        controller.account.shippingCity = 'Austin';
        controller.account.shippingCountry = 'US';
        PageReference pr = controller.generateImportUrl();
        
        Test.stopTest();
        
        System.assertNotEquals(controller.navigateToUrl, null);
    }
    
    @isTest(SeeAllData=true)
    public static void importObjectTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Test.startTest();
        controller.account.name = 'Test Account';
        controller.account.shippingStreet = '123 Fake St';
        controller.account.shippingCity = 'Austin';
        controller.account.shippingCountry = 'US';
        PageReference pr = controller.importObject();
        
        System.assertNotEquals(pr, null);
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void searchTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Test.startTest();
        
        controller.account.name = 'Test Account';
        controller.account.shippingStreet = '123 Fake St';
        controller.account.shippingCity = 'Austin';
        controller.account.shippingCountry = 'US';
        PageReference pr = controller.search();
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
    }
    
    @isTest(seeAllData=true)
    public static void applyFilterToResultsTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=testing&filter=sources:Salesforce');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        controller.screenWidthText = '';
        controller.showSearchForm = false;
        controller.developmentMode = false;
        controller.countryError = false;
        controller.errorTitle = '';
        controller.errorMessage = '';
        controller.temp = '';
        controller.type = '';
        controller.updatedFilterCategory = '';
        controller.updatedFilterName = '';
        
        Account account = new Account();
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, score, sourceSystem);
        List<Ni_Cmdm_Account_Search_Result> results = new List<Ni_Cmdm_Account_Search_Result>();
        results.add(accountResult);
        Test.startTest();
        
        controller.applyFilterToResults(results);
        
        Test.stopTest();
        
        //System.assertEquals(pr, null);
    }     
    
    @isTest(SeeAllData=true)
    public static void performSearchTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=testing&filter=sources:Salesforce');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        insert account1;
        PageReference pr = controller.performSearch();
        System.assertEquals(pr, null);
    }
    
    @isTest(SeeAllData=true)
    public static void clearFiltersTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=testing&filter=sources:Salesforce');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        Account account = new Account();
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, score, sourceSystem);
        List<Ni_Cmdm_Account_Search_Result> results = new List<Ni_Cmdm_Account_Search_Result>();
        results.add(accountResult);
        controller.allResults = results;
        PageReference pr = controller.clearFilters();
        System.assertEquals(pr, null);
    }    
    
    @isTest(SeeAllData=true)
    public static void mergeResultsTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=testing&filter=sources:Salesforce');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        Account account2 = new Account(Name='TestCompany2', ShippingStreet='Street1', ShippingCity='City2', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        
        Ni_Cmdm_Account_Search_Result accountResult1 = new Ni_Cmdm_Account_Search_Result(account1, score, sourceSystem);
        List<Ni_Cmdm_Account_Search_Result> cmdmResults = new List<Ni_Cmdm_Account_Search_Result>();
        cmdmResults.add(accountResult1);
        
        Ni_Cmdm_Account_Search_Result accountResult2 = new Ni_Cmdm_Account_Search_Result(account2, score, sourceSystem);
        List<Ni_Cmdm_Account_Search_Result> localResults = new List<Ni_Cmdm_Account_Search_Result>();
        localResults.add(accountResult2);        
        
        List<Ni_Cmdm_Account_Search_Result> mergedResults = controller.mergeResults(cmdmResults, localResults);
        System.assertNotEquals(mergedResults, null);
        
        controller.filter = new Map<String, List<Ni_OneSearch_Account_Extension.FilterCriteria>> ();
    }      
    
    @isTest(SeeAllData=true)
    public static void updateFilterTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=testing&filter=sources:Salesforce');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);
        
        Account account = new Account();
        Integer score = 0;
        String sourceSystem = 'Salesforce';
        Ni_Cmdm_Account_Search_Result accountResult = new Ni_Cmdm_Account_Search_Result(account, score, sourceSystem);
        List<Ni_Cmdm_Account_Search_Result> results = new List<Ni_Cmdm_Account_Search_Result>();
        results.add(accountResult);
        controller.allResults = results;       
        
        Ni_OneSearch_Account_Extension.FilterCriteria fc = new Ni_OneSearch_Account_Extension.FilterCriteria('test','test',false);
        fc.compareTo(fc);
        List<Ni_OneSearch_Account_Extension.FilterCriteria> fcl = new List<Ni_OneSearch_Account_Extension.FilterCriteria>();
        fcl.add(fc); 
        
        Map<String, List<Ni_OneSearch_Account_Extension.FilterCriteria>> filter = new Map<String, List<Ni_OneSearch_Account_Extension.FilterCriteria>>();
        filter.put('test', fcl);
        controller.filter = filter;
        
        PageReference pr = controller.updateFilter();
        System.assertEquals(pr, null);
        
    }    
    
    /*
    @isTest static void searchTest() {
        // Perform some DML to insert test data
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Account_Extension controller = new Ni_OneSearch_Account_Extension(sc);

        // Call Test.startTest before performing callout
        // but after setting test data.
        Test.startTest();

        // Set mock callout class 
        //Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Mock());
        
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=averna');
        Test.setCurrentPage(pageRef);
        System.assertNotEquals(controller.results, null);

        Test.stopTest();
    }
    */
    
}