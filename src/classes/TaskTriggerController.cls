/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            TaskTriggerController
   Description:     This class invokes methods of TaskHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class TaskTriggerController{
    private Boolean m_isExecuting = false;
    private integer batchSize = 0;
    
    /*Constructor*/
    public TaskTriggerController (Boolean isExecuting, integer size){
        string errorDetail = system.label.Space;
        try{
            m_isExecuting = isExecuting;
            batchSize = size;
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);  
        }
    }

    /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Task> (Trigger.New),Map<Id,Task> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all before Insert methods of TaskHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onBeforeInsert(List<Task> newTaskList){
        string errorDetail = system.label.Space;
        try{
            TaskHandler.initiateCReq(newTaskList);
            //TaskHandler.populateParentTypes(newTaskList, null);
        }catch(Exception e){
            errorDetail = e.getMessage() + e.getCause() + ' line: ' + e.getLineNumber() + '\n' + e.getStackTraceString();
            System.debug(errorDetail);   
        }
    }
    /**********************************************************************************************************************
    * Author:
    * Date: 
    * Param: List<Task> (Trigger.New),Map<Id,Task> (Trigger.OldMap)
    * Return: void
    * Description: This method Invokes all after insert methods of TaskHandler.
    * Summary of Changes :   - Initial version
    **********************************************************************************************************************/
    public static void onAfterInsert (Map<Id,Task> newTaskMap){
        string errorDetail = system.label.Space;
        try{
            TaskHandler.OppLastActiveDate(newTaskMap.values());
            TaskHandler.updateParentNextDueDate(newTaskMap, null);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);
        }
    }
    
    public static void onBeforeUpdate (Map<Id,Task> newTaskMap, Map<Id, Task> oldTaskMap){
        string errorDetail = system.label.Space;
        try{
            //TaskHandler.populateParentTypes(newTaskMap.values(), oldTaskMap);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);            
        }
    }
    
    public static void onAfterUpdate (Map<Id,Task> newTaskMap, Map<Id, Task> oldTaskMap){
        string errorDetail = system.label.Space;
        try{
            TaskHandler.updateParentNextDueDate(newTaskMap, oldTaskMap);
            //TaskHandler.populateParentTypes(newTaskMap.values(), oldTaskMap);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);            
        }
    }
    
    public static void onAfterDelete (Map<Id,Task> oldTaskMap){
        string errorDetail = system.label.Space;
        try{
            TaskHandler.updateParentNextDueDate(null, oldTaskMap);
        }catch(Exception e){
            errorDetail = e.getMessage()+e.getCause()+e.getLineNumber()+e.getStackTraceString();
            System.debug(errorDetail);            
        }
    }
    
}