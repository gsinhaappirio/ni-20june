/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NI_Sales_Sales_OrderLineDTO_Test {

    static testMethod void constructorWithParametersTest() {
        
        String partNumber = '123';
        String partDescription = 'test';
        Decimal orderedQuantity = 1; 
        Decimal unitListPrice = 100; 
        Decimal unitSellingPrice = 100;
        
        NI_Sales_Sales_OrderLineDTO orderLine = new NI_Sales_Sales_OrderLineDTO(partNumber, partDescription, orderedQuantity, unitListPrice, unitSellingPrice );
        
        System.assertEquals('123', orderLine.partNumber);
        System.assertEquals('test', orderLine.partDescription);
        System.assertEquals('123', orderLine.partNumber);
        System.assertEquals('123', orderLine.partNumber);
        System.assertEquals('123', orderLine.partNumber);
        
    }
    
    static testMethod void constructorWithoutParametersTest() {
        
        NI_Sales_Sales_OrderLineDTO orderLine = new NI_Sales_Sales_OrderLineDTO();        
        System.assertNotEquals(null, orderLine);
    }
}