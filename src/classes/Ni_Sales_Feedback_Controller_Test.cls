@isTest(seeAllData=true)
public with sharing class Ni_Sales_Feedback_Controller_Test {

    @isTest
    public static void sendEmailToSfdcAccountAdminsTest(){
        PageReference pageRef = new PageReference('/apex/Ni_Feedback_Sidebar');
        Test.setCurrentPage(pageRef);
        Ni_Sales_Feedback_Controller controller = new Ni_Sales_Feedback_Controller();
        
        Test.startTest();
        controller.explanation = 'Unit Test';
        controller.recipient = 'sfdc';
        controller.sendEmail();
        Test.stopTest();
        
    }
    
    @isTest
    public static void sendEmailToCrmTest(){
        PageReference pageRef = new PageReference('/apex/Ni_Feedback_Sidebar');
        Test.setCurrentPage(pageRef);
        Ni_Sales_Feedback_Controller controller = new Ni_Sales_Feedback_Controller();
        
        Test.startTest();
        controller.explanation = 'Unit Test';
        controller.recipient = 'crm';
        controller.sendEmail();
        Test.stopTest();
        
    }
    
    @isTest
    public static void restartTest(){
        PageReference pageRef = new PageReference('/apex/Ni_Feedback_Sidebar');
        Test.setCurrentPage(pageRef);
        Ni_Sales_Feedback_Controller controller = new Ni_Sales_Feedback_Controller();
        
        Test.startTest();
        controller.explanation = 'Unit Test';
        controller.recipient = 'crm';
        controller.restart();
        Test.stopTest();
        
    }
    
}