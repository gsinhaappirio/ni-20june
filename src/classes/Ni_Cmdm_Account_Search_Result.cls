global class Ni_Cmdm_Account_Search_Result implements Comparable {

    public Double score{get;set;}
    public Account account{get;set;}
    public String sourceSystem{get;set;}
    
    public Ni_Cmdm_Account_Search_Result(){}
    
    public Ni_Cmdm_Account_Search_Result(Account account, Integer score, String sourceSystem){
        this.account = account;
        this.score = Double.valueOf(score)/10;
        this.sourceSystem = sourceSystem;
    }
    
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        Ni_Cmdm_Account_Search_Result other = (Ni_Cmdm_Account_Search_Result)compareTo;
        
        Integer returnValue = 0;

        if (score > other.score) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (score < other.score) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        
        
        return returnValue;       
    }
    
}