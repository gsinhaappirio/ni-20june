public class ContactOwnerSync implements BatchUtil.BatchUtilable {
    
    public boolean HandleRecord(sObject s, BatchUtil callee) {
        if (s.getSObjectType() == Contact.SObjectType){
            Contact contact = (Contact) s;
            Id ownerId = contact.account.ownerId;
            //String ownerId = (String) s.get('Account_Owner_Id__c');
            callee.Log('***** ' + ownerId + ' \n');
            s.put('OwnerId', ownerId);
            return true;
        }else{
            return false;
        }
    }
    
}