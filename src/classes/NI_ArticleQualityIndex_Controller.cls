/**
*
*Name        : NI_ArticleQualityIndex_Controller 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 06th June, 2017
*Purpose     : Apex Controller for NI_Article_Quality_Index Component
*
**/

public with sharing class NI_ArticleQualityIndex_Controller {
 	/**
    * @method : getComponentVisibility
    * @purpose : Method to be called for setting the initial details for the component
    *
    */
    @auraEnabled
    public static NI_Global_Wrapper getComponentVisibility(String recordId){
        try{
            Knowledge__kav artcl = [SELECT Id, KnowledgeArticleId, Status__c, Approver__c, EOL_Archive__c, PublishStatus 
                                    FROM Knowledge__kav 
                                    where id =:recordId LIMIT 1]; 
            Set<Id> permissionSetIds = new Set<Id>();
            for (SetupEntityAccess access : [SELECT ParentId 
                                             FROM SetupEntityAccess 
                                             WHERE SetupEntityId IN (SELECT Id 
                                                                     FROM CustomPermission 
                                                                     WHERE DeveloperName =: NI_Constants.NI_Article_ByPass_Approval_Permission)]){
                permissionSetIds.add(access.ParentId);
            }
            List<User> userList= permissionSetIds.isEmpty() ? new List<User>() : [SELECT Username 
                                                                                  FROM User 
                                                                                  WHERE Id IN (SELECT AssigneeId 
                                                                                               FROM PermissionSetAssignment 
                                                                                               WHERE PermissionSetId IN :permissionSetIds) 
                                                                                  				and ID =: UserInfo.getUserId()];
            
            NI_Global_Wrapper wrapperObj = new NI_Global_Wrapper();
            wrapperObj.articleStatus = artcl.Status__c;
            wrapperObj.articleApproverId = artcl.Approver__c;
            wrapperObj.isVisible = false;
            wrapperObj.isAvailable = false;
            wrapperObj.isEditable = false;
            wrapperObj.isReadOnly = false;
            wrapperObj.isReject = false;
            wrapperObj.displayButtons = false;
            wrapperObj.isSubmitter = false;
            wrapperObj.isPublisher = false;
            wrapperObj.isEOLArchive = false;
            wrapperObj.isEOLArchiveApprover = false;
            wrapperObj.isPublishPermission = false;
            wrapperObj.isRejectedArticle = false;
            wrapperObj.isSetEOLApprover = false;
            
            if(artcl.EOL_Archive__c == true && artcl.Status__c == NI_Constants.NI_SubmittedEOL_Status && artcl.Approver__c == UserInfo.getUserId() && artcl.PublishStatus == NI_Constants.NI_Draft_Status){
                wrapperObj.isEOLArchiveApprover = true; 
                wrapperObj.displayButtons = true;              	
            }
            if(userList.size()>0){
                wrapperObj.isPublishPermission = true;
            }
            if(wrapperObj.isPublishPermission == true && artcl.PublishStatus != NI_Constants.NI_Online_Publish_Status && artcl.Status__c != NI_Constants.NI_Submitted_Status && artcl.Status__c != NI_Constants.NI_SubmittedEOL_Status && artcl.Approver__c != UserInfo.getUserId()){
                wrapperObj.isAvailable = true;
                wrapperObj.isPublisher = true;
            } 
            if(artcl.EOL_Archive__c == false && artcl.Status__c == NI_Constants.NI_Submitted_Status && artcl.Approver__c == UserInfo.getUserId()){
                wrapperObj.isEditable = true;
                wrapperObj.totalScore = 0;     
            }
            else if(artcl.Status__c == NI_Constants.NI_PublishedEOL_Status || artcl.Status__c == NI_Constants.NI_Rejected_Status || artcl.PublishStatus == NI_Constants.NI_Online_Publish_Status || artcl.PublishStatus == NI_Constants.NI_Archived_Status){
                wrapperObj.isVisible = true;
                wrapperObj.isAvailable = true;
                List<NI_Article_Quality_Index__c> qualityList = new List<NI_Article_Quality_Index__c>();
                qualityList = [SELECT Id, Comments__c, Rejection_Reason__c, Approval_Type__c, Total_score_out_of_100__c, CreatedDate FROM NI_Article_Quality_Index__c where Article_ID__c =:recordId ORDER BY CreatedDate DESC LIMIT 1];
                NI_Article_Quality_Index__c qualityIndex = null;
                if(qualityList.size()>0){
                    qualityIndex = qualityList[0];
                    wrapperObj.totalScore = Integer.valueOf(qualityIndex.Total_score_out_of_100__c);
                    wrapperObj.comments = qualityIndex.Comments__c;
                    wrapperObj.rejectReason = qualityIndex.Rejection_Reason__c;
                    if(artcl.Status__c == NI_Constants.NI_Rejected_Status){
                       wrapperObj.isRejectedArticle = true;
                    } 
                } 
                else{
                    wrapperObj.isVisible = false;
                    wrapperObj.isAvailable = false; 
                }
                if(artcl.PublishStatus == NI_Constants.NI_Online_Publish_Status){
                    List<Knowledge__kav> art = [SELECT Id, KnowledgeArticleId, PublishStatus FROM Knowledge__kav where KnowledgeArticleId=:artcl.KnowledgeArticleId AND PublishStatus = 'Draft'];
                    if(art != null && art.size()<=0){
                        wrapperObj.isEOLArchive = true; 
                    }
                }
            }
            if(artcl.PublishStatus == NI_Constants.NI_Draft_Status && (artcl.Status__c == NI_Constants.NI_Draft_Status || artcl.Status__c == NI_Constants.NI_Rejected_Status) && wrapperObj.isPublishPermission == false){
                wrapperObj.isSubmitter = true;
                wrapperObj.isAvailable = true;
            }
            return wrapperObj;
        }
        catch(Exception e){
            return null;
        }
    }
    
    /**
    * @method : approveArticle
    * @purpose : Method for approving the article
    *
    */
    @auraEnabled
    public static String approveArticle(String recordId, Integer unique, Integer complete, Integer contentClear, Integer titleReflectsArticle, Integer linksValid, Integer metaData, String comments, Boolean isEOL){
        try{
            KnowledgeArticleVersion artcl = [SELECT Title, KnowledgeArticleId, OwnerId FROM KnowledgeArticleVersion where id =:recordId LIMIT 1];
            if(!isEOL){
                NI_Article_Quality_Index__c qualityIndexObj = new NI_Article_Quality_Index__c();
                qualityIndexObj.Unique__c = unique;
                qualityIndexObj.Complete__c = complete;
                qualityIndexObj.Content_Clear__c = contentClear;
                qualityIndexObj.Title_Reflects_Article__c = titleReflectsArticle;
                qualityIndexObj.Links_Valid__c = linksValid;
                qualityIndexObj.Metadata_Correct__c = metaData;
                qualityIndexObj.Comments__c = comments;
                qualityIndexObj.OwnerId = UserInfo.getUserId();
                qualityIndexObj.Article_ID__c = recordId;
                qualityIndexObj.Article_Knowledge_Id__c = artcl.KnowledgeArticleId;
                qualityIndexObj.Article_Name__c = artcl.Title;
                qualityIndexObj.Status__c = NI_Constants.NI_Approved_Status;
                qualityIndexObj.Approval_Type__c = NI_Constants.NI_Standard_Type;
                qualityIndexObj.Submitted_By__c = artcl.OwnerId;
                insert qualityIndexObj;
            }
            
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments(comments);
            req.setAction(NI_Constants.NI_Approve_Action);
            if(!test.isRunningTest()){
                ProcessInstanceWorkitem workItem  = [Select Id from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =: recordId LIMIT 1];
                req.setWorkitemId(workItem.Id); 
            }
                Approval.ProcessResult result =  Approval.process(req);
        	return NI_Constants.NI_Success_Msg;
        }catch(Exception e){
            return NI_Constants.NI_Failure_Msg;
        }
    } 
    
    /**
    * @method : approveArticle
    * @purpose : Method for rejecting the article
    *
    */
    @auraEnabled
    public static String rejectArticle(String recordId, Integer unique, Integer complete, Integer contentClear, Integer titleReflectsArticle, Integer linksValid, Integer metaData, String comments, String rejectReason, Boolean isEOL){
        try{
            KnowledgeArticleVersion artcl = [SELECT Title, KnowledgeArticleId, OwnerId FROM KnowledgeArticleVersion where id =:recordId LIMIT 1];
            if(!isEOL){
                NI_Article_Quality_Index__c qualityIndexObj = new NI_Article_Quality_Index__c();
                qualityIndexObj.Unique__c = unique;
                qualityIndexObj.Complete__c = complete;
                qualityIndexObj.Content_Clear__c = contentClear;
                qualityIndexObj.Title_Reflects_Article__c = titleReflectsArticle;
                qualityIndexObj.Links_Valid__c = linksValid;
                qualityIndexObj.Metadata_Correct__c = metaData;
                qualityIndexObj.Comments__c = comments;
                qualityIndexObj.OwnerId = UserInfo.getUserId();
                qualityIndexObj.Article_ID__c = recordId;
                qualityIndexObj.Article_Knowledge_Id__c = artcl.KnowledgeArticleId;
                qualityIndexObj.Article_Name__c = artcl.Title;
                qualityIndexObj.Rejection_Reason__c = rejectReason;
                qualityIndexObj.Status__c = NI_Constants.NI_Rejected_Status;            	
                qualityIndexObj.Approval_Type__c = NI_Constants.NI_Standard_Type;
                qualityIndexObj.Submitted_By__c = artcl.OwnerId;
                insert qualityIndexObj;
            }
            
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments(rejectReason);
            req.setAction(NI_Constants.NI_Reject_Action);
            if(!test.isRunningTest()){
                ProcessInstanceWorkitem workItem  = [Select Id from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =: recordId LIMIT 1];
                req.setWorkitemId(workItem.Id);
            }
            Approval.ProcessResult result =  Approval.process(req);
        	return NI_Constants.NI_Success_Msg;
        }catch(Exception e){
            return NI_Constants.NI_Failure_Msg;
        }
    } 
    
    /**
    * @method : approveArticle
    * @purpose : Method to submit the article for approval
    *
    */
    @auraEnabled
    public static String submit(String recordId){
        try{
            Knowledge__kav artcl = [SELECT ValidationStatus, Approver__c, Status__c, Submit_For_Approval__c FROM Knowledge__kav where id =:recordId LIMIT 1];
            if(artcl != null && (artcl.Status__c==NI_Constants.NI_Draft_Status||artcl.Status__c==NI_Constants.NI_Rejected_Status) && artcl.Approver__c != null){
               	artcl.Submit_For_Approval__c = true;
                artcl.Submitter__c = UserInfo.getUserId();
                artcl.Submit_for_Approval_Date__c = Date.today();
                update artcl;
                return NI_Constants.NI_Success_Msg; 
            }else{
            	return NI_Constants.NI_Failure_Msg; 
            }
        }catch(Exception e){
            return NI_Constants.NI_Failure_Msg;
        }
    } 
    
    /**
    * @method : approveArticle
    * @purpose : Method for publishing the article
    *
    */
    @auraEnabled
    public static String publish(String recordId){
        try{
            Knowledge__kav  articleId = [Select id,knowledgearticleid,PublishStatus,Submit_for_Approval__c from knowledge__kav where id =: recordId LIMIT 1];
            articleId.Submit_for_Approval__c = true;
            update articleId;
            return NI_Constants.NI_Success_Msg; 
        }catch(Exception e){
            System.debug('==== > '+e.getMessage());
            return NI_Constants.NI_Failure_Msg;
        }
    }
    
    /**
    * @method : approveArticle
    * @purpose : Method for submitting the article to EOL Archive
    *
    */
    @auraEnabled
    public static String submitForArchivalApproval(String recordId, String approverId){
        try{
            Knowledge__kav artcl = [SELECT KnowledgeArticleId, Title, UrlName FROM Knowledge__kav where id =:recordId LIMIT 1];
            Id newId = KbManagement.PublishingService.editOnlineArticle(artcl.KnowledgeArticleId, false);
            Knowledge__kav newArtcl = [SELECT Id, EOL_Archive__c, Status__c, Title, UrlName FROM Knowledge__kav WHERE Id=:newId AND PublishStatus='Draft' LIMIT 1];
            newArtcl.EOL_Archive__c = true;
            newArtcl.Status__c = NI_Constants.NI_SubmittedEOL_Status;
            newArtcl.Title = NI_Constants.NI_EOL_Archive_Prefix+ artcl.Title;
            newArtcl.UrlName = NI_Constants.NI_EOL_Archive_Prefix+ artcl.UrlName;
            newArtcl.Submitter__c  = UserInfo.getUserId();
            newArtcl.Submit_for_Approval_Date__c = Date.today();
            newArtcl.Approver__c = approverId;
            update newArtcl;
            return NI_Constants.NI_Success_Msg; 
        }catch(Exception e){
            return NI_Constants.NI_Failure_Msg;
        }
    } 
}