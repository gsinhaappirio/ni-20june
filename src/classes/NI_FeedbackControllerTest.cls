/* Class Name   : NI_FeedbackControllerTest
 * Description  : NI_FeedbackController test class to handle all the unit test cases for the controller 
 * Created By   : Meghana Agarwal
 * Created On   : 15-06-2017

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Meghana Agarwal        15-06-2017                1                   Initial version
 * 
*/

@isTest
private class NI_FeedbackControllerTest {
    //Variables
    private static List<String> picklistValues;
    
    //Test Method
    private static testMethod void testPicklistValues(){ 
        test.startTest();
        picklistValues = NI_FeedbackController.getPicklist(); 
        test.stopTest();
        System.assert(picklistValues != null);
        System.assert(picklistValues.size()>0);
    }
    
    //Test Method
    private static testMethod void testInsertFeedback(){
        List<Knowledge__kav> articleList = TestUtility.createArticles(5, 'Test-Article', true);
        test.startTest();
        Knowledge__kav art = articleList[0];
        NI_FeedbackController.insertArticleFeedback(art.Id,4,'','Rating');
        test.stopTest();
    }
}