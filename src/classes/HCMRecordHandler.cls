public class HCMRecordHandler {
    
    public static void updateApprovedUserRequests(List<HCM_Record__c> newList){
        
        List<String> emails = new List<String>();
        for(HCM_Record__c hcmRecord : newList){
            emails.add(hcmRecord.Email__c);
        }
        
        List<User_Request__c> userRequests = [Select Id, Attempt_User_Creation__c, RecordType.DeveloperName from User_Request__c where email__c in :emails and status__c = 'Processing' and RecordType.DeveloperName = 'Create_New_User'];
        for(User_Request__c userRequest : userRequests){
            userRequest.Attempt_User_Creation__c = true;
        }
        
        update userRequests;
    }
    
    public static void updateUsers(List<HCM_Record__c> newList){

        //Set<String> emails = hcmMap.keySet();
        List<String> emails = new List<String>();
        Map<String, HCM_Record__c> hcmMap = new Map<String, HCM_Record__c>();
        for(HCM_Record__c hcm : newList){
            emails.add(hcm.Email__c);
            hcmMap.put(hcm.Email__c, hcm);
        }
        
        List<String> userFields = new List<String>(User.SObjectType.getDescribe().fields.getMap().keySet());
        String userQuery = 'Select ' + String.join(userFields, ',') + ' from User where Email in :emails';
        List<User> users = Database.query(userQuery);
        for(User user : users){
            HCM_Record__c hcm = hcmMap.get(user.Email);
            user.Cost_Center__c = hcm.Cost_Center__c != null && user.Cost_Center__c != hcm.Cost_Center__c ? hcm.Cost_Center__c : user.Cost_Center__c;
            user.Manager_Person_Number__c = hcm.HCM_Manager_Person_Number__c != null && user.Manager_Person_Number__c != hcm.HCM_Manager_Person_Number__c ? hcm.HCM_Manager_Person_Number__c : user.Manager_Person_Number__c;
            user.Manager_HCM__c = hcm.HCM_Manager_Name__c != null && user.Manager_HCM__c  != hcm.HCM_Manager_Name__c ? hcm.HCM_Manager_Name__c : user.Manager_HCM__c;
            user.Region_HCM__c = hcm.HCM_Region__c != null && user.Region_HCM__c != hcm.HCM_Region__c ? hcm.HCM_Region__c : user.Region_HCM__c;
            user.Location_Name__c = hcm.Location_Name__c != null && user.Location_Name__c != hcm.Location_Name__c ? hcm.Location_Name__c : user.Location_Name__c;
            user.Department = hcm.Department__c != null && user.Department != hcm.Department__c ? hcm.Department__c : user.Department;
            //user.EmailEncodingKey = hcm.Email_Encoding_Key__c != null && user.EmailEncodingKey != hcm.Email_Encoding_Key__c ? hcm.Email_Encoding_Key__c : user.EmailEncodingKey;
            //user.LocaleSidKey = hcm.Locale_SID_Key__c != null && user.LocaleSidKey != hcm.Locale_SID_Key__c ? hcm.Locale_SID_Key__c : user.LocaleSidKey;
            //user.TimezoneSidKey = hcm.Time_Zone_SID_Key__c != null && user.TimezoneSidKey != hcm.Time_Zone_SID_Key__c ? hcm.Time_Zone_SID_Key__c : user.TimezoneSidKey;
        }
        
        update users;
    }
    
}