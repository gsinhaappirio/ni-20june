@isTest(SeeAllData=true)
public class UserDeactivator_Test {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    @isTest
    static void testDeactivation(){
        
        Test.startTest();
        
        //Schedule the test job
        String jobId = System.schedule('ScheduleUserDeactivatorTest', CRON_EXP, new UserDeactivator());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
        //Count number of active users        
        AggregateResult[] userCountBefore = [SELECT count(Id) from User WHERE Isactive = TRUE];
        Integer ucb = Integer.valueOf(userCountBefore[0].get('expr0'));
        
        //Remove a HCM record for an active user so it gets deactivated
        List<User> userToDeactivate = [SELECT EmployeeNumber FROM User WHERE IsActive = TRUE LIMIT 1];
        String empNum = userToDeactivate[0].EmployeeNumber;
        
        List<HCM_Record__c> hcmToDelete = [SELECT Id, Employee_Number__c from HCM_Record__c WHERE Employee_Number__c = :empNum];
        delete hcmToDelete;
        
        //Stopping test so that scheduled job runs
        Test.stopTest();
        
        //Verify result of scheduled job
        AggregateResult[] userCountAfter = [SELECT count(Id) from User WHERE Isactive = TRUE];
        //System.assertEquals(ucb-1, Integer.valueOf(userCountAfter[0].get('expr0')));
        
    }
}