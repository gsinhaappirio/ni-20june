/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class NI_Sales_Sales_QuoteLineDTO_Test {

    static testMethod void constructorWithParametersTest() {
        
	 	Decimal quoteLineNumber = 123;
		String bookingClass = 'bctest';
		String partNumber = 'partNumberTest';
		String partDescription = 'descTest';
		Decimal quantity = 1;
		Decimal lineListPrice = 100;
		String currencyCode = 'USD';
		Decimal lineAdjustedAmount = 100;
		Decimal lineAdjustedPercent = 100;
		Decimal lineQuotePrice = 100;
		Decimal lineTotalPrice = 100;
        
        NI_Sales_Sales_QuoteLineDTO quoteLine = new NI_Sales_Sales_QuoteLineDTO(quoteLineNumber, bookingClass, partNumber, partDescription, quantity,
        																		lineListPrice, currencyCode, lineAdjustedAmount, lineAdjustedPercent,
        																		lineQuotePrice, lineTotalPrice );
        
        System.assertEquals(123, quoteLine.quoteLineNumber);
        System.assertEquals('bctest', quoteLine.bookingClass);
        System.assertEquals('partNumberTest', quoteLine.partNumber);
        System.assertEquals('descTest', quoteLine.partDescription);
        System.assertEquals(1, quoteLine.quantity);
        System.assertEquals(100, quoteLine.lineListPrice);
        System.assertEquals('USD', quoteLine.currencyCode);
        System.assertEquals(100, quoteLine.lineAdjustedAmount);
        System.assertEquals(100, quoteLine.lineAdjustedPercent);
        System.assertEquals(100, quoteLine.lineQuotePrice);
        System.assertEquals(100, quoteLine.lineTotalPrice);
        
    }
    
    static testMethod void constructorWithoutParametersTest() {
        
        NI_Sales_Sales_QuoteLineDTO quoteLine = new NI_Sales_Sales_QuoteLineDTO();        
        System.assertNotEquals(null, quoteLine);
    }
}