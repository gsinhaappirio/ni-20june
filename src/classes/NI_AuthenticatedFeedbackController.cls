/**
*
*Name        : NI_AuthenticatedFeedbackController 
*Created By  : Meghana Agarwal (Appirio)
*Date        : 16th June, 2017
*Purpose     : Controller for Authenticated Article Feedback
*
**/

public class NI_AuthenticatedFeedbackController {
    
    /**
    * @method : insertArticleFeedback
    * @purpose : inserting article feedback for authenticated users.
    *
    */
    @AuraEnabled
    public static String insertArticleFeedback(String recordId, String summary, String areaOfExpertise, String comments){
        try{
            KnowledgeArticleVersion artcl = [SELECT Title, KnowledgeArticleId, OwnerId FROM KnowledgeArticleVersion where id =:recordId LIMIT 1];
            Article_Feedback__c articleFeedback = new Article_Feedback__c();
            articleFeedback.Article_Id__c = recordId;
            articleFeedback.Summary__c = summary;
            articleFeedback.Area_of_Expertise__c = areaOfExpertise;
            articleFeedback.Comments__c = comments;
            articleFeedback.Knowledge_Article_Version_Id__c = artcl.KnowledgeArticleId;
            if(UserInfo.getUserId() != null){
            	articleFeedback.OwnerId = UserInfo.getUserId();
            }
            articleFeedback.Article_Name__c = artcl.Title;
            insert articleFeedback;
        	return NI_Constants.NI_Success_Msg;
        }catch(Exception e){
            return NI_Constants.NI_Failure_Msg;
        }
    }
    
    /**
    * @method : getPicklist
    * @purpose : getting picklist values for Summary and Area of Expertise fields on article feedback.
    *
    */
    @AuraEnabled
    public static Map<String,List<String>> getPicklist(){
        Map<String,List<String>> pickListMap = new Map<String,List<String>>();
        List<String> summaryValues = new List<String>();
        NI_Article_Feedback_Summary_Value__mdt authenticatedSummary = [SELECT MasterLabel, Values__c FROM NI_Article_Feedback_Summary_Value__mdt where MasterLabel =: NI_Constants.NI_Authenticated_Feedback_Type LIMIT 1];
        if(authenticatedSummary != null){
            summaryValues = authenticatedSummary.Values__c.split(';');
        }
        Schema.DescribeFieldResult areaFieldResult = Article_Feedback__c.Area_of_Expertise__c.getDescribe();
        List<Schema.PicklistEntry> areaPle = areaFieldResult.getPicklistValues();
        List<String> areaValues = new List<String>();
        for( Schema.PicklistEntry f : areaPle){
  			areaValues.add(f.getValue());
        }
        pickListMap.put('summary', summaryValues);
        pickListMap.put('areaOfExpertise', areaValues);
        return pickListMap;   
    }
}