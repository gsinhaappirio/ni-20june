public class AccountHandler {
    final static string TRGRNAME='Account_Trigger';
    final static string CLASSNAME = 'AccountHandler';
    final static string METHODNAME = 'accountSubmitForApproval';

    @future
    public static void onMergeContactMove (Set<Id> accountIds){
    
        List<Contact> contacts = new List<Contact>();
        if(!accountIds.IsEmpty()){
            for(Contact contact : [Select Id, AccountId, LastUndeletedDate__c, Name from Contact where AccountId =: accountIds]){
                // this is after delete so the contacts have already been moved to the new account
                // just update the last undelete date so that the outbound message is sent to CDI
                contact.lastUndeletedDate__c = System.now();
                contacts.add(contact);
            }
        }
        update contacts;
    }
    
    public static void updateCountry(List<Account> accounts){
        List<Country__c> countries = [Select Id, Name, Full_Name__c, Three_Digit_ISO_Code__c, Other_Names__c from Country__c];
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for(Country__c country : countries){
            // name and full name are required so no need for a null check here
            countryMap.put(country.Name.toUpperCase().trim(), country);
            countryMap.put(country.Full_Name__c.toUpperCase().trim(), country);
            if(country.Three_Digit_ISO_Code__c != null){
                countryMap.put(country.Three_Digit_ISO_Code__c.toUpperCase().trim(), country);
            }
            if(country.Other_Names__c != null && !country.Other_Names__c.equals('')){
                List<String> otherNames = country.Other_Names__c.split('\n');
                for(String otherName : otherNames){
                    countryMap.put(otherName.toUpperCase().trim(), country);
                }
            }
        }
        
        for(Account account : accounts){
            if(account.ShippingCountry != null){
                Country__c country = countryMap.get(account.ShippingCountry.trim().touppercase());
                if(country != null){
                    account.ShippingCountry = country.Name;
                    account.Country__c = country.Id;
                }
            }else if(account.BillingCountry != null){
                account.ShippingCountry = account.BillingCountry;
                account.ShippingState = account.BillingState;
                account.ShippingCity = account.BillingCity;
                account.ShippingPostalCode = account.BillingPostalCode;
                account.ShippingStreet = account.BillingStreet;
                Country__c country = countryMap.get(account.BillingCountry.trim().touppercase());
                if(country != null){
                    account.ShippingCountry = country.Name;
                    account.Country__c = country.Id;
                }
            }
        }
    }
    
    @future
    public static void syncContactOwnersWithAccountOwners(Set<Id> accountIds){
        
        List<Contact> contacts = [Select Id, OwnerId, Account.OwnerId from Contact where AccountId in :accountIds];
        List<Contact> toUpdate = new List<Contact>();
        for(Contact contact : contacts){
            if(contact.ownerId != contact.account.ownerId){
                contact.OwnerId = contact.account.ownerId;
                toUpdate.add(contact);
            }
        }
        
        Database.update(toUpdate, false);
        
    }
    
    
    /**********************************************************************************************************************
    * Author:
    * Date:
    * Param: set<Id> (AccountId)
    * Return: void
    * Description:
    ***********************************************************************************************************************
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Undelete
    * Summary of Changes : - Initial version
    **********************************************************************************************************************/
    @future
    public static void undeleteAccount(set<Id> AccountId){
        string appname=system.label.App_Name;
        string update_FailedLbl = system.label.Update_Failed;
        list<Account> newAccountList = new list<Account>();
        list<Contact> ContactList = new list<contact>();
        newAccountList =[select id,LastUndeletedDate__c from account where id in:AccountId ];
        contactList=[select id,LastUndeletedDate__c from contact where accountID IN:AccountId];
        try{
            for(Account newacc :newAccountList ){
                newacc.LastUndeletedDate__c=system.now();
            }
            for(contact con: ContactList){
                con.LastUndeletedDate__c=system.now();
            }
            update ContactList;
            update newAccountList ;
            }Catch(Exception e){
            system.LoggingLevel newLoglevel = LoggingLevel.ERROR;
            if(e.getMessage().contains(update_FailedLbl) ){
                UTIL_LoggingService.logDmlResults(null,null,newAccountList,appname,
                CLASSNAME,METHODNAME,TRGRNAME,newLoglevel);
            }
        }
    }
}