@isTest	
public with sharing class NI_Sales_QuoteWS_QuoteRequest_MockImpl implements WebServiceMock{

	boolean error;
	
	public NI_Sales_QuoteWS_QuoteRequest_MockImpl(){
		this.error = false;
	}
	
	public NI_Sales_QuoteWS_QuoteRequest_MockImpl(boolean error){
		this.error = error;
	}

	public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	 
	       NI_Sales_QuoteWS_Quote.GetQuoteResponse_element respElement = 
	           new NI_Sales_QuoteWS_Quote.GetQuoteResponse_element();
	           
	       if (!error){
	       	   System.debug('doInvoke, no error');
		       respElement.responseStatus = 'S';
		       respElement.QuoteInformation = new NI_Sales_QuoteWS_Quote.Quote();
		       respElement.QuoteInformation.QuoteHeader = new NI_Sales_QuoteWS_Quote.QuoteHeader();
		       respElement.QuoteInformation.QuoteHeader.headerId = 123;
		       
		       respElement.QuoteInformation.quoteLines = new NI_Sales_QuoteWS_Quote.QuoteLines(); 
		       respElement.QuoteInformation.quoteLines.quoteLine = new NI_Sales_QuoteWS_Quote.QuoteLine[2];
		       respElement.QuoteInformation.quoteLines.quoteLine[0] = new NI_Sales_QuoteWS_Quote.QuoteLine();
		       respElement.QuoteInformation.quoteLines.quoteLine[0].partNumber = '1';
		       respElement.QuoteInformation.quoteLines.quoteLine[0].lineListPrice = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[0].lineAdjustedAmount = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[0].lineAdjustedPercent = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[0].lineQuotePrice = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[0].lineTotalPrice = 100;
		       
		       respElement.QuoteInformation.quoteLines.quoteLine[1] = new NI_Sales_QuoteWS_Quote.QuoteLine();
		       respElement.QuoteInformation.quoteLines.quoteLine[1].partNumber = '2';
		       respElement.QuoteInformation.quoteLines.quoteLine[1].lineListPrice = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[1].lineAdjustedAmount = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[1].lineAdjustedPercent = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[1].lineQuotePrice = 100;
		       respElement.QuoteInformation.quoteLines.quoteLine[1].lineTotalPrice = 100;
	       	   
	       }
	       else{
	       	System.debug('doInvoke, error');
	       	respElement.responseStatus = 'E';
	       }
	        	           
	       response.put('response_x', respElement); 
	       
	
	}
}