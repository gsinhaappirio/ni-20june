global class CustomLookupController {

    public CustomLookupController(ApexPages.StandardController controller) {
//        setOppUrl();
    }
    
    @RemoteAction
    global static List<Opportunity> getOpportunities(String opportunityName){
        if(opportunityName != ''){
            List<Opportunity> vOpplst = new List<Opportunity>();
            vOpplst = database.query('SELECT Id, Name from Opportunity where Name LIKE \'%' + opportunityName + '%\' LIMIT 20');
            return vOpplst ;
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static List<Lead> getLeads(String leadName){
        if(leadName != ''){
            List<Lead> vLeadlst = new List<Lead>();
            vLeadlst = database.query('SELECT Id, Name from Lead where Name LIKE \'%' + leadName + '%\' LIMIT 20');
            return vLeadlst ;
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static List<Contact> getContacts(String contactName){
        if(contactName != ''){
            List<Contact> vContactlst = new List<Contact>();
            vContactlst = database.query('SELECT Id, Name from Contact where Name LIKE \'%' + contactName + '%\' LIMIT 20');
            return vContactlst;
        }
        else{
            return null;
        }
    }   

}