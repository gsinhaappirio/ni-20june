/* Class Name   : NI_Header_ControllerTest
 * Description  : NI_Header_Controller test class to handle all the unit test cases for the controller 
 * Created By   : Meghana Agarwal
 * Created On   : 15-06-2017

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Meghana Agarwal        15-06-2017                1                   Initial version
 * 
*/

@isTest
private class NI_Header_ControllerTest {
    //variables
    private static String locale = 'en-US';
    
    //Test Method
    private static testMethod void testData1(){ 
        test.startTest();
        String response = NI_Header_Controller.getData1(); 
        test.stopTest();
        System.assertEquals(response,'res.getBody()');
    }
    
    //Test Method
    private static testMethod void testHiearchySettings(){
        test.startTest();
        Authentication_Wrapper__c wrapper = NI_Header_Controller.getHiearchySettings();   
        test.stopTest();
        System.assertEquals(wrapper.API_Key__c,Authentication_Wrapper__c.getInstance().API_Key__c);
        System.assertEquals(wrapper.Application_Name__c,Authentication_Wrapper__c.getInstance().Application_Name__c);
    }
    
    //Test Method
    private static testMethod void testData3(){
        NI_Header_Controller hc = new NI_Header_Controller(); 
        test.startTest();
        String response = NI_Header_Controller.getData3(hc.wrapperNameSetting,hc.apiKeySetting,locale); 
        test.stopTest();
        HttpRequest req = new HttpRequest();
        System.assertEquals(response, new NI_Mock_Http_Response().respond(req).getBody());
    }   
}