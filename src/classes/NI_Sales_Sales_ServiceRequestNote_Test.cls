@isTest
public with sharing class NI_Sales_Sales_ServiceRequestNote_Test {
    @isTest
    public static void parameterizedConstructorTest() {
        String createdBy = 'creatorName';
        DateTime creationDate = System.today();
        String type = 'typeName';
        String comments = 'typeName';
        
        NI_Sales_Sales_ServiceRequestNoteDTO note = new NI_Sales_Sales_ServiceRequestNoteDTO(createdBy, creationDate, type, comments);
        
        system.assertEquals(note.createdBy, createdBy);
        system.assertEquals(note.creationDate, creationDate);
        system.assertEquals(note.type, type);
        system.assertEquals(note.comments, comments);
    }
    
    @isTest
    public static void emptyConstructorTest() {
        NI_Sales_Sales_ServiceRequestNoteDTO note = new NI_Sales_Sales_ServiceRequestNoteDTO();
        
        system.assertNotEquals(note, null);
    }
}