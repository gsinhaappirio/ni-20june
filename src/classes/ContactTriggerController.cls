/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            ContactTriggerController
   Description:     This class invokes methods of ContactHandler. 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class ContactTriggerController{

    public static void onAfterUndelete(List<Contact> newContactList){
        set<ID> ContactId = new set<Id>();
        for(Contact acc : newContactList)
        {
            ContactId.add(acc.Id);
        }
           ContactHandler.UndeleteContact(ContactId); 
    }
    
    public static void onAfterInsert(Map<Id, Contact> newContactMap){
        ContactHandler.setOwnerFromAccount(newContactMap.keySet());
    }
    
    public static void onAfterUpdate(Map<Id, Contact> oldContactMap, List<Contact> newContactList){
        Set<Id> contactIds = new Set<Id>();
        for(Contact contact : newContactList){
            Contact oldContact = oldContactMap.get(contact.Id);
            if(contact.accountId != oldContact.accountId){
                contactIds.add(contact.Id);
            }
        }
        if(!contactIds.isEmpty()){
            ContactHandler.setOwnerFromAccount(contactIds);
        }
    }
}