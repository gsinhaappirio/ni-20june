@isTest
public class AssignNewLeadExtension_Test {

    @isTest(SeeAllData=true)
    private static void assignLeadFromQueue(){

        Test.startTest();
        
        // This code runs as the system user
          Profile p = [SELECT Id FROM Profile WHERE Name='NI System Administrator'];
          User u = new User(Alias = 'test', Email='test@ni.com',
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
          LocaleSidKey='en_US', ProfileId = p.Id,
          TimeZoneSidKey='America/Los_Angeles', UserName='test@ni.com');
          System.runAs(u) {

             // The following code runs as user 'u' user
            List<Lead> leadList = [SELECT Name FROM Lead LIMIT 20];
        
            ApexPages.StandardSetController  sc = new ApexPages.StandardSetController (leadList);
            AssignNewLeadExtension testLeadToMer = new AssignNewLeadExtension(sc);
         
            PageReference pageRef = Page.AssignLeadFromQueue; 
            Test.setCurrentPage(pageRef);
        
            testLeadToMer.assignLeadFromQueue();
          }
    
         

        
        Test.stopTest();
        
    }
    
    
    
}