public class ContactUtils
{
    
    public class Request
    {
        @InvocableVariable(label='Contact Id' required=true)
        public Id contactId;
    }
    
    @InvocableMethod
    public static void DeleteContacts(List<Id> contactIds)
    {
        List<Contact> contacts = [Select Id from Contact where Id in :contactIds and active__c = false];
        delete contacts;
    }
    
}