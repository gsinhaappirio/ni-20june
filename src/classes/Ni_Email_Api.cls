public class Ni_Email_Api {

    public class Ni_Email_Api_Exception extends Exception {}
    
    public enum EmailType{HTML, PLAIN}
    
    public Ni_Email_Api(){}
    
    public void sendEmail(String fromAddress, String fromName, String replyTo, List<String> toAddresses, List<String> ccAddresses, EmailType emailType,
                          String subject, String body, String attachmentName, String contentType, List<Blob> attachmentBlobs){

		// First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        if(!Test.isRunningTest()){
            Messaging.reserveSingleEmailCapacity(2);
        }

        // create a new single email message object
        // that will send out a single email to the addresses in the to, cc & bcc lists.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        if(attachmentBlobs != null && !attachmentBlobs.isEmpty()){
            if(attachmentName == null){
                System.debug('***** Error: You must provide an attachment name.');
                throw new Ni_Email_Api_Exception('You must provide an attachment name.');
            }
            
            if(contentType == null){
                System.debug('***** Error: You must provide a content type.');
                throw new Ni_Email_Api_Exception('You must provide a content type.');
            }
            
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            for(Blob b : attachmentBlobs){
                Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                attachment.setFileName(attachmentName);
                attachment.setBody(b);
                attachment.setContentType(contentType);
                attachments.add(attachment);
            }
            mail.setFileAttachments(attachments);
        }

        // assign the addresses for the to and cc
        mail.setToAddresses(toAddresses);
        mail.setCcAddresses(ccAddresses);

        // specify the address used when the recipients reply to the email
        mail.setReplyTo(replyTo != null ? replyTo : null);

        // specify the name used as the display name instead of the email address
        mail.setSenderDisplayName(fromName != null ? fromName : null);
                              
        // specify the subject                  
        mail.setSubject(subject);

        // specify the type of the email. default to plain text
		if(emailType == Ni_Email_Api.EmailType.HTML){
            mail.setHtmlBody(body);
        }else{
            mail.setPlainTextBody(body);
        }

        // send the email
        if(!Test.isRunningTest()){
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
}