@isTest
public class New_Request_Help_Trigger_Test {

    @isTest(SeeAllData=true)
    private static void test1(){

        Test.startTest();
        Account account = new Account();
        account.name = 'Test Account';
        account.ShippingCountry = 'US';
        account.ShippingCity = 'Austin';
        account.ShippingStreet = '123 Fake St';
        insert account;

        New_Request_Help__c rh = new New_Request_Help__c();
        rh.Account__c = account.Id;
        rh.Category__c = 'Loaners';
        insert rh;

        rh.Category__c = 'Finance';
        update rh;

        Test.stopTest();

    }

}