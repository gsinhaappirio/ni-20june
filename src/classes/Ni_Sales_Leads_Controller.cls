public class Ni_Sales_Leads_Controller
{
    public List<Lead> myLeads{get;set;}
    private final Lead lead;
    public String warningMessage{get;set;}

    public String oldSortField{get; set;}
    public String sortField{get; set;}
    public String sortDirection{get; set;}
    public String testType{get; set;}
    public String sortType{get; set;}

    public Boolean displayFullView{get;set;}
    public Map<String, String> fieldsApiNames{get;set;}

    public List<GroupMember> queues{get;set;}
    public Map<Id, Skill__c> userSkills{get;set;}
    public List<User_Skill__c> userSkillSetList{get;set;}
    public List<Skill__c> exclusiveSkills{get;set;}
    public User user{get;set;}

    private static String QUALIFIED = System.Label.Qualified;
    private static String ASSIGNED = System.Label.Assigned;
    private static String ACCEPTED = System.Label.Accepted;

    public Ni_Sales_Leads_Controller(ApexPages.StandardSetController controller){
        this.lead = (Lead)controller.getRecord();

        Lead_Settings__c leadSettings = Lead_Settings__c.getInstance(UserInfo.getUserID());
        displayFullView = leadSettings.Access_Manager_View__c;

        Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.Lead.fields.getMap();
        fieldsApiNames = new Map<String, String>();

        for(Schema.FieldSetMember f : SObjectType.Lead.FieldSets.Leads_Tab.getFields()) {
            fieldsApiNames.put(f.getLabel(), f.getFieldPath());
        }

        sortField = 'Full Name';
        oldSortField = sortField;
        sortDirection = 'asc';

        String query = 'SELECT ';
        for(Schema.FieldSetMember f : SObjectType.Lead.FieldSets.Leads_Tab.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        String userId = UserInfo.getUserId();
        query += 'Id FROM Lead where ownerId = :userId AND Status IN (:QUALIFIED, :ASSIGNED, :ACCEPTED) and isConverted = false ORDER BY Name ASC';

        myLeads = Database.query(query);

        queues = [SELECT Group.Id, Group.Name FROM GroupMember WHERE UserOrGroupId =: userId AND Group.Type = 'Queue'];
        if(queues != null && queues.size() == 0){
            queues = null;
        }

        user = [Select Id, FirstName, LastName, SmallPhotoUrl, FullPhotoUrl, profile.name, 
                        (Select Id, Name, Exclusive__c, Skill__c, Skill__r.Name, Skill__r.Id
                         From User_Skills__r)
                From User
                where Id = :UserInfo.getUserId() LIMIT 1];
        List<User_Skill__c> skillSets = user.User_Skills__r;
        
        // check if any skills are exclusive
        if(skillSets != null && skillSets.size() > 0){
            exclusiveSkills = new List<Skill__c>();
            userSkillSetList = new List<User_Skill__c>();
            userSkills = new Map<Id, Skill__c>();
            for(User_Skill__c skillSet : skillSets){
                if(skillSet.Exclusive__c){
                    exclusiveSkills.add(skillSet.skill__r);
                    System.debug('***** exclusive skill: ' + skillSet.skill__r.name);
                }
                userSkillSetList.add(skillSet);
                userSkills.put(skillSet.Skill__r.Id, skillSet.Skill__r);
            }
        }

        if(exclusiveSkills == null || exclusiveSkills.size() == 0){
            exclusiveSkills = null;
        }

        if(Test.isRunningTest()){
            for(Integer i = 0; i < 10; i++){
                Lead lead = new Lead();
                lead.FirstName = '' + String.valueOf(i);
                lead.LastName = ' lead';
                //lead.Name = String.valueOf(i) + ' lead';
                lead.company = 'National Instruments ' + String.valueOf(i);
                lead.country = 'US';
                lead.State = 'TX';
                lead.Budget__c = true;
                lead.Date_Accepted__c  = DateTime.now().date();
                myLeads.add(lead);
            }
        }

    }

    public PageReference refreshTable(){
        if(oldSortField.equals(sortField)){
            // same sort field, change the asc/desc order
            if(sortDirection == null){
                sortDirection = 'asc';
            }else if(sortDirection.equals('asc')){
                sortDirection = 'desc';
            }else{
                sortDirection = 'asc';
            }
        }else{
            // new sort field, change it and order by asc
            sortDirection = 'asc';
            oldSortField = sortField;
        }

        if(Test.isRunningTest()){
            if(sortType != null && sortType.toLowerCase().equals('boolean')){
                oldSortField = 'Budget';
                sortField = 'Budget';
            }else if(sortType != null && sortType.toLowerCase().equals('decimal')){
                oldSortField = '';
                sortField = '';
            }else if(sortType != null && sortType.toLowerCase().equals('string')){
                oldSortField = 'First Name';
                sortField = 'First Name';
            }else if(sortType != null && sortType.toLowerCase().equals('date')){
                oldSortField = 'Date Accepted';
                sortField = 'Date Accepted';
            }
        }

        updateSortedList();

        return null;
    }

    private List<Lead> updateSortedList(){
        if(myLeads == null || myLeads.isEmpty()){
            return null;
        }else{
            List<SortedLeadColumn> sortedLeads = new List<SortedLeadColumn>();

            for(Lead lead : myLeads){
                String field = fieldsApiNames.get(sortField);
                Object value = lead.get(field);

                SortedLeadColumn sl = new SortedLeadColumn(lead, value, sortDirection);
                sortedLeads.add(sl);
            }


            sortedLeads.sort();

            myLeads.clear();
            for(SortedLeadColumn sl : sortedLeads){
                myLeads.add(sl.lead);
            }

            return null;
        }
    }

    public PageReference GetNextLead(){
        //Id nextLead = assignNewLead(UserInfo.getUserID());
        Id nextLead = assignNewLeadWithSkills(UserInfo.getUserID());
        if(Test.isRunningTest()){
            Lead tempLead = new Lead();
            tempLead.FirstName = 'Test';
            tempLead.LastName = 'Lead';
            tempLead.Country = 'US';
            tempLead.State = 'TX';
            tempLead.Company = 'NI';
            if(testType != null && testType.toLowerCase().equals('creq')){
                tempLead.cREQ_Date__c = DateTime.now().date();
            }
            insert tempLead;
            nextLead = tempLead.Id;
        }
        if(nextLead == null){
            Lead_Settings__c leadSettings = Lead_Settings__c.getInstance(UserInfo.getUserID());
            warningMessage = leadSettings.No_More_Leads_Message__c;
            return null;
        }else{
            PageReference newpage = new PageReference ('/' + nextLead + '?retURL=/' + nextLead);
            return newpage;
        }
    }

    public Id assignNewLeadWithSkills(Id userId){
        // get the queues the user belongs to

        List<Id> queueIds = new List<Id>();
        if(queues != null && !queues.isEmpty()){
            for(GroupMember groupMember : queues){
                System.debug('***** queue id: ' + groupMember.group.Id);
                queueIds.add(groupMember.group.Id);
            }
        }

        List<Lead> leads;
        Lead leadToAssign;

        if(exclusiveSkills != null && !exclusiveSkills.isEmpty()){
            // there are exclusive skills. only query for leads that have this skill
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c, RecordType.DeveloperName,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true)
                    from Lead
                    where ownerId in :queueIds
                    and status = :QUALIFIED
                    and Ready_to_be_Assigned__c = true
                    and isConverted = false
                    and Id in
                        (Select Lead__c from Lead_Skill__c where Skill__c in :exclusiveSkills)
                    ORDER BY CreatedDate DESC];
            System.debug('***** number of leads with skill: ' + leads.size());

        }else{
            // query leads but only get required skills for that lead
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c, RecordType.DeveloperName,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true) 
                    from Lead 
                    where ownerId in :queueIds 
                    and status = :QUALIFIED 
                    and Ready_to_be_Assigned__c = true 
                    and isConverted = false
                    ORDER BY CreatedDate DESC];
            System.debug('***** number of leads: ' + leads.size());

        }
        
        List<SortedLead> sortedLeads = new List<SortedLead>();
        
        for(Lead lead : leads){
            SortedLead sl = new SortedLead(lead);
            sortedLeads.add(sl);
        }

        sortedLeads.sort();
        
        for(SortedLead sl : sortedLeads){
            Lead lead = sl.lead;
            System.debug('***** iterating over: ' + lead.name + ' - ' + lead.Id);
            if(lead.lead_skills__r != null && !lead.lead_skills__r.isEmpty()){
                // the lead has required skills. loop through to verify the user has a match
                // TODO - how can we get rid of this n^2 runtime?
                //for ( Integer i=lead.lead_skills__r.size()-1 ; i>=0 ; i-- )
                for ( Integer i=0 ; i<lead.lead_skills__r.size() ; i++ )
                {
                    Lead_Skill__c leadSkill = lead.lead_skills__r.get(i);
                    if(leadSkill.Required__c){
                        System.debug('+++++ required skill on lead2: ' + leadSkill.Skill__r.Name);
                        if(userSkills != null && userSkills.get(leadSkill.skill__r.id) != null){
                            System.debug('+++++ has a required skill13: ' + userSkills.get(leadSkill.skill__r.id).Name);
                            leadToAssign = lead;
                            if ( ! leadSkill.Skill__r.Name.equals('Academic') )
                                break;
                        }else{
                            System.debug('+++++ user does not have required skill: ' + leadSkill.Skill__r.Name);
                            leadToAssign = null;
                            break;
                        }
                    }

                    //if ( i == 0 )
                    if ( i == lead.lead_skills__r.size()-1 )
                    {
                        leadToAssign = lead;
                        break;
                    }

                }
            }else{
                leadToAssign = lead;
            }
            
            if(leadToAssign != null){
                break;
            }
            
        }

        if(leadToAssign != null){
            leadToAssign.ownerId = userId;
            leadToAssign.Status = ASSIGNED;
            update leadToAssign;
            return leadToAssign.Id;
        }else{
            Lead_Settings__c leadSettings = Lead_Settings__c.getInstance(UserInfo.getUserID());
            warningMessage = leadSettings.No_More_Leads_Message__c;
            return null;
        }

        return null;

    }
    
    public class SortedLead implements Comparable {
        public Lead lead{get;set;}
        
        public SortedLead(Lead lead){
            this.lead = lead;
        }
        
        public Integer compareTo(Object compareTo) {
            SortedLead other = (SortedLead)compareTo;
            
            // this.lead is the one that is getting ordered.
            // "other" is the lead that it is being compared to
            // -1 pushes lead to the top (first), 1 pushes it down (last)
            
            if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c == null){
                // creq, other is not
                return -1;
            }else if(lead.cREQ_Date__c == null && other.lead.cREQ_Date__c != null){
                // not creq, other is
                return 1;
            }else if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c != null){
                // they're both cReqs, this should stay FIFO for cReqs
                if(lead.createdDate > other.lead.createdDate){
                    return 1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return -1;
                }
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') &&
                    !other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // sgl, other is not
                return -1;
            }else if(!lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // not sgl, other is
                return 1;
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // they're both sgl's, this should stay LIFO
                if(lead.createdDate > other.lead.createdDate){
                    return -1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return 1;
                }
            }else if(lead.createdDate > other.lead.createdDate){
                return -1;
            }else if(lead.createdDate < other.lead.createdDate){
                return 1;
            }
            
            return 0;
            
        }
        
    }

    public class SortedLeadColumn implements Comparable {

        public Lead lead{get;set;}
        public String direction{get;set;}
        public Object value{get;set;}

        public SortedLeadColumn(Lead lead, Object value, String direction){
            this.lead = lead;
            this.value = value;
            this.direction = direction;
        }

        public Integer compareTo(Object compareTo) {
            SortedLeadColumn other = (SortedLeadColumn)compareTo;

            if(value instanceOf String){
                if(direction.equals('asc')){
                    if(value == null){
                        return -1;
                    }else if(other.value == null){
                        return 1;
                    }else{
                        String temp = (String)value;
                        return temp.compareTo((String)other.value);
                    }
                }else{
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else{
                        String temp = (String)other.value;
                        return temp.compareTo((String)value);
                    }
                }
            }else if(value instanceof Decimal){
                if(direction.equals('asc')){
                    if(value == null){
                        return -1;
                    }else if(other.value == null){
                        return 1;
                    }else {
                        return (Decimal)value > (Decimal)other.value ? 1 : -1;
                    }
                }else{
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else {
                        return (Decimal)value > (Decimal)other.value ? -1 : 1;
                    }
                }
            }else if(value instanceof Date){

                if(direction.equals('asc')){
                    if(value == null){
                        return -1;
                    }else if(other.value == null){
                        return 1;
                    }else {
                        Date d = (Date)value;
                        DateTime dt = datetime.newInstance(d.year(), d.month(),d.day());
                        Date otherd = (Date)other.value;
                        DateTime otherdt = datetime.newInstance(otherd.year(), otherd.month(), otherd.day());
                        return dt > otherdt ? 1 : -1;
                    }
                }else{
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else {
                        Date d = (Date)value;
                        DateTime dt = datetime.newInstance(d.year(), d.month(),d.day());
                        Date otherd = (Date)other.value;
                        DateTime otherdt = datetime.newInstance(otherd.year(), otherd.month(), otherd.day());
                        return dt > otherdt ? -1 : 1;
                    }
                }
            }else if(value instanceof DateTime){

                if(direction.equals('asc')){
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else {
                        DateTime dt = (DateTime) value;
                        DateTime otherdt = (DateTime) other.value;
                        return dt > otherdt ? 1 : -1;
                    }
                }else{
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else {
                        DateTime dt = (DateTime) value;
                        DateTime otherdt = (DateTime) other.value;
                        return dt > otherdt ? -1 : 1;
                    }
                }
            }else if(value instanceof Boolean){
                if(direction.equals('asc')){
                    if(value == null){
                        return -1;
                    }else if(other.value == null){
                        return 1;
                    }else {
                        Boolean temp = (Boolean)value;
                        return temp ? 1 : -1;
                    }
                }else{
                    if(value == null){
                        return 1;
                    }else if(other.value == null){
                        return -1;
                    }else {
                        Boolean temp = (Boolean)value;
                        return temp ? -1 : 1;
                    }
                }
            }

            return null;

        }

    }

}