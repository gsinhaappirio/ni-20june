/*
  * @author        OffshoreTeam_Accenture
  * @date          29/10/2014
  * @description   Test Class for opportunityReview/Interaction_Trigger class.
  */
  
@isTest(SeeAllData=true)
Private class NI_OppReviewInteraction_Trigger_Test
{

       private static final String ALIAS = 'sch1';
       private static final String SYSTEM_ADMIN = 'System Administrator';
  /*
    * @description       Test method to verify OpportunityReview/Interaction trigger, controller and handler successfully calls.
    * @param             NA
    * @return            void
    * @throws            NA
    */
    
    static testMethod void testOpportunityReviewInteraction_Trigger()
    {
        test.starttest();
    
        Boolean m_isExecuting = false;
        Integer batchSize = 0;
        
        //-----------Insert Account----------
        list<account> acclist = new list<Account>();
            User runUser = Common_Class.runningUser(ALIAS, SYSTEM_ADMIN);
            
        account acc= new account(name= 'test',OwnerId = UserInfo.getUserId(),ShippingCountry = 'United States',ShippingStreet = '1/AX39',ShippingCity = 'New York',ShippingState = 'New York',ShippingPostalCode = '12119');
        acclist.add(acc);
        insert acclist;
        
        //-----------Insert Multi Phase Opportunity----------
        list<Multi_Phase_Opportunity__c > mopplist= new list<Multi_Phase_Opportunity__c >();
        Multi_Phase_Opportunity__c mopp = new Multi_Phase_Opportunity__c (Account__c=acclist[0].id,Current_Amount_del__c= null );
        mopplist.add(mopp);
        insert mopplist;
        
        //-----------Insert Opportunity----------
        list<opportunity> opplist =  new list<opportunity>();
        Opportunity opp = new opportunity(name='test2',AccountId=acclist[0].id,Multi_Phase_Opportunity__c=mopplist[0].id,StageName='Qualify',ForecastCategoryName='funnel',CloseDate=system.today(),Amount = 1000  );
        opplist.add(opp);
        insert opplist;
        
        //-----------Insert Opportunity Review Interaction Plan----------
        list<Opportunity_Review_Interaction_Plan__c> oplanlist = new list<Opportunity_Review_Interaction_Plan__c>();
        Opportunity_Review_Interaction_Plan__c oplan = new Opportunity_Review_Interaction_Plan__c(opportunity__c=opplist[0].id );
        oplanlist.add(oplan);
        insert oplanlist;
        
        //----------OpportunityReviewInteraction Handler Class Methods----------
        Opp_Review_InterTriggerController ortc=new Opp_Review_InterTriggerController(m_isExecuting,batchSize);
         test.stoptest();
    }
}