global class JiraUtils {
    
 	/**
 	 * mappingId:	ServiceRocket mapping configuration in Jira. This is setup and maintained in Jira.
 	 * objectType:	name of the object. used by ServiceRocket to map to issues
 	 * objectId:	SFDC Id of the object to maintain syncing
 	 * projectKey:	jira project key.
 	 * issueTypeId:	Id of the issue type in jira. can be derived from the jira admin setup
 	 */
    @future(callout=true) 
    webService static void createIssue(String mappingId, String objectType, String objectId, String projectKey, String issueTypeId) {
        try {
            Jira_Settings__c jiraSettings = Jira_Settings__c.getInstance();
            String baseUrl = jiraSettings.Server_Url__c;
            String username = jiraSettings.Username__c;
            String password = jiraSettings.Password__c;
            Boolean enabled = jiraSettings.Enabled__c;
            if(enabled){
                String url = baseUrl + '/rest/customware/connector/1.0/' + mappingId + '/' + objectType + '/' + objectId + '/issue/create.json';
                String body = '{"project":"' + projectKey + '", "issueType":"' + issueTypeId + '"}';
                HttpRequest req = CalloutUtils.getPOSTRequest(url, username, password, body);
                req.setHeader('Content-Type','application/json');
                CalloutUtils.sendRequest(req);
            }
        } catch(System.CalloutException e) {
            System.debug(e);
        }
    }
    
    /**
 	 * mappingId:	ServiceRocket mapping configuration in Jira. This is setup and maintained in Jira.
 	 * objectType:	name of the object. used by ServiceRocket to map to issues
 	 * objectId:	SFDC Id of the object to maintain syncing
 	 */
    @future(callout=true) 
    webService static void synchronizeIssue(String mappingId, String objectType, String objectId) {
        try {
            Jira_Settings__c jiraSettings = Jira_Settings__c.getInstance();
            String baseUrl = jiraSettings.Server_Url__c;
            String username = jiraSettings.Username__c;
            String password = jiraSettings.Password__c;
            Boolean enabled = jiraSettings.Enabled__c;
            if(enabled){
                String url = baseUrl + '/rest/customware/connector/1.0/' + mappingId + '/' + objectType + '/' + objectId + '/issue/synchronize.json';
                HttpRequest req = CalloutUtils.getPUTRequest(url, username, password);
                req.setHeader('Content-Type','application/json');
                req.setHeader('Content-Length', '0');
                CalloutUtils.sendRequest(req);
            }
        } catch(System.CalloutException e) {
            System.debug(e);
        }
    }
    
    // Detects whether current user is not JIRA agent. By calling this you can make sure that
    // infinite loops won't happen in triggers (for instance when synchronizing an issue with JIRA)
    public static Boolean currentUserIsNotJiraAgent(){
        Jira_Settings__c jiraSettings = Jira_Settings__c.getInstance();
        String jiraUserId = jiraSettings.Jira_User_Id__c;
        Boolean allow = false;
        if(jiraUserId != null){
            User jiraUser = [Select Id from User where Id = :jiraUserId];
            allow = UserInfo.getUserId() != jiraUser.Id;
        }else{
            // want to err on the side of caution and prevent synchs if the jira user id is null
            allow = false;
        }
        return allow || Test.isRunningTest();
    }
 
}