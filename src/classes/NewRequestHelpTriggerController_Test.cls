@isTest(seeAllData=true)
public class NewRequestHelpTriggerController_Test {
    
    @isTest(seeAllData=true)
    public static void onBeforeInsertTest(){
        
        Test.startTest();
        Account account = new Account();
        account.name = 'False Account';
        account.ShippingCountry = 'US';
        account.ShippingCity = 'Austin';
        account.ShippingStreet = '123 Fake St';
        insert account;
        
        List<New_Request_Help__c> rhList = new List<New_Request_Help__c>();
        List<String> categories = new List<String>{'loaners','finance','sales operations','contracts & deal desk','support-close opportunity'};
            for(String category : categories){
                New_Request_Help__c rh = new New_Request_Help__c();
                rh.Account__c = account.Id;
                rh.Category__c = category;
                rhList.add(rh);
            }
        
        Account account2 = new Account();
        account2.name = 'Not Real Account 2';
        account2.ShippingCountry = 'zzzz';
        account2.ShippingCity = 'San Antonio';
        account2.ShippingStreet = '123 Real St';
        insert account2;
        
        New_Request_Help__c rh = new New_Request_Help__c();
        rh.Account__c = account2.Id;
        rh.Category__c = 'loaners';
        rhList.add(rh);
        
        NewRequestHelpTriggerController.onBeforeInsert(rhList);
        
        Test.stopTest();
        
    }
    
    @isTest(seeAllData=true)
    public static void onBeforeUpdateTest(){
        
        Test.startTest();
        Account account = new Account();
        account.name = 'False Account 1';
        account.ShippingCountry = 'US';
        account.ShippingCity = 'Austin';
        account.ShippingStreet = '123 Fake St';
        insert account;
        
        List<New_Request_Help__c> rhList = new List<New_Request_Help__c>();
        List<String> categories = new List<String>{'loaners','finance','sales operations','contracts & deal desk','support-close opportunity'};
        for(String category : categories){
        	New_Request_Help__c rh = new New_Request_Help__c();
            rh.Account__c = account.Id;
            rh.Category__c = category;
            rhList.add(rh);
        }
        
        insert rhList;
        
        Account account2 = new Account();
        account2.name = 'Not Real 2';
        account2.ShippingCountry = 'zzzz';
        account2.ShippingCity = 'Austin';
        account2.ShippingStreet = '123 Real St';
        insert account2;
        
        Map<Id, New_Request_Help__c> updatedRHList = new Map<Id, New_Request_Help__c>();
        for(New_Request_Help__c rh : rhList){
            rh.Category__c = 'Loaners';
            updatedRHList.put(rh.Id, rh);
        }
        
        NewRequestHelpTriggerController.onBeforeUpdate(rhList, updatedRHList);
        
        Test.stopTest();
        
    }
    
}