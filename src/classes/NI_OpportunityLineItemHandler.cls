public class NI_OpportunityLineItemHandler {
    
    public static void updateLineItemList(List<OpportunityLineItem> oppLineItemList){
        string oppId=oppLineItemList[0].OpportunityId;
        string stripe='';
        List<OpportunityLineItem> newList =  [Select Id,OpportunityId,Product2Id from OpportunityLineItem where OpportunityId =:oppId];
        List<Opportunity> opp = new List<Opportunity>();
        
        opp=[select id, Product_Stripe_List__c from Opportunity where id= :oppId ];
        List<Product2> pbe = new List<Product2>();
        pbe = [Select Id,Name from Product2];
        Map<Id,string> productMap = new Map<Id, string>();
        Map<Id,string> addedProducts= new Map<Id,string>();
        
        //Complete Product list
        for(Product2 p:pbe){
            productMap.put(p.Id,p.Name);
        }
        
        for(OpportunityLineItem oli:newList ){
            string newProduct = productMap.get(oli.Product2Id);
            if(!(addedProducts.get(oli.Product2Id)!=null)){
                addedProducts.put(oli.Product2Id,newProduct);
                stripe+= productMap.get(oli.Product2Id)+ '\n';
            }
        }
        
        if(opp.size()>0){
            opp[0].Product_Stripe_List__c=stripe;
        }
        try {
            update opp;
        }catch(Exception e){
            System.debug('Error inserting a product!');
        }

    }
}