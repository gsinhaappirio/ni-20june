public class NI_DispatcherQuoteViewExtension {

    private ApexPages.StandardController sc;
    public String quoteId {get; set;}
    public boolean quoteFound {get; set;}
    
    public NI_DispatcherQuoteViewExtension (ApexPages.StandardController controller) {
        this.sc= controller;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('isSF1')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
               ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
               (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }    

    public PageReference redirectToExternalObject() {
        quoteFound = false;
        NI_QUOTE__c quote = [Select id, site__c, oracle_header_id__c From NI_Quote__c Where Id = :ApexPages.currentPage().getParameters().get('id')];        
        String siteHeaderId = quote.site__c + '-' + quote.oracle_header_id__c;
        
        NI_OD_QUOTE_HDR_ALL_V__x [] externalQuotes;
        
        if(Test.isRunningTest()){
            NI_OD_QUOTE_HDR_ALL_V__x externalTestQuote = new NI_OD_QUOTE_HDR_ALL_V__x();
            externalQuotes= new List<NI_OD_QUOTE_HDR_ALL_V__x>();
            externalQuotes.add(externalTestQuote );
        }
        else{
            externalQuotes = [select id, quote_name__c from NI_OD_QUOTE_HDR_ALL_V__x where externalid = :siteHeaderId ];
        }
        
        if(externalQuotes.size() > 0){
            PageReference quotePage = new ApexPages.StandardController(externalQuotes[0]).view();
            quoteId = externalQuotes[0].id;
            quoteFound = true;
            if (!isSF1){
              return quotePage.setRedirect(true);
            }
            else{
              return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This quote has not been found in our external system.'));
            return null;
        }
        
    }
}