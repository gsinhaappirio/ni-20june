@isTest
public class Ni_OneSearch_Contact_Extension_Test {
    @isTest(SeeAllData=true)
    public static void noParametersTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_contacts');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        System.assertNotEquals(controller.lead, null);
        System.assertEquals(controller.contact.firstname, null);
        System.assertEquals(controller.client, null);
        System.assertEquals(controller.returnUrl, null);
    }
    
    @isTest(SeeAllData=true)
    public static void constructorSearchTest(){
        Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Contact_Mock());
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?firstname=tyler&lastname=hobbs');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        Lead l = new Lead(firstname='Tyler',lastname='Hobbs',country='US',company='National Instruments');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        controller.performSearch();
        Test.stopTest();
        System.assertNotEquals(controller.editing, false);
    }
    
    
    @isTest(SeeAllData=true)
    public static void gatherFieldSetsTest(){
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        System.assert(controller.leftFields != null);
    }
    
    @isTest(SeeAllData=true)
    public static void createObjectTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?import=true');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        PageReference pr = controller.createObject();
        controller.addContact();
        Test.stopTest();
        
        System.assertEquals(controller.done, true);
    }
    
    @isTest(SeeAllData=true)
    public static void importObjectTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_contacts');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        PageReference pr = controller.importObject();
        
        System.assertNotEquals(pr, null);
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void importObjectParametersTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?firstname=tyler&lastname=hobbs');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        PageReference pr = controller.importObject();
        
        System.assertNotEquals(pr, null);
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void importObjectScreenpopParametersTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?first_name=tyler&last_name=hobbs&email_address=tyler.hobbs@ni.com&account_site_name=ni&city=austin&postal_code=78759&phone_country=1&phone_area=512&phone_number=6830000&phone_ext=123');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        PageReference pr = controller.importObject();
        
        System.assertNotEquals(pr, null);
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
        
    }
    
    @isTest(SeeAllData=true)
    public static void searchTest() {
        PageReference pageRef = new PageReference('/apex/onesearch_contacts');
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        PageReference pr = controller.search();
        
        Test.stopTest();
        
        System.assertNotEquals(pr, null);
    }
    
    @isTest(SeeAllData=true)
    public static void selectAccountTest() {
        
        Account account = new Account(name='Test', ShippingStreet='123 Fake St', ShippingCity='Austin', ShippingCountry='US');
        insert account;
        
        PageReference pageRef = new PageReference('/apex/onesearch_contacts?accountId=' + String.valueOf(account.Id));
        Test.setCurrentPage(pageRef);
        
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);
        
        Test.startTest();
        
        controller.contact.firstName = 'Tyler';
        controller.contact.lastName = 'Hobbs';
        controller.accountId = account.Id;
        PageReference pr = controller.selectAccount();
        controller.selectObject();
        controller.updateLead();
        
        Test.stopTest();
        
        System.assertNotEquals(contact.accountId, null);
    }
    
    /*
    @isTest static void searchTest() {
        // Perform some DML to insert test data
        Lead l = new Lead(firstname='First',lastname='Last',country='US',company='Test Inc');
        insert l;
        ApexPages.StandardController sc = new ApexPages.standardController(l);
        Ni_OneSearch_Contact_Extension controller = new Ni_OneSearch_Contact_Extension(sc);

        // Call Test.startTest before performing callout
        // but after setting test data.
        Test.startTest();

        // Set mock callout class 
        //Test.setMock(WebServiceMock.class, new Ni_Cmdm_Search_Mock());
        
        PageReference pageRef = new PageReference('/apex/onesearch_accounts?name=averna');
        Test.setCurrentPage(pageRef);
        System.assertNotEquals(controller.results, null);

        Test.stopTest();
    }
    */
}