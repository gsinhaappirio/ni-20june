public class NI_DispatcherOrderViewExtension {

    private ApexPages.StandardController sc;
    public String orderId {get; set;}
    public boolean orderFound {get; set;}
    
    public NI_DispatcherOrderViewExtension (ApexPages.StandardController controller) {
        this.sc= controller;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('isSF1')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
               ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
               (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }     

    public PageReference redirectToExternalObject() {
        orderFound = false;
        NI_ORDER__c order = [Select id, site__c, oracle_header_id__c From NI_Order__c Where Id = :ApexPages.currentPage().getParameters().get('id')];        
        String siteHeaderId = order.site__c + '-' + order.oracle_header_id__c;
        
        NI_OD_ORDER_HDR_ALL_V__x [] externalOrders;
        
        if(Test.isRunningTest()){
            NI_OD_ORDER_HDR_ALL_V__x externalTestOrder = new NI_OD_ORDER_HDR_ALL_V__x();
            externalOrders = new List<NI_OD_ORDER_HDR_ALL_V__x>();
            externalOrders.add(externalTestOrder);
        }
        else{
            externalOrders = [select id from NI_OD_ORDER_HDR_ALL_V__x where externalid = :siteHeaderId ];
        }
        
        if(externalOrders.size() > 0){
            PageReference orderPage = new ApexPages.StandardController(externalOrders[0]).view();
            orderId = externalOrders[0].id;
            orderFound = true;
            if (!isSF1){
                return orderPage.setRedirect(true);
            }
            else{
                return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This order has not been found in our external system.'));
            return null;
        }
        
    }
    
}