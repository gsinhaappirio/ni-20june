@isTest
public class Ni_Cmdm_Search_Fault_Test {

    @isTest
    public static void ApiFaultTest(){
        Ni_Cmdm_Search_Fault.ApiFault apiFault = new Ni_Cmdm_Search_Fault.ApiFault();
        apiFault.exceptionCode = 'Test';
        apiFault.exceptionMessage = 'Test';
    }
    
    @isTest
    public static void InvalidSObjectFaultTest(){
        Ni_Cmdm_Search_Fault.InvalidSObjectFault fault = new Ni_Cmdm_Search_Fault.InvalidSObjectFault();
    }
    
    @isTest
    public static void MalformedQueryFaultTest(){
        Ni_Cmdm_Search_Fault.MalformedQueryFault fault = new Ni_Cmdm_Search_Fault.MalformedQueryFault();
    }
    
    @isTest
    public static void InvalidIdFaultTest(){
        Ni_Cmdm_Search_Fault.InvalidIdFault fault = new Ni_Cmdm_Search_Fault.InvalidIdFault();
    }
    
    @isTest
    public static void MalformedSearchFaultTest(){
        Ni_Cmdm_Search_Fault.MalformedSearchFault fault = new Ni_Cmdm_Search_Fault.MalformedSearchFault();
    }
    
    @isTest
    public static void InvalidFieldFaultTest(){
        Ni_Cmdm_Search_Fault.InvalidFieldFault fault = new Ni_Cmdm_Search_Fault.InvalidFieldFault();
    }
    
    @isTest
    public static void ApiQueryFaultTest(){
        Ni_Cmdm_Search_Fault.ApiQueryFault fault = new Ni_Cmdm_Search_Fault.ApiQueryFault();
        fault.row = 1;
        fault.column = 1;
    }
    
    @isTest
    public static void UnexpectedErrorFaultTest(){
        Ni_Cmdm_Search_Fault.UnexpectedErrorFault fault = new Ni_Cmdm_Search_Fault.UnexpectedErrorFault();
    }
    
    @isTest
    public static void InvalidQueryLocatorFaultTest(){
        Ni_Cmdm_Search_Fault.InvalidQueryLocatorFault fault = new Ni_Cmdm_Search_Fault.InvalidQueryLocatorFault();
    }
    
    @isTest
    public static void LoginFaultTest(){
        Ni_Cmdm_Search_Fault.LoginFault fault = new Ni_Cmdm_Search_Fault.LoginFault();
    }
    
    @isTest
    public static void InvalidNewPasswordFaultTest(){
        Ni_Cmdm_Search_Fault.InvalidNewPasswordFault fault = new Ni_Cmdm_Search_Fault.InvalidNewPasswordFault();
    }
    
}