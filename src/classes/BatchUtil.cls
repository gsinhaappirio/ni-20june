global class BatchUtil implements Database.Batchable<sObject>, Database.Stateful {
    
    global final String query;
    global final BatchUtilable caller;
    private String runningLog;
    
    global interface BatchUtilable {
        boolean HandleRecord(sObject obj, BatchUtil bu);
    }
    
    global void log(String log) {
        runningLog += log;
    }
    
    global BatchUtil(String query, BatchUtilable caller) {
        this.query = query;
        this.caller = caller;
        RunningLog = 'Started BatchUtil call...\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(this.query);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> sObjects){
		List<sObject> toCommit = new List<Sobject>();
        
        for(sObject obj : sObjects) {
            if (caller.handleRecord(obj, this)) {
                toCommit.add(obj);
            }
        }
        
        if (toCommit.size() > 0){
            database.update(toCommit, false);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        System.debug('\n\n\n\n********************\nStarting BatchUtil Log:\n\n' + runningLog + '\n\nEnd BatchUtil Log\n********************\n');
    }
}