/*                                                                                        
* Version 1.00                                                                                                                                             
* Creation Date: 26.Sep.2013                   Anjani Kumar Rai          
* Last Modified:                                                                                           
*© 2008 - Accenture - All Rights Reserved 
*/


@isTest(SeeAllData=true)
private class AP01_Opportunity_TEST{

    @isTest(SeeAllData=true)
    static void testOpportunityExpectedRevenueUpdate(){
       // integer loopCounter;
        list<Opportunity> lsOppInserts = new list<Opportunity>();
        list<Opportunity> lsOppUpdates = new list<Opportunity>();
        list<Plan_Opportunity__c> lsPlanOppInserts= new list<Plan_Opportunity__c>();
        list<Plan_Opportunity__c> lsPlanOppUpdates= new list<Plan_Opportunity__c>();

        //create period
        Period__c periodRecordInsert = new Period__c(Name='Q1', Period_Start_Date__c=system.Today(),Period_End_Date__c=system.Today());
        insert periodRecordInsert;
        //create business plan
        Business_Plan__c bsPlanRecordInsert = new Business_Plan__c(Name='Business Plan',Period__c=periodRecordInsert.Id,Type__c='local');
        insert bsPlanRecordInsert;
        //create account
        Account accountRecordInsert = new Account(Name='Account',Shipping_Street_1__c='11500',Shipping_Street_2__c='N Mopac',ShippingCity='Austin',ShippingCountry='USA',ShippingState='TX',ShippingPostalCode='78759');
        insert accountRecordInsert ;
       
            
        // create account plan
        Plan_Account__c accPlanRecordInsert= new Plan_Account__c(Name='Account plan',Account__c=accountRecordInsert .Id,Business_Plan__c=bsPlanRecordInsert.Id);
        insert accPlanRecordInsert;
        // create strategy 
        Strategy__c strategyRecordInsert = new Strategy__c(Name='Strategy',Plan_Account__c=accPlanRecordInsert.Id);
        insert strategyRecordInsert ;   
             
        for(integer loopICounter=0;loopICounter<10;loopICounter++)
        {
        Opportunity newOppRecord = new Opportunity(Name='Opp'+loopICounter, StageName='Qualification', Amount=100,
                         Probability = 95, CloseDate=system.today());
        lsOppInserts.add(newOppRecord);
        }
        insert lsOppInserts ;
        //create Plan Opportunity
        for(integer loopJCounter=0;loopJCounter<10;loopJCounter++)
        {
        Plan_Opportunity__c PlanOpp = new Plan_Opportunity__c(Strategy__c=strategyRecordInsert.Id,Opportunity__c=lsOppInserts[loopJCounter].Id);
        lsPlanOppInserts.add(PlanOpp);
        }
        insert lsPlanOppInserts;
        List<Opportunity> lsOppor = [select Id,Name,StageName,Probability,ExpectedRevenue from Opportunity where name Like :'Opp%'];

        for(Opportunity oppNew: lsOppor ){
        oppNew.StageName = 'Closed Won';
        oppNew.Probability =100;
        lsOppUpdates.add(oppNew); 
        }
         update lsOppUpdates;
        list<Plan_Opportunity__c> planOppSelect = [Select p.Strategy__c, p.Opportunity__r.ExpectedRevenue, p.Opportunity__r.StageName, p.Opportunity__c, p.Opportunity_Stage__c, p.Name, p.Id, p.Expected_Revenue__c From Plan_Opportunity__c p where p.Strategy__c =: strategyRecordInsert.Id];
        for(Plan_Opportunity__c planOppNew: planOppSelect ){
        planOppNew.Opportunity_Stage__c = planOppNew.Opportunity__r.StageName;
        planOppNew.Expected_Revenue__c =planOppNew.Opportunity__r.ExpectedRevenue ;
        lsPlanOppUpdates.add(planOppNew); 
        }
         update lsPlanOppUpdates;
    }
}