public class Ni_Cmdm_Contact_Search_Result implements Comparable {

    public Double score{get;set;}
    public Contact contact{get;set;}
    public String sourceSystem{get;set;}
    public String sourceId{get;set;}
    
    public Ni_Cmdm_Contact_Search_Result(){}
    
    public Ni_Cmdm_Contact_Search_Result(Contact contact, Integer score, String sourceSystem){
        this.contact = contact;
        this.score = Double.valueOf(score)/10;
        this.sourceSystem = sourceSystem;
    }
    
    public Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        Ni_Cmdm_Contact_Search_Result other = (Ni_Cmdm_Contact_Search_Result)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (score > other.score) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (score < other.score) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        
        return returnValue;       
    }
    
}