public class NI_Sales_Quote_Extension {
    
    private final NI_Quote__c quote;
    private ApexPages.StandardController sc;
    public List<NI_Sales_Sales_QuoteLineDTO> quoteLineList = new List<NI_Sales_Sales_QuoteLineDTO>();
    public Boolean alreadyHasOpportunity {get;set;}
    public String pdf{get;set;}
    public String pdfUrl{get;set;}
    
    public NI_Sales_Quote_Extension(ApexPages.StandardController stdController){
        
        this.sc = stdController;
        this.quote = (NI_Quote__c)stdController.getRecord();
        
        if (this.quote.opportunity__r.id != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'There is an opportunity already linked to this quote. Any changes must be made on the quote form in Oracle.'));
            alreadyHasOpportunity = true;   
        }
        else{
            alreadyHasOpportunity = false;
        }
        
        Ni_Quote_Settings__c quoteSettings = Ni_Quote_Settings__c.getInstance(UserInfo.getUserID());
        Boolean renderPDF = quoteSettings.Render_PDF__c;
        Boolean calloutForQuoteLines = quoteSettings.Callout_for_Quote_Lines__c;
        
        if(renderPDF || Test.isRunningTest()){
            pdfUrl = quoteSettings.PDF_URL__c;
            pdfUrl += '?';
            pdfUrl += quoteSettings.Quote_Number_Parameter__c + '=' + quote.Quote_number__c;
            pdfUrl += '&' + quoteSettings.Header_ID_Parameter__c + '=' + quote.Oracle_Header_Id__c;
            pdf = downloadPdf(pdfUrl);
            //pdf = downloadPdf('http://sine.ni.com/apps/utf8/niwq.get_pdf_quote?p_quote_number=0F8A4172F53CC729&p_quote_header_id=6BA41FE66A94795B);
        }
        
    }
    
    public String downloadPdf(String url){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        return EncodingUtil.base64Encode(res.getBodyAsBlob());
    }
    
    public List<NI_Sales_Sales_QuoteLineDTO> getQuoteLineList(){
        NI_Sales_Sales_Quote_DAO dao = new NI_Sales_Sales_Quote_DAO();
        this.quoteLineList = dao.getQuoteLines(String.valueOf(this.quote.oracle_Header_Id__c));
        return this.quoteLineList;
    }

}