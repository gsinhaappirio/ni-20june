public class AssignNewLeadExtension {

    public String returnUrl {get;set;}
    public Boolean loading {get;set;}

    // this has to be an extension to show up on the leads tab page
    public AssignNewLeadExtension(ApexPages.StandardSetController controller) {
        loading = true;
        returnUrl = System.currentPageReference().getParameters().get('returnUrl');
    }
    
    public PageReference assignLeadFromQueue(){
        Id userId = UserInfo.getUserId();
        
        // get public groups this user is assigned to -- Group.DeveloperName = queue name
        List<GroupMember> groupMembers = [SELECT Group.Id, Group.Name, Group.Type, UserOrGroupId, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: userId and Group.Type = 'Regular'];
        List<String> queueNames = new List<String>();
        for(GroupMember groupMember : groupMembers){
            queueNames.add(groupMember.group.DeveloperName);
        }
        
        // query queues by queue name
        Map<Id, Group> queues = new Map<Id, Group>([SELECT Id, Name FROM Group WHERE developerName in :queueNames]);
        Set<Id> queueIds = queues.keySet();
        
        // query the user's info and skills
        User user = [Select Id, FirstName, LastName, profile.name, (Select Id, Name, Exclusive__c, Skill__c, Skill__r.Name, Skill__r.Id From User_Skills__r) From User where Id = :UserInfo.getUserId() LIMIT 1];
        List<User_Skill__c> skillSets = user.User_Skills__r;
        List<User_Skill__c> userSkillSetList = new List<User_Skill__c>();
        List<Skill__c> exclusiveSkills = new List<Skill__c>();
        Map<Id, Skill__c> userSkills = new Map<Id, Skill__c>();

        // check if any skills are exclusive
        if(skillSets != null && skillSets.size() > 0){
            exclusiveSkills = new List<Skill__c>();
            userSkillSetList = new List<User_Skill__c>();
            userSkills = new Map<Id, Skill__c>();
            for(User_Skill__c skillSet : skillSets){
                if(skillSet.Exclusive__c){
                    exclusiveSkills.add(skillSet.skill__r);
                    System.debug('***** exclusive skill: ' + skillSet.skill__r.name);
                }
                userSkillSetList.add(skillSet);
                userSkills.put(skillSet.Skill__r.Id, skillSet.Skill__r);
            }
        }
        
        List<Lead> leads;
        Lead leadToAssign;

        if(exclusiveSkills != null && !exclusiveSkills.isEmpty()){
            // only query for leads that have the exclusive skills
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c, RecordType.DeveloperName,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true)
                    from Lead
                    where ownerId in :queueIds
                    and status = 'Qualified'
                    and Ready_to_be_Assigned__c = true
                    and isConverted = false
                    and Id in
                        (Select Lead__c from Lead_Skill__c where Skill__c in :exclusiveSkills)
                    ORDER BY CreatedDate DESC];

        }else{
            // query leads but only get required skills for that lead
            leads = [SELECT Id, ownerid, Name, CreatedDate, Industry_Segment__c, cREQ_Date__c, RecordType.DeveloperName,
                    (SELECT Lead__c, Lead__r.Id, Skill__c, Skill__r.Id, Skill__r.Name, Required__c from Lead_Skills__r where Required__c = true)
                    from Lead
                    where ownerId in :queueIds
                    and status = 'Qualified'
                    and Ready_to_be_Assigned__c = true
                    and isConverted = false
                    ORDER BY CreatedDate DESC];

        }
        
        List<SortedLead> sortedLeads = new List<SortedLead>();
        
        for(Lead lead : leads){
            SortedLead sl = new SortedLead(lead);
            sortedLeads.add(sl);
        }

        sortedLeads.sort();
        
        for (SortedLead sl : sortedLeads){
            Lead lead = sl.lead;
            if ( lead.lead_skills__r!=null && !lead.lead_skills__r.isEmpty() ){
                // the lead has required skills. loop through to verify the user has a match
                // TODO - how can we get rid of this n^2 runtime?
                //for ( Integer i=0 ; i<lead.lead_skills__r.size() ; i++ )
                for ( Integer i=lead.lead_skills__r.size()-1 ; i>=0 ; i-- ){
                    Lead_Skill__c leadSkill = lead.lead_skills__r.get(i);
                    if( leadSkill.Required__c ){
                        if ( userSkills!=null && userSkills.get(leadSkill.skill__r.id)!=null ){
                            System.debug('+++++ has a required skill: ' + userSkills.get(leadSkill.skill__r.id).Name);
                            leadToAssign = lead;
                            if ( leadSkill.Skill__r.Name.equals('Academic') ){
                                continue;
                            }else{
                                break;
                            }
                        }else{
                            System.debug('+++++ user does not have required skill: ' + leadSkill.Skill__r.Name);
                            leadToAssign = null;
                            break;
                        }
                    }

                    //if ( i == lead.lead_skills__r.size()-1 )
                    if ( i == 0 ){
                        leadToAssign = lead;
                        break;
                    }

                }
            }else{
                leadToAssign = lead;
            }

            if(leadToAssign != null){
                break;
            }
        }
        
        if(leadToAssign != null){
            leadToAssign.ownerId = userId;
            leadToAssign.Status = 'Assigned';
            update leadToAssign;
            PageReference pr = new PageReference('/' + leadToAssign.Id);
            return pr;
        }
        
        loading = false;
        return null;

    }
    
    public class SortedLead implements Comparable {
        public Lead lead{get;set;}
        
        public SortedLead(Lead lead){
            this.lead = lead;
        }
        
        public Integer compareTo(Object compareTo) {
            SortedLead other = (SortedLead)compareTo;
            
            // this.lead is the one that is getting ordered.
            // "other" is the lead that it is being compared to
            // -1 pushes lead to the top (first), 1 pushes it down (last)
            
            if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c == null){
                // creq, other is not
                return -1;
            }else if(lead.cREQ_Date__c == null && other.lead.cREQ_Date__c != null){
                // not creq, other is
                return 1;
            }else if(lead.cREQ_Date__c != null && other.lead.cREQ_Date__c != null){
                // they're both cReqs, this should stay FIFO for cReqs
                if(lead.createdDate > other.lead.createdDate){
                    return 1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return -1;
                }
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') &&
                    !other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // sgl, other is not
                return -1;
            }else if(!lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // not sgl, other is
                return 1;
            }else if(lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead') && other.lead.RecordType.DeveloperName.toLowerCase().equals('sales_generated_lead')){
                // they're both sgl's, this should stay LIFO
                if(lead.createdDate > other.lead.createdDate){
                    return -1;
                }else if(lead.createdDate < other.lead.createdDate){
                    return 1;
                }
            }else if(lead.createdDate > other.lead.createdDate){
                return -1;
            }else if(lead.createdDate < other.lead.createdDate){
                return 1;
            }
            
            return 0;
            
        }
        
    }

}