@isTest
public class NI_Sales_Sales_Order_DAO_Test {
    @isTest
    public static void mapOrderLineToDTOTest() {
        NI_Sales_OrderWS_Order.OrderLine orderLine = new NI_Sales_OrderWS_Order.OrderLine();
        NI_Sales_Sales_Order_DAO dao = new NI_Sales_Sales_Order_DAO();
        NI_Sales_Sales_OrderLineDTO orderLineDTO = new NI_Sales_Sales_OrderLineDTO();
        
        String partNumber = 'partNumber';
        String partDescription = 'partDescription';
        Decimal orderedQuantity = 1;
        Decimal unitListPrice = 2;
        Decimal unitSellingPrice = 3;
        
        orderLine.partNumber = partNumber;
        orderLine.partDescription = partDescription;
        orderLine.orderedQuantity = orderedQuantity;
        orderLine.unitListPrice = unitListPrice;
        orderLine.unitSellingPrice = unitSellingPrice;
        
        orderLineDTO = dao.mapOrderLineToDTO(orderLine);
        
        system.assertEquals(orderLineDTO.partNumber, partNumber);
        system.assertEquals(orderLineDTO.partDescription, partDescription);
        system.assertEquals(orderLineDTO.orderedQuantity, orderedQuantity);
        system.assertEquals(orderLineDTO.unitListPrice, unitListPrice);
        system.assertEquals(orderLineDTO.unitSellingPrice, unitSellingPrice);
        system.assertEquals(orderLineDTO.lineAdjustedPercent, Math.round((unitListPrice - unitSellingPrice) / (unitListPrice / 100)));
        system.assertEquals(orderLineDTO.lineTotalPrice, unitSellingPrice * orderedQuantity);
    }
    
    @isTest(seeAllData=true)
    public static void getOrderLinesTest() {
        Test.setMock(WebServiceMock.class, new NI_Sales_OrderWS_OrderRequest_MockImpl());
        NI_Sales_Sales_Order_DAO dao = new NI_Sales_Sales_Order_DAO();

        List<NI_Sales_Sales_OrderLineDTO> response = dao.getOrderLines('orderId');
        
        System.assertEquals(response.get(0).partNumber, 'partNumber1');
        System.assertEquals(response.get(0).partDescription, 'partDescription1');
        System.assertEquals(response.get(0).orderedQuantity, 1);
        System.assertEquals(response.get(0).unitListPrice, 2);
        System.assertEquals(response.get(0).unitSellingPrice, 3);
        
        System.assertEquals(response.get(1).partNumber, 'partNumber2');
        System.assertEquals(response.get(1).partDescription, 'partDescription2');
        System.assertEquals(response.get(1).orderedQuantity, 4);
        System.assertEquals(response.get(1).unitListPrice, 5);
        System.assertEquals(response.get(1).unitSellingPrice, 6);
    }
}