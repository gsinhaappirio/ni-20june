/* Class Name   : NI_AuthenticatedFeedbackControllerTest
 * Description  : NI_AuthenticatedFeedbackController test class to handle all the unit test cases for the controller 
 * Created By   : Meghana Agarwal
 * Created On   : 16-06-2017

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Meghana Agarwal        16-06-2017                1                   Initial version
 * 
*/

@isTest
private class NI_AuthenticatedFeedbackControllerTest {
    //Variables
    private static List<String> summaryPicklistValues;
    private static List<String> areaPicklistValues;
    
    //Test Method
    private static testMethod void testPicklistValues(){ 
        test.startTest();
        Map<String, List<String>> picklistMap = NI_AuthenticatedFeedbackController.getPicklist();
        test.stopTest();
        System.assert(picklistMap.get('summary') != null && picklistMap.get('areaOfExpertise') != null);
        System.assert(picklistMap.get('summary').size()>0 && picklistMap.get('areaOfExpertise').size()>0);
    }
    
    //Test Method
    private static testMethod void testInsertFeedback(){
        List<Knowledge__kav> articleList = NI_TestUtils.createArticles(5, 'Test-Article', true);
        test.startTest();
        Knowledge__kav art = articleList[0];
        NI_AuthenticatedFeedbackController.insertArticleFeedback(art.Id,'','','OK');
        test.stopTest();
    }
}