@isTest(seeAllData = true)
public class NI_OpportunityLineItem_Trigger_Test
{
    static testMethod void testOpportunityLineItem_Trigger()
    {
        test.starttest();
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        Opportunity opp = new Opportunity(Name='TestOpp1', AccountId=account1.Id, StageName='Qualify', CloseDate=System.today());
        Map<Id,string> productMap = new Map<Id, string>();
        List<Product2> pbe = [Select Id,Name from Product2];
        for(Product2 p:pbe){
            productMap.put(p.Id,p.Name);
        }
        
        Product2 tempProd = new Product2(Name='Temp Prod1',Family='IP Communications',IsActive=true,ProductCode='77-77');
        insert tempProd;
        Id pricebookId = Test.getStandardPricebookId();
                           
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = tempProd.Id,UnitPrice = 100, IsActive = true);   
        insert standardPrice;
        insert opp; 
        
        
        OpportunityLineItem oli=new OpportunityLineItem(OpportunityId=opp.id,PricebookEntryId=standardPrice.id,Quantity=1,TotalPrice=500);
        insert oli;
        delete oli;
        
        
         test.stoptest();
    }
}