@isTest
public class ReleaseNotes_Test {

    @isTest
    public static void noParametersTest(){
        
        Test.startTest();
        List<Release_Note__c> releaseNotes = new List<Release_Note__c>();
        for(Integer i = 0; i < 10; i++){
            Release_Note__c rn = new Release_Note__c();
            rn.Summary__c = 'This is the summary';
            rn.Description__c = 'This is the description';
            rn.Jira_ID__c = 'STEPDEV-999999';
            rn.Release_Date__c = Date.today();
            rn.Rank__c = i;
            releaseNotes.add(rn);
        }
        
        insert releaseNotes;
        
        ReleaseNotes controller = new ReleaseNotes();
        
        Test.stopTest();
        
    }
    
}