public class NI_WebOrder_Extension{

    private ApexPages.StandardController sc;
    private final NI_OD_ORDER_HDR_ALL_V__x order;
    
    public NI_WebOrder_Extension (ApexPages.StandardController controller) {
        this.sc = controller;
        this.order= (NI_OD_ORDER_HDR_ALL_V__x)sc.getRecord();
    }

    public PageReference redirectToWebOrderPage() {
            
        String oracleHeaderId = '' + order.oracle_header_id__c;
        
        List<NI_OD_ORDER_ACC_ALL_V__x> accounts = new List<NI_OD_ORDER_ACC_ALL_V__x>();
        if(!Test.isRunningTest()){
            accounts = [select id, SFDC_ACCOUNT_ID__r.ShippingPostalCode from NI_OD_ORDER_ACC_ALL_V__x where ORACLE_HEADER_ID__c = :oracleHeaderId];
        }
        
        String url;
        
        if(Test.isRunningTest()){
            this.order.PO_NUMBER__c = '12345';
            
            Account account = new Account();
            account.shippingStreet = '123 Fake St';
            account.ShippingCity = 'Austin';
            account.ShippingCountry = 'US';
            account.ShippingPostalCode = '54321';
            account.Name = 'Fake Account';
            insert account;
            
            NI_OD_ORDER_ACC_ALL_V__x acc = new NI_OD_ORDER_ACC_ALL_V__x();
            acc.SFDC_ACCOUNT_ID__c = account.Id;
            accounts.add(acc);
        }

        if(accounts.size() > 0){
        
            NI_Server_URL_List__c urls = NI_Server_URL_List__c.getInstance('SineURL');
            url = urls.Server_URL__c + '/apps/utf8/niors.main?action=VALIDATE&p_option1=s&p_po_num=';
            url += order.PO_NUMBER__c;
            url += '&p_data1=' + accounts[0].SFDC_ACCOUNT_ID__r.ShippingPostalCode;
     
            
        }
        else{
            url = 'http://www.ni.com/status';
        }
        
        System.debug('****'+url);
        
        PageReference pageRef = new PageReference(url);
        return pageRef.setRedirect(true);

    }

}