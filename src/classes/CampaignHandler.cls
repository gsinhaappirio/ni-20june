public class CampaignHandler {

    public static void addNewCampaignMemberStatuses(List<Campaign> campaigns){
        
        
        List<Campaign_Member_Status__c> campaignMemberStatusList = Campaign_Member_Status__c.getall().values();
        List<Id> campaignIds = new List<Id>();
        String defaultStatusName;
        
        
        List<CampaignMemberStatus> toInsert = new List<CampaignMemberStatus>();
        
        for(Campaign campaign : campaigns){
            campaignIds.add(campaign.Id);
            for(Campaign_Member_Status__c status : campaignMemberStatusList){
                
                // set the default status name, if necessary
                if(defaultStatusName == null && status.isDefault__c){
                    defaultStatusName = status.Name;
                }
                
                System.debug('***** status: ' + status.Name + ' ... ' + status.isDefault__c);
                CampaignMemberStatus cmstatus = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = status.hasResponded__c, 
                                                                         Label = status.Name, SortOrder = status.Sort_Order__c.intValue(), 
                                                                         isDefault = status.isDefault__c);
                toInsert.add(cmstatus);
            }
        }
        
        Database.insert(toInsert, false);
        
        List<CampaignMemberStatus> toUpdate = new List<CampaignMemberStatus>();
        
        List<CampaignMemberStatus> statuses = [Select Id, Label, CampaignId from CampaignMemberStatus where Label = :defaultStatusName and CampaignId in :campaignIds];
        for(CampaignMemberStatus status : statuses){
            status.isDefault = true;
            toUpdate.add(status);
        }
        
        update toUpdate;
        
    }
    
}