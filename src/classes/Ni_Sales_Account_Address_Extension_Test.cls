@isTest
public with sharing class Ni_Sales_Account_Address_Extension_Test {
    
    @isTest
    public static void constructorTest(){
        PageReference pageRef = new PageReference('/apex/NI_Sales_Account_Address');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        Ni_Sales_Account_Address_Extension extension = new Ni_Sales_Account_Address_Extension(sc);
        
        Test.stopTest();
        
        System.assertEquals(extension.editing, false);
        System.assertNotEquals(extension.controller, null);
    }
    
    @isTest
    public static void editTest(){
        PageReference pageRef = new PageReference('/apex/NI_Sales_Account_Address');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        Ni_Sales_Account_Address_Extension extension = new Ni_Sales_Account_Address_Extension(sc);
        extension.edit();
        Test.stopTest();
        
        System.assertEquals(extension.editing, true);
    }
    
    @isTest
    public static void saveTest(){
        PageReference pageRef = new PageReference('/apex/NI_Sales_Account_Address');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        Ni_Sales_Account_Address_Extension extension = new Ni_Sales_Account_Address_Extension(sc);
        extension.save();
        Test.stopTest();
        
        System.assertEquals(extension.editing, false);
    }
    
    @isTest
    public static void cancelTest(){
        PageReference pageRef = new PageReference('/apex/NI_Sales_Account_Address');
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        Account a = new Account();
        a.name = 'Test Account';
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        Ni_Sales_Account_Address_Extension extension = new Ni_Sales_Account_Address_Extension(sc);
        extension.cancel();
        Test.stopTest();
        
        System.assertEquals(extension.editing, false);
    }
    
}