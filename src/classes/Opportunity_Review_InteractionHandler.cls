/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:          Opportunity Review/Interaction Plan
   Description:     This class contains support actions for triggers on Opportunity_Review_Interaction_Plan__c
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class  Opportunity_Review_InteractionHandler {
    
       static final string TRGRNAME = 'OpportunityReviewInteraction_Trigger';
       static final string OPPREVIEW = 'Opportunity Review';
       static final string CLASSNAME = 'Opportunity_Review_InteractionHandler';
       static final string METHODNAME = 'OppLastActiveDate';
          public static final string WON = system.label.won;
     public static final string LOST = system.label.lost;
     public static final string ERRMSG = system.label.Opportunity_Errormsg ;
        
    /**********************************************************************************************************************
    * Author: 
    * Date:
    * Param: List<Opportunity_Review_Interaction_Plan__c> (Trigger.New)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Applicable
    * Trigger Events : After Insert
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/
    
    public static void oppLastActiveDate(List<Opportunity_Review_Interaction_Plan__c> newOpportunity_Review_Interaction_PlantList){ 
        Set<Id> oppsIds = new Set<Id>();
        List<Opportunity> lstopp = new List<Opportunity>();
        List<Database.SaveResult> dbSaveList = new List<Database.SaveResult> ();
        string error1= System.label.Update_Failed;
        string app_NameLabel = System.label.App_Name;
        
        try{    
            if(trigger.isInsert)
            {
                for(Opportunity_Review_Interaction_Plan__c Oppriview: newOpportunity_Review_Interaction_PlantList)
                {
                     if(Oppriview.Opportunity__c != null ) 
                    {
                    oppsIds.add(Oppriview.Opportunity__c);  
                        
                    }
               }
               lstopp=[select Last_Activity_Date_Plain__c,Stagename,Close_Reason__c,Target_Application_Value__c from  Opportunity where id IN : oppsIds];
               for(Opportunity_Review_Interaction_Plan__c Oppriview: newOpportunity_Review_Interaction_PlantList){               
                   for(opportunity opp : lstopp)
                   {
                       /*------- Check if the opportunity record meets the desired validation rule -------- */
                      if(((opp.StageName==WON)||(opp.StageName==Lost))&&((opp.Close_Reason__c==null)||(opp.Target_Application_Value__c==null))){
                          Oppriview.addError(ERRMSG );
                      }
                      else{
                          opp.Last_Activity_Date_Plain__c= date.today();
                      }
                   }
                
               }
                dbSaveList = database.update(lstopp,false);
            }
        }catch(Exception e){
            
            system.LoggingLevel newloglevel = LoggingLevel.ERROR;
                
                if(e.getMessage().contains(error1) )
                {                

                    UTIL_LoggingService.logDmlResults(dbSaveList,null,lstopp,app_NameLabel, 
                                                                     CLASSNAME,METHODNAME,TRGRNAME,newloglevel);
                }
        }
    } 
}