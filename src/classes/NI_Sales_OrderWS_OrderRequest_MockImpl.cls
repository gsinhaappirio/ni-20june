@isTest 
public with sharing class NI_Sales_OrderWS_OrderRequest_MockImpl implements WebServiceMock{

    public void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
     
           NI_Sales_OrderWS_Order.GetOrderResponse_element respElement = 
               new NI_Sales_OrderWS_Order.GetOrderResponse_element();
               
           respElement.responseStatus = 'S';
           respElement.OrderInformation = new NI_Sales_OrderWS_Order.OrderInformation();
           respElement.OrderInformation.OrderHeader = new NI_Sales_OrderWS_Order.OrderHeader();
           respElement.OrderInformation.OrderHeader.headerId = 123;
           
           
           respElement.OrderInformation.orderLines = new NI_Sales_OrderWS_Order.OrderLines(); 
           respElement.OrderInformation.orderLines.orderLine = new NI_Sales_OrderWS_Order.OrderLine[2];
           respElement.OrderInformation.orderLines.orderLine[0] = new NI_Sales_OrderWS_Order.OrderLine();
           respElement.OrderInformation.orderLines.orderLine[0].partNumber = 'partNumber1';
           respElement.OrderInformation.orderLines.orderLine[0].partDescription = 'partDescription1';
           respElement.OrderInformation.orderLines.orderLine[0].orderedQuantity = 1;
           respElement.OrderInformation.orderLines.orderLine[0].unitListPrice = 2;
           respElement.OrderInformation.orderLines.orderLine[0].unitSellingPrice = 3;
           
           
           respElement.OrderInformation.orderLines.orderLine[1] = new NI_Sales_OrderWS_Order.OrderLine();
           respElement.OrderInformation.orderLines.orderLine[1].partNumber = 'partNumber2';
           respElement.OrderInformation.orderLines.orderLine[1].partDescription = 'partDescription2';
           respElement.OrderInformation.orderLines.orderLine[1].orderedQuantity = 4;
           respElement.OrderInformation.orderLines.orderLine[1].unitListPrice = 5;
           respElement.OrderInformation.orderLines.orderLine[1].unitSellingPrice = 6;
                       
           response.put('response_x', respElement); 
           
    
    }
}