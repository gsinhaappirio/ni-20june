/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class NI_Quote_Controller_Test {

    @isTest(seeAllData=true)
    static void validateSave(){
            
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        System.debug('after inserting opportunity ' + opp.id);
        
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        System.debug('after inserting quote ' + quote.opportunity__r.id);
        
        Test.startTest();
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Opportunity__c = opp.id;
        
        PageReference detailPage;
        
        detailPage = quoteController.save();
        Test.stopTest();
        
        System.assertNotEquals(null, detailPage);
        
       
    }
    
    @isTest(seeAllData=true)
    static void validateSave2(){
            
        Lead ld = new Lead();
        ld.FirstName = 'Lead';
        ld.LastName = 'Test';
        ld.company = 'NI';
        ld.LeadSource = 'Marketing';
        ld.Country = 'US';
        insert ld;
        
        System.debug('after inserting lead ' + ld.id);
        
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        System.debug('after inserting quote ' + quote.lead__r.id);
        
        Test.startTest();
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Lead_Quote_WS_MockImpl());
        
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Lead__c = ld.id;
        
        PageReference detailPage;
        
        detailPage = quoteController.save();
        Test.stopTest();
        
        System.assertNotEquals(null, detailPage);
        
       
    }    
    
    @isTest(seeAllData=true)
    static void validateHasOpportunity(){
            
        System.debug('Begin validateHasOpportunity');
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        System.debug('after inserting opportunity ' + opp.id);
        
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        quote.Opportunity__c = opp.id;
        update quote;
        
        System.debug('after inserting quote ' + quote.opportunity__r.id);
      
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
       
        System.debug('End validateHasOpportunity');
        System.assertEquals(true, quoteController.alreadyHasOpportunityOrLead);
        
        
       
    }      
    
   @isTest
    static void validateSaveNullPageWithNullOpp(){
            
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Opportunity__c = null;
        
        PageReference detailPage;
        
        detailPage = quoteController.save(); 
        
        System.assertEquals(null, detailPage);
        
       
    }       
    
    @isTest
    static void validateSaveNullPage(){
            
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        Opportunity opp = new Opportunity();
        opp.name = 'Test Opp for Quote Controller';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        quoteController.quote.Opportunity__c = opp.id;
        
        PageReference detailPage;
        
        detailPage = quoteController.save(); 
        
        System.assertEquals(null, detailPage);
        
       
    }       
    
    @isTest
    static void validateCancel(){
            
        NI_Quote__c quote = new NI_Quote__c();
        quote.site__c = 'NIC';
        quote.org_Id__c = 1;
        quote.oracle_Header_Id__c = 123;
        insert quote;
        
        PageReference pageRef = Page.NI_Sales_Sales_Quote_Detail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',quote.id);
        
        Test.setMock(WebServiceMock.class, new NI_Sales_Opp_Quote_WS_MockImpl());
        NI_Quote_Controller quoteController = new NI_Quote_Controller(new ApexPages.StandardController(quote));
        
        PageReference detailPage;
        
        detailPage = quoteController.cancel(); 
        
        System.assertNotEquals(null, detailPage);
        
    }      
}