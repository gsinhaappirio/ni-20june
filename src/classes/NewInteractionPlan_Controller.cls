public class NewInteractionPlan_Controller {

    private Id fromId {get;set;}
    private Id recordTypeId {get;set;}
    
    public NewInteractionPlan_Controller() {
        
        String fromIdParameter = System.currentPageReference().getParameters().get('fromId');
        
        if(fromIdParameter != null){
            fromId = (Id) fromIdParameter;
            RecordType recordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Interaction_Plan'];
            if(recordType != null){
                recordTypeId = recordType.Id;
            }
        }
        
    }
    
    public PageReference forwardToTaskCreation(){
        String url = '/a13/e';
        PageReference pr = new PageReference(url);
        
        if(fromId.getSObjectType() == Schema.Lead.SObjectType){
            // use a list to prevent throwing a query exception if record isnt found
            List<Lead> leads = [Select Id, Name from Lead where Id = :fromId];
            if(leads != null && !leads.isEmpty()){
                Lead lead = leads.get(0);
                pr.getParameters().put('CF00Ni000000FoVz1', lead.Name);
                pr.getParameters().put('CF00Ni000000FoVz1_lkid', lead.Id);
                
            }
        }else if(fromId.getSObjectType() == Schema.Opportunity.SObjectType){
            List<Opportunity> opps = [Select Id, Name from Opportunity where Id = :fromId];
            if(opps != null && !opps.isEmpty()){
                Opportunity opp = opps.get(0);
                pr.getParameters().put('CF00Ni000000FoVz7', opp.Name);
                pr.getParameters().put('CF00Ni000000FoVz7_lkid', opp.Id);
                
            }
        }
        
        pr.getParameters().put('retURL', fromId);
        pr.getParameters().put('RecordType', recordTypeId);
        
        return pr;
    }
    
}