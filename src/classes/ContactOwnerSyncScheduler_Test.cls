@isTest(SeeAllData=true)
private class ContactOwnerSyncScheduler_Test {

    @isTest(Seealldata=true)
    public static void test() {
        Test.startTest();
        
        User user1 = TestUtils.getUserForProfile('Last1', 'NI Sales User');
        insert user1;
        User user2 = TestUtils.getUserForProfile('Last2', 'NI Sales User');
        insert user2;
        Account account = TestUtils.getAccount('accountname');
        account.OwnerId = user1.Id;
        insert account;
        Contact contact = TestUtils.getContact('lastname', account.Id);
        contact.OwnerId = user2.Id;
        insert contact;
        
        // Schedule the test job
        String jobId = System.schedule('testContactOwnerSyncScheduledApex', ContactOwnerSyncScheduler.CRON_EXP, new ContactOwnerSyncScheduler());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(LeadAutoCloseScheduler.CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime)); 
    }
}