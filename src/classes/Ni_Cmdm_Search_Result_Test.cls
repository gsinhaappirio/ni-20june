@isTest
public class Ni_Cmdm_Search_Result_Test {

    @isTest
    public static void data_elementTest(){
        Ni_Cmdm_Search_Result.data_element de = new Ni_Cmdm_Search_Result.data_element();
        de.sObject_x = new Ni_Cmdm_Search_Enterprise_Object.sObject_x();
        de.account = new Ni_Cmdm_Search_Enterprise_Object.Account();
        de.contact = new Ni_Cmdm_Search_Enterprise_Object.Contact();
    }
    
    @isTest
    public static void ContactSearchResultTypeTest(){
        Ni_Cmdm_Search_Result.ContactSearchResultType de = new Ni_Cmdm_Search_Result.ContactSearchResultType();
        de.filters = new Ni_Cmdm_Search_Result.filterType();
        de.entryList = new Ni_Cmdm_Search_Result.entryList_element();
    }
    
    @isTest
    public static void AccountSearchResultTypeTest(){
        Ni_Cmdm_Search_Result.AccountSearchResultType de = new Ni_Cmdm_Search_Result.AccountSearchResultType();
        de.filters = new Ni_Cmdm_Search_Result.filterType();
        de.entryList = new Ni_Cmdm_Search_Result.entryList_element();
    }
    
    @isTest
    public static void entryList_elementTest(){
        Ni_Cmdm_Search_Result.entryList_element de = new Ni_Cmdm_Search_Result.entryList_element();
        de.entry = new List<Ni_Cmdm_Search_Result.entry_element>();
    }
    
    @isTest
    public static void sourceRef_elementTest(){
        Ni_Cmdm_Search_Result.sourceRef_element sr = new Ni_Cmdm_Search_Result.sourceRef_element();
        sr.accountID = 'accountId';
        sr.memIdnum = 'memIdNum';
        sr.sourceSystem = 'sourceSystem';
        sr.tcaOrgContactId = 'orgContactId';
        sr.tcaPersonId = 'personId';
        sr.upAddressId = 'upAddressId';
    }
    
    @isTest
    public static void entry_elementTest(){
        Ni_Cmdm_Search_Result.entry_element element = new Ni_Cmdm_Search_Result.entry_element();
        element.data = new Ni_Cmdm_Search_Result.data_element();
        element.sourceRef = new Ni_Cmdm_Search_Result.sourceRef_element();
        element.score = 10;
    }
    
    @isTest
    public static void filterTypeTest(){
        Ni_Cmdm_Search_Result.filterType filterType = new Ni_Cmdm_Search_Result.filterType();
        filterType.system_x = 'system';
        filterType.maxRows = 10;
        filterType.sforceIDsOnly = false;
    }
    
}