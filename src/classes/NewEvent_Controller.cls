public class NewEvent_Controller {

    private Id fromId {get;set;}
    
    public NewEvent_Controller() {
        
        String fromIdParameter = System.currentPageReference().getParameters().get('fromId');
        
        if(fromIdParameter != null){
            fromId = (Id) fromIdParameter;
        }
        
    }
    
    public PageReference forwardToTaskCreation(){
        String url = '/00U/e';
        PageReference pr = new PageReference(url);
        
        if(fromId.getSObjectType() == Schema.Multi_Phase_Opportunity__c.SObjectType)
        {
           pr.getParameters().put('what_id', fromId);
        }else if(fromId.getSObjectType() == Schema.Account.SObjectType){
            pr.getParameters().put('what_id', fromId);
            pr.getParameters().put('title', 'Call');
            pr.getParameters().put('00Ne0000000jsO1', 'Interaction');
        }else if(fromId.getSObjectType() == Schema.Opportunity.SObjectType)
        {
            pr.getParameters().put('what_id', fromId);
            // save query result to a list instead of an object to prevent query zero exception
            List<OpportunityContactRole> ocrList = [Select ContactId from OpportunityContactRole where OpportunityId = :fromId and isPrimary = true LIMIT 1];
            if(ocrList != null && !ocrList.isEmpty()){
                pr.getParameters().put('who_id', ocrList.get(0).ContactId);
            }
        }else if(fromId.getSObjectType() == Schema.Lead.SObjectType || 
                 fromId.getSObjectType() == Schema.Contact.SObjectType)
        {
           pr.getParameters().put('who_id', fromId);
        }
        
        pr.getParameters().put('followup', '1');
        pr.getParameters().put('retURL', fromId);
        
        return pr;
    }
    
}