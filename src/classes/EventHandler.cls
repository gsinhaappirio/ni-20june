/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            EventHandler
   Description:     This class contains support actions for triggers on Event 
                                         
   Date                            Version            Author                                    Summary of Changes 
   ---------------------         ---------------     -----------------------                  ----------------------------------------------------------------------------------------
   25th July 2014           1.0                   OffshoreTeam_Accenture            Initial Release 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class EventHandler {
    
   public final static  string TRGRNAME='Event_Trigger';
   public final static string APPNAMELBL = system.label.App_Name;
   final static string CLASSNAME = 'EventHandler';
   final static string METHODNAME = 'OppLastActiveDate';
   public static final string WON = system.label.won;
     public static final string LOST = system.label.lost;
     public static final string ERRMSG = system.label.Opportunity_Errormsg ;
   
   /**********************************************************************************************************************
    * Author: 
    * Date: 
    * Param: List<Event> (Trigger.New)
    * Return: void
    * Description: 
    ***********************************************************************************************************************                          
    * Applicable RecordTypes : All
    * Dataload Bypass Check : Not Applicable
    * Trigger Events : After Insert
    * Summary of Changes :  - Initial version
    **********************************************************************************************************************/
    public static void OppLastActiveDate(List<Event> newEventList)
    {
        string updateError  = System.label.Update_Failed;
        List<Database.SaveResult> dbSaveList = new List<Database.SaveResult> ();
       
        List<Id> Oppidlist = new list<Id>();
        List<opportunity> opplist = new list<opportunity>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        try{
            for (Event record:newEventList) {
                if(record.whatid != null && record.whatid.getsobjecttype() == opportunity.sobjecttype) {
                    Oppidlist.add(record.whatid);
                }
            }
            opplist =[select Last_Activity_Date_Plain__c,Stagename,Close_Reason__c,Target_Application_Value__c from  Opportunity where id IN : Oppidlist];
            for (Event record:newEventList) {
                for(opportunity oppObj : opplist ){
                    /*------- Check if the opportunity record meets the desired validation rule -------- */
                    if(((oppObj.StageName==WON)||(oppObj.StageName==Lost))&&((oppObj.Close_Reason__c==null)||(oppObj.Target_Application_Value__c==null))){
                        record.addError(ERRMSG );
                    }
                    else{
                        oppObj.Last_Activity_Date_Plain__c=Date.TODAY();
                        oppsToUpdate.add(oppObj);
                    }
                }
           }
             
           dbSaveList = database.update(oppsToUpdate,false);
           }catch(Exception e){system.LoggingLevel NEWLOGLEVEL = LoggingLevel.ERROR;if(e.getMessage().contains(updateError) ){
                UTIL_LoggingService.logDmlResults(dbSaveList,null,oppsToUpdate,APPNAMELBL,CLASSNAME,METHODNAME,TRGRNAME,NEWLOGLEVEL);
            } 
        }  
    }        
}