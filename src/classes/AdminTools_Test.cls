@isTest(SeeAllData=true)
public class AdminTools_Test {

    @isTest(seeAllData=true)
    public static void initTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        AdminTools controller = new AdminTools();
        
        List<SelectOption> mySelectOptions = controller.statusOptions;
        String allNewPassword = controller.allNewPassword;
        String allNewStatus= controller.allNewStatus;
        
        controller.updateEachUser();

        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void updateLicenseCostTest() {
        Test.startTest();
        AdminTools controller = new AdminTools();
        controller.updateLicenseCosts();
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    public static void updateAllUsers() {
        Test.startTest();
        AdminTools controller = new AdminTools();
        controller.allNewStatus = 'ACTIVE';
        controller.updateAllUsers();
        Test.stopTest();
    }    

    @isTest(seeAllData=true)
    public static void validatePasswordTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();

        AdminTools controller = new AdminTools();
        // unacceptable passwords
        System.assertEquals(controller.isValidPassword('abcdefg'), false);
        System.assertEquals(controller.isValidPassword('ABCde'), false);
        System.assertEquals(controller.isValidPassword('ab12345'), false);
        System.assertEquals(controller.isValidPassword('Ab2'), false);

        // acceptable password
        System.assertEquals(controller.isValidPassword('Abcdefg123'), true);

        Test.stopTest();
    }

    @isTest(seeAllData=true)
    public static void resetPasswordNotAcceptedTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        AdminTools controller = new AdminTools();
        controller.newPassword = 'abc123';
        controller.resetPassword();

        System.assertEquals(controller.active, 0);

        Test.stopTest();
    }

    @isTest(seeAllData=true)
    public static void resetPasswordAcceptedTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.isTestUser__c = true;
        insert u;

        AdminTools controller = new AdminTools();
        controller.newPassword = 'Abcd123!';
        controller.resetPassword();

        Test.stopTest();
    }

    @isTest(seeAllData=true)
    public static void activateAllTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.IsActive = true;
        u.isTestUser__c = true;
        insert u;

        AdminTools controller = new AdminTools();
        controller.activateAll();

        System.assertEquals(u.IsActive, true);

        Test.stopTest();
    }

    @isTest(seeAllData=true)
    public static void deactivateAllTest() {
        PageReference pageRef = new PageReference('/apex/AdminTools');
        Test.setCurrentPage(pageRef);

        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@ni.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@ni.com');
        u.IsActive = false;
        u.isTestUser__c = true;
        insert u;

        AdminTools controller = new AdminTools();
        controller.deactivateAll();

        System.assertEquals(u.IsActive, false);

        Test.stopTest();
    }

}