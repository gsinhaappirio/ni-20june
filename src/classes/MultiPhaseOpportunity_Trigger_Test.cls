@isTest
public with sharing class MultiPhaseOpportunity_Trigger_Test{
    
    @isTest(seeAllData=true)
    static void validateTrigger() {
        
        Account account1 = new Account(Name='TestCompany1', ShippingStreet='Street1', ShippingCity='City1', ShippingCountry='United States', ShippingState='Texas', ShippingPostalCode='PostalCode1');
        insert account1;
        
        Multi_Phase_Opportunity__c mp1 = new Multi_Phase_Opportunity__c();
        Integer originalAmount = 5500;
        mp1.CurrencyIsoCode = 'CAD';
        mp1.Current_Amount_del__c = originalAmount;
        mp1.account__c = account1.id;
        
        insert mp1;
        System.debug ('Amount: ' + mp1.Current_Amount_del__c + ' Currency: ' + mp1.CurrencyIsoCode);
        
        mp1.CurrencyIsoCode = 'USD';
        
        update mp1;
        
        mp1 = [Select id, Current_Amount_del__c, CurrencyIsoCode from Multi_Phase_Opportunity__c where id = :mp1.id];
        System.debug ('Amount: ' + mp1.Current_Amount_del__c + ' Currency: ' + mp1.CurrencyIsoCode);
        
        List<CurrencyType> currencyTypeList = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE and ISOCode in ('CAD','USD')];
        Map<String, CurrencyType> currencyTypes = new Map<String, CurrencyType>();
        
        for(CurrencyType currencyType : currencyTypeList){
            currencyTypes.put(currencyType.ISOCode, currencyType);
        }
        
        CurrencyType oldMPCurrencyType = currencyTypes.get('CAD');
        CurrencyType newMpCurrencyType = currencyTypes.get('USD');
        
        Decimal convertedAmount = (originalAmount/oldMPCurrencyType.ConversionRate) * newMpCurrencyType.ConversionRate;
        
        //System.assertEquals(convertedAmount, mp1.Current_Amount_del__c);
        
    }   

}