public class NI_OpportunityLineItemTrigger_controller {
    
    public static void updateProduct(List<OpportunityLineItem> oppLineItemList){
    
        NI_OpportunityLineItemHandler.updateLineItemList(oppLineItemList);
    }
    
     public static void deleteProduct(Map <Id, OpportunityLineItem> oldOppLineItemList){
         NI_OpportunityLineItemHandler.updateLineItemList(oldOppLineItemList.values());   
    }
   
}