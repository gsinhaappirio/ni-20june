public with sharing class NI_Service_Request_Extension {
    
    private final NI_Service_Request__c serviceRequest;
    public List<NI_Sales_Sales_ServiceRequestNoteDTO> noteList { get; set; }
    
    /**
    * Constructor
    * @param stdController a Service Request standard controller
    */
    public NI_Service_Request_Extension(ApexPages.StandardController stdController){
        this.serviceRequest = (NI_Service_Request__c)stdController.getRecord();
        
        NI_Sales_Sales_ServiceRequestDAO dao = new NI_Sales_Sales_ServiceRequestDAO();
        this.noteList = dao.getServiceRequestNotes(serviceRequest.Oracle_Service_Request_Id__c);
    }
    
}