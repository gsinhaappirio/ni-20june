@isTest
public with sharing class NI_Sales_QuoteWS_Quote_Test {
    
    @isTest(seeAllData=true)
    static void validateGetQuote(){
        
        Test.setMock(WebServiceMock.class, new NI_Sales_QuoteWS_QuoteRequest_MockImpl());
        String QuoteId = '123';
        NI_Sales_QuoteWS_QuoteRequest.Get_Quote_XMLBindingQSPort service = new NI_Sales_QuoteWS_QuoteRequest.Get_Quote_XMLBindingQSPort();
        NI_Sales_QuoteWS_Quote.GetQuoteResponse_element response = service.getQuote(quoteId);
        
        System.assertEquals('S', response.responseStatus);
        System.assertEquals(123, response.QuoteInformation.QuoteHeader.headerId);
        System.assertEquals('1', response.QuoteInformation.quoteLines.quoteLine[0].partNumber);
        System.assertEquals('2', response.QuoteInformation.quoteLines.quoteLine[1].partNumber);
        
    }   

}